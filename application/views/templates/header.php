<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap-4.1.3/dist/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/jquery-ui/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/DataTables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/nie.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/tagify/dist/tagify.css">
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>assets/jQuery-Autocomplete-master/content/styles.css"> -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="<?php echo base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/bootstrap-4.1.3/dist/js/bootstrap.js"></script>
  <script src="<?php echo base_url() ?>assets/DataTables/datatables.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/pdf.js"></script>
  <script src="<?php echo base_url() ?>assets/jquery-ui/jquery-ui.js"></script>
  <script src="<?php echo base_url() ?>assets/amcharts4/core.js"></script>
  <script src="<?php echo base_url() ?>assets/amcharts4/charts.js"></script>
  <script src="<?php echo base_url() ?>assets/amcharts4/themes/animated.js"></script>
  <script src="<?php echo base_url() ?>assets/tagify/dist/jQuery.tagify.min.js"></script>
  <script src="<?php echo base_url() ?>assets/tinymce_5.0.12/tinymce/js/tinymce/tinymce.min.js"></script>


  <!-- Dipasang di sini karena bermasalah jika di load external library js -->
  <script type="text/javascript">
    var siteUrl = "<?php echo site_url(); ?>";

    $(function() {
      $('[data-toggle="tooltip"]').tooltip()
    });
  </script>

  <title><?php echo $judul; ?></title>
</head>

<body>
  <div class="container">
    <div class="row mt-4">
      <div class="col">
        <a href="<?php echo base_url(); ?>Nie/index"><img src="<?php echo base_url() ?>assets/images/sanbe-crop.png" width="115" height="30" alt=""></a>
      </div>
      <div class="col">
        <div class="float-right">
          <?php if ($this->session->userdata('logged_user')) : ?>
            <div class="dropdown show">
              <a style="font-size: 13pt; color: #a5a7a8; text-decoration: none;" href="#" class="dropdown-toggle" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 14pt;"></i>&nbsp;<?php echo $this->session->userdata('name'); ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo base_url() ?>Auth/change_password">Change Password</a>

                <?php if ($this->session->userdata('privilege_page') == '1') : ?>
                  <a class="dropdown-item" href="<?php echo base_url() ?>privilege">Privilege</a>
                <?php endif; ?>

                <?php if ($this->session->userdata('user_page') == '1') : ?>
                  <a class="dropdown-item" href="<?php echo base_url() ?>user-list">User List</a>
                <?php endif; ?>

                <!-- <a class="dropdown-item" href="<?php echo base_url() ?>manage-submission">Manage Submission</a> -->

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo base_url() ?>logout">Logout</a>
              </div>
            </div>
          <?php else : ?>
            <a href="<?php echo base_url() ?>Auth" class="btn btn-outline-primary">Sign In <i class="fa fa-sign-in" aria-hidden="true"></i></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <p>Jl. Tamansari No.10, Tamansari, Bandung Wetan, Kota Bandung</p>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row mt-1">
      <div class="col">
        <form class="form-inline">
          <a href="<?php echo base_url() ?>nie"><button class="btn btn-primary" type="button"><i class="fa fa-home" aria-hidden="true" style="color:white;"></i></button></a>&nbsp;

          <?php if ($this->session->userdata('nie_product_page') == '1') : ?>
            <a href="<?php echo base_url() ?>produk"><button class="btn btn-primary" type="button">Produk</button></a>&nbsp;
          <?php endif; ?>

          <a href="<?php echo base_url() ?>sarana"><button class="btn btn-primary" type="button">Sarana</button></a>
        </form>
      </div>
    </div>
  </div>