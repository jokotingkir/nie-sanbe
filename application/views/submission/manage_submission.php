<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Add/Edit Submission
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label for="doc_lvl1">Document #1</label>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#modal_doc1">Add New</a>
                            <select class="form-control" id="doc_lvl1" size="<?php echo count($doc_lvl1) ?>">
                                <?php foreach ($doc_lvl1 as $doc1) : ?>
                                    <option value="<?php echo $doc1->nie_doc1_code ?>"><?php echo $doc1->nie_doc1_desc ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div style="display: none;" id="show_lvl2">
                            <hr style="border: 0.5px dashed grey;" />
                            <div class="form-group">
                                <label for="doc_lvl2">Document #2</label>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm float-right">Add New</a>
                                <select class="form-control" id="doc_lvl2"></select>
                            </div>
                        </div>

                        <div style="display: none;" id="show_lvl3">
                            <hr style="border: 0.5px dashed grey;" />
                            <div class="form-group">
                                <label for="doc_lvl3">Document #3</label>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm float-right">Add New</a>
                                <select class="form-control" id="doc_lvl3"></select>
                            </div>
                        </div>

                        <div style="display: none;" id="show_lvl4">
                            <hr style="border: 0.5px dashed grey;" />
                            <div class="form-group">
                                <label for="doc_lvl4">Document #4</label>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm float-right">Add New</a>
                                <select class="form-control" id="doc_lvl4"></select>
                            </div>
                        </div>

                        <div style="display: none;" id="show_lvl5">
                            <hr style="border: 0.5px dashed grey;" />
                            <div class="form-group">
                                <label for="doc_lvl5">Document #5</label>&nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-primary btn-sm float-right">Add New</a>
                                <select class="form-control" id="doc_lvl5"></select>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_doc1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Document #1</h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Document Name</label>
                        <input type="text" class="form-control input_capital" name="new_doc_1" id="new_doc_1" onkeyup="isEnableSubmit1()" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary col-md-3" data-dismiss="modal">Close</button>
                <button type="button" onclick="submitDoc1()" id="submit_doc1" class="btn btn-primary col-md-3" disabled>Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.input_capital').on('input', function(evt) {
        $(this).val(function(_, val) {
            return val.toUpperCase();
        });
    });

    function isEnableSubmit1() {
        var new_doc1 = document.getElementById('new_doc_1').value;
        if (new_doc1 != '') {
            document.getElementById('submit_doc1').disabled = false;
        } else {
            document.getElementById('submit_doc1').disabled = true;
        }
    }

    function submitDoc1() {
        $.ajax({
            url: "<?php echo base_url() ?>add-doc-1",
            method: "POST",
            data: {
                new_doc1: $('#new_doc_1').val()
            },
            dataType: "JSON",
            success: function(res) {
                if (res.length > 0) {
                    var options = '';
                    for (var i = 0; i < res.length; i++) {
                        options += '<option value="' + res[i].nie_doc1_code + '">' + res[i].nie_doc1_desc + '</option>';
                    }

                    $('#doc_lvl1').empty();
                    $('#doc_lvl1').append(options);
                    if (res.length < 5) {
                        document.getElementById('doc_lvl1').setAttribute("size", res.length);
                    }
                    $('#modal_doc1').modal('hide');
                }
            }
        });
    }
</script>