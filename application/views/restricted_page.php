<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Restrict Page</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/css_restricted_page/style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>Oops!</h1>
				<h2>Not Authorized to this page</h2>
			</div>
			<?php if (!$this->session->userdata('logged_user')) : ?>
				<!-- <a href="<?php echo base_url() ?>login">Go TO Login</a> -->
				<form name="go_to_login" method="POST" action="<?php echo base_url() ?>login">
					<input type="hidden" name="url_request" id="url_request" />
				</form>
				<a href="#" onclick="goToLogin()">Go TO Login</a>
			<?php endif; ?>
		</div>
	</div>

	<script>
		function goToLogin() {
			var current_url = window.location.pathname;
			if (current_url.includes('submission-process')) {
				var parts = current_url.split("/");
				var request = parts[2] + "/" + parts[3];
				document.getElementById('url_request').value = request;
				document.go_to_login.submit();
			} else{
				document.go_to_login.submit();
			}
		}
	</script>

</body>

</html>