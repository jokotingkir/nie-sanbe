<div class="container">
    <div class="row mt-3 left-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    NIE Submission Monitoring
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body" style="background-color: #d7eaf7;">
                            <table>
                                <tr>
                                    <td>
                                        <h6>Nama Produk</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['product_name'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>NIE</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['nie_no'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Terbit</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_start_date'])) ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Kadaluarsa</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_end_date'])) ?></h6>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <br><br>
                    <h5>Submission Document</h5><br />

                    <div class="row">
                        <!-- <div class="col-md-1">&nbsp;</div> -->
                        <div class="col">
                            <div class="accordion" id="accordionExample">

                                <div class="card">
                                    <div class="card-header" id="dokumen_1" style="background-color: #d7eaf7;">
                                        <h6 class="mb-0">
                                            <a href="#" data-toggle="collapse" data-target="#collapseDocOne" aria-expanded="true" aria-controls="collapseDocOne" style="color: black;">
                                                #1
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseDocOne" class="collapse show" aria-labelledby="dokumen_1" data-parent="#accordionExample">
                                        <div class="card-body" style="background-color: white;">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <?php foreach ($doc_1 as $doc) : ?>
                                                        <tr>
                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="doc_lvl_1[]" class="custom-control-input" id="<?php echo $doc->id ?>">
                                                                    <label class="custom-control-label" for="<?php echo $doc->id ?>"><?php echo $doc->text ?></label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col">
                                                    <button type="button" id="next_lvl_2" class="btn btn-primary btn-sm float-right" onclick="showLvl_2()" disabled>Selanjutnya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card" id="doc_lvl_2" style="display: none;">
                                    <div class="card-header" id="dokumen_2" style="background-color: #d7eaf7;">
                                        <h6 class="mb-0">
                                            <a href="#" data-toggle="collapse" data-target="#collapseDocTwo" aria-expanded="true" aria-controls="collapseDocTwo" style="color: black;">
                                                #2
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseDocTwo" class="collapse" aria-labelledby="dokumen_2" data-parent="#accordionExample">
                                        <div class="card-body" style="background-color: white;">
                                            <input type="text" id="search_doc_2" class="form-control form-control-sm" placeholder="Ketik kata kunci untuk lakukan pencarian..." />
                                            <table class="table table-borderless" id="table_doc_2">
                                                <tbody></tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col">
                                                    <button type="button" id="next_lvl_3" class="btn btn-primary btn-sm float-right" onclick="showLvl_3()" disabled>Selanjutnya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card" id="doc_lvl_3" style="display: none;">
                                    <div class="card-header" id="dokumen_3" style="background-color: #d7eaf7;">
                                        <h6 class="mb-0">
                                            <a href="#" data-toggle="collapse" data-target="#collapseDocThree" aria-expanded="true" aria-controls="collapseDocThree" style="color: black;">
                                                #3
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseDocThree" class="collapse" aria-labelledby="dokumen_3" data-parent="#accordionExample">
                                        <div class="card-body" style="background-color: white;">
                                            <input type="text" id="search_doc_3" class="form-control form-control-sm" placeholder="Ketik kata kunci untuk lakukan pencarian..." />
                                            <table class="table table-borderless" id="table_doc_3">
                                                <tbody></tbody>
                                            </table>
                                            <div class="row">
                                                <div class="col">
                                                    <button type="button" id="next_lvl_4" class="btn btn-primary btn-sm float-right" onclick="showLvl_4()" disabled>Finish</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class="col">
                            <div id="selected_level"></div>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    var lvl_1 = [];
    var lvl_2 = [];
    var lvl_3 = [];
    var lvl_4 = [];
    var collapse_doc_2 = document.getElementById('doc_lvl_2');
    var collapse_doc_3 = document.getElementById('doc_lvl_3');
    var selected_level = document.getElementById('selected_level');

    var table_doc_2 = $('#table_doc_2');
    var table_doc_3 = $('#table_doc_3');

    var checkbox_lvl_1 = $('input[name="doc_lvl_1[]"]');

    checkbox_lvl_1.change();
    checkbox_lvl_1.change(function() {
        $('#next_lvl_2').prop('disabled', checkbox_lvl_1.filter(':checked').length < 1);
    });

    function isEnableNextLevel3() {
        var checkbox_lvl_2 = $('input[name="doc_lvl_2[]"]');
        $('#next_lvl_3').prop('disabled', checkbox_lvl_2.filter(':checked').length < 1);
    }

    function isEnableNextLevel4() {
        var checkbox_lvl_3 = $('input[name="doc_lvl_3[]"]');
        $('#next_lvl_4').prop('disabled', checkbox_lvl_3.filter(':checked').length < 1);
    }

    function getDocLvl2() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_2",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_1": lvl_1
            },
            success: function(res) {
                if (res.length > 0) {
                    var rows = "";
                    for (var i = 0; i < res.length; i++) {
                        rows += '<tr><td><div class="custom-control custom-checkbox"><input type="checkbox" name="doc_lvl_2[]" class="custom-control-input" id="' + res[i].id + '" onchange="isEnableNextLevel3()"/><label class="custom-control-label" for="' + res[i].id + '">' + res[i].text + '</label></div></td></tr>';
                    }
                    table_doc_2.find('tbody').append(rows);
                }
            },
            error: function(res) {
                // do something
            }
        });
    }

    function getDocLvl3() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_3",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_2": lvl_2
            },
            success: function(res) {
                if (res.length > 0) {
                    if ('id' in res[0] && 'text' in res[0]) {
                        var rows = "";
                        for (var i = 0; i < res.length; i++) {
                            rows += '<tr><td><div class="custom-control custom-checkbox"><input type="checkbox" name="doc_lvl_3[]" class="custom-control-input" id="' + res[i].id + '" onchange="isEnableNextLevel4()"/><label class="custom-control-label" for="' + res[i].id + '">' + res[i].text + '</label></div></td></tr>';
                        }

                        if (collapse_doc_3.style.display == 'none') {
                            collapse_doc_3.style.display = 'block';
                            $('#collapseDocTwo').collapse();
                            document.getElementById('collapseDocTwo').style.display = '';
                            table_doc_3.find('tbody').append(rows);
                            $('#collapseDocThree').collapse().show();
                        }

                    } else {
                        var list = '<ul>';
                        // var parent_list = res.map(item => item.nie_doc1_code).filter((value, index, self) => self.indexOf(value) === index);
                        for (var i = 0; i < res.length; i++) {
                            if (i == 0) {
                                list += '<li>' + res[i].nie_doc1_desc + '</li><ul>';
                            } else {
                                if (res[i].nie_doc1_code != res[i - 1].nie_doc1_code) {
                                    list += '</ul><li>' + res[i].nie_doc1_code + '</li>><ul>';
                                }
                            }
                            list += '<li>' + res[i].nie_doc2_desc + '</li>';
                        }
                        list += '</ul></ul>';

                        selected_level.innerHTML = list;
                        document.getElementById('collapseDocTwo').style.display = '';
                    }
                }
            },
            error: function(res) {
                // do something
            }
        });
    }

    function getDocLvl4() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_4",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_3": lvl_3
            },
            success: function(res) {
                if (res.length > 0) {
                    var list = '<ul>';
                    for (var i = 0; i < res.length; i++) {
                        if (i == 0) {
                            list += '<li>' + res[i].nie_doc1_desc + '</li><ul>';
                        }
                    }
                }
            },
            error: function(res) {
                // do something
            }
        });
    }

    function showLvl_2() {
        lvl_1 = [];
        $('input[name="doc_lvl_1[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_1.push($(this)[0].id);
            }
        });

        if (lvl_1.length > 0) {
            if (collapse_doc_2.style.display == 'none') {
                collapse_doc_2.style.display = 'block';
                $('#collapseDocOne').collapse();
                getDocLvl2();
                $('#collapseDocTwo').collapse().show();
            }
        }
    }

    function showLvl_3() {
        lvl_2 = [];
        $('input[name="doc_lvl_2[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_2.push($(this)[0].id);
            }
        });

        if (lvl_2.length > 0) {
            getDocLvl3();
        }
    }

    function showLvl_4() {
        lvl_3 = [];
        $('input[name="doc_lvl_3[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_3.push($(this)[0].id);
            }
        });
        if (lvl_3.length > 0) {
            $('#collapseDocThree').collapse();
            getDocLvl4();
        }
    }

    $("#search_doc_2").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#table_doc_2 tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#search_doc_3").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#table_doc_3 tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
</script>