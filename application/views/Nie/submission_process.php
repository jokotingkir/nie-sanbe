<div class="container">
    <div class="row mt-3 left-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    NIE Submission Monitoring
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body" style="background-color: #d7eaf7;">
                            <table>
                                <tr>
                                    <td>
                                        <h6>Nama Produk</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['product_name'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>NIE</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['nie_no'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Terbit</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_start_date'])) ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Kadaluarsa</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_end_date'])) ?></h6>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br />
                    <!-- Session flashdata -->
                    <?php if ($this->session->flashdata('message')) : ?>
                    <?php echo  $this->session->flashdata('message') ?>
                    <?php endif; ?>

                    <?php if ($this->session->userdata('dept_code') == 'ADMIN' && $this->session->userdata('receive_submission') == '1') : ?>
                    <?php if (is_array($document_upload) && count($document_upload) > 0) : ?>
                    <div class="row mt-3">
                        <div class="col">
                            <button type="button" class="btn btn-primary col-md-2 float-right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-clipboard" aria-hidden="true"></i>&nbsp;Uploaded Document</button>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>

                    <div id="additional_doc_upload" style="display: none;">
                        <form enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    <input type="hidden" name="nie_no" />
                                    <input type="hidden" name="nie_alert_id" />
                                    <div class="row">
                                        <div class="col">
                                            <label for="upload-doc">Document Upload</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="doc_upload[]" id="upload-doc" />
                                                <label class="custom-file-label" for="upload-doc">Select File</label>
                                            </div>
                                            <small style="color: red; display: none;" id="upload-error"></small>
                                            <div class="row">
                                                <div class="col">
                                                    <table class="table file-list-submission" id="file-list">
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="remark-doc-upload">Remark</label>
                                                <textarea id="remark-doc-upload" name="submission_remark"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col text-right">
                                            <button type="button" class="btn btn-danger btn-sm col-md-1" onclick="showAdditionalUpload()">Cancel</button>&nbsp;
                                            <button type="button" class="btn btn-success btn-sm col-md-1" onclick="saveAdditionalUpload()">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div id="selected_document">
                        <div class="row mt-4">
                            <div class="col">
                                <h6>Submission Document</h6>
                            </div>
                            <?php if ($this->session->userdata('dept_code') == 'ADMIN' && $this->session->userdata('receive_submission') == '1') : ?>
                            <div class="col">
                                <div class="float-right"><small><i><b>R</b></i> : <i>Receive</i>;&nbsp;<i><b>NR</b></i> : <i>Not Receive</i>;</small></div>
                            </div>
                            <?php endif; ?>

                            <?php if ($this->session->userdata('submit_submission') == '1' && ($this->session->userdata('dept_code') == 'QA' || $this->session->userdata('dept_code') == 'RND')) : ?>
                            <div class="col">
                                <div class="float-right">
                                    <a href="javascript:void(0)" onclick="showAdditionalUpload()"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload Doc</a>
                                    <?php if (is_array($document_upload) && count($document_upload) > 0) : ?>
                                    | <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-clipboard" aria-hidden="true"></i>&nbsp;Uploaded Doc</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="row mt-3">
                            <div class="col">
                                <?php if ($this->session->userdata('dept_code') == 'ADMIN' && $this->session->userdata('receive_submission') == '1') : ?>
                                <div id="accordion">
                                    <?php echo $tree_doc_submisssion; ?>
                                </div>
                                <?php endif; ?>

                                <?php if ($this->session->userdata('submit_submission') == '1' && ($this->session->userdata('dept_code') == 'QA' || $this->session->userdata('dept_code') == 'RND')) : ?>
                                <?php echo $tree_doc_submisssion; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($this->session->userdata('submit_submission') == '1' && ($this->session->userdata('dept_code') == 'QA' || $this->session->userdata('dept_code') == 'RND')) : ?>
<!-- Completed NIE confirmation -->
<div class="modal fade" tabindex="-1" role="dialog" id="ask_for_submit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center"><i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 85px; color: #636363;"></i></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col d-flex justify-content-center">
                            <h3 class="modal-title" style="color: #636363;">Anda Yakin ?</h3>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center" style="color: #999999;">Submit dokumen yang dipilih?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
                        </div>
                        <div class="col">
                            <!-- <button type="button" class="btn btn-danger btn-block" onclick="showNextNie()" >Ya</button> -->
                            <button type="button" class="btn btn-success btn-block" onclick="submittingSubmission()">Ya</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (is_array($document_upload) && count($document_upload) > 0) : ?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-clipboard" aria-hidden="true"></i>&nbsp;Uploaded Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                        <div class="col">
                            <form class="form-inline">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="user_option">Filter berdasarkan</label>
                                    </div>
                                    <select class="custom-select" id="kategori_pencarian">
                                        <option value="by_date">Tanggal Upload</option>
                                        <option value="by_remark">Remark</option>
                                    </select>
                                </div>
                                &nbsp;&nbsp;
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control column_filter" placeholder="Ketikkan kata kunci.." name="keyword" id="by_remark" style="display: none;" />
                                    <input type="date" class="form-control column_filter" name="keyword" id="by_date" />
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" id="btn-filter">Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> -->
                <div class="row">
                    <div class="col">
                        <table class="table uploaded-doc-submission">
                            <tbody>
                                <?php for ($p = 0; $p < count($document_upload); $p++) : ?>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col">
                                                <div class="float-right">
                                                    <i>uploaded by : <?php echo $document_upload[$p]['created_by'] ?> ~ date : <?php echo date('d/m/Y', strtotime($document_upload[$p]['created_at'])) ?></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-1">
                                            <div class="col">
                                                <div class="card">
                                                    <div class="card-body" style="background-color: #d7eaf7;"><?php echo $document_upload[$p]['submission_remark'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col">
                                                <b>Uploaded File :</b><br />
                                                <ol>
                                                    <?php for ($j = 0; $j < count($document_upload[$p]['document']); $j++) : ?>
                                                    <li><a href="javascript:void(0)" data-path="<?php echo $document_upload[$p]['document'][$j]['filepath'] ?>" data-filename="<?php echo $document_upload[$p]['document'][$j]['filename'] ?>" onclick="downloadDoc(this)"><?php echo $document_upload[$p]['document'][$j]['filename'] ?></a></li>
                                                    <?php endfor; ?>
                                                </ol>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if ($this->session->userdata('receive_submission') == '1' && $this->session->userdata('dept_code') == 'ADMIN') : ?>
<!-- Completed NIE confirmation -->
<div class="modal fade" tabindex="-1" role="dialog" id="ask_for_remove">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center"><i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 85px; color: #636363;"></i></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col d-flex justify-content-center">
                            <h3 class="modal-title" style="color: #636363;">Anda Yakin ?</h3>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center" style="color: #999999;">Hapus dokumen yang dipilih?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-default btn-block btn-no-remove" data-dismiss="modal">Tidak</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-success btn-block btn-yes-remove">Ya</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<script>
    var current_submission_id = 0;
    var current_btn_id = "";
    var file_temp = [];
    var is_bpom = "false";

    tinymce.init({
        selector: '#remark-doc-upload',
        menubar: false,
        statusbar: false,
        plugins: 'lists',
        toolbar: 'undo redo | numlist bullist'
    });

    function updateDoc(val, doc_submission_id, additional_bpom = null) {
        if (additional_bpom != null) {
            is_bpom = "true";
        }

        $.ajax({
            url: "<?php echo base_url() ?>Nie/receiveSubmissionDoc",
            type: "POST",
            dataType: "JSON",
            data: {
                "val": val,
                "submission_id": doc_submission_id,
                "is_bpom": is_bpom
            },
            success: function(res) {
                if (res == "success") {
                    is_bpom = "false";
                }
            }
        });
    }

    //Ini untuk doc submission yang biasa bukan tambahan data BPOM
    function submitSubmissionDoc(btn_id, doc_submission_id) {
        current_submission_id = doc_submission_id;
        current_btn_id = btn_id;
        $('#ask_for_submit').modal('toggle');
    }

    function submittingSubmission() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/submitDoc",
            type: "POST",
            dataType: "JSON",
            data: {
                "val": "submitted",
                "submission_id": current_submission_id,
                "is_bpom": is_bpom
            },
            success: function(res) {
                if (res == "success") {
                    $('#ask_for_submit').modal('hide');
                    $('#btn-' + current_btn_id).removeClass('btn-primary').addClass('btn-success');
                    $('#btn-' + current_btn_id).prop('disabled', true);
                    document.getElementById('btn-' + current_btn_id).innerText = 'Submitted';
                }
                current_submission_id = 0;
                current_btn_id = "";
                is_bpom = "false";
            }
        });
    }

    //Validasi ekstensi file size yaitu 500 MB
    function ValidateSize(file) {
        var FileSize = file.size / 1024 / 1024; // in MB
        if (FileSize > 500) {
            return false;
        } else {
            return true;
        }
    }

    function randomNumber() {
        var val = Math.floor(100 + Math.random() * 900);
        return val;
    }

    function removeUploadItem(r, random_digit) {
        var i = r.parentNode.parentNode.rowIndex;

        /*Hapus row di table*/
        document.getElementById('file-list').deleteRow(i);

        /*Hapus item object di temporary array*/
        file_temp = $.grep(file_temp, function(obj, index) {
            return obj.id == random_digit
        }, true);
    }

    function showAdditionalUpload() {
        $('#additional_doc_upload').slideToggle(300);
    }

    $('#upload-doc').on('change', function() {
        var upload_error = document.getElementById('upload-error');
        var fileName = "Select File";
        var ext = $('#upload-doc').val().split('.').pop().toLowerCase();
        var file_list = "";
        var random_digit = randomNumber();

        if (!$.inArray(ext, ['pdf']) == -1) {
            upload_error.style.display = "block";
            upload_error.innerHTML = '<i>hanya mendukung file yang berekstensi .pdf</i>';
            $(this).val(null);
        } else {
            var submission_doc = document.getElementById('upload-doc').files[0];
            if (!ValidateSize(submission_doc)) {
                upload_error.style.display = "block";
                upload_error.innerHTML = '<i>Ukuran maksimal yang diperbolehkan yaitu 500MB</i>';
                $(this).val(null);
            } else {
                upload_error.style.display = "none";
                fileName = $(this).val().split('\\').pop(); //untuk mengambil file name
            }
        }

        file_list = '<tr><td><div class="fileNameSubmission">' + fileName + '</div></td><td width="2%"><a href="javascript:void(0)" onclick="removeUploadItem(this,' + random_digit + ')"><i class="fa fa-times" aria-hidden="true" style="color: red;"></i></a></td></tr>';
        $('#file-list tbody').append(file_list);

        var lg = $(this)[0].files.length;
        var items = $(this)[0].files;
        if (lg > 0) {
            for (var i = 0; i < lg; i++) {
                var upload = {
                    id: random_digit,
                    fileName: items[i].name,
                    fileSize: items[i].size,
                    fileType: items[i].type,
                    files: items[i]
                };

                file_temp.push(upload);
            }
        }

        // $(this).next('.custom-file-label').html(fileName);
    });

    function uploadTambahanBpom(pic_dept) {
        $('#bpom-upload-' + pic_dept).on('change', function() {
            var ext = $('#bpom-upload-' + pic_dept).val().split('.').pop().toLowerCase();
            var fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').html(fileName);
        });
    }

    function saveAdditionalUpload() {
        var form_data = new FormData();

        if (file_temp.length > 0) {
            for (var n = 0; n < file_temp.length; n++) {
                form_data.append("doc_upload[]", file_temp[n].files);
            }
        }

        tinyMCE.triggerSave();
        form_data.append("submission_remark", $('#remark-doc-upload').val());
        form_data.append("nie_no", "<?php echo $nie['nie_no'] ?>");
        form_data.append("nie_alert_id", "<?php echo $nie_alert_id ?>");

        $.ajax({
            url: "<?php echo site_url(); ?>additional-submission-upload",
            type: "POST",
            dataType: "JSON",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    window.location.reload();
                }
            }
        });
    }

    function downloadDoc(r) {
        var path = r.dataset.path;
        path = path.substr(2);
        window.location.href = '<?php echo base_url() ?>' + path;
    }

    function removeItemBpom(r_bpom, pic_dept_id) {
        var i = r_bpom.parentNode.parentNode.rowIndex;
        var show_upload = document.getElementById('show-upload-' + pic_dept_id);
        var show_submit = document.getElementById('submit-bpom-' + pic_dept_id);

        /*Hapus row di table*/
        document.getElementById('table-bpom-' + pic_dept_id).deleteRow(i);

        var table_length = $('#table-bpom-' + pic_dept_id + ' tr').length;

        if (table_length == 0 && show_upload.style.display == "block") {
            show_upload.style.display = "none";
            show_submit.style.display = "none";
        }
    }

    function addDataBpom(pic_dept_id) {
        var input_bpom = document.getElementById('bpom-' + pic_dept_id);
        var show_upload = document.getElementById('show-upload-' + pic_dept_id);
        var show_submit = document.getElementById('submit-bpom-' + pic_dept_id);

        if (input_bpom.value != "") {
            var row = '<tr><td><label class="count"></label>.&nbsp;' + input_bpom.value + '</td><td width="2%"><a href="javascript:void(0)" onclick="removeItemBpom(this,\'' + pic_dept_id + '\')"><i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 18px;"></i></a></td></tr>';
            $('#table-bpom-' + pic_dept_id + ' tbody').append(row);

            var table_length = $('#table-bpom-' + pic_dept_id + ' tr').length;

            if (table_length > 0 && show_upload.style.display == "none") {
                show_upload.style.display = "block";
                show_submit.style.display = "block";
            }

            input_bpom.value = "";
        }
    }

    function submitTambahanData(nie_alert_id, pic_dept_id) {
        var form_bpom = new FormData();

        $('#table-bpom-' + pic_dept_id + ' tbody tr').each(function() {
            var td = $(this).find("td:first").text();
            td = td.replace('.', '');
            td = td.trim(td.replace(/\s+/g, ''));
            form_bpom.append("tambahan_bpom_" + pic_dept_id + "[]", td);
        });

        var upload = document.getElementById('bpom-upload-' + pic_dept_id);
        if (typeof upload.files[0] != "undefined") {
            form_bpom.append("bpom_upload_" + pic_dept_id, upload.files[0]);
        }

        form_bpom.append("alert_id", nie_alert_id);
        form_bpom.append("pic_dept", pic_dept_id);

        $.ajax({
            url: "<?php echo site_url(); ?>save-bpom-doc",
            type: "POST",
            data: form_bpom,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                var res = JSON.parse(response);
                if (res == "success") {
                    window.location.reload();
                }
            }
        });
    }

    // Ini untuk submit yang tambahan BPOM di QA & RND
    function submitTambahanBpom(doc_submission_id) {
        is_bpom = "true"; //untuk menandakan kalau ini tambahan data bpom;
        current_submission_id = doc_submission_id;
        current_btn_id = doc_submission_id;
        $('#ask_for_submit').modal('toggle');
    }

    $('.btn-yes-remove').click(function() {
        var document_id = $(this).attr('data-document');
        var document_type = $(this).attr('document-type');

        $.ajax({
            url: "<?php echo site_url(); ?>Nie/removeSubmissionItem",
            type: "POST",
            dataType: "JSON",
            data: {
                'document_id': document_id,
                'document_type': document_type
            },
            success: function(response) {
                if (response == "success") {
                    $('#ask_for_remove').modal('hide');
                    window.location.reload();
                }
            }
        });
    });

    function removeSelectedBpomDoc(document_id) {
        $('#ask_for_remove').modal('toggle');
        $('.btn-yes-remove').attr('data-document', document_id);
        $('.btn-yes-remove').attr('document-type', 'bpom');
    }

    function removeSelectedDoc(document_id) {
        $('#ask_for_remove').modal('toggle');
        $('.btn-yes-remove').attr('data-document', document_id);
        $('.btn-yes-remove').attr('document-type', 'submission');
    }

    function resendSubmissionEmail(email_no) {
        $.ajax({
            url: "<?php echo site_url(); ?>Nie/resendFailedEmail",
            type: "POST",
            dataType: "JSON",
            data: {
                'email_no': email_no
            },
            success: function(response) {
                if (response == "success") {
                    window.location.reload();
                }
            }
        });
    }
</script>