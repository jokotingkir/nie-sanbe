<script type="text/javascript">
    $(document).ready(function() {
        $('#carimodal').click(function() {
            $.ajax({
                url: "<?php echo base_url(); ?>nie/cariDataProduct/",
                method: "post",
                data: {
                    keyword: $('#keyword').val()
                },
                dataType: 'json',
                success: function(data) {
                    // console.log(data);
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        var country = data[i].con_desc;
                        if (country == null || country == "")
                            country = "-";

                        html += "<tr onclick='getProductModal(" + JSON.stringify(data[i]) + ")'>";
                        html += '<td>' + data[i].PRODUCT_CODE + '</td>';
                        html += '<td>' + data[i].PRODUCT_DESC + '</td>';
                        html += '<td>' + data[i].PRODUCT_OWNER + '</td>';
                        html += '<td>' + data[i].PLANNING_AREA + '</td>';
                        html += '<td>' + data[i].EXPIRE_DAYS + '</td>';
                        html += '<td>' + data[i].PG4_DESC + '</td>';
                        html += '<td>' + data[i].PG7_DESC + '</td>';
                        html += '<td>' + country + '</td>';
                        html += '</tr>';
                    }
                    $('#tabelmodal').html(html);
                }
            });
        });

        $('#prodname').autocomplete({
            serviceUrl: "<?php echo site_url('Nie/AutocompleteProduct') ?>",
            transformResult: function(res) {
                return {
                    suggestions: $.map(res, function(item) {
                        return {
                            value: item.PRODUCT_CODE,
                            data: item.PRODUCT_DESC
                        };
                    })
                };
            },
            onSelect: function(suggestion) {
                console.log(suggestion.value);
            }
        });

        var product_type = '<?php echo $product_type_id; ?>';

        var x = document.getElementById("exportcard");
        
        if (product_type == "1") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    });

    function getProductModal(arr) {
        var country = arr.con_desc;
        if (country == null || country == "")
            country = "-";

        $('#prodcode').val(arr.PRODUCT_CODE);
        $('#prodname').val(arr.PRODUCT_DESC);
        $('#prodown').val(arr.PRODUCT_OWNER);
        $('#prodplan').val(arr.PLANNING_AREA);
        $('#prodform').val(arr.PG4_DESC);
        $('#prodpckg').val(arr.PG7_DESC);
        $('#prodlife').val(arr.EXPIRE_DAYS);
        $('#prodctry').val(country);
        $('#myModal').modal('hide');
    }

    function ptodtypefunc() {
        var e = document.getElementById("prodtype");
        var tipe = e.options[e.selectedIndex].value;

        //alert(tipe);
        var x = document.getElementById("exportcard");
        if (tipe == "1") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>

<div class="container">
    <form name="nie_update_form" action="<?php echo base_url(); ?>Nie/edit_item/<?php echo $nie_id ?>" method="POST" enctype="multipart/form-data">
        <div class="row mt-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        UPDATE NIE
                    </div>
                    <div class="card-body">
                        <?php if (!isset($nie_no)) : ?>
                            <div class="alert alert-danger" role='alert'>
                                Data Tidak Ditemukan
                            </div>
                        <?php endif; ?>

                        <!-- Session flashdata -->
                        <?php if ($this->session->flashdata('message')) : ?>
                            <?php echo  $this->session->flashdata('message') ?>
                        <?php endif; ?>

                        <input type="hidden" name="nie_id" value="<?php echo html_escape($nie_id); ?>" />
                        <div class="form-group">
                            <label for="prodname">Product Name</label>
                            <!-- <div class="input-group mb-3"> -->
                            <input type="text" id="prodname" name="prodname" class="form-control" value="<?php echo html_escape($product_name); ?> " readonly />
                            <!-- <div class="input-group-append">
                                <button type="button" class='btn btn-info' data-toggle='modal' data-target='#myModal'>Choose Product</button>
                            </div> -->
                            <!-- </div> -->
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodcode">Product Code</label>
                                <input type="text" class="form-control" id="prodcode" name="prodcode" value="<?php echo html_escape($product_code); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodtype">Product Type</label>
                                <select class="form-control" id="prodtype" name="prodtype" onchange="ptodtypefunc();" disabled>
                                    <?php foreach ($typelist as $type) : ?>
                                        <option value="<?php echo $type['id']; ?>" <?php if ($type['id'] == $product_type_id) : ?> selected="selected" <?php endif; ?>><?php echo $type['product_type_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodown">Product Owner</label>
                                <input type="text" class="form-control" id="prodown" name="prodown" value="<?php echo html_escape($product_owner); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodplan">Planning Area</label>
                                <input type="text" class="form-control" id="prodplan" name="prodplan" value="<?php echo html_escape($planning_area); ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodform">Finished Dosage Form</label>
                                <input type="text" class="form-control" id="prodform" name="prodform" value="<?php echo html_escape($fd_form); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodpckg">Packaging Presentation</label>
                                <input type="text" class="form-control" id="prodpckg" name="prodpckg" value="<?php echo html_escape($packaging_presentation); ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodlife">Shelf Life</label>
                                <input type="text" class="form-control" id="prodlife" name="prodlife" value="<?php echo html_escape($shelf_life); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodscon">Storage Condition</label>
                                <select class="form-control" id="prodscon" name="prodscon" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?>>
                                    <?php foreach ($conlist as $con) : ?>
                                        <option value="<?php echo $con['id']; ?>" <?php if ($con['id'] == $storage_condition_id) : ?> selected="selected" <?php endif; ?>><?php echo $con['storage_condition_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodnie">NIE Number<span class="required">*</span></label>
                                <input type="text" class="form-control" id="prodnie" name="prodnie" value="<?php echo html_escape($nie_no); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niestat">NIE Status</label>
                                <select class="form-control" id="niestat" name="niestat" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> >
                                    <?php if($bpom_submit_status == 'submitted') : ?>
                                        <option>Completed</option>
                                    <?php else : ?>
                                        <?php foreach ($statlist as $stat) : ?>
                                            <option value="<?php echo $stat['stat_desc']; ?>" <?php if ($stat['stat_desc'] == $nie_status) : ?> selected="selected" <?php endif; ?>><?php echo $stat['stat_desc']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niestartdate">NIE Start Date</label>
                                <input type="date" class="form-control" id="niestartdate" name="niestartdate" value="<?php echo html_escape($nie_start_date); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nieenddate">NIE End Date</label>
                                <input type="date" class="form-control" id="nieenddate" name="nieenddate" value="<?php echo html_escape($nie_end_date); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niehldr">NIE Holder</label>
                                <input type="text" class="form-control" id="niehldr" name="niehldr" value="<?php echo html_escape($nie_holder); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niepblsh">NIE Publisher</label>
                                <input type="text" class="form-control" id="niepblsh" name="niepblsh" value="<?php echo html_escape($nie_publisher); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <button class="btn btn-primary" aria-describedby="compotition_help" id="add_compotition" type="button" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> >Add Compotition</button>
                                <small id="compotition_help" class="text-muted">
                                    <i>Click to add <b>compotition</b></i>
                                </small>
                            </div>
                        </div>

                        <div id="compotition-col" class="input-fields-wrap">
                            <div class="form-row align-items-center parent-compotition">
                                <div class="form-group col-md-1">
                                    <center><label class="count"></label></center>
                                </div>
                                <input type="hidden" name="compotition_id[]" id="composId_1" />
                                <input type="hidden" name="item_code[]" id="item_1" />
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_1" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_1" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_1" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                                </div>
                                <!-- <div class="form-group col-md-1">
                                    <a href="#" class="remove_compotition"><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a>
                                </div> -->
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="prodcmpt">Compotition</label>
                            <textarea class="form-control" id="prodcmpt" name="prodcmpt" rows="3"><?php echo html_escape($compotition); ?></textarea>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        &nbsp;
                    </div>
                    <div class="card-body">

                        <?php 
                            if($nie_certificate != ""){
                                $pointer = $nie_certificate . ".pdf";
                            }
                        ?>

                        <div class="form-group" id="current_certificate" style="display: none;">
                            <label for="upload_cert">NIE Certificate</label>
                            <br /><canvas id="the-canvas" style="border:1px solid black;" onclick="open_pdf()" class="pointer"></canvas>
                            <br /><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#change_certificate">Change File</button>
                        </div>
                        <div class="row" id="block_current_certificate" style="display: none;">&nbsp;</div>
                        <div class="form-group" id="new_upload_certificate" style="display: none;">
                            <label for="upload_cert">NIE Certificate</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="nie_cert" id="upload_cert" />
                                <label class="custom-file-label" for="upload_cert">Pilih File</label>
                            </div>
                            <small style="color: red; display: none;" id="upload_error"></small>
                            <?php if (isset($upload_error) && $upload_error != '') : ?>
                                <small style="color: red;"><i><?php echo $upload_error ?></i></small>
                            <?php endif; ?>
                        </div>

                        <!-- Start Tipe EXPORT -->
                        <div id="exportcard" style="display:none" class="mt-2">
                            <div class="form-group">
                                <label for="prodbrand">Brand Name</label>
                                <input type="text" class="form-control" id="prodbrand" name="prodbrand" value="<?php echo html_escape($brand_name); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="prodctry">Country</label>
                                <input type="text" class="form-control" id="prodctry" name="prodctry" readonly value="<?php echo html_escape($country); ?>" />
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="niercvdate">Receive Date</label>
                                    <input type="date" class="form-control" id="niercvdate" name="niercvdate" value="<?php if($receive_date != '0000-00-00') { echo html_escape($receive_date); } ?>" />
                                </div>
                                <div class="col-md-6">
                                    <label for="niedspdate">Dispatch Date</label>
                                    <input type="date" class="form-control" id="niedspdate" name="niedspdate" value="<?php if($dispatch_date != '0000-00-00') { echo html_escape($dispatch_date); } ?>" />
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label for="prodagnt">Agent</label>
                                <select class="form-control" id="prodagnt" name="prodagnt">
                                    <?php foreach ($agent as $v_agent) : ?>
                                        <option value="<?php echo $v_agent['VENDOR_NAME']; ?>" <?php if ($v_agent['VENDOR_NAME'] == $agent) : ?> selected="selected" <?php endif; ?>><?php echo $v_agent['VENDOR_NAME']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prodcrtf">Certificate</label>
                                <select class="form-control" id="prodcrtf" name="prodcrtf">
                                    <?php foreach ($certifilist as $crtf) : ?>
                                        <option value="<?php echo $crtf['id']; ?>" <?php if ($crtf['id'] == $certificate) : ?> selected="selected" <?php endif; ?>><?php echo $crtf['certi_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prodrglt">Regulatory</label>
                                <select class="form-control" id="prodrglt" name="prodrglt">
                                    <?php foreach ($regulatolist as $regulate) : ?>
                                        <option value="<?php echo $regulate['regulatory_code']; ?>" <?php if ($regulate['regulatory_code'] == $regulatory_code) : ?> selected="selected" <?php endif; ?>><?php echo $regulate['regulatory_desc']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!-- End of Tipe EXPORT -->
                        <div class="form-group">
                            <label for="pic">PIC</label>
                            <input type="text" class="form-control" name="pic" id="pic" value="<?php echo html_escape($pic); ?>" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> />
                        </div>
                        <div class="form-group">
                            <label for="remark">Remark</label>
                            <textarea class="form-control" id="remark" name="remark" rows="3" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> ><?php echo html_escape($remark); ?></textarea>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" id="SaveForm" name="submit_edit" value="submit" class="btn btn-primary btn-block" <?php if($bpom_submit_status == 'submitted') : ?> disabled <?php endif; ?> >Save</button>
                            </div>
                            <div class="col">
                                <?php if($internal_submit_status == "submitted") :?>
                                    <?php if($bpom_submit_status == "not submitted") : ?>
                                        <a href="<?php echo base_url() ?>Nie/submit_bpom/<?php echo $nie_id ?>" id="SubmitBpom" class="btn btn-success btn-block" enabled>Submit BPOM</a>
                                    <?php else : ?>
                                        <button type="button" id="SubmitBpom" class="btn btn-success btn-block" disabled>BPOM Submitted</button>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <button type="button" id="SubmitBpom" class="btn btn-success btn-block" disabled>Submit BPOM</button>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col">
                                <?php if($internal_submit_status == "submitted" && $bpom_submit_status == "submitted") : ?>
                                    <?php if($alert_status != "Completed") :?>
                                        <button type="button" id="nie_completed" name="nie_completed" value="completed" class="btn btn-warning btn-block" data-toggle="modal" data-target="#ask_completed" >Complete</button>
                                    <?php else :?>
                                        <button type="button" id="nie_completed" name="nie_completed" value="completed" class="btn btn-warning btn-block" data-toggle="modal" data-target="#ask_completed" disabled>NIE Completed</button>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <button type="button" id="nie_completed" name="nie_completed" value="completed" class="btn btn-warning btn-block" data-toggle="modal" data-target="#ask_completed" disabled>Complete</button>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal produk-->
<div id="myModal" class="modal fade" role="dialog">
    <!-- <div class="modal-dialog modal-xl" style="width:90%;font-size:10px;max-height:60vh"> -->
    <div class="modal-dialog modal-lg" tabindex="-1">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product List</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="post">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Cari Produk..." name="keyword" id='keyword'>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" id='carimodal'>Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" id='container' style="height:50vh;max-heught:50vh;overflow-y:scroll">
                        <?php if (empty($prodlist)) : ?>
                            <div class="alert alert-danger" role='alert'>
                                Data Tidak Ditemukan
                            </div>
                        <?php endif; ?>
                        <table class="table table-condensed table-striped search_product">
                            <thead>
                                <tr>
                                    <th scope='col' nowrap>PRODUCT CODE</th>
                                    <th scope='col' nowrap>PRODUCT NAME</th>
                                    <th scope='col' nowrap>PRODUCT OWNER</th>
                                    <th scope='col' nowrap>PLANNING AREA</th>
                                    <th scope='col' nowrap>EXPIRE DAYS</th>
                                    <th scope='col' nowrap>FINISHED DOSAGE FORM</th>
                                    <th scope='col' nowrap>PACKAGING PRESENTATION</th>
                                    <th scope='col' nowrap>COUNTRY</th>
                                </tr>
                            </thead>
                            <tbody id='tabelmodal'>
                                <?php for ($n = 0; $n < count($prodlist); $n++) : ?>
                                    <tr onclick='getProductModal(<?php echo json_encode($prodlist[$n]); ?>)'>
                                        <td scope="row"><?php echo $prodlist[$n]['PRODUCT_CODE']; ?></td>
                                        <td><?php echo $prodlist[$n]['PRODUCT_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['PRODUCT_OWNER']; ?></td>
                                        <td><?php echo $prodlist[$n]['PLANNING_AREA']; ?></td>
                                        <td><?php echo $prodlist[$n]['EXPIRE_DAYS']; ?></td>
                                        <td><?php echo $prodlist[$n]['PG4_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['PG7_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['con_desc']; ?></td>
                                    </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Change certificate confirmation -->
<div class="modal fade" tabindex="-1" role="dialog" id="change_certificate" aria-labelledby="exampleModalLongTitle">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center"><i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 85px; color: #636363;"></i></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col d-flex justify-content-center">
                            <h3 class="modal-title" style="color: #636363;">Anda Yakin ?</h3>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center">
                            <p style="color: #999999;">Ubah file sertifikat saat ini?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger btn-block" onclick="changeCertificate()">Ya</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Completed NIE confirmation -->
<div class="modal fade" tabindex="-1" role="dialog" id="ask_completed">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center"><i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 85px; color: #636363;"></i></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col d-flex justify-content-center">
                            <h3 class="modal-title" style="color: #636363;">Anda Yakin ?</h3>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <p class="text-center" style="color: #999999;">Ubah NIE saat ini menjadi complete, dan buat NIE selanjutnya?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
                        </div>
                        <div class="col">
                            <!-- <button type="button" class="btn btn-danger btn-block" onclick="showNextNie()" >Ya</button> -->
                            <a href="<?php echo base_url() ?>completed-nie/<?php echo $nie_id ?>" class="btn btn-danger btn-block">Ya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Next NIE Data -->
<div class="modal fade" id="next_nie" tabindex="-1" role="dialog" aria-labelledby="nextNieModal" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nextNieModal">Next NIE Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="nie" class="col-sm-3 col-form-label">NIE Number</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="nie_num_prodlett" id="nie_num_prodlett">
                                <option value="">-- Select NIE Number --</option>
                            </select>
                            <!-- <input type="text" class="form-control" id="nie" /> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="next_status" class="col-sm-3 col-form-label">NIE Status</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="next_status">
                                <?php foreach ($statlist as $stat) : ?>
                                    <option value="<?php echo $stat['stat_desc']; ?>" <?php if ($stat['stat_desc'] == $nie_status) : ?> selected="selected" <?php endif; ?>><?php echo $stat['stat_desc']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="next_start_date" class="col-sm-3 col-form-label">Start Date</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" name="next_start_date" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="next_end_date" class="col-sm-3 col-form-label">End Date</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" name="next_end_date" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="next_end_date" class="col-sm-3 col-form-label">Remark</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="next_remark" rows="3" ></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 156px;">Close</button>
                <button type="button" class="btn btn-primary" style="width: 156px;">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    var pdf_path = '<?php echo $nie_certificate; ?>';
    if(pdf_path != ""){
        var url = '<?php echo base_url() ?>' + pdf_path + '.pdf';    
        var pdfjsLib = window['pdfjs-dist/build/pdf'];
        // pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
        pdfjsLib.GlobalWorkerOptions.workerSrc = '<?php echo base_url() ?>assets/js/pdf.worker.js';
    }

    var max_fields = 11; //maksimum kolom isian untuk compotition
    var wrapper = $('.input-fields-wrap'); //fields wrapper
    var add_button = $('#add_compotition'); //tombol tambah compotition
    var x = 2;

    var current_certificate = document.getElementById('current_certificate');
    var block = document.getElementById('block_current_certificate');
    var new_certificate = document.getElementById('new_upload_certificate');

    <?php if (isset($pointer) && $pointer <> "") : ?>
        <?php if (file_exists($pointer)) : ?>
            current_certificate.style.display = "block";
            block.style.display = "block";
            new_certificate.style.display = "none";
        <?php else : ?>
            current_certificate.style.display = "none";
            block.style.display = "none";
            new_certificate.style.display = "block";
            document.getElementById("exportcard").className = "mt-3";
        <?php endif; ?>
    <?php endif; ?>
    
    if(typeof url !== 'undefined'){
        var loadingTask = pdfjsLib.getDocument(url);
        loadingTask.promise.then(function(pdf) {
            pdf.getPage(1).then(function(page) {
                var desireWidth = 100;
                var viewport = page.getViewport({
                    scale: 0.25,
                });
                var scale = desireWidth / viewport.width;
                var scaledViewport = page.getViewport({
                    scale: scale,
                });

                var canvas = document.getElementById('the-canvas');
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };

                page.render(renderContext);
            });
        });
    }

    //Set nie compotition by nie alert id dari controller Nie.php
    $nie_alert_id = '<?php echo $nie_id ?>';
    set_nie_compotition($nie_alert_id);

    function setCompotitionChild(x) {
        var is_completed = "";
        var completed = '<?php echo $alert_status ?>';
        
        if(completed == 'Completed'){
            is_completed = "disabled";
        }

        var html = '';
        html += '<div class="form-row align-items-center parent-compotition">';
        html += '<div class="form-group col-md-1"><center><label class="count"></label></center></div><input type="hidden" name="compotition_id[]" id="composId_' + x + '" /><input type="hidden" name="item_code[]" id="item_' + x + '" ' + is_completed +'  />';
        html += '<div class="form-group col-md-4"><input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_' + x + '" ' + is_completed +' /></div>';
        html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_' + x + '" ' + is_completed +' /></div>';
        html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_' + x + '" ' + is_completed +' /></div>';
        if(is_completed == "disabled"){
            html += '</div>';
        } else{
            html += '<div class="form-group col-md-1"><a href="#" class="remove_compotition" ><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a></div></div>';
        }

        return html;
    }

    //inisialisasi nie compotition saat data di edit
    function set_nie_compotition(nie_alert_id) {
        $.ajax({
            url: "<?php echo site_url('Nie/nie_compotition'); ?>",
            type: "GET",
            data: {
                nie_alert_id: nie_alert_id
            },
            error: function() {
                alert('NIE Compotition is Not Found!');
            },
            success: function(data) {
                var res = JSON.parse(data);
                if (Array.isArray(res)) {
                    if (res.length > 0) {
                        for (var n = 0; n < res.length; n++) {
                            document.getElementById('composId_' + (n + 1)).value = res[n].id;
                            document.getElementById('item_' + (n + 1)).value = res[n].item_code;
                            document.getElementById('compotition_' + (n + 1)).value = res[n].item_desc;
                            document.getElementById('strength_' + (n + 1)).value = res[n].strength;
                            document.getElementById('uom_' + (n + 1)).value = res[n].uom;

                            if (res.length >= x && x < max_fields) {
                                var compotition_child = setCompotitionChild(x);
                                $(wrapper).append(compotition_child);
                                x++;
                            }
                        }
                    }
                }
            }
        });
    }

    function open_pdf() {
        window.open(url);
    }

    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            //menggunakan 2 digit angka random agar tidak terdapat id yang sama dengan yang lainnya
            var r = Math.floor((Math.random() * 90) + 10);
            var compotition_child = setCompotitionChild(r);
            $(wrapper).append(compotition_child);
            x++;
        }
    });

    $(wrapper).on('click', '.remove_compotition', function(e) {
        e.preventDefault();
        $(this).parents('.parent-compotition').remove();
        x--;
    });

    $(document).on('keydown', '.autocomplete_compotition', function() {
        var id = this.id;
        var split_id = id.split('_');
        var index = split_id[1];

        $('#' + id).autocomplete({
            source: "<?php echo site_url('Nie/getItemsDesc'); ?>",
            select: function(event, ui) {
                //set value item_code to input hidden
                document.getElementById('composId_' + index).value = 'new_' + index;
                document.getElementById('item_' + index).value = ui.item.item_code;
            }
        });
    });

    function changeCertificate() {
        current_certificate.style.display = "none";
        block.style.display = "none";
        new_certificate.style.display = "block";
        document.getElementById("exportcard").className = "mt-3";
        $('#change_certificate').modal('hide');
    }

    //Validasi ekstensi file sertifikat hanya boleh .pdf
    function hasExtension(inputID, exts) {
        var fileName = document.getElementById(inputID).value;
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }

    //Validasi ekstensi file sertifikat maksimal size yaitu 10 MB
    function ValidateSize(file) {
        var FileSize = file.size / 1024 / 1024; // in MB
        if (FileSize > 10) {
            return false;
        } else {
            return true;
        }
    }

    $('#upload_cert').on('change', function() {
        var upload_error = document.getElementById('upload_error');
        var fileName = "Pilih File";
        if (!hasExtension('upload_cert', ['.pdf'])) {
            upload_error.style.display = "block";
            upload_error.innerHTML = '<i>hanya mendukung file yang berekstensi .pdf</i>';
            $(this).val(null);
        } else {
            var certificate = document.getElementById('upload_cert').files[0];
            if (!ValidateSize(certificate)) {
                upload_error.style.display = "block";
                upload_error.innerHTML = '<i>Ukuran maksimal yang diperbolehkan yaitu 10MB</i>';
                $(this).val(null);
            } else {
                upload_error.style.display = "none";
                fileName = $(this).val().split('\\').pop(); //untuk mengambil file name
            }
        }
        $(this).next('.custom-file-label').html(fileName);
    });

    function showNextNie()
    {
        $('#next_nie').modal('toggle');
        $('#ask_completed').modal('hide');
    }
</script>