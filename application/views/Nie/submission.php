<div class="container">
    <div class="row mt-3 left-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    NIE Submission Monitoring
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body" style="background-color: #d7eaf7;">
                            <table>
                                <tr>
                                    <td>
                                        <h6>Nama Produk</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['product_name'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>NIE</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo $nie['nie_no'] ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Terbit</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_start_date'])) ?></h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6>Tanggal Kadaluarsa</h6>
                                    </td>
                                    <td width="15%">&nbsp;</td>
                                    <td>
                                        <h6>:</h6>
                                    </td>
                                    <td>
                                        <h6>&nbsp;<?php echo date("d-m-Y", strtotime($nie['nie_end_date'])) ?></h6>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br />

                    <br />
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="document_pic">PIC Department</label>
                            <select name="document_pic" id="document_pic" class="form-control selected-doc-pic">
                                <option value="">Select PIC Department</option>
                                <?php foreach ($submission_dept as $dept) : ?>
                                <option value="<?php echo $dept->dept_id ?>" data-deptcode="<?php echo $dept->dept_code ?>"><?php echo $dept->dept_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col">
                            <!-- <label for="submission_doc">Submission Document</label> -->
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#submission_document" data-toggle="tab" role="tab" aria-controls="submission_document" aria-selected="true" id="submission-tab">Submission Document</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#additional_bpom" data-toggle="tab" role="tab" aria-controls="additional_bpom" aria-selected="false" id="additionalbpom-tab">Additional BPOM Data</a>
                                </li>
                            </ul>
                            <div class="tab-content mt-2">
                                <div class="tab-pane fade show active" id="submission_document" role="tabpanel" aria-labelledby="submission-tab">
                                    <div class="row">
                                        <div class="col">
                                            <div class="card" id="submission_doc">
                                                <div class="card-body" style="background-color: white;">
                                                    <div id="content_1">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <?php foreach ($doc_1 as $doc) : ?>
                                                                <tr>
                                                                    <td>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" name="doc_lvl_1[]" class="custom-control-input checked-content-one" id="<?php echo $doc->id ?>">
                                                                            <label class="custom-control-label" for="<?php echo $doc->id ?>"><?php echo $doc->text ?></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                        <div class="row">
                                                            <div class="col">
                                                                <button type="button" id="next_lvl_2" class="btn btn-primary btn-sm col-md-1" onclick="showLvl_2()" disabled>Next</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="content_2" style="display: none; height: 19rem;">
                                                        <div class="row">
                                                            <div class="col">
                                                                <input type="text" id="search_doc_2" class="form-control form-control-sm" placeholder="Ketik kata kunci untuk lakukan pencarian..." />
                                                            </div>
                                                        </div>
                                                        <div class="row">&nbsp;</div>
                                                        <div class="row">
                                                            <div class="col" style="height: 200px; overflow-y: auto; max-height: 250px;">
                                                                <table class="table table-borderless" id="table_doc_2">
                                                                    <tbody id="tbody_doc2"></tbody>
                                                                </table>
                                                                <p class="error" style="display: none;">Pencarian tidak ditemukan</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">&nbsp;</div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <button type="button" id="prev_to_1" class="btn btn-primary btn-sm col-md-1">Previous</button>&nbsp;
                                                                <button type="button" id="next_lvl_3" class="btn btn-primary btn-sm col-md-1" onclick="showLvl_3()" disabled>Next</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="content_3" style="display: none; height: 19rem;">
                                                        <div class="row">
                                                            <div class="col">
                                                                <input type="text" id="search_doc_3" class="form-control form-control-sm" placeholder="Ketik kata kunci untuk lakukan pencarian..." />
                                                            </div>
                                                        </div>
                                                        <div class="row">&nbsp;</div>
                                                        <div class="row">
                                                            <div class="col" style="height: 200px; overflow-y: auto; max-height: 250px;">
                                                                <table class="table table-borderless" id="table_doc_3">
                                                                    <tbody id="tbody_doc3"></tbody>
                                                                </table>
                                                                <p class="error" style="display: none;">Pencarian tidak ditemukan</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">&nbsp;</div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <button type="button" id="prev_to_2" class="btn btn-primary btn-sm col-md-1">Previous</button>&nbsp;
                                                                <button type="button" id="add-submission-pic" class="btn btn-primary btn-sm col-md-1" onclick="showLvl_4()" disabled>Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="additional_bpom" role="tabpanel" aria-labelledby="additionalbpom-tab">
                                    <div class="row">
                                        <div class="col">
                                            <div class="card">
                                                <div class="card-body" style="background-color: white;">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="input-group mb-3">
                                                                <input type="text" class="form-control" name="tambahan_bpom" id="tambahan_bpom" placeholder="Type here. . ." />
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-primary" type="button" onclick="addDataBpom()"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col">
                                                            <div id="show-pre-table-bpom">
                                                                <table class="table table-bordered" id="pre-table-bpom">
                                                                    <thead>
                                                                        <tr>
                                                                            <th colspan="2">Document Name</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr>
                                                                            <td colspan="2" class="text-center">
                                                                                <i>Additional BPOM Data is empty</i>
                                                                            </td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                            <div id="show-table-bpom" style="display: none;">
                                                                <table class="table table-bordered sub-document-bpom" id="table-bpom">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col" colspan="2" width="10%">Document Name</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="row mt-3" id="show-upload" style="display: none;">
                                                        <div class="col">
                                                            <form enctype="multipart/form-data">
                                                                <div class="form-group">
                                                                    <div class="custom-file"><input type="file" class="custom-file-input" name="bpom_upload" id="bpom-upload" onclick="uploadTambahanBpom()" /><label class="custom-file-label" for="bpom-upload">Pilih File</label></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div> -->
                                                    <div class="row mt-3">
                                                        <div class="col">
                                                            <button type="button" id="submit-bpom" class="btn btn-primary col-md-1 btn-sm" onclick="submitTambahanData()" disabled>Add</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br /><br />
                    <div id="accordion" style="display: none;"></div>
                    <form name="submit_submission" action="<?php echo base_url() ?>submit-submission" method="POST">
                        <!-- untuk data submission -->
                        <input type="hidden" name="end_of_submission" id="end_of_submission" />
                        <input type="hidden" name="email_recipients" id="email_recipients" />

                        <!-- untuk tambahan data bpom -->
                        <input type="hidden" name="end_of_bpom" id="end_of_bpom" />
                        <input type="hidden" name="email_recipients_bpom" id="email_recipients_bpom" />

                        <input type="hidden" name="nie_no" value="<?php echo $nie['nie_no'] ?>" />
                        <input type="hidden" name="nie_alert_id" value="<?php echo $nie_alert_id ?>" />
                        <input type="hidden" name="planning_area" id="planning_area" value="<?php echo $nie['planning_area'] ?>" />
                    </form>
                    <div class="row mt-4" id="submit-doc-submission" style="display: none;">
                        <div class="col">
                            <button type="button" class="btn btn-primary col-md-2 float-right" onclick="postSubmission()">Submit</button>
                        </div>
                    </div>
                    <br /><br />
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var count_accordion = 0;
    var lvl_1 = [];
    var unlevel_3 = []; //variable untuk menampung level 2 yang tidak ada di level 3
    var checkbox_lvl_1 = $('input[name="doc_lvl_1[]"]');
    var content_1 = document.getElementById('content_1');
    var content_2 = document.getElementById('content_2');
    var content_3 = document.getElementById('content_3');
    var selected_document = document.getElementById('selected_document');
    var submit_doc_submission = document.getElementById('submit-doc-submission');
    var user_input = document.getElementById('user_input');
    var table_doc_2 = $('#table_doc_2');
    var table_doc_3 = $('#table_doc_3');
    var current_pic_dept_id = "";
    var current_pic_dept_code = "";
    var current_pic_name = "";
    var posting_submission = [];
    var submission_position = [];
    var recipient_id = [];
    var bpom_data = [];

    var tmp_submission = [];

    checkbox_lvl_1.change();
    checkbox_lvl_1.change(function() {
        $('#next_lvl_2').prop('disabled', checkbox_lvl_1.filter(':checked').length < 1);
    });

    function isEnableNextLevel3() {
        var checkbox_lvl_2 = $('input[name="doc_lvl_2[]"]');
        $('#next_lvl_3').prop('disabled', checkbox_lvl_2.filter(':checked').length < 1);
    }

    function getDocLvl2() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_2",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_1": lvl_1
            },
            success: function(res) {
                if (res.length > 0) {
                    var rows = "";
                    for (var i = 0; i < res.length; i++) {
                        rows += '<tr><td><div class="custom-control custom-checkbox"><input type="checkbox" name="doc_lvl_2[]" class="custom-control-input" id="' + res[i].id + '" onchange="isEnableNextLevel3()"/><label class="custom-control-label" for="' + res[i].id + '">' + res[i].text + '</label></div></td></tr>';
                    }
                    content_1.style.display = 'none';
                    content_2.style.display = 'block';
                    table_doc_2.find('tbody').append(rows);
                }
            },
            error: function(res) {
                // do something
            }
        });
    }

    function getDocLvl3() {
        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_3",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_2": lvl_2
            },
            success: function(res) {
                if (res.length > 0) {
                    if ('id' in res[0] && 'text' in res[0]) {
                        var rows = "";
                        for (var i = 0; i < res.length; i++) {
                            if (res[i].id != null && res[i].text != null) {
                                rows += '<tr><td><div class="custom-control custom-checkbox"><input type="checkbox" name="doc_lvl_3[]" class="custom-control-input" id="' + res[i].id + '"/><label class="custom-control-label" for="' + res[i].id + '">' + res[i].text + '</label></div></td></tr>';
                            } else {
                                unlevel_3.push(res[i].doc2_code);
                            }
                        }
                        content_2.style.display = 'none';
                        content_3.style.display = 'block';
                        table_doc_3.find('tbody').append(rows);
                    } else {
                        var parent_list = res.map(item => item.nie_doc1_code).filter((value, index, self) => self.indexOf(value) === index);
                        var tab = "";
                        var section = "";

                        for (var n = 0; n < parent_list.length; n++) {
                            var index = res.findIndex(item => item.nie_doc1_code === parent_list[n]);
                            if (n == 0) {
                                tab += '<a class="nav-item nav-link active" id="nav-' + parent_list[n] + '-tab" data-toggle="tab" href="#nav-' + parent_list[n] + '" role="tab" aria-controls="nav-' + parent_list[n] + '" aria-selected="true">' + res[index].nie_doc1_desc + '</a>';
                            } else {
                                tab += '<a class="nav-item nav-link" id="nav-' + parent_list[n] + '-tab" data-toggle="tab" href="#nav-' + parent_list[n] + '" role="tab" aria-controls="nav-' + parent_list[n] + '" aria-selected="false">' + res[index].nie_doc1_desc + '</a>';
                            }
                        }

                        for (var n = 0; n < parent_list.length; n++) {
                            if (n == 0) {
                                section += '<div class="tab-pane fade show active" id="nav-' + parent_list[n] + '" role="tabpanel" aria-labelledby="nav-' + parent_list[n] + '-tab">';
                            } else {
                                section += '<div class="tab-pane fade" id="nav-' + parent_list[n] + '" role="tabpanel" aria-labelledby="nav-' + parent_list[n] + '-tab">';
                            }
                            var content = "";
                            for (var i = 0; i < res.length; i++) {
                                if (res[i].nie_doc1_code == parent_list[n]) {
                                    content += '<tr><td>' + res[i].nie_doc2_desc + '</td></tr>';
                                }
                                unlevel_3.push(res[i].nie_doc2_code);
                            }
                            section += '<div class="container"><br /><div class="row"><div class="col"><table class="table table-bordered"><tbody>' + content + '</tbody></table></div></div></div></div>';
                        }
                        selected_document.style.display = 'block';
                        $('#nav-tab').append(tab);
                        $('#nav-tabContent').append(section);
                        user_input.style.display = 'block';
                    }
                    // document.getElementById('unlevel_3').value = unlevel_3;
                }
            },
            error: function(res) {
                // do something
            }
        });
    }

    function getDocLvl4() {
        this.addCurrentSubmission = function() {
            var temp = {
                pic_dept_id: current_pic_dept_id,
                pic_dept_code: current_pic_dept_code,
                unlevel_3: unlevel_3,
                lvl_3: lvl_3
            };
            posting_submission.push(temp);
        }

        if (posting_submission.length == 0) {
            this.addCurrentSubmission();
        } else {
            var idx = posting_submission.findIndex(t => t.pic_dept_id == current_pic_dept_id);

            if (idx != -1) {
                //Add item value level 3 into current pic dept wihout duplicate
                if (lvl_3.length > 0) {
                    lvl_3.forEach(function(val) {
                        if (posting_submission[idx].lvl_3.indexOf(val) == -1) posting_submission[idx].lvl_3.push(val);
                    });
                    posting_submission[idx].lvl_3 = posting_submission[idx].lvl_3.sort();
                }

                //Add item value unlevel 3 into current pic dept wihout duplicate
                if (unlevel_3.length > 0) {
                    unlevel_3.forEach(function(val) {
                        if (posting_submission[idx].unlevel_3.indexOf(val) == -1) posting_submission[idx].unlevel_3.push(val);
                    });
                    posting_submission[idx].unlevel_3 = posting_submission[idx].unlevel_3.sort();
                }
            } else {
                this.addCurrentSubmission();
            }
        }

        var index = posting_submission.findIndex(x => x.pic_dept_id == current_pic_dept_id);

        $.ajax({
            url: "<?php echo base_url() ?>Nie/get_doc_lvl_4",
            type: "POST",
            dataType: "json",
            data: {
                "lvl_3": posting_submission[index].lvl_3,
                "unlevel_3": posting_submission[index].unlevel_3,
                "pic_dept_id": current_pic_dept_id,
                "pic_name": current_pic_name
            },
            success: function(res) {
                if (submission_position.length == 0) {
                    var t = {
                        id: current_pic_dept_id,
                        html: res
                    };
                    submission_position.push(t);
                } else {
                    var x = submission_position.findIndex(v => v.id == current_pic_dept_id);
                    if (x == -1) {
                        var t = {
                            id: current_pic_dept_id,
                            html: res
                        };
                        submission_position.push(t);
                    } else {
                        submission_position[x].html = res;
                    }
                }

                for (var j = 0; j < submission_position.length; j++) {
                    if ($('#card_' + submission_position[j].id).length) {
                        document.getElementById('card_' + submission_position[j].id).remove();
                        count_accordion -= 1;
                    }
                    $('#accordion').append(submission_position[j].html);
                    count_accordion += 1;
                }

                document.getElementById('accordion').style.display = 'block';
                if (count_accordion > 0) {
                    if (submit_doc_submission.style.display == "none") {
                        submit_doc_submission.style.display = "block";
                    }
                }

                document.getElementById('add-submission-pic').disabled = true;

                for (var j = 0; j < submission_position.length; j++) {
                    //untuk nge tag email penerima, cek dulu element nya ada atau engga
                    if ($('#recipient_' + submission_position[j].id).length) {
                        recipient_id.push(submission_position[j].id);
                        $('[name=recipient_' + submission_position[j].id + ']').tagify({
                            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                            maxTags: 10,
                        });
                    }
                }

                current_pic_dept_id = "";
                current_pic_dept_code = "";
                current_pic_name = "";
                $('#tbody_doc3').empty();
                $('#tbody_doc2').empty();
                content_3.style.display = 'none';
                content_2.style.display = 'none';
                content_1.style.display = 'block'
                $('.selected-doc-pic').val("");
                $('.checked-content-one').prop("checked", false);
                unlevel_3 = [];
                lvl_3 = [];
            },
            error: function(res) {
                // do something
            }
        });
    }

    function showLvl_2() {
        lvl_1 = [];
        $('input[name="doc_lvl_1[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_1.push($(this)[0].id);
            }
        });

        if (lvl_1.length > 0) {
            if (content_2.style.display == 'none') {
                getDocLvl2();
            }
        }
    }

    function showLvl_3() {
        lvl_2 = [];
        $('input[name="doc_lvl_2[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_2.push($(this)[0].id);
            }
        });

        if (lvl_2.length > 0) {
            getDocLvl3();
        }
    }

    function showLvl_4() {
        lvl_3 = [];
        $('input[name="doc_lvl_3[]"]').each(function() {
            if ($(this)[0].checked == true) {
                lvl_3.push($(this)[0].id);
            }
        });
        getDocLvl4();
    }

    $('#search_doc_2').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('#table_doc_2 tr').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('#prev_to_1').on('click', function() {
        if (content_2.style.display == 'block') {
            content_1.style.display = 'block'
            $('#tbody_doc2').empty();
            document.getElementById('next_lvl_3').disabled = true;
            content_2.style.display = 'none';
        }
    });

    $('#prev_to_2').on('click', function() {
        if (content_3.style.display == 'block') {
            content_2.style.display = 'block'
            $('#tbody_doc3').empty();
            content_3.style.display = 'none';
        }
    });

    //Validasi ekstensi file sertifikat hanya boleh .pdf
    function hasExtension(inputID, exts) {
        var fileName = document.getElementById(inputID).value;
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }

    //Validasi ekstensi file sertifikat maksimal size yaitu 500 MB
    function ValidateSize(file) {
        var FileSize = file.size / 1024 / 1024; // in MB
        if (FileSize > 500) {
            return false;
        } else {
            return true;
        }
    }

    $('#upload_submission').on('change', function() {
        var upload_error = document.getElementById('upload_error');
        var fileName = "Select File";
        // if (!hasExtension('upload_submission', ['.rar'])) {
        //     upload_error.style.display = "block";
        //     upload_error.innerHTML = '<i>hanya mendukung file yang berekstensi .zip / .rar</i>';
        //     $(this).val(null);
        // } else {
        var submission_doc = document.getElementById('upload_submission').files[0];
        if (!ValidateSize(submission_doc)) {
            upload_error.style.display = "block";
            upload_error.innerHTML = '<i>Ukuran maksimal yang diperbolehkan yaitu 500MB</i>';
            $(this).val(null);
        } else {
            upload_error.style.display = "none";
            fileName = $(this).val().split('\\').pop(); //untuk mengambil file name
        }
        // }
        $(this).next('.custom-file-label').html(fileName);
    });

    $('#document_pic').change(function() {
        if (this.value != "") {
            current_pic_dept_id = this.value;
            current_pic_dept_code = $(this).find('option:selected').data('deptcode');
            current_pic_name = $(this).find('option:selected').text();
            document.getElementById('add-submission-pic').disabled = false;
        } else {
            document.getElementById('add-submission-pic').disabled = true;
        }
    });

    function removeAccordion(dept_id) {
        document.getElementById('card_' + dept_id).remove();
        count_accordion -= 1;

        if (posting_submission.length > 0) {
            var idx = posting_submission.findIndex(x => x.pic_dept_id === dept_id);
            posting_submission.splice(idx, 1);
        }

        if (count_accordion == 0) {
            if (submit_doc_submission.style.display == "block") {
                submit_doc_submission.style.display = "none";
            }
        }
    }

    function postSubmission() {
        // Untuk Data Submission
        if (posting_submission.length > 0) {
            if (recipient_id.length > 0) {
                var email_recipient = [];
                for (var n = 0; n < recipient_id.length; n++) {
                    var email = $('#recipient_' + recipient_id[n]).val();
                    if (email != "") {
                        email = JSON.parse(email);
                        var email_temp = [];
                        for (var p = 0; p < email.length; p++) {
                            email_temp.push(email[p].value);
                        }

                        var recipients = {
                            dept_id: recipient_id[n],
                            recipient_value: email_temp
                        };
                        email_recipient.push(recipients);
                    }
                }
                document.getElementById('email_recipients').value = JSON.stringify(email_recipient);
            }
            document.getElementById('end_of_submission').value = JSON.stringify(posting_submission);
        }

        // Untuk Tambahan Data BPOM
        if (bpom_data.length > 0) {
            var email_recipient_bpom = [];
            for (var j = 0; j < bpom_data.length; j++) {
                var mail_list = $('#recipient_' + bpom_data[j].pic_dept_id + '_bpom').val();
                if (mail_list != "") {
                    mail_list = JSON.parse(mail_list);
                    var tmp = [];
                    for (var k = 0; k < mail_list.length; k++) {
                        tmp.push(mail_list[k].value);
                    }

                    var t = {
                        bpom_dept_id: bpom_data[j].pic_dept_id,
                        recipient_bpom: tmp
                    };
                    email_recipient_bpom.push(t);
                }
            }
            if (email_recipient_bpom.length > 0) {
                document.getElementById('email_recipients_bpom').value = JSON.stringify(email_recipient_bpom);
            }
            document.getElementById('end_of_bpom').value = JSON.stringify(bpom_data);
        }

        document.submit_submission.submit();
    }

    function removeItemBpom(r_bpom) {
        var i = r_bpom.parentNode.parentNode.rowIndex;

        var show_pre_table_bpom = document.getElementById('show-pre-table-bpom');
        var show_table_bpom = document.getElementById('show-table-bpom');

        var pre_table_bpom = document.getElementById('pre-table-bpom');
        var table_bpom = document.getElementById('table-bpom');
        // var show_upload = document.getElementById('show-upload');
        var show_submit = document.getElementById('submit-bpom');

        /*Hapus row di table*/
        document.getElementById('table-bpom').deleteRow(i);

        var table_length = $('#table-bpom tr').length;

        if (table_length == 0) {
            // show_upload.style.display = "none";
            show_submit.disabled = true;
            show_pre_table_bpom.style.display = "block";
            show_table_bpom.style.display = "none";
        }
    }

    function removeSelectedItemBpom(selected_row_bpom, dept_id) {
        var i = selected_row_bpom.parentNode.parentNode.rowIndex;
        document.getElementById('table-bpom-' + dept_id).deleteRow(i);
        var sel_bpom_length = $('#table-bpom-' + dept_id + ' tr').length;

        if (sel_bpom_length - 1 == 0) {
            var i = bpom_data.findIndex(val => val.pic_dept_id == dept_id);
            if (i > -1) {
                bpom_data.splice(i, 1);
            }
            document.getElementById('card_' + dept_id + '_bpom').remove();
        }
    }

    function removeAccordionBpom(dept_id) {
        var i = bpom_data.findIndex(val => val.pic_dept_id == dept_id);
        if (i > -1) {
            bpom_data.splice(i, 1);
        }
        document.getElementById('card_' + dept_id + '_bpom').remove();
        count_accordion -= 1;

        if (count_accordion == 0) {
            if (submit_doc_submission.style.display == "block") {
                submit_doc_submission.style.display = "none";
            }
        }
    }

    function addDataBpom() {
        var show_pre_table_bpom = document.getElementById('show-pre-table-bpom');
        var show_table_bpom = document.getElementById('show-table-bpom');
        var input_bpom = document.getElementById('tambahan_bpom');
        var show_submit = document.getElementById('submit-bpom');
        // var pre_table_bpom = document.getElementById('pre-table-bpom');
        // var table_bpom = document.getElementById('table-bpom');
        // var show_upload = document.getElementById('show-upload');

        if (input_bpom.value != "") {
            if (show_table_bpom.style.display == "none") {
                show_table_bpom.style.display = "block";
            }

            var row = '<tr><td><label class="count"></label>.&nbsp;' + input_bpom.value + '</td><td width="2%"><a href="javascript:void(0)" onclick="removeItemBpom(this)"><i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 18px;"></i></a></td></tr>';
            $('#table-bpom tbody').append(row);


            var table_length = $('#table-bpom tr').length;

            if (table_length > 0) {
                // show_upload.style.display = "block";
                show_pre_table_bpom.style.display = "none";
                show_submit.disabled = false;
            }

            input_bpom.value = "";
        }
    }

    function submitTambahanData() {
        var pic_dept = document.getElementById('document_pic').value;
        var tmp_bpom = [];
        var bpom_table = Array.prototype.map.call(document.querySelectorAll('#table-bpom tr'), function(tr) {
            return Array.prototype.map.call(tr.querySelectorAll('td:nth-child(1)'), function(td) {
                tmp_bpom.push(td.innerHTML.replace('<label class="count"></label>.&nbsp;', ''));
            });
        });

        if (tmp_bpom.length != 0 && pic_dept != "") {
            var show_pre_table_bpom = document.getElementById('show-pre-table-bpom');
            var show_table_bpom = document.getElementById('show-table-bpom');
            var show_submit = document.getElementById('submit-bpom');

            if (bpom_data.length > 0) {
                var t = bpom_data.findIndex(n => n.pic_dept_id == current_pic_dept_id);

                if (t != -1) {
                    var tmp = bpom_data[t].data.concat(tmp_bpom.filter(function(item) {
                        return bpom_data[t].data.indexOf(item) < 0;
                    }));
                    bpom_data[t].data = tmp
                } else {
                    var bpom = {
                        pic_dept_id: current_pic_dept_id,
                        pic_dept_code: current_pic_dept_code,
                        pic_dept_name: current_pic_name,
                        data: tmp_bpom
                    };
                    bpom_data.push(bpom);
                }
            } else {
                var bpom = {
                    pic_dept_id: current_pic_dept_id,
                    pic_dept_code: current_pic_dept_code,
                    pic_dept_name: current_pic_name,
                    data: tmp_bpom
                };
                bpom_data.push(bpom);
            }

            var k = bpom_data.findIndex(val => val.pic_dept_id == current_pic_dept_id);
            var row = '';
            for (var j = 0; j < bpom_data[k].data.length; j++) {
                row += '<tr><td><label>' + (j + 1) + '</label>.&nbsp;' + bpom_data[k].data[j] + '</td><td width="2%"><a href="javascript:void(0)" onclick="removeSelectedItemBpom(this,\'' + bpom_data[k].pic_dept_id + '\')"><i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 18px;"></i></a></td></tr>';
            }

            if ($('#table-bpom-' + current_pic_dept_id).length) {
                $('#table-bpom-' + current_pic_dept_id + ' tbody tr').remove();
                $('#table-bpom-' + current_pic_dept_id + ' tbody').append(row);
            } else {
                var bpom_elem = '';
                bpom_elem += '<div class="card" id="card_' + current_pic_dept_id + '_bpom">';
                bpom_elem += '<div class="card-header" id="heading_' + current_pic_dept_id + '_bpom" style="background-color: #d7eaf7;">';
                bpom_elem += '<h6 class="mb-0"><a href="#" data-toggle="collapse" data-target="#collapse_' + current_pic_dept_id + '_bpom" aria-expanded="true" aria-controls="collapse_' + current_pic_dept_id + '_bpom" style="color: black;">' + current_pic_name + ' Department</a><span class="float-right" style="color: black;">Additional BPOM Data</span></h6></div>';
                bpom_elem += '<div id="collapse_' + current_pic_dept_id + '_bpom" class="collapse show" aria-labelledby="heading_' + current_pic_dept_id + '_bpom" data-parent="#accordion">';
                bpom_elem += '<div class="card-body" style="background-color: #f7f7f7;">';
                bpom_elem += '<div class="row mt-2"><div class="col"><div style="background-color: white;"><input name="recipient_' + current_pic_dept_id + '_bpom" id="recipient_' + current_pic_dept_id + '_bpom" placeholder="ketik email yang dituju"></div></div></div>';
                bpom_elem += '<div class="row mt-3"><div class="col"><table class="table table-bordered sub-document-bpom" id="table-bpom-' + current_pic_dept_id + '"><thead><tr><th scope="col" colspan="2" width="10%">Document Name</th></tr></thead><tbody>' + row + '</tbody></table></div></div>';
                bpom_elem += '<div class="row mt-3"><div class="col"><button type="button" class="btn btn-danger col-md-1 float-right" onclick="removeAccordionBpom(' + current_pic_dept_id + ')">Remove</button></div></div>';
                bpom_elem += '</div></div></div></div>';

                $('#accordion').append(bpom_elem);
                $('[name=recipient_' + current_pic_dept_id + '_bpom]').tagify({
                    pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    maxTags: 10,
                });
                document.getElementById('accordion').style.display = 'block';
            }

            $('#table-bpom tbody tr').remove();
            show_table_bpom.style.display = "none";
            show_pre_table_bpom.style.display = "block";
            show_submit.disabled = true;
            document.getElementById('document_pic').value = "";
            count_accordion += 1;
            if (submit_doc_submission.style.display == "none") {
                submit_doc_submission.style.display = "block";
            }
        }
    }
</script>