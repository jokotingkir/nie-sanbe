<div class="container">
    <br />
    <div class="alert alert-warning" role="alert">
        <b>NIE-Monitoring System</b><br /> Sistem Informasi Pengelolaan Administrasi Data NIE (Nomor Ijin Edar).
    </div>
    <?php if ($this->session->flashdata('message')) : ?>
        <?php echo $this->session->flashdata('message'); ?>
    <?php endif; ?>
    <br />
    <div class="row">
        <div class="col">
            <h5>Daftar Produk Obat&nbsp;&nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('default')" data-toggle="tooltip" data-placement="top" title="Reset"><i class="fa fa-refresh" aria-hidden="true" style="font-size: 13px; color:blue;"></i></a></h5> 
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-7">
            <form class="form-inline" action="" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="user_option">Filter berdasarkan</label>
                    </div>
                    <select class="custom-select" id="kategori_pencarian">
                        <!-- <option value=all">ALL</option> -->
                        <option value="nie_no">NOMOR REGISTRASI</option>
                        <option value="PRODUCT_CODE">KODE PRODUK</option>
                        <option value="product_name">NAMA PRODUK</option>
                        <option value="brand_name">MERK</option>
                        <!-- <option value="">JUMLAH & KEMASAN</option>
                        <option value="">BENTUK SEDIAAN</option> -->
                        <option value="compotition">KOMPOSISI</option>
                        <option value="planning_area">UNIT</option>
                        <!-- <option value="npwp_owner">NPWP PENDAFTAR</option> -->
                    </select>
                </div>
                &nbsp;&nbsp;
                <div class="input-group mb-3">
                    <input type="text" class="form-control column_filter" placeholder="Ketikkan kata kunci.." name="keyword" id="kata_kunci">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="btn-filter">Cari</button>
                    </div>
                </div>
            </form>
        </div>

        <!-- Privilege tambah data nie product -->
        <?php if ($this->session->userdata('add_nie_product') == '1') : ?>
            <div class="col-md-5">
                <a href="<?php echo base_url(); ?>add-nie" class="float-right btn btn-primary mb-3">Add Data</a>
            </div>
        <?php endif; ?>
        <!-- End of Privilege tambah data nie product -->

    </div>
    <!-- buat export excel  -->
    <form method="POST" action="<?php echo base_url() ?>Nie/exportExcel" id="hidden_export">
        <input type="hidden" name="kategori_pencarian" id="hidden_kategori" />
        <input type="hidden" name="kata_kunci" id="hidden_keyword" />
        <input type="hidden" name="filter_color" id="hidden_filter_color" />
        <input type="hidden" name="nie_config" id="hidden_nie_config" />
        <input type="hidden" name="tipe_export" id="hidden_tipe_export" />
    </form>

    <div class="row">
        <div class="col">
            <form>
                <div class="dropdown">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-describedby="download_help">
                        <i class="fa fa-download" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#" onclick="downloadExcel('2')">Berdasarkan template NIE</a>
                        <a class="dropdown-item" href="#" onclick="downloadExcel('1')">Sesuai tampilan Display Web</a>
                        <a class="dropdown-item" href="#" onclick="downloadExcel('3')">Berdasarkan NIE komposisi</a>
                    </div>
                    <small id="download_help" class="text-muted">
                        <i>Unduh File NIE berdasarkan kategori</i>
                    </small>
                </div>
            </form>
        </div>
        <div class="col">
            <div class="float-right filter-alert">
                &nbsp;<a href="javascript:void(0)" data-html="true" data-container="span" data-placement="left" data-toggle="popover" data-trigger="focus" title="Alert Color" data-content='<i class="fa fa-circle fa-1x" aria-hidden="true" style="color: gray;"></i>&nbsp;NIE <b>Belum Memasuki</b> Alert System<br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: green;"></i>&nbsp;Status Submit NIE <b>Aman</b><br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: yellow;"></i>&nbsp;Status Submit NIE <b>Hampir Mendekati</b><br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: red;"></i>&nbsp;Status Submit NIE <b>Sangat Mendekati</b><br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: pink;"></i>&nbsp;Status NIE <b>Belum Expired</b> dan <b>Sudah Submit</b><br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: blue;"></i>&nbsp;Status NIE <b>Sudah Expired</b> dan <b>Sudah Submit</b><br /><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: brown;"></i>&nbsp;Status NIE <b>Sudah Expired</b> dan <b>Belum Submit</b><br />'><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>
                &nbsp;<span style="color: #bfbfbd;">|</span>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('gray')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: gray;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('green')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: green;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('yellow')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: yellow;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('red')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: red;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('pink')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: pink;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('blue')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: blue;"></i></a>
                &nbsp;<a href="javascript:void(0)" onclick="filterAlertColor('brown')"><i class="fa fa-circle fa-1x" aria-hidden="true" style="color: brown;"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if (empty($nie)) : ?>
                <div class="alert alert-danger" role='alert'>
                    Data Tidak Ditemukan
                </div>
            <?php endif; ?>
            <table class="table table-hover display" id="nie_table">
                <thead>
                    <tr>
                        <th></th>
                        <th>NO</th>
                        <th>NOMOR REGISTRASI</th>
                        <th>KODE PRODUK</th>
                        <th>PRODUK</th>
                        <th>PIC</th>
                        <th>TGL TERBIT</th>
                        <th>MASA BERLAKU</th>
                        <th>UNIT</th>
                        <th>ACTIONS</th>
                        <th>ALERT</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

</div>

<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $('.popover-dismiss').popover({
        trigger: 'focus'
    });

    //Cek hak akses
    <?php if ($this->session->userdata('logged_user')): ?>
        var edit_nie_product = '<?php echo $this->session->userdata('edit_nie_product') ?>';
        var detail_nie_product = '<?php echo $this->session->userdata('detail_nie_product') ?>';
        var access_submission = '<?php echo $this->session->userdata('submission_page') ?>';
        var submit_bpom = '<?php echo $this->session->userdata('submit_bpom_nie_product') ?>';
    <?php endif; ?>

    //akses button action
    var add_button = "";
    var edit_button = "";
    var detail_button = "";
    var submission_btn = "";

    var x_start_schedule;
    var x_yellow_schedule;
    var x_red_schedule;
    var start_schedule;
    var yellow_schedule;
    var red_schedule;
    var today = new Date();
    var notif;
    var table = $('#nie_table');
    var current_color = "default";
    var nie_config;

    function convertDate(inputFormat) {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }
        if (inputFormat != '0000-00-00') {
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('-');
        } else {
            return "-";
        }
    }

    /* Ditutup karena ada perubahan pada nie_alert_config
    function set_nie_config() {
        $.ajax({
            url: "<?php //echo site_url('Nie/alert_config'); ?>",
            type: "GET",
            error: function() {
                alert('Alert Config is Not Found!');
            },
            success: function(data) {
                if (localStorage.getItem("nie_config") === null) {
                    localStorage.setItem('nie_config', data);
                } else {
                    localStorage.clear();
                }
            }
        });
    }
    */

    function processDataTable() {
        var tableConfig = {
            "processing": true,
            "serverSide": true,
            "order": [],
            'stripeClasses': ['stripe1', 'stripe2'],
            "ordering": true,
            "bFilter": false,
            "lengthMenu": [
                [5, 25, 50, 100, -1],
                [5, 25, 50, 100, "All"]
            ],
            "dom": 'rt<"row"<"col-sm-6"l><"col-sm-6"p>>i<"clear">',
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data/halaman",
                "zeroRecords": "Data tidak ditemukan",
                "infoEmpty": "Tidak ada data",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                // "infoFiltered": "(Hasil pencarian dari _MAX_ data)",
                "infoFiltered": "",
                "paginate": {
                    "previous": "&laquo;",
                    "next": "&raquo;"
                },
                select: {
                    rows: {
                        _: "%d baris dipilih",
                        0: ""
                    }
                }
            },
            "ajax": {
                "url": "<?php echo site_url('Nie/all_data'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.kategori_pencarian = $('#kategori_pencarian').val();
                    data.kata_kunci = $('#kata_kunci').val();
                    data.filter_color = current_color;
                    // data.nie_config = nie_config;
                }
            },
            "columns": [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "render": function() {
                        return '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                    },
                    width: "2%"
                },
                {
                    "data": "num",
                    "width": "2%",
                    "orderable": false
                },
                {
                    "data": "nie_no",
                    "width": "13%"
                },
                {
                    "data": "PRODUCT_CODE",
                    "width": "11%"
                },
                {
                    "data": "product_name",
                    "width": "18%"
                },
                {
                    "data": "create_user",
                    "width": "10%"
                },
                {
                    "data": "nie_start_date",
                    "width": "10%",
                    "className": "text-center",
                    "render": function(data) {
                        return convertDate(data)
                    }
                },
                {
                    "data": "nie_end_date",
                    "width": "11%",
                    "className": "text-center",
                    "render": function(data) {
                        return convertDate(data)
                    }
                },
                {
                    "data": "planning_area",
                    "width": "5%"
                },
                {
                    "render": function(data, type, row) {
                        // var alert = alert_options(row.month_diff, row.export, row.bpom_submit_status, row.expiration_flag);

                        //cek hak akses edit nie product
                        if (typeof edit_nie_product != 'undefined') {
                            if (edit_nie_product == '1') {
                                edit_btn = '<a href="<?php echo base_url() ?>update/' + row.id + '"><i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Update NIE" style="color: #000000; font-size: 9pt;"></i></a>&nbsp;';
                            } else {
                                edit_btn = '';
                            }
                        }

                        //cek hak akses detail nie product
                        if (typeof detail_nie_product != 'undefined') {
                            if (detail_nie_product == '1') {
                                detail_btn = '<a href="<?php echo base_url(); ?>nie-detail/' + row.id + '/' + row.alert_color + '"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="View Detail" style="color: #000000; font-size: 9pt;"></i></a>&nbsp;';
                            } else {
                                detail_btn = '';
                            }
                        }

                        //cek hak akses submission page
                        if (typeof access_submission != 'undefined') {
                            if (access_submission == '1') {
                                submission_btn = '<a href="<?php echo base_url() ?>submission/' + row.id + '"><i class="fa fa-calendar" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Submission" style="color: #000000; font-size: 9pt;"></i></a>&nbsp;';
                            } else {
                                submission_btn = '';
                            }
                        }

                        return edit_btn + detail_btn + submission_btn;
                    },
                    // "width": "5%",
                    "className": "text-center",
                    "orderable": false
                },
                {
                    "data": "alert_color",
                    "render": function(data) {
                        // var alert = alert_options(data, row.product_type, row.submit_status);
                        // diganti karena untuk menentukan export diambil berdasarkan product type
                        // var alert = alert_options(data, row.export, row.submit_status);
                        return '<i class="fa fa-circle fa-2x" aria-hidden="true" style="color:' + data + '"></i>';
                    },
                    "width": "7%",
                    "className": "text-center",
                    "orderable": false
                },
            ]
        };
        
        document.getElementById('hidden_kategori').value = $('#kategori_pencarian').val();
        document.getElementById('hidden_keyword').value = $('#kata_kunci').val();
        document.getElementById('hidden_filter_color').value = current_color;
        
        table.DataTable(tableConfig);
    }

    function filterAlertColor(alert_color) {
        current_color = alert_color;
        if (!$.fn.DataTable.isDataTable('#nie_table')) {
            processDataTable();
        } else {
            table.DataTable().destroy();
            processDataTable();
        }
        document.getElementById('hidden_filter_color').value = current_color;
    }

    /* Ditutup karena alert color di table nie_alert di-update setiap hari pukul 01.00 malam WIB
    function alert_options(month_diff, exp, bpom_submit_status, expiration_flag) {
        if (localStorage.getItem("nie_config") != null) {
            nie_config = JSON.parse(localStorage.getItem("nie_config"));
            $.each(nie_config, function(i, item) {
                if (item.config_type == "Export") {
                    x_start_schedule = item.start_schedule;
                    x_yellow_schedule = item.yellow_notif_start;
                    x_red_schedule = item.red_notif_start;
                } else {
                    start_schedule = item.start_schedule;
                    yellow_schedule = item.yellow_notif_start;
                    red_schedule = item.red_notif_start;
                }
            });
            localStorage.clear();
        }

        if (exp != 'Export') {
            if (month_diff > start_schedule) {
                notif = "green";
            } else if (month_diff <= start_schedule && month_diff > red_schedule) {
                notif = "yellow";
            } else if (month_diff <= red_schedule && !(month_diff < 0)) {
                notif = "red";
            } else if (expiration_flag == "EXPIRED" && bpom_submit_status == "submitted") {
                notif = "blue";
            } else if (expiration_flag == "EXPIRED" && bpom_submit_status == "not submitted") {
                notif = "brown";
            }
        } else {
            if (month_diff > x_start_schedule) {
                notif = "green";
            } else if (month_diff <= x_start_schedule && month_diff > x_red_schedule) {
                notif = "yellow";
            } else if (month_diff <= x_red_schedule && !(month_diff < 0)) {
                notif = "red";
            } else if (expiration_flag == "EXPIRED" && bpom_submit_status == "submitted") {
                notif = "blue";
            } else if (expiration_flag == "EXPIRED" && bpom_submit_status == "not submitted") {
                notif = "brown";
            }
        }

        return notif;
    }
    */

    function format(d) {
        var merk;
        if (merk == null)
            merk = '-';
        else
            merk = d.brand_name;

        return '<table class="normal" cellpadding="2" cellspacing="0">' +
            '<tr>' +
            '<td>Nomor Registrasi</td>' +
            '<td>' + d.nie_no + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Nama Produk</td>' +
            '<td>' + d.product_name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tanggal Terbit</td>' +
            '<td>' + convertDate(d.nie_start_date) + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Komposisi</td>' +
            '<td>' + d.compotition + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Masa Berlaku s/d</td>' +
            '<td>' + convertDate(d.nie_end_date) + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Merk</td>' +
            '<td>' + merk + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Produk</td>' +
            '<td>' + d.product_type_desc + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Kemasan</td>' +
            '<td>' + d.packaging_presentation + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Unit</td>' +
            '<td>' + d.planning_area + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            // '<td>Diproduksi Oleh</td>' +
            // '<td>' + d.product_owner + '</td>' +
            '<td>Alamat Pembuatan</td>' +
            '<td>' + d.manufact_addr + '</td>' +
            '</tr>' +
            '</table>';
    }

    //Ketika loading pertama kali berawal dari sini ----
    // set_nie_config();
    processDataTable();

    if(typeof edit_nie_product != 'undefined'){
        if((edit_nie_product!='1' && detail_nie_product!='1') && (access_submission!='1' && submit_bpom!='1')){
            table.DataTable().column(9).visible(false);
        }
    }

    $('#btn-filter').click(function() {
        document.getElementById('hidden_kategori').value = $('#kategori_pencarian').val();
        document.getElementById('hidden_keyword').value = $('#kata_kunci').val();
        document.getElementById('hidden_filter_color').value = current_color;
        table.DataTable().ajax.reload();
    });

    //Reset filter ketika teks box kata kunci kosong
    $('#kata_kunci').bind('input', function() {
        if ($(this).val() == "") {
            table.DataTable().ajax.reload();
        }
        document.getElementById('hidden_keyword').value = $(this).val();
    });

    // $('#nie_table tbody').on('click', 'td.details-control', function() {
    $('#nie_table tbody').on('click', 'tr', function() {
        var tr = $(this).closest('tr');
        var tdi = tr.find("i.fa");
        var row = table.DataTable().row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
            tdi.first().removeClass('fa-minus-circle');
            tdi.first().addClass('fa-plus-circle');
        } else {
            row.child(format(row.data())).show();
            tr.addClass('shown');
            tdi.first().removeClass('fa-plus-circle');
            tdi.first().addClass('fa-minus-circle');
        }
    });

    table.on("user-select", function(e, dt, type, cell, originalEvent) {
        if ($(cell.node()).hasClass("details-control")) {
            e.preventDefault();
        }
    });

    function downloadExcel(type) {
        document.getElementById('hidden_tipe_export').value = type;
        document.getElementById('hidden_nie_config').value = JSON.stringify(nie_config);
        
        var form = document.getElementById('hidden_export');
        form.submit();
    }
</script>