<div class="container">
    <form id="nie_complete_form" name="nie_complete_form" action="<?php echo base_url(); ?>complete" method="POST" enctype="multipart/form-data">
        <div class="row mt-4">
            <div class="col">
                <p class="small"><i>Bagi yang bertanda <span class="required">*</span> wajib diisi</i></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        NEXT NIE DATA
                    </div>
                    <div class="card-body">
                        <?php if (!isset($nie_no)) : ?>
                            <div class="alert alert-danger" role='alert'>
                                Data Tidak Ditemukan
                            </div>
                        <?php endif; ?>
                        <input type="hidden" name="nie_id" value="<?php echo html_escape($nie_id); ?>" />
                        <div class="form-group">
                            <label for="prodname">Product Name</label>
                            <input type="text" id="prodname" name="prodname" class="form-control" value="<?php echo html_escape($product_name); ?> " readonly />
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodcode">Product Code</label>
                                <input type="text" class="form-control" id="prodcode" name="prodcode" value="<?php echo html_escape($product_code); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodtype">Product Type</label>
                                <select class="form-control" id="prodtype" name="last_prodtype" onchange="ptodtypefunc();" disabled>
                                    <?php foreach ($typelist as $type) : ?>
                                        <option value="<?php echo $type['id']; ?>" <?php if ($type['id'] == $product_type_id) : ?> selected="selected" <?php endif; ?>><?php echo $type['product_type_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- Hanya untuk mengirim product type id, karena element yang di disabled tidak dapat mengirim nilai kembali -->
                                <input type="hidden" name="prodtype" value="<?php echo $product_type_id; ?>" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodown">Product Owner</label>
                                <input type="text" class="form-control" id="prodown" name="prodown" value="<?php echo html_escape($product_owner); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodplan">Planning Area</label>
                                <input type="text" class="form-control" id="prodplan" name="prodplan" value="<?php echo html_escape($planning_area); ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodform">Finished Dosage Form</label>
                                <input type="text" class="form-control" id="prodform" name="prodform" value="<?php echo html_escape($fd_form); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodpckg">Packaging Presentation</label>
                                <input type="text" class="form-control" id="prodpckg" name="prodpckg" value="<?php echo html_escape($packaging_presentation); ?>" readonly />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodlife">Shelf Life</label>
                                <input type="text" class="form-control" id="prodlife" name="prodlife" value="<?php echo html_escape($shelf_life); ?>" readonly />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="last_prodscon">Storage Condition</label>
                                <select class="form-control" id="last_prodscon" name="last_prodscon" disabled>
                                    <?php foreach ($conlist as $con) : ?>
                                        <option value="<?php echo $con['id']; ?>" <?php if ($con['id'] == $storage_condition_id) : ?> selected="selected" <?php endif; ?>><?php echo $con['storage_condition_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <!-- Hanya untuk mengirim storage condition id, karena element yang di disabled tidak dapat mengirim nilai kembali -->
                                <input type="hidden" name="prodscon" value="<?php echo $storage_condition_id; ?>" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div style="display: none;" id="new_number">
                                    <label for="new_prodnie">NIE Number<span class="required">*</span>&nbsp;&nbsp;<small id="new_nie_help" class="text-muted"><i><a href="javascript:void(0)" onclick="showNewNumber()">Klik</a> untuk menggunakan nomor yang ada</i></small></label>
                                    <input type="text" class="form-control" id="new_prodnie" name="new_prodnie" />
                                </div>

                                <div id="selected_number">
                                    <label for="prodnie_combobox">NIE Number<span class="required">*</span>&nbsp;&nbsp;<small id="nie_help" class="text-muted"><i><a href="javascript:void(0)" onclick="showNewNumber()">Klik</a> untuk membuat nomor baru</i></small></label>
                                    <select class="form-control selected-last-nie" id="prodnie_combobox" name="prodnie" required>
                                        <option value="">-- Select Current NIE --</option>
                                        <?php foreach ($last_nie_number as $row) : ?>
                                            <option value="<?php echo $row->nie_no ?>" data-startdate="<?php echo $row->nie_start_date ?>" data-enddate="<?php echo $row->nie_end_date ?>"><?php echo $row->nie_no ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niestat">NIE Status<span class="required">*</span></label>
                                <select class="form-control" id="niestat" name="niestat" required>
                                    <?php foreach ($statlist as $stat) : ?>
                                        <option value="<?php echo $stat['stat_desc']; ?>" <?php if ($stat['stat_desc'] == $nie_status) : ?> selected="selected" <?php endif; ?>><?php echo $stat['stat_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niestartdate">NIE Start Date<span class="required">*</span></label>
                                <input type="date" class="form-control" id="niestartdate" name="niestartdate" required />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nieenddate">NIE End Date<span class="required">*</span></label>
                                <input type="date" class="form-control" id="nieenddate" name="nieenddate" required />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niehldr">NIE Holder</label>
                                <input type="text" class="form-control" id="niehldr" name="niehldr" value="<?php echo html_escape($nie_holder); ?>" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niepblsh">NIE Publisher</label>
                                <input type="text" class="form-control" id="niepblsh" name="niepblsh" value="<?php echo html_escape($nie_publisher); ?>" />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <button class="btn btn-primary" aria-describedby="compotition_help" id="add_compotition" type="button">Add Compotition</button>
                                <small id="compotition_help" class="text-muted">
                                    <i>Click to add <b>compotition</b></i>
                                </small>
                            </div>
                        </div>

                        <div id="compotition-col" class="input-fields-wrap">
                            <div class="form-row align-items-center parent-compotition">
                                <div class="form-group col-md-1">
                                    <center><label class="count"></label></center>
                                </div>
                                <input type="hidden" name="compotition_id[]" id="composId_1" />
                                <input type="hidden" name="item_code[]" id="item_1" />
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_1" />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_1" />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_1" />
                                </div>
                                <!-- <div class="form-group col-md-1">
                                    <a href="#" class="remove_compotition"><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a>
                                </div> -->
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="prodcmpt">Compotition</label>
                            <textarea class="form-control" id="prodcmpt" name="prodcmpt" rows="3"><?php echo html_escape($compotition); ?></textarea>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        &nbsp;
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="upload_cert">NIE Certificate</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="nie_cert" id="upload_cert" />
                                <label class="custom-file-label" for="upload_cert">Pilih File</label>
                            </div>
                            <small style="color: red; display: none;" id="upload_error"></small>
                            <?php if (isset($upload_error) && $upload_error != '') : ?>
                                <small style="color: red;"><i><?php echo $upload_error ?></i></small>
                            <?php endif; ?>
                        </div>

                        <!-- Start Tipe EXPORT -->
                        <div id="exportcard" style="display:none" class="mt-3">
                            <div class="form-group">
                                <label for="prodbrand">Brand Name</label>
                                <input type="text" class="form-control" id="prodbrand" name="prodbrand" value="<?php echo html_escape($brand_name); ?>" />
                            </div>
                            <div class="form-group">
                                <label for="prodctry">Country</label>
                                <input type="text" class="form-control" id="prodctry" name="prodctry" readonly value="<?php echo html_escape($country); ?>" />
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="niercvdate">Receive Date</label>
                                    <input type="date" class="form-control" id="niercvdate" name="niercvdate" value="<?php if ($receive_date != '0000-00-00') {echo html_escape($receive_date);} ?>" />
                                </div>
                                <div class="col-md-6">
                                    <label for="niedspdate">Dispatch Date</label>
                                    <input type="date" class="form-control" id="niedspdate" name="niedspdate" value="<?php if ($dispatch_date != '0000-00-00') {echo html_escape($dispatch_date);} ?>" />
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label for="prodagnt">Agent</label>
                                <select class="form-control" id="prodagnt" name="prodagnt">
                                    <?php foreach ($agent as $v_agent) : ?>
                                        <option value="<?php echo $v_agent['VENDOR_NAME']; ?>" <?php if ($v_agent['VENDOR_NAME'] == $agent) : ?> selected="selected" <?php endif; ?>><?php echo $v_agent['VENDOR_NAME']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prodcrtf">Certificate</label>
                                <select class="form-control" id="prodcrtf" name="prodcrtf">
                                    <?php foreach ($certifilist as $crtf) : ?>
                                        <option value="<?php echo $crtf['id']; ?>" <?php if ($crtf['id'] == $certificate) : ?> selected="selected" <?php endif; ?>><?php echo $crtf['certi_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prodrglt">Regulatory</label>
                                <select class="form-control" id="prodrglt" name="prodrglt">
                                    <?php foreach ($regulatolist as $regulate) : ?>
                                        <option value="<?php echo $regulate['regulatory_code']; ?>" <?php if ($regulate['regulatory_code'] == $regulatory_code) : ?> selected="selected" <?php endif; ?>><?php echo $regulate['regulatory_desc']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!-- End of Tipe EXPORT -->
                        <div class="form-group">
                            <label for="remark">Remark</label>
                            <textarea class="form-control" id="remark" name="remark" rows="3"></textarea>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" name="submit_complete" id="submit_complete" value="completed" class="btn btn-primary btn-block">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    var max_fields = 11; //maksimum kolom isian untuk compotition
    var wrapper = $('.input-fields-wrap'); //fields wrapper
    var add_button = $('#add_compotition'); //tombol tambah compotition
    var x = 2;
    var is_valid = false;

    var product_type = '<?php echo $product_type_id; ?>';
    var export_card = document.getElementById("exportcard");

    if (product_type == "1") {
        export_card.style.display = "block";
    } else {
        export_card.style.display = "none";
    }

    //Set nie compotition by nie alert id dari controller Nie.php
    $nie_alert_id = '<?php echo $nie_id ?>';
    set_nie_compotition($nie_alert_id);

    function setCompotitionChild(x) {
        var html = '';
        html += '<div class="form-row align-items-center parent-compotition">';
        html += '<div class="form-group col-md-1"><center><label class="count"></label></center></div><input type="hidden" name="compotition_id[]" id="composId_' + x + '" /><input type="hidden" name="item_code[]" id="item_' + x + '" />';
        html += '<div class="form-group col-md-4"><input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_' + x + '" /></div>';
        html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_' + x + '" /></div>';
        html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_' + x + '" /></div>';
        html += '<div class="form-group col-md-1"><a href="#" class="remove_compotition"><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a></div></div>';

        return html;
    }

    //inisialisasi nie compotition saat data di edit
    function set_nie_compotition(nie_alert_id) {
        $.ajax({
            url: "<?php echo site_url('Nie/nie_compotition'); ?>",
            type: "GET",
            data: {
                nie_alert_id: nie_alert_id
            },
            error: function() {
                alert('NIE Compotition is Not Found!');
            },
            success: function(data) {
                var res = JSON.parse(data);
                if (Array.isArray(res)) {
                    if (res.length > 0) {
                        for (var n = 0; n < res.length; n++) {
                            document.getElementById('composId_' + (n + 1)).value = res[n].id;
                            document.getElementById('item_' + (n + 1)).value = res[n].item_code;
                            document.getElementById('compotition_' + (n + 1)).value = res[n].item_desc;
                            document.getElementById('strength_' + (n + 1)).value = res[n].strength;
                            document.getElementById('uom_' + (n + 1)).value = res[n].uom;

                            if (res.length >= x && x < max_fields) {
                                var compotition_child = setCompotitionChild(x);
                                $(wrapper).append(compotition_child);
                                x++;
                            }
                        }
                    }
                }
            }
        });
    }

    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            //menggunakan 2 digit angka random agar tidak terdapat id yang sama dengan yang lainnya
            var r = Math.floor((Math.random() * 90) + 10);
            var compotition_child = setCompotitionChild(r);
            $(wrapper).append(compotition_child);
            x++;
        }
    });

    $(wrapper).on('click', '.remove_compotition', function(e) {
        e.preventDefault();
        $(this).parents('.parent-compotition').remove();
        x--;
    });

    $(document).on('keydown', '.autocomplete_compotition', function() {
        var id = this.id;
        var split_id = id.split('_');
        var index = split_id[1];

        $('#' + id).autocomplete({
            source: "<?php echo site_url('Nie/getItemsDesc'); ?>",
            select: function(event, ui) {
                //set value item_code to input hidden
                document.getElementById('composId_' + index).value = 'new_' + index;
                document.getElementById('item_' + index).value = ui.item.item_code;
            }
        });
    });

    //Validasi ekstensi file sertifikat hanya boleh .pdf
    function hasExtension(inputID, exts) {
        var fileName = document.getElementById(inputID).value;
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }

    //Validasi ekstensi file sertifikat maksimal size yaitu 10 MB
    function ValidateSize(file) {
        var FileSize = file.size / 1024 / 1024; // in MB
        if (FileSize > 10) {
            return false;
        } else {
            return true;
        }
    }

    $('#upload_cert').on('change', function() {
        var upload_error = document.getElementById('upload_error');
        var fileName = "Pilih File";
        if (!hasExtension('upload_cert', ['.pdf'])) {
            upload_error.style.display = "block";
            upload_error.innerHTML = '<i>hanya mendukung file yang berekstensi .pdf</i>';
            $(this).val(null);
        } else {
            var certificate = document.getElementById('upload_cert').files[0];
            if (!ValidateSize(certificate)) {
                upload_error.style.display = "block";
                upload_error.innerHTML = '<i>Ukuran maksimal yang diperbolehkan yaitu 10MB</i>';
                $(this).val(null);
            } else {
                upload_error.style.display = "none";
                fileName = $(this).val().split('\\').pop(); //untuk mengambil file name
            }
        }
        $(this).next('.custom-file-label').html(fileName);
    });

    function showNewNumber() {
        var new_number = document.getElementById('new_number');
        var selected_number = document.getElementById('selected_number');

        var new_nie = document.getElementById('new_prodnie');
        var last_nie = document.getElementById('prodnie_combobox');

        if (new_number.style.display == 'none') {
            selected_number.style.display = 'none';
            last_nie.required = false;

            new_number.style.display = 'block';
            new_nie.required = true;

            $('#prodnie_combobox').val("");
            $('#niestartdate').val("");
            $('#nieenddate').val("");
        } else {
            selected_number.style.display = 'block';
            last_nie.required = true;

            new_number.style.display = 'none';
            new_nie.required = false;

            $('#new_prodnie').val("");
            $('#selected_nie_number').val("");
        }

    }

    $(document).ready(function() {
        $('select.selected-last-nie').change(function() {
            var nie_start_date = $(this).find(':selected').data('startdate');
            var nie_end_date = $(this).find(':selected').data('enddate');

            $('#niestartdate').val(nie_start_date);
            $('#nieenddate').val(nie_end_date);
        });
    });
</script>