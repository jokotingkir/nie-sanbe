<div class="container">
    <br />
    <h5>Statistik Produk Yang Mendapat Persetujuan Izin Edar</h5>
    <br />
    <div class="row">
        <div class="col">
            <div id="by_alert"></div>
            <table id="legend-by-alert" style="display: none;">
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#dbd5d7;">
                    </td>
                    <td>&nbsp;</td>
                    <td>NIE <b>Belum Memasuki</b> Alert System</td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#45f442;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status Submit NIE <b>Aman</b></td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#e8f441;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status Submit NIE <b>Hampir Mendekati</b></td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ef1010;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status Submit NIE <b>Sangat Mendekati</b></td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ff6e9e;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status NIE <b>Belum Expired</b> dan <b>Sudah Submit</b></td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#0e73cc;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status NIE <b>Sudah Expired</b> dan <b>Sudah Submit</b></td>
                </tr>
                <tr>
                    <td width="20%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#c4b487;">
                    </td>
                    <td>&nbsp;</td>
                    <td>Status NIE <b>Sudah Expired</b> dan <b>Belum Submit</b></td>
                </tr>
            </table>
        </div>
        <div class="col">
            <div id="by_unit"></div>
            <table id="legend-by-unit" style="display: none;">
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#fa7a43;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SANBE UNIT 1</td>
                </tr>
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ffd000;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SANBE UNIT 2</td>
                </tr>
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#f54758;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SANBE UNIT 3</td>
                </tr>
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#f7a00a;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SANBE UNIT 4 (ONCOLOGY)</td>
                </tr>
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#92e83c;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SANBE UNIT 5 (BIOLOGICAL)</td>
                </tr>
                <tr>
                    <td width="27%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#0cb7fa;">
                    </td>
                    <td>&nbsp;</td>
                    <td>CAPRI</td>
                </tr>
            </table>
        </div>
        <div class="col">
            <div id="by_product_type"></div>
            <table id="legend-by-product-type" style="display: none;">
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#35978f;">
                    </td>
                    <td>&nbsp;</td>
                    <td>ETHICAL</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#017351;">
                    </td>
                    <td>&nbsp;</td>
                    <td>SUPLEMEN</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#03c383;">
                    </td>
                    <td>&nbsp;</td>
                    <td>GENERIK</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#aad962;">
                    </td>
                    <td>&nbsp;</td>
                    <td>ALAT KESEHATAN</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#fbbf45;">
                    </td>
                    <td>&nbsp;</td>
                    <td>PANGAN</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ef6a32;">
                    </td>
                    <td>&nbsp;</td>
                    <td>PKRT</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ed0345;">
                    </td>
                    <td>&nbsp;</td>
                    <td>OBAT TRADISIONAL</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#a12a5e;">
                    </td>
                    <td>&nbsp;</td>
                    <td>PRODUK BIOLOGI</td>
                </tr>
            </table>
        </div>
        <!-- <div class="col">
            <div id="by_trans"></div>
            <table>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#ff5100;">
                    </td>
                    <td>&nbsp;</td>
                    <td>EXPORT</td>
                </tr>
                <tr>
                    <td width="35%">&nbsp;</td>
                    <td>
                        <div style="width:40px;height:10px;background-color:#07bfe8;">
                    </td>
                    <td>&nbsp;</td>
                    <td>IMPORT</td>
                </tr>
            </table>
        </div> -->
    </div>
    <div class="row" id="drilldown-chart1" style="display: none;">&nbsp;</div>
    <div class="row" id="drilldown-chart2" style="display: none;">&nbsp;</div>
    <div class="row" id="drilldown-chart3" style="display: none;"></div>
    <div class="row" id="drilldown-chart4" style="display: none;">
        <div class="col">
            <table class="table table-hover display" id="drilldown_table">
                <thead>
                    <tr>
                        <th></th>
                        <th>NO</th>
                        <th>NOMOR REGISTRASI</th>
                        <th>KODE PRODUK</th>
                        <th>PRODUK</th>
                        <th>PIC</th>
                        <th>TGL TERBIT</th>
                        <th>MASA BERLAKU</th>
                        <th>UNIT</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script>
    var drilldown_chart1 = document.getElementById('drilldown-chart1');
    var drilldown_chart2 = document.getElementById('drilldown-chart2');
    var drilldown_chart3 = document.getElementById('drilldown-chart3');
    var drilldown_chart4 = document.getElementById('drilldown-chart4');

    var drilldownDataTable = $('#drilldown_table');

    function convertDate(inputFormat) {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }
        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('-');
    }

    function format(d) {
        var merk;
        if (merk == null)
            merk = '-';
        else
            merk = d.brand_name;

        return '<table class="normal" cellpadding="2" cellspacing="0">' +
            '<tr>' +
            '<td>Nomor Registrasi</td>' +
            '<td>' + d.nie_no + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Nama Produk</td>' +
            '<td>' + d.product_name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tanggal Terbit</td>' +
            '<td>' + convertDate(d.nie_start_date) + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Komposisi</td>' +
            '<td>' + d.compotition + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Masa Berlaku s/d</td>' +
            '<td>' + convertDate(d.nie_end_date) + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Merk</td>' +
            '<td>' + merk + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Produk</td>' +
            '<td>' + d.product_type_desc + '</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Kemasan</td>' +
            '<td>' + d.packaging_presentation + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Unit</td>' +
            '<td>' + d.planning_area + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td style="width:150px">&nbsp;</td>' +
            '<td>Alamat Pembuatan</td>' +
            '<td>' + d.manufact_addr + '</td>' +
            '</tr>' +
            '</table>';
    }

    function reprocessDataTable(list_nie_id) { //Set konfigurasi dan data untuk DataTable
        var tableOptions = {
            "processing": true,
            "serverSide": true,
            "order": [],
            'stripeClasses': ['stripe1', 'stripe2'],
            "ordering": true,
            "bFilter": false,
            "lengthMenu": [
                [5, 25, 50, 100, -1],
                [5, 25, 50, 100, "All"]
            ],
            "dom": 'rt<"row"<"col-sm-6"l><"col-sm-6"p>>i<"clear">',
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data/halaman",
                "zeroRecords": "Data tidak ditemukan",
                "infoEmpty": "Tidak ada data",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                // "infoFiltered": "(Hasil pencarian dari _MAX_ data)",
                "infoFiltered": "",
                "paginate": {
                    "previous": "&laquo;",
                    "next": "&raquo;"
                },
                select: {
                    rows: {
                        _: "%d baris dipilih",
                        0: ""
                    }
                }
            },
            "ajax": {
                "url": "<?php echo site_url('Nie/chartDrilldown'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.chart_index = 0;
                    data.nie_id = list_nie_id;
                }
            },
            "columns": [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "render": function() {
                        return '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                    },
                    width: "2%"
                },
                {
                    "data": "num",
                    "width": "2%",
                    "orderable": false
                },
                {
                    "data": "nie_no",
                    "width": "13%"
                },
                {
                    "data": "PRODUCT_CODE",
                    "width": "11%"
                },
                {
                    "data": "product_name",
                    "width": "18%"
                },
                {
                    "data": "create_user",
                    "width": "10%"
                },
                {
                    "data": "nie_start_date",
                    "width": "10%",
                    "className": "text-center",
                    "render": function(data) {
                        return convertDate(data)
                    }
                },
                {
                    "data": "nie_end_date",
                    "width": "11%",
                    "className": "text-center",
                    "render": function(data) {
                        return convertDate(data)
                    }
                },
                {
                    "data": "planning_area",
                    "width": "5%"
                }
            ]
        };

        drilldownDataTable.DataTable(tableOptions);
    }

    function alert_text(alert_color) {
        var alert;
        switch (alert_color) {
            case 'RED':
                alert = "Status Submit NIE Sangat Mendekati";
                break;
            case 'YELLOW':
                alert = "Status Submit NIE Hampir Mendekati";
                break;
            case 'BLUE':
                alert = "Status NIE Sudah Expired dan Sudah Submit";
                break;
            case 'BROWN':
                alert = "Status NIE Sudah Expired dan Belum Submit";
                break;
            case 'PINK':
                alert = "Status NIE Belum Expired dan Sudah Submit";
                break;
            case 'GRAY':
                alert = "NIE Belum Memasuki Alert System";
                break;
            default:
                alert = "Status submit NIE Aman";
                break;
        }

        return alert;
    }

    function expandByAlert(ev) { //Inisialisasi DataTable untuk Drilldown data
        drilldown_chart1.style.display = "block";
        drilldown_chart2.style.display = "block";
        drilldown_chart3.style.display = "block";
        drilldown_chart4.style.display = "block";

        //Set untuk judul drilldown datatable
        switch (ev.target._dataItem._dataContext.chart_id) { //chart_id digunakan untuk menandai chart yang ditampilkan berdasarkan index dari kiri ke kanan 1 s/d n
            case '1':
                $('#drilldown-chart3').html("<div class='col'><h6>Drilldown Percentage NIE by Alert - <i class='fa fa-circle' aria-hidden='true' style='color:" + ev.target._dataItem._dataContext.color + ";'></i>&nbsp;" + alert_text(ev.target._dataItem._dataContext.alert_color) + "</h6></div>");
                break;
            case '2':
                $('#drilldown-chart3').html("<div class='col'><h6>Drilldown Percentage NIE by Unit - " + ev.target._dataItem._dataContext.planning_area + "</h6></div>");
                break;
            case '3':
                $('#drilldown-chart3').html("<div class='col'><h6>Drilldown Percentage NIE by Product Type - " + ev.target._dataItem._dataContext.product_type_desc + "</h6></div>");
                break;
        }

        if (!$.fn.DataTable.isDataTable('#drilldown_table')) {
            reprocessDataTable(ev.target._dataItem._dataContext.nie_id);
        } else {
            drilldownDataTable.DataTable().destroy();
            reprocessDataTable(ev.target._dataItem._dataContext.nie_id);
        }
    }

    $(document).on('click', '#drilldown_table tbody tr', function() {
        var tr = $(this).closest('tr');
        var tdi = tr.find("i.fa");
        var row = drilldownDataTable.DataTable().row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
            tdi.first().removeClass('fa-minus-circle');
            tdi.first().addClass('fa-plus-circle');
        } else {
            row.child(format(row.data())).show();
            tr.addClass('shown');
            tdi.first().removeClass('fa-plus-circle');
            tdi.first().addClass('fa-minus-circle');
        }

        // e.preventDefault();
    });

    if (typeof drilldownDataTable != 'undefined') {
        drilldownDataTable.on("user-select", function(e, dt, type, cell, originalEvent) {
            if ($(cell.node()).hasClass("details-control")) {
                e.preventDefault();
            }
        });
    }

    am4core.ready(function() {
        var indicator_1;
        var indicator_2;
        var indicator_3;

        function showIndicator_1() {
            if (!indicator_1) {
                indicator_1 = chart1.tooltipContainer.createChild(am4core.Container);
                indicator_1.background.fill = am4core.color("#fff");
                indicator_1.background.fillOpacity = 0.8;
                indicator_1.width = am4core.percent(100);
                indicator_1.height = am4core.percent(100);

                var indicatorLabel_1 = indicator_1.createChild(am4core.Label);
                indicatorLabel_1.text = "Loading stuff...";
                indicatorLabel_1.align = "center";
                indicatorLabel_1.valign = "middle";
                indicatorLabel_1.fontSize = 15;
                indicatorLabel_1.dy = 30;

                var hourglass_1 = indicator_1.createChild(am4core.Image);
                hourglass_1.href = "<?php echo base_url() ?>assets/images/ellipsis-loading.svg";
                hourglass_1.align = "center";
                hourglass_1.valign = "middle";
                hourglass_1.horizontalCenter = "middle";
                hourglass_1.verticalCenter = "middle";
                hourglass_1.scale = 1;
            }

            indicator_1.hide(0);
            indicator_1.show();
        }

        function showIndicator_2() {
            if (!indicator_2) {
                indicator_2 = chart2.tooltipContainer.createChild(am4core.Container);
                indicator_2.background.fill = am4core.color("#fff");
                indicator_2.background.fillOpacity = 0.8;
                indicator_2.width = am4core.percent(100);
                indicator_2.height = am4core.percent(100);

                var indicatorLabel_2 = indicator_2.createChild(am4core.Label);
                indicatorLabel_2.text = "Loading stuff...";
                indicatorLabel_2.align = "center";
                indicatorLabel_2.valign = "middle";
                indicatorLabel_2.fontSize = 15;
                indicatorLabel_2.dy = 30;

                var hourglass_2 = indicator_2.createChild(am4core.Image);
                hourglass_2.href = "<?php echo base_url() ?>assets/images/ellipsis-loading.svg";
                hourglass_2.align = "center";
                hourglass_2.valign = "middle";
                hourglass_2.horizontalCenter = "middle";
                hourglass_2.verticalCenter = "middle";
                hourglass_2.scale = 1;
            }

            indicator_2.hide(0);
            indicator_2.show();
        }

        function showIndicator_3() {
            if (!indicator_3) {
                indicator_3 = chart3.tooltipContainer.createChild(am4core.Container);
                indicator_3.background.fill = am4core.color("#fff");
                indicator_3.background.fillOpacity = 0.8;
                indicator_3.width = am4core.percent(100);
                indicator_3.height = am4core.percent(100);

                var indicatorLabel_3 = indicator_3.createChild(am4core.Label);
                indicatorLabel_3.text = "Loading stuff...";
                indicatorLabel_3.align = "center";
                indicatorLabel_3.valign = "middle";
                indicatorLabel_3.fontSize = 15;
                indicatorLabel_3.dy = 30;

                var hourglass_3 = indicator_3.createChild(am4core.Image);
                hourglass_3.href = "<?php echo base_url() ?>assets/images/ellipsis-loading.svg";
                hourglass_3.align = "center";
                hourglass_3.valign = "middle";
                hourglass_3.horizontalCenter = "middle";
                hourglass_3.verticalCenter = "middle";
                hourglass_3.scale = 1;
            }

            indicator_3.hide(0);
            indicator_3.show();
        }

        function hideIndicator_1() {
            indicator_1.hide();
        }

        function hideIndicator_2() {
            indicator_2.hide();
        }

        function hideIndicator_3() {
            indicator_3.hide();
        }

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        var chart1 = am4core.create("by_alert", am4charts.PieChart3D);
        showIndicator_1();
        chart1.hiddenState.properties.opacity = 0;
        chart1.preloader.disabled = true;

        // chart1.legend = new am4charts.Legend();
        // chart1.legend.useDefaultMarker = true;
        // var markerTemplate = chart1.legend.markers.template;
        // markerTemplate.width = 30;
        // markerTemplate.height = 10;
        // chart1.legend.itemContainers.template.paddingRight = 40;
        // console.log(chart1.legend);


        // var legendContainer1 = am4core.create("legend_by_alert", am4core.Container);
        // legendContainer1.width = am4core.percent(100);
        // legendContainer1.height = am4core.percent(100);
        // chart1.legend.parent = legendContainer1;

        var chart2 = am4core.create("by_unit", am4charts.PieChart3D);
        showIndicator_2();
        chart2.hiddenState.properties.opacity = 0;
        chart2.preloader.disabled = true;

        // var chart3 = am4core.create("by_trans", am4charts.PieChart3D);
        var chart3 = am4core.create("by_product_type", am4charts.PieChart3D);
        showIndicator_3();
        chart3.hiddenState.properties.opacity = 0;
        chart3.preloader.disabled = true;

        // chart.legend = new am4charts.Legend();

        var colorSet1 = new am4core.ColorSet();
        var color_chart1 = new Array();
        $.ajax({
            url: "<?php echo site_url('Nie/percentageByAlert') ?>",
            type: 'GET',
            dataType: 'JSON',
            success: function(res1) {
                res1[0].pulled = true;
                chart1.data = res1;
                for (var i = 0; i < res1.length; i++) {
                    color_chart1.push(res1[i].color);
                }

                colorSet1.list = color_chart1.map(function(color) {
                    return new am4core.color(color);
                });
                hideIndicator_1();
                document.getElementById('legend-by-alert').style.display = "block";
            }
        });

        var colorSet2 = new am4core.ColorSet();
        var color_chart2 = new Array();
        $.ajax({
            url: "<?php echo site_url('Nie/percentageByUnit') ?>",
            type: 'GET',
            dataType: 'JSON',
            success: function(res2) {
                res2[1].pulled = true;
                chart2.data = res2;

                if (res2.length > 0) {
                    for (var n = 0; n < res2.length; n++) {
                        switch (res2[n].planning_area) {
                            case 'BIOLOGICAL':
                                res2[n].chart_color = '#92e83c';
                                break;
                            case 'CAPRI':
                                res2[n].chart_color = '#0cb7fa';
                                break;
                            case 'ONCOLOGY':
                                res2[n].chart_color = '#f7a00a';
                                break;
                            case 'SANBE2':
                                res2[n].chart_color = '#ffd000';
                                break;
                            case 'SANBE3':
                                res2[n].chart_color = '#f54758';
                                break;

                            default:
                                res2[n].chart_color = '#fa7a43';
                                break;
                        }
                        color_chart2.push(res2[n].chart_color);
                    }
                }

                colorSet2.list = color_chart2.map(function(color) {
                    return new am4core.color(color);
                });
                hideIndicator_2();
                document.getElementById('legend-by-unit').style.display = "block";
            }
        });

        // Percentage by EXPORT / IMPORT
        var colorSet3 = new am4core.ColorSet();
        var color_chart3 = new Array();
        // $.ajax({
        //     url: "<?php //echo site_url('Nie/percentageByTrans') 
                        ?>",
        //     type: 'GET',
        //     dataType: 'JSON',
        //     success: function(res3) {
        //         res3[0].pulled = true;
        //         chart3.data = res3;

        //         if (res3.length > 0) {
        //             for (var r = 0; r < res3.length; r++) {
        //                 if (res3[r].status == 'EXPORT') {
        //                     res3[r].chart_color = '#ff5100';
        //                 } else {
        //                     res3[r].chart_color = '#07bfe8';
        //                 }

        //                 color_chart3.push(res3[r].chart_color);
        //             }
        //         }

        //         colorSet3.list = color_chart3.map(function(color) {
        //             return new am4core.color(color);
        //         });
        //     }
        // });
        $.ajax({
            url: "<?php echo site_url('Nie/percentageByProductType') ?>",
            type: 'GET',
            dataType: 'JSON',
            success: function(res3) {
                res3[0].pulled = true;
                chart3.data = res3;

                if (res3.length > 0) {
                    for (var r = 0; r < res3.length; r++) {
                        switch (res3[r].product_type_code) {
                            case 'ETHICAL':
                                res3[r].chart_color = '#35978f';
                                break;
                            case 'SUPLEMEN':
                                res3[r].chart_color = '#017351';
                                break;
                            case 'GENERIK':
                                res3[r].chart_color = '#03c383';
                                break;
                            case 'ALKES':
                                res3[r].chart_color = '#aad962';
                                break;
                            case 'PANGAN':
                                res3[r].chart_color = '#fbbf45';
                                break;
                            case 'PKRT':
                                res3[r].chart_color = '#ef6a32';
                                break;
                            case 'OT':
                                res3[r].chart_color = '#ed0345';
                                break;
                            case 'PB':
                                res3[r].chart_color = '#a12a5e';
                                break;

                            default:
                                res3[r].chart_color = '#710162';
                                break;
                        }
                        color_chart3.push(res3[r].chart_color);
                    }
                }

                colorSet3.list = color_chart3.map(function(color) {
                    return new am4core.color(color);
                });
                hideIndicator_3();
                document.getElementById('legend-by-product-type').style.display = "block";
            }
        });

        var title1 = chart1.titles.create();
        title1.text = "Percentage NIE by Alert";
        title1.fontSize = 15;
        title1.marginBottom = 25;

        var series1 = chart1.series.push(new am4charts.PieSeries3D());
        series1.dataFields.value = "jumlah";
        series1.dataFields.category = "alert_color";
        series1.colors = colorSet1;
        series1.alignLabels = false;
        series1.labels.template.radius = am4core.percent(-30);
        series1.ticks.template.disabled = true;
        series1.labels.template.fill = am4core.color("black");
        series1.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        series1.labels.template.adapter.add("radius", function(radius, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 5)) {
                return 0;
            }
            return radius;
        });

        series1.labels.template.adapter.add("fill", function(color, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                return am4core.color("#000");
            }
            return color;
        });
        series1.slices.template.propertyFields.isActive = "pulled";
        series1.slices.template.events.on("hit", expandByAlert, this);

        series1.ticks.template.adapter.add("hidden", hideSmall);
        series1.labels.template.adapter.add("hidden", hideSmall);

        series1.slices.template.events.on("hit", function(ev) {
            let series = ev.target.dataItem.component;
            series.slices.each(function(item) {
                if (item.isActive && item != ev.target) {
                    item.isActive = false;
                }
            })
        });

        //--------------------------------------------------------------------------------------------------
        var title2 = chart2.titles.create();
        title2.text = "Percentage NIE by Unit";
        title2.fontSize = 15;
        title2.marginBottom = 25;

        var series2 = chart2.series.push(new am4charts.PieSeries3D());
        series2.dataFields.value = "cnt";
        series2.dataFields.category = "planning_area";
        series2.colors = colorSet2;
        series2.alignLabels = false;
        series2.labels.template.radius = am4core.percent(-30);
        series2.ticks.template.disabled = true;
        series2.labels.template.fill = am4core.color("black");
        series2.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        series2.labels.template.adapter.add("radius", function(radius, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                return 0;
            }
            return radius;
        });

        series2.labels.template.adapter.add("fill", function(color, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                return am4core.color("#000");
            }
            return color;
        });
        series2.slices.template.propertyFields.isActive = "pulled";
        series2.slices.template.events.on("hit", expandByAlert, this);

        series2.ticks.template.adapter.add("hidden", hideSmall);
        series2.labels.template.adapter.add("hidden", hideSmall);

        series2.slices.template.events.on("hit", function(ev) {
            let series = ev.target.dataItem.component;
            series.slices.each(function(item) {
                if (item.isActive && item != ev.target) {
                    item.isActive = false;
                }
            })
        });


        //--------------------------------------------------------------------------------------------------
        var title3 = chart3.titles.create();
        // title3.text = "Percentage NIE by Export/Import";
        title3.text = "Percentage NIE by Product Type";
        title3.fontSize = 15;
        title3.marginBottom = 25;

        var series3 = chart3.series.push(new am4charts.PieSeries3D());
        // series3.dataFields.value = "cnt";
        // series3.dataFields.category = "status";
        series3.dataFields.value = "count";
        series3.dataFields.category = "product_type_desc";
        series3.colors = colorSet3;
        series3.alignLabels = false;
        series3.labels.template.radius = am4core.percent(-30);
        series3.ticks.template.disabled = true;
        series3.labels.template.fill = am4core.color("black");
        series3.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        series3.labels.template.adapter.add("radius", function(radius, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 5)) {
                return 0;
            }
            return radius;
        });

        series3.labels.template.adapter.add("fill", function(color, target) {
            if (target.dataItem && (target.dataItem.values.value.percent < 10)) {
                return am4core.color("#000");
            }
            return color;
        });

        series3.ticks.template.adapter.add("hidden", hideSmall);
        series3.labels.template.adapter.add("hidden", hideSmall);

        function hideSmall(hidden, target) {
            return target.dataItem.values.value.percent < 2 ? true : false;
        }

        series3.slices.template.propertyFields.isActive = "pulled";
        series3.slices.template.events.on("hit", expandByAlert, this);

        series3.slices.template.events.on("hit", function(ev) {
            let series = ev.target.dataItem.component;
            series.slices.each(function(item) {
                if (item.isActive && item != ev.target) {
                    item.isActive = false;
                }
            })
        });

    }); // end am4core.ready()
</script>