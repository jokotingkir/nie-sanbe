<script type="text/javascript">
    $(document).ready(function() {
        // Set nilai default agar user tidak bingung saat mengisi
        $('#niepblsh').val('BPOM');
        $('#niehldr').val('SANBE');
        // End of set nilai default agar user tidak bingung saat mengisi

        $('#carimodal').click(function() {
            $.ajax({
                url: "<?php echo base_url(); ?>nie/cariDataProduct/",
                method: "POST",
                data: {
                    keyword: $('#keyword').val()
                },
                dataType: 'json',
                success: function(data) {
                    // console.log(data);
                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        var country = data[i].con_desc;
                        if (country == null || country == "")
                            country = "-";

                        html += "<tr onclick='getProductModal(" + JSON.stringify(data[i]) + ")'>";
                        html += '<td>' + data[i].PRODUCT_CODE + '</td>';
                        html += '<td>' + data[i].PRODUCT_DESC + '</td>';
                        html += '<td>' + data[i].PRODUCT_OWNER + '</td>';
                        html += '<td>' + data[i].PLANNING_AREA + '</td>';
                        html += '<td>' + data[i].EXPIRE_DAYS + '</td>';
                        html += '<td>' + data[i].PG4_DESC + '</td>';
                        html += '<td>' + data[i].PG7_DESC + '</td>';
                        html += '<td>' + country + '</td>';
                        html += '</tr>';
                    }
                    $('#tabelmodal').html(html);
                }
            });
        });

        $('#prodname').autocomplete({
            serviceUrl: "<?php echo site_url('Nie/AutocompleteProduct') ?>",
            transformResult: function(res) {
                return {
                    suggestions: $.map(res, function(item) {
                        return {
                            value: item.PRODUCT_CODE,
                            data: item.PRODUCT_DESC
                        };
                    })
                };
            },
            onSelect: function(suggestion) {
                console.log(suggestion.value);
            }
        });

        var dropdownProductType = $('#prodtype');
        dropdownProductType[0].selectedIndex = 0;
        var product_type = document.getElementById("prodtype");
        var x = document.getElementById("exportcard");
        if (product_type.value == "1") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    });

    function getProductModal(arr) {
        var country = arr.con_desc;
        if (country == null || country == "")
            country = "-";

        $('#prodcode').val(arr.PRODUCT_CODE);
        $('#prodname').val(arr.PRODUCT_DESC);
        $('#prodown').val(arr.PRODUCT_OWNER);
        $('#prodplan').val(arr.PLANNING_AREA);
        
        //NIE Holder value
        $('#niehldr').val(arr.PLANNING_AREA);

        $('#prodform').val(arr.PG4_DESC);
        $('#prodpckg').val(arr.PG7_DESC);
        $('#prodlife').val(arr.EXPIRE_DAYS);
        $('#prodctry').val(country);
        $('#myModal').modal('hide');
    }

    function ptodtypefunc() {
        var e = document.getElementById("prodtype");
        var tipe = e.options[e.selectedIndex].value;
        //alert(tipe);
        var x = document.getElementById("exportcard");
        if (tipe == "1") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>

<div class="container">
    <form name="nie_update_form" action="<?php echo base_url(); ?>Nie/tambah" method="POST" enctype="multipart/form-data">
        <div class="row mt-4">
            <div class="col">
                <p class="small"><i>Bagi yang bertanda <span class="required">*</span> wajib diisi</i></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?php if ($this->session->flashdata('message')) : ?>
                    <?php echo $this->session->flashdata('message'); ?>
                <?php endif; ?>
                <div class="card">
                    <div class="card-header">
                        Add New NIE
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="prodname">Product Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="prodname" name="prodname" value="<?php echo set_value('prodname'); ?>" readonly />
                                <div class="input-group-append">
                                    <button type="button" class='btn btn-primary' data-toggle='modal' data-target='#myModal'>Choose Product</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodcode">Product Code</label>
                                <input type="text" class="form-control" id="prodcode" name="prodcode" value="<?php echo set_value('prodcode'); ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodtype">Product Type</label>
                                <select class="form-control" id="prodtype" name="prodtype" value="<?php echo set_value('prodtype'); ?>" onchange="ptodtypefunc();">
                                    <?php foreach ($typelist as $type) : ?>
                                        <option value='<?php echo $type['id']; ?>'><?php echo $type['product_type_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodown">Product Owner</label>
                                <input type="text" class="form-control" id="prodown" name="prodown" value="<?php echo set_value('prodown'); ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodplan">Planning Area</label>
                                <input type="text" class="form-control" id="prodplan" name="prodplan" value="<?php echo set_value('prodplan'); ?>" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodform">Finished Dosage Form</label>
                                <input type="text" class="form-control" id="prodform" name="prodform" value="<?php echo set_value('prodform'); ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodpckg">Packaging Presentation</label>
                                <input type="text" class="form-control" id="prodpckg" name="prodpckg" value="<?php echo set_value('prodpckg'); ?>" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodlife">Shelf Life</label>
                                <input type="text" class="form-control" id="prodlife" name="prodlife" value="<?php echo set_value('prodlife'); ?>" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="prodscon">Storage Condition</label>
                                <select class="form-control" id="prodscon" name="prodscon">
                                    <?php foreach ($conlist as $con) : ?>
                                        <option value='<?php echo $con['id']; ?>' <?php echo set_select('prodscon', $con['id'], (!empty($prodscon) && $prodscon == $con['id']) ? TRUE : FALSE); ?>><?php echo $con['storage_condition_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="prodnie">NIE Number<span class="required">*</span></label>
                                <input type="text" class="form-control" id="prodnie" name="prodnie" onkeyup="isEnableSubmit()" value="<?php echo set_value('prodnie'); ?>" />
                                <?php echo form_error('prodnie'); ?>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niestat">NIE Status</label>
                                <select class="form-control" id="niestat" name="niestat">
                                    <?php foreach ($statlist as $stat) : ?>
                                        <option value='<?php echo $stat['stat_desc']; ?>' <?php echo set_select('niestat', $stat['stat_desc'], (!empty($niestat) && $niestat == $stat['stat_desc']) ? TRUE : FALSE); ?>><?php echo $stat['stat_desc']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niestartdate">NIE Start Date</label>
                                <input type="date" class="form-control" id="niestartdate" name="niestartdate" value="<?php echo set_value('niestartdate'); ?>" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nieenddate">NIE End Date</label>
                                <input type="date" class="form-control" id="nieenddate" name="nieenddate" value="<?php echo set_value('nieenddate'); ?>" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="niehldr">NIE Holder</label>
                                <input type="text" class="form-control" id="niehldr" name="niehldr" value="<?php echo set_value('niehldr'); ?>" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="niepblsh">NIE Publisher</label>
                                <input type="text" class="form-control" id="niepblsh" name="niepblsh" value="<?php echo set_value('niepblsh'); ?>" />
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <button class="btn btn-primary" aria-describedby="compotition_help" id="add_compotition" type="button">Add Compotition</button>
                                <small id="compotition_help" class="text-muted">
                                    <i>Click to add <b>compotition</b></i>
                                </small>
                            </div>
                        </div>

                        <div id="compotition-col" class="input-fields-wrap">
                            <div class="form-row align-items-center parent-compotition">
                                <div class="form-group col-md-1">
                                    <center><label class="count"></label></center>
                                </div>
                                <input type="hidden" name="item_code[]" id="item_1" />
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_1" />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_1" />
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_1" />
                                </div>
                                <!-- <div class="form-group col-md-1">
                                    <a href="#" class="remove_compotition"><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-header">
                    &nbsp;
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="upload_cert">NIE Certificate</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="nie_cert" id="upload_cert" value="<?php echo set_value('nie_cert'); ?>" />
                            <label class="custom-file-label" for="upload_cert">Pilih File</label>
                        </div>
                        <small style="color: red; display: none;" id="upload_error"></small>
                        <?php if (isset($upload_error) && $upload_error != '') : ?>
                            <small style="color: red;"><i><?php echo $upload_error ?></i></small>
                        <?php endif; ?>
                    </div>

                    <!-- Start Tipe EXPORT -->
                    <div id="exportcard" style="display:none" class="mt-3">
                        <div class="form-group">
                            <label for="prodbrand">Brand Name</label>
                            <input type="text" class="form-control" id="prodbrand" name="prodbrand" value="<?php echo set_value('prodbrand'); ?>" />
                        </div>
                        <div class="form-group">
                            <label for="prodctry">Country</label>
                            <input type="text" class="form-control" id="prodctry" name="prodctry" value="<?php echo set_value('prodctry'); ?>" readonly />
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="niercvdate">Receive Date</label>
                                <input type="date" class="form-control" id="niercvdate" name="niercvdate" value="<?php echo set_value('niercvdate'); ?>" />
                            </div>
                            <div class="col-md-6">
                                <label for="niedspdate">Dispatch Date</label>
                                <input type="date" class="form-control" id="niedspdate" name="niedspdate" value="<?php echo set_value('niedspdate'); ?>" />
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <label for="prodagnt">Agent</label>
                            <select class="form-control" id="prodagnt" name="prodagnt">
                                <?php foreach ($agent as $v_agent) : ?>
                                    <option value="<?php echo $v_agent['VENDOR_NAME']; ?>" <?php echo set_select('prodagnt', $v_agent['VENDOR_NAME'], (!empty($prodagnt) && $prodagnt == $v_agent['VENDOR_NAME']) ? TRUE : FALSE); ?>><?php echo $v_agent['VENDOR_NAME']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="prodcrtf">Certificate</label>
                            <select class="form-control" id="prodcrtf" name="prodcrtf">
                                <?php foreach ($certifilist as $crtf) : ?>
                                    <option value='<?php echo $crtf['id']; ?>' <?php echo set_select('prodcrtf', $crtf['id'], (!empty($prodcrtf) && $prodcrtf == $crtf['id']) ? TRUE : FALSE); ?>><?php echo $crtf['certi_desc']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="prodrglt">Regulatory</label>
                            <select class="form-control" id="prodrglt" name="prodrglt">
                                <?php foreach ($regulatolist as $regulate) : ?>
                                    <option value='<?php echo $regulate['regulatory_code']; ?>' <?php echo set_select('prodrglt', $regulate['regulatory_code'], (!empty($prodrglt) && $prodrglt == $regulate['regulatory_code']) ? TRUE : FALSE); ?>><?php echo $regulate['regulatory_desc']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <!-- End of Tipe EXPORT -->
                    <div class="form-group">
                        <label for="pic">PIC</label>
                        <input type="text" class="form-control" name="pic" id="pic" value="<?php echo set_value('pic'); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="remark">Remark</label>
                        <textarea class="form-control" id="remark" name="remark" rows="3"><?php echo set_value('remark'); ?></textarea>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-success btn-block float-right" id="submit_btn" name="submit_nie" value="submit" disabled>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Modal produk-->
<div id="myModal" class="modal fade" role="dialog">
    <!-- <div class="modal-dialog modal-xl" style="width:90%;font-size:10px;max-height:60vh"> -->
    <div class="modal-dialog modal-lg" tabindex="-1">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product List</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="post">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Cari Produk..." name="keyword" id='keyword'>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" id='carimodal'>Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" id='container' style="height:50vh;max-height:50vh;overflow-y:scroll">
                        <?php if (empty($prodlist)) : ?>
                            <div class="alert alert-danger" role='alert'>
                                Data Tidak Ditemukan
                            </div>
                        <?php endif; ?>
                        <table class="table table-condensed table-striped search_product">
                            <thead>
                                <tr>
                                    <th scope='col' nowrap>PRODUCT CODE</th>
                                    <th scope='col' nowrap>PRODUCT NAME</th>
                                    <th scope='col' nowrap>PRODUCT OWNER</th>
                                    <th scope='col' nowrap>PLANNING AREA</th>
                                    <th scope='col' nowrap>EXPIRE DAYS</th>
                                    <th scope='col' nowrap>FINISHED DOSAGE FORM</th>
                                    <th scope='col' nowrap>PACKAGING PRESENTATION</th>
                                    <th scope='col' nowrap>COUNTRY</th>
                                </tr>
                            </thead>
                            <tbody id='tabelmodal'>
                                <?php for ($n = 0; $n < count($prodlist); $n++) : ?>
                                    <tr onclick='getProductModal(<?php echo json_encode($prodlist[$n]); ?>)'>
                                        <td scope="row"><?php echo $prodlist[$n]['PRODUCT_CODE']; ?></td>
                                        <td><?php echo $prodlist[$n]['PRODUCT_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['PRODUCT_OWNER']; ?></td>
                                        <td><?php echo $prodlist[$n]['PLANNING_AREA']; ?></td>
                                        <td><?php echo $prodlist[$n]['EXPIRE_DAYS']; ?></td>
                                        <td><?php echo $prodlist[$n]['PG4_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['PG7_DESC']; ?></td>
                                        <td><?php echo $prodlist[$n]['con_desc']; ?></td>
                                    </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var max_fields = 11; //maksimum kolom isian untuk compotition
        var wrapper = $('.input-fields-wrap'); //fields wrapper
        var add_button = $('#add_compotition'); //tombol tambah compotition

        isEnableSubmit();
        // var is_hidden = document.getElementById('compotition-col');

        //Validasi ekstensi file sertifikat hanya boleh .pdf
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }

        //Validasi ekstensi file sertifikat maksimal size yaitu 10 MB
        function ValidateSize(file) {
            var FileSize = file.size / 1024 / 1024; // in MB
            if (FileSize > 10) {
                return false;
            } else {
                return true;
            }
        }

        var x = 2; //mulai dari 2 karena 1 sudah jadi default
        $(add_button).click(function(e) {
            e.preventDefault();

            if (x < max_fields) {
                var html = '';
                html += '<div class="form-row align-items-center parent-compotition">';
                html += '<div class="form-group col-md-1"><center><label class="count"></label></center></div><input type="hidden" name="item_code[]" id="item_' + x + '" />';
                html += '<div class="form-group col-md-4"><input type="text" class="form-control autocomplete_compotition" name="compotition_name[]" placeholder="Compotition Name" id="compotition_' + x + '" /></div>';
                html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_strength[]" placeholder="Strength" id="strength_' + x + '" /></div>';
                html += '<div class="form-group col-md-3"><input type="text" class="form-control" name="compotition_uom[]" placeholder="UOM" id="uom_' + x + '" /></div>';
                html += '<div class="form-group col-md-1"><a href="#" class="remove_compotition"><i class="fa fa-minus-square" aria-hidden="true" style="color: red;"></i></a></div></div>';

                $(wrapper).append(html);

                x++;
            }
        });

        $(wrapper).on('click', '.remove_compotition', function(e) {
            e.preventDefault();
            $(this).parents('.parent-compotition').remove();
            x--;
        });

        $(document).on('keydown', '.autocomplete_compotition', function() {
            var id = this.id;
            var split_id = id.split('_');
            var index = split_id[1];

            $('#' + id).autocomplete({
                // delay: 200,
                // minLength: 0,
                source: "<?php echo site_url('Nie/getItemsDesc'); ?>",
                select: function(event, ui) {
                    //set value item_code to input hidden
                    document.getElementById('item_' + index).value = ui.item.item_code;
                }
            });
        });

        $('#upload_cert').on('change', function() {
            var upload_error = document.getElementById('upload_error');
            var fileName = "Pilih File";
            if (!hasExtension('upload_cert', ['.pdf'])) {
                upload_error.style.display = "block";
                upload_error.innerHTML = '<i>hanya mendukung file yang berekstensi .pdf</i>';
                $(this).val(null);
            } else {
                var certificate = document.getElementById('upload_cert').files[0];
                if (!ValidateSize(certificate)) {
                    upload_error.style.display = "block";
                    upload_error.innerHTML = '<i>Ukuran maksimal yang diperbolehkan yaitu 10MB</i>';
                    $(this).val(null);
                } else {
                    upload_error.style.display = "none";
                    fileName = $(this).val().split('\\').pop(); //untuk mengambil file name
                }
            }
            $(this).next('.custom-file-label').html(fileName);
        });

        function isEnableSubmit() {
            var nie_number = document.getElementById('prodnie').value;
            if (nie_number != '') {
                document.getElementById('submit_btn').disabled = false;
            } else {
                document.getElementById('submit_btn').disabled = true;
            }
        }

        // $('input[type=file]').on('change',function(){
        //     let file = document.getElementById('upload_cert').files;
        //     console.log(file);
        // });
    </script>