<div class="container">
    <div class="row mt-3 left-content-md-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    NIE DETAIL
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body" style="background-color: #d7eaf7;">
                            <?php if (!isset($nie_no)) : ?>
                                <div class="alert alert-danger" role='alert'>
                                    Data Tidak Ditemukan
                                </div>
                            <?php else : ?>
                                <!-- Kalau mau expand satu collapse satu tinggal tambahin aja atribut data-parent = "#accordion" di div sebelum card-body -->
                                <div id="accordion">
                                    <div class="card">
                                        <!-- #007bff biru primary button -->
                                        <div class="card-header" id="product_detail" style="background-color: #eaeef2;">
                                            <h6 class="mb-0">
                                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#productDetail" aria-expanded="true" aria-controls="productDetail" style="color: black;">
                                                    Product Detail
                                                    <i class="fa fa-chevron-right float-right" aria-hidden="true"></i>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="productDetail" class="collapse show" aria-labelledby="product_detail" data-parent="#accordion">
                                            <div class="card-body" style="background-color: #f7f7f7;">
                                                <div class="row">
                                                    <div class="col-auto">&nbsp;</div>
                                                    <div class="col-10">
                                                        <table class="nie-detail" border="0">
                                                            <tr>
                                                                <th>Product Code</th>
                                                                <td width="3%">:</td>
                                                                <th>
                                                                    <div style="font-size:10pt;"><?php echo $product_code; ?></div>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th>Product Name</th>
                                                                <td>:</td>
                                                                <th>
                                                                    <div style="font-size:10pt;"><?php echo $product_name; ?></div>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th>Product Type</th>
                                                                <td>:</td>
                                                                <td>
                                                                    <?php
                                                                    if (count($typelist) > 0) {
                                                                        foreach ($typelist as $row) {
                                                                            if ($row['id'] == $product_type_id) {
                                                                                $product_type = $row['product_type_desc'];
                                                                            }
                                                                        }

                                                                        echo $product_type;
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Product Owner</th>
                                                                <td>:</td>
                                                                <td><?php ($product_owner == "") ? $owner = "-" : $owner = $product_owner;
                                                                    echo $owner; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Planning Area</th>
                                                                <td>:</td>
                                                                <td><?php echo $planning_area; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Finished Dosage Form</th>
                                                                <td>:</td>
                                                                <td><?php echo $fd_form; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Packaging Presentation</th>
                                                                <td>:</td>
                                                                <td><?php echo $packaging_presentation; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>Shelf Life</th>
                                                                <td>:</td>
                                                                <td><?php ($shelf_life == "") ? $shelf_life = "-" : $shelf_life = $shelf_life;
                                                                    echo $shelf_life;  ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th valign="top">Storage Condition</th>
                                                                <td valign="top">:</td>
                                                                <td>
                                                                    <?php
                                                                    if (count($conlist) > 0) {
                                                                        foreach ($conlist as $row) {
                                                                            if ($row['id'] == $storage_condition_id) {
                                                                                $storage_condition = $row['storage_condition_desc'];
                                                                            } else {
                                                                                $storage_condition = "-";
                                                                            }
                                                                        }
                                                                        echo wordwrap($storage_condition, 50, '<br/>', true);
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th valign="top">Compotition</td>
                                                                <td valign="top">:</td>
                                                                <td>
                                                                    <?php
                                                                    if (is_array($compotition)) {
                                                                        if (count($compotition) > 0) {
                                                                            $fragment = "";
                                                                            for ($i = 0; $i < count($compotition); $i++) {
                                                                                $fragment = $fragment . ($i + 1) . ". " . $compotition[$i]['item_desc'] . "&#13;&#10;";
                                                                            }
                                                                            echo "<textarea class='form-control' disabled style='font-size: 13px; background-color: #f7f7f7;' rows='5'>" . $fragment . "</textarea>";
                                                                        } else {
                                                                            echo "-";
                                                                        }
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <!-- #007bff biru primary button -->
                                        <div class="card-header" id="current_product" style="background-color: #eaeef2;">
                                            <h6 class="mb-0">
                                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#currentProduct" aria-expanded="false" aria-controls="currentProduct" style="color: black;">
                                                    Current NIE
                                                    <i class="fa fa-chevron-right float-right" aria-hidden="true"></i>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="currentProduct" class="collapse" aria-labelledby="current_product">
                                            <div class="card-body" style="background-color: #f7f7f7;">
                                                <div class="row">
                                                    <div class="col-auto">&nbsp;</div>
                                                    <div class="col-10">
                                                        <?php if ($nie_status != "") : ?>
                                                            <?php
                                                            ?>
                                                            <!-- <div class="alert alert-primary" role="alert">
                                                                Produk <b><?php echo $product_name; ?></b> sedang berada dalam status <b>Registrasi NIE</b>
                                                            </div> -->
                                                            <?php
                                                            ?>
                                                            <!-- <h6><?php echo $product_code . " - " . $product_name; ?></h6> -->
                                                            <table class="nie-detail" border="0">
                                                                <tr>
                                                                    <th>Product Code</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $product_code; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Product Name</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $product_name; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Number</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $nie_no; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Status</th>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <?php
                                                                        // if (count($statlist) > 0) {
                                                                        //     $stat_desc = "-";
                                                                        //     foreach ($statlist as $row) {
                                                                        //         if (strtoupper($row['stat_desc']) == strtoupper($nie_status)) {
                                                                        //             $stat_desc = $row['stat_desc'];
                                                                        //         }
                                                                        //     }
                                                                        //     echo $stat_desc;
                                                                        // }
                                                                        echo $nie_status;
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Start Date</th>
                                                                    <td>:</td>
                                                                    <td><?php echo date("d-m-Y", strtotime($nie_start_date)); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE End Date</th>
                                                                    <td>:</td>
                                                                    <td><?php echo date("d-m-Y", strtotime($nie_end_date)); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Holder</th>
                                                                    <td>:</td>
                                                                    <td><?php echo $nie_holder; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Publisher</th>
                                                                    <td>:</td>
                                                                    <td><?php ($nie_publisher == "") ? $nie_publisher = "-" : $nie_publisher = $nie_publisher;
                                                                        echo $nie_publisher; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Manufacture Address</th>
                                                                    <td>:</td>
                                                                    <td><?php echo $manufact_addr; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Remark</th>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <?php
                                                                        if ($remark != "") {
                                                                            echo wordwrap($remark, 50, '<br/>', true);
                                                                        } else {
                                                                            echo "-";
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Current NIE Status</th>
                                                                    <td>:</td>
                                                                    
                                                                    <td>
                                                                        <?php if($alert == 'GRAY') : ?>    
                                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="color: #bfc0c1; font-size: 16px;"></i>&nbsp; Alert System [<span style="color: red;">X</span>] & Registration Preparation
                                                                        <?php else : ?>
                                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="color: green; font-size: 16px;"></i>&nbsp; Alert System [<i class="fa fa-circle" aria-hidden="true" style="color: <?php echo $alert; ?>; font-size: 12px;"></i>] & Registration Preparation
                                                                        <?php endif;?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <?php
                                                                        $color = "#bfc0c1";
                                                                        if ($submit_status == "submitted") {
                                                                            $color = "green";
                                                                        }
                                                                        ?>
                                                                        <i class="fa fa-check-square-o" aria-hidden="true" style="color: <?php echo $color; ?>; font-size: 16px;"></i>&nbsp; Document collection from (QA, R&D)
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                    <?php
                                                                        $color = "#bfc0c1";
                                                                        if ($bpom_submit_status == "submitted") {
                                                                            $color = "green";
                                                                        }
                                                                        ?>
                                                                        <i class="fa fa-check-square-o" aria-hidden="true" style="color: <?php echo $color; ?>; font-size: 16px;"></i>&nbsp; BPOM Submition
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Certificate</th>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <?php
                                                                        $pointer = $nie_certificate . ".pdf";
                                                                        $filename = str_replace("./uploads/Certificates/", "", $nie_certificate);
                                                                        if (file_exists($pointer)) :
                                                                            ?>
                                                                            <a href="#" onClick="openFile('<?php echo $pointer; ?>')" data-toggle="tooltip" data-placement="bottom" title="Click here"><i class="fa fa-file-pdf-o" aria-hidden="true" style="color: red;"></i>&nbsp;<?php echo $filename ?>.pdf</a>
                                                                        <?php else : ?>
                                                                            -
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <span>&nbsp;</span>
                                                                    </th>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                            <?php
                                                            ?>
                                                        <?php else : ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                Status untuk Produk <b><?php echo $product_name; ?></b> tidak ditemukan, perbarui di <a href="<?php echo base_url() ?>Nie/edit_item/<?php echo $nie_id; ?>">sini</a>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <!-- #007bff biru primary button -->
                                        <div class="card-header" id="product_history" style="background-color: #eaeef2;">
                                            <h6 class="mb-0">
                                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#productHistory" aria-expanded="false" aria-controls="productHistory" style="color: black;">
                                                    NIE History
                                                    <i class="fa fa-chevron-right float-right" aria-hidden="true"></i>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="productHistory" class="collapse" aria-labelledby="product_history">
                                            <div class="card-body" style="background-color: #f7f7f7;">
                                                <div class="row">
                                                    <div class="col-auto">&nbsp;</div>
                                                    <div class="col scroll">
                                                        <?php if (count($history) > 0) : ?>
                                                            <?php foreach ($history as $row) : ?>
                                                                <div class="card border-light" style="max-width: 48rem;">
                                                                    <div class="card-body" style="background-color: #f7f7f7;">
                                                                        <table class="nie-detail">
                                                                            <tr>
                                                                                <td>Product Code</td>
                                                                                <td>:</td>
                                                                                <td><span style="font-size: 14px; font-weight: 600;"><?php echo $product_code; ?></span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Product Name</td>
                                                                                <td>:</td>
                                                                                <td><span style="font-size: 14px; font-weight: 600;"><?php echo $product_name; ?></span></td>
                                                                            </tr>
                                                                        </table>
                                                                        <table class="nie-detail" border="0">
                                                                            <tr>
                                                                                <td rowspan="4" width="10%">&nbsp;</td>
                                                                                <td>NIE Number</td>
                                                                                <td width="3%">:</td>
                                                                                <td><?php echo $row['nie_no']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>NIE Start Date</td>
                                                                                <td>:</td>
                                                                                <td><?php echo date("d-m-Y", strtotime($row['nie_start_date'])); ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>NIE End Date</td>
                                                                                <td>:</td>
                                                                                <td><?php echo date("d-m-Y", strtotime($row['nie_end_date'])); ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>NIE Status</td>
                                                                                <td>:</td>
                                                                                <td><?php echo $row['alert_status']; ?></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        <?php else : ?>
                                                            <div class="alert alert-primary" role="alert">
                                                                Data history tidak ditemukan untuk NIE dengan Nama Produk <b><?php echo $product_name; ?></b>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <!-- #007bff biru primary button -->
                                        <div class="card-header" id="renewal" style="background-color: #eaeef2;">
                                            <h6 class="mb-0">
                                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#renewalNie" aria-expanded="false" aria-controls="renewalNie" style="color: black;">
                                                    NIE Submit Process
                                                    <i class="fa fa-chevron-right float-right" aria-hidden="true"></i>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="renewalNie" class="collapse" aria-labelledby="renewal">
                                            <div class="card-body" style="background-color: #f7f7f7;">
                                                <div class="row">
                                                    <div class="col-auto">&nbsp;</div>
                                                    <div class="col-10">
                                                        <?php if ($nie_status != "") : ?>
                                                            <?php
                                                            ?>
                                                            <!-- <div class="alert alert-primary" role="alert">
                                                                Produk <b><?php echo $product_name; ?></b> sedang berada dalam status <b>Registrasi NIE</b>
                                                            </div> -->
                                                            <?php
                                                            ?>
                                                            <!-- <h6><?php echo $product_code . " - " . $product_name; ?></h6> -->
                                                            <table class="nie-detail">
                                                                <tr>
                                                                    <th>Product Code</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $product_code; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Product Name</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $product_name; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Number</th>
                                                                    <td width="3%">:</td>
                                                                    <th>
                                                                        <div style="font-size:10pt;"><?php echo $nie_no; ?></div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE Start Date</th>
                                                                    <td>:</td>
                                                                    <td><?php echo date("d-m-Y", strtotime($nie_start_date)); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>NIE End Date</th>
                                                                    <td>:</td>
                                                                    <td><?php echo date("d-m-Y", strtotime($nie_end_date)); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Alert Status</th>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <?php
                                                                        switch ($alert) {
                                                                            case "YELLOW":
                                                                                $notes = "Status submit NIE Hampir Mendekati";
                                                                                break;
                                                                            case "RED":
                                                                                $notes = "Status submit NIE Sangat Mendekati";
                                                                                break;
                                                                            case "BLUE":
                                                                                $notes = "Status NIE Sudah Expired dan Sudah Submit";
                                                                                break;
                                                                            case "BROWN":
                                                                                $notes = "Status NIE Sudah Expired dan Belum Submit";
                                                                                break;
                                                                            case "PINK":
                                                                                $notes = "Status NIE Belum Expired dan Sudah Submit";
                                                                                break;
                                                                            case "GRAY":
                                                                                $notes = "NIE Belum Memasuki Alert System";
                                                                                break;

                                                                            default:
                                                                                $notes = "Status submit NIE Aman";
                                                                                break;
                                                                        }

                                                                        echo $notes . '&nbsp;[<i class="fa fa-circle" aria-hidden="true" style="color:' . $alert . '; font-size: 12px;"></i>]';
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <span>&nbsp;</span>
                                                                    </th>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                            <?php if(count($submissions) > 0) :?>
                                                                <h6>Document Collection Process</h6>
                                                                <?php if($submit_status == "not submitted") :?>
                                                                    <tr>
                                                                        <td width="10%">&nbsp;</td>
                                                                        <td>Submission Process Status</td>
                                                                        <td>:</td>
                                                                        <td><span class="badge badge-primary">On Process</span>&nbsp; (klik <a href="<?php echo base_url() ?>submission/<?php echo $nie_id ?>">di sini</a> untuk melihat)</td>
                                                                    </tr>
                                                                <?php else : ?>
                                                                    <tr>
                                                                        <td width="10%">&nbsp;</td>
                                                                        <td>Submission Process Status</td>
                                                                        <td>:</td>
                                                                        <td><span class="badge badge-success">Done</span>&nbsp; (klik <a href="<?php echo base_url() ?>submission/<?php echo $nie_id ?>">di sini</a> untuk melihat)</td>
                                                                    </tr>
                                                                <?php endif; ?>
                                                            <?php endif;?>

                                                        <?php else : ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                Status untuk Produk <b><?php echo $product_name; ?></b> tidak ditemukan, perbarui di <a href="<?php echo base_url() ?>Nie/edit_item/<?php echo $nie_id; ?>">sini</a>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="card">
                                        <div class="card-header" id="new_product" style="background-color: #eaeef2;">
                                            <h6 class="mb-0">
                                                <a href="#" class="collapsed" data-toggle="collapse" data-target="#newProduct" aria-expanded="false" aria-controls="newProduct" style="color: black;">
                                                    New Product/NIE Registration
                                                    <i class="fa fa-chevron-right float-right" aria-hidden="true"></i>
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="newProduct" class="collapse" aria-labelledby="new_product">
                                            <div class="card-body" style="background-color: #f7f7f7;">
                                                <div class="row">
                                                    <div class="col-auto">&nbsp;</div>
                                                    <div class="col-10">
                                                        <?php if ($nie_status != "") : ?>
                                                            <?php if (strpos(strtolower($nie_status), "reg") !== false || strpos(strtolower($nie_status), "renewal") !== false) : ?>
                                                                <table class="nie-detail">
                                                                    <tr>
                                                                        <td>Product Code</td>
                                                                        <td>:</td>
                                                                        <td><span style="font-size: 14px; font-weight: 600;"><?php echo $product_code; ?></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Product Name</td>
                                                                        <td>:</td>
                                                                        <td><span style="font-size: 14px; font-weight: 600;"><?php echo $product_name; ?></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>NIE Number</td>
                                                                        <td>:</td>
                                                                        <td><i><?php echo $nie_no; ?></i></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Start Date</td>
                                                                        <td>:</td>
                                                                        <td><?php echo date("d-m-Y", strtotime($nie_start_date)); ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>End Date</td>
                                                                        <td>:</td>
                                                                        <td><?php echo date("d-m-Y", strtotime($nie_end_date)); ?></td>
                                                                    </tr>
                                                                </table>
                                                            <?php else : ?>
                                                                <h6><?php echo $product_code . " - " . $product_name; ?></h6>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                Status untuk Produk <b><?php echo $product_name; ?></b> tidak ditemukan, perbarui di <a href="<?php echo base_url() ?>Nie/edit_item/<?php echo $nie_id; ?>">sini</a>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function openFile(path) {
        var url = '<?php echo base_url() ?>' + path;
        window.open(url);
    }

    //Default collapse show
    $('.collapse.show').each(function() {
        $(this).prev('.card-header').find('.fa').addClass('fa-chevron-down').removeClass('fa-chevron-right');
    });

    //On collapse show or hide
    $('.collapse').on('show.bs.collapse', function() {
        $(this).prev('.card-header').find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-down');
    }).on('hide.bs.collapse', function() {
        $(this).prev('.card-header').find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-right');
    });
</script>