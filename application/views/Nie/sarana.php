<div class="container">
    <br />
    <h5>Daftar Semua Sarana</h5>
    <div class="row mt-3">
        <div class="col-md-7">
            <form class="form-inline" action="" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="user_option">Filter berdasarkan</label>
                    </div>
                    <select class="custom-select" id="user_option">
                        <!-- <option value=all">ALL</option> -->
                        <option value="nama_sarana">NAMA SARANA</option>
                        <option value="alamat">ALAMAT</option>
                    </select>
                </div>
                &nbsp;&nbsp;
                <div class="input-group mb-3">
                    <input type="text" class="form-control column_filter" placeholder="Ketikkan kata kunci.." name="keyword" id='keyword'>
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="btn-filter">Cari</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <?php if (empty($sarana)) : ?>
                <div class="alert alert-danger" role='alert'>
                    Data Tidak Ditemukan
                </div>
            <?php endif; ?>
            <table class="table table-hover display" id="unit_table">
                <thead>
                    <tr>
                        <th></th>
                        <th>NO</th>
                        <th>NAMA SARANA</th>
                        <th>ALAMAT</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

</div>

<script>
    var table = $('#unit_table');

    function convertDate(inputFormat) {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }
        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('-');
    }

    function format(d) {
        return '<table cellpadding="2" cellspacing="0">' +
            '<tr>' +
            '<td>Jumlah Produk Yang Didaftarkan</td>' +
            '<td>' + d.produk_terdaftar + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Jumlah Produk Yang Diproduksi</td>' +
            '<td>' + d.produksi + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Jumlah Produk Yang Diekspor</td>' +
            '<td>' + d.produk_ekspor + '</td>' +
            '</tr>' +
            '</table>';
    }

    function processTableSarana() {
        var saranaConfig = {
            "processing": true,
            "serverSide": true,
            "order": [],
            'stripeClasses': ['stripe1', 'stripe2'],
            "ordering": false,
            "bFilter": false,
            "lengthMenu": [
                [25, 50, 100, -1],
                [25, 50, 100, "All"]
            ],
            "dom": 'rt<"row"<"col-sm-6"l><"col-sm-6"p>>i<"clear">',
            "language": {
                "lengthMenu": "Tampilkan _MENU_ data/halaman",
                "zeroRecords": "Data tidak ditemukan",
                "infoEmpty": "Tidak ada data",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                // "infoFiltered": "(Hasil pencarian dari _MAX_ data)",
                "infoFiltered": "",
                "paginate": {
                    "previous": "&laquo;",
                    "next": "&raquo;"
                },
                select: {
                    rows: {
                        _: "%d baris dipilih",
                        0: ""
                    }
                }
            },
            "ajax": {
                "url": "<?php echo site_url('Nie/unit_sanbe'); ?>",
                "type": "POST",
                "data": function(data) {
                    data.kategori_pencarian = $('#user_option').val();
                    data.kata_kunci = $('#keyword').val();
                }
            },
            "columns": [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": '',
                    "render": function() {
                        return '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                    },
                    width: "15px"
                },
                {
                    "data": "num",
                    "width": "2%"
                },
                {
                    "data": "planning_area",
                    "width": "20%"
                },
                {
                    "data": "address",
                    "width": "80%"
                }
            ]
        };

        table.DataTable(saranaConfig);
    }

    //Generate data table sarana
    processTableSarana();

    $('#btn-filter').click(function() {
        table.DataTable().ajax.reload();
    });

    //Reset filter ketika teks box kata kunci kosong
    $('#keyword').bind('input', function() {
        if ($(this).val() == "") {
            table.DataTable().ajax.reload();
        }
    });

    // $('#nie_table tbody').on('click', 'td.details-control', function() {
    $('#unit_table tbody').on('click', 'tr', function() {
        var tr = $(this).closest('tr');
        var tdi = tr.find("i.fa");
        var row = table.DataTable().row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
            tdi.first().removeClass('fa-minus-circle');
            tdi.first().addClass('fa-plus-circle');
        } else {
            row.child(format(row.data())).show();
            tr.addClass('shown');
            tdi.first().removeClass('fa-plus-circle');
            tdi.first().addClass('fa-minus-circle');
        }
    });

    table.on("user-select", function(e, dt, type, cell, originalEvent) {
        if ($(cell.node()).hasClass("details-control")) {
            e.preventDefault();
        }
    });
</script>