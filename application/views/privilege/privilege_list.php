<div class="container">
    <br />
    <h5>Hak Akses Pengguna</h5>
    <div class="row mt-3">
        <div class="col-md-7">
            <form class="form-inline">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="user_option">User Level</label>
                    </div>
                    <?php if (isset($department) && count($department) > 0) : ?>
                        <select class="custom-select" name="filter_dept" id="filter_dept">
                            <?php foreach ($department as $dept) : ?>
                                <option value="<?php echo $dept->dept_code ?>" <?php if ($dept->dept_code == $selected_dept_code) : ?> selected="selected" <?php endif; ?>><?php echo $dept->dept_name ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                    <div class="input-group-append">
                        <a href="#" class="btn btn-primary" id="btn-filter"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-5">
            <div class="btn-group float-right" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Atur Hak Akses
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="<?php echo base_url() ?>add-privilege">Tambah Level Akses</a>
                    <a class="dropdown-item" href="#" id="edit-privilege">Edit Akses</a>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#delete_level">Hapus Level Akses</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">

            <?php if (isset($data_privilege) && count($data_privilege) <> 0) : ?>
                <!-- Tabel untuk menampilkan list privilege -->
                <table class="table table-striped privilege">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">MENU APLIKASI</th>
                            <th scope="col" colspan="20">
                                <center>HAK AKSES</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (is_array($data_privilege) && count($data_privilege) <> 0) : ?>
                            <?php for ($n = 0; $n < count($data_privilege); $n++) : ?>
                                <tr>
                                    <td>
                                        <?php echo capitalize($data_privilege[$n]['main']) ?>
                                    </td>
                                    <td width="20%"><?php echo show_access($data_privilege[$n]['main'] . ";" . $data_privilege[$n]['main_value'], 'page') ?> &nbsp;</td>
                                    <?php for ($p = 0; $p < count($data_privilege[$n]['sub']); $p++) : ?>
                                        <td <?php if(strpos($data_privilege[$n]['sub'][$p], 'receive_submission') !== false) { ?> colspan="2" <?php } ?>><?php echo show_access($data_privilege[$n]['sub'][$p], $data_privilege[$n]['key']) ?></td>
                                    <?php endfor; ?>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
                <!-- End of tabel untuk menampilkan list privilege -->

            <?php else : ?>

                <div class="alert alert-warning" role="alert">
                    Data tidak ditemukan!
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>

<!-- Delete User Level confirmation -->
<div class="modal fade" tabindex="-1" role="dialog" id="delete_level" aria-labelledby="exampleModalLongTitle">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col d-flex justify-content-center"><i class="fa fa-question-circle-o" aria-hidden="true" style="font-size: 85px; color: #636363;"></i></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col d-flex justify-content-center">
                            <h3 class="modal-title" style="color: #636363;">Anda Yakin ?</h3>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col d-flex">
                            <p class="text-center" style="color: #999999;">Hapus User Level yang dipilih saat ini?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button>
                        </div>
                        <div class="col">
                            <a href="#" id="delete-level" class="btn btn-danger btn-block">Ya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#edit-privilege').on('click', function() {
        var dept_code = document.getElementById('filter_dept').value;
        $(this).attr("href", "<?php echo base_url() ?>privilege/edit/" + dept_code);
    });

    $('#btn-filter').on('click', function() {
        var dept_code = document.getElementById('filter_dept').value;
        $(this).attr("href", "<?php echo base_url() ?>privilege/search/" + dept_code);
    });

    $('#delete-level').on('click', function() {
        var dept_code = document.getElementById('filter_dept').value;
        $(this).attr("href", "<?php echo base_url() ?>privilege/delete/" + dept_code);
    });
</script>