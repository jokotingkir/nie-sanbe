<div class="container">
    <br />
    <h5>Add User Privilege</h5><br/>
    <form action="<?php echo base_url() ?>privilege/add" method="POST">
        <div class="form-group row mb-3">
            <label for="current_dept" class="col-md-1 col-form-label">User Level</label>
            <div class="col-md-3">
                <input type="text" class="form-control input_capital" name="new_level" />
            </div>
        </div>

        <div class="row mt-3">
            <div class="col">
                <table class="table table-striped privilege">
                    <thead>
                        <tr>
                            <th scope="col" width="3%"><input type="checkbox" id="check_all" /></th>
                            <th scope="col" width="20%">MENU APLIKASI</th>
                            <th scope="col" colspan="20">
                                <center>HAK AKSES</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (is_array($data_privilege) && count($data_privilege) <> 0) : ?>
                            <?php for ($n = 0; $n < count($data_privilege); $n++) : ?>
                                <tr>
                                    <td><input type="checkbox" class="main-menu group-head-check" /></td>
                                    <td>
                                        <?php echo capitalize($data_privilege[$n]['main']) ?>
                                    </td>
                                    <td width="20%"><?php echo add_access($data_privilege[$n]['main'] . ";" . $data_privilege[$n]['main_value'], 'page') ?> &nbsp;</td>
                                    <?php for ($p = 0; $p < count($data_privilege[$n]['sub']); $p++) : ?>
                                        <td <?php if(strpos($data_privilege[$n]['sub'][$p], 'receive_submission') !== false) { ?> colspan="2" <?php } ?>><?php echo add_access($data_privilege[$n]['sub'][$p], $data_privilege[$n]['key']) ?> &nbsp;</td>
                                    <?php endfor; ?>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-row mt-3">
            <div class="col-md-8">&nbsp;</div>
            <div class="col-md-2">
                <a href="<?php echo base_url() ?>privilege" class="btn btn-danger btn-block">Cancel</a>
            </div>
            <div class="col-md-2">
                <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block">Save</button>
            </div>
        </div>
    </form>
</div>

<script>
    $('.input_capital').on('input', function(evt) {
        $(this).val(function(_, val) {
            return val.toUpperCase();
        });
    });

    $('#check_all').click(function() {
        $('input:checkbox.main-menu').not(this).prop('checked', this.checked);
    });

    $('table tbody').each((index, tbody) => {
            $(tbody).find('tr').each((index, tr) => checkMaster(tr))
        })
        .on('change', 'input[type=checkbox]', function(e) {
            var currentCB = $(this);
            var isChecked = this.checked;

            if (currentCB.is(".group-head-check")) {
                var allCbs = currentCB.closest('tr').find('[type="checkbox"]').prop('checked', isChecked);
            } else {
                checkMaster(currentCB.closest('tr'));
            }
        });

    function checkMaster(tr) {
        var allCbs = $(tr).find('[type="checkbox"]');
        var allSlaves = allCbs.not(".group-head-check");
        var master = allCbs.filter(".group-head-check");
        var allChecked = allSlaves.filter(":checked").length === allSlaves.length;
        master.prop("checked", allChecked);
    }

    $('#submission_page').on('change', function() {
        if ($('.collapse.in').collapse('show')) {
            $('.collapse.in').collapse('hide');
        } else {
            $('.collapse.in').collapse('show');
        }
    });

    $('.collapse').on('show.bs.collapse', function() {
        $('.collapse.in').collapse('hide');
    });

    function dossierChecked(obj) {
        var hidden_element = "";
        var id = obj.id.split("_");
        var index = id[1];

        var str_name = obj.name.split("_");
        switch (str_name[0]) {
            case "psubmit":
                hidden_element = "hidesubmit_";
                break;
            case "preceive":
                hidden_element = "hidereceive_";
                break;
            case "pupload":
                hidden_element = "hideupload_";
                break;
        }

        if ($(obj).is(":checked")) {
            document.getElementById(hidden_element + index).disabled = true;
        } else {
            document.getElementById(hidden_element + index).disabled = false;
        }
    }
</script>