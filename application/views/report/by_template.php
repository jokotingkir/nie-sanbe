<?php
header("Content-type: application/vnd-ms-excel");
header('Content-Disposition: attachment; filename="' . $title . '.xls"');
header("Pragma: no-cache");
header("Expires: 0");
?>

<html>

<head>
    <style>
        th,
        td {
            padding: 10px;
            vertical-align: top;
        }

        table {
            border-collapse: separate;
            border-spacing: 5px;
        }
    </style>
</head>

<body>
    <table border="1">
        <thead>
            <tr>
                <th>NO</th>
                <th>NIE NO</th>
                <th>NIE STATUS</th>
                <th>NIE DATE</th>
                <th>START DATE</th>
                <th>END DATE</th>
                <th>HOLDER</th>
                <th>PRODUCT TYPE</th>
                <th width="425">PRODUCT</th>
                <th>PRODUCT CODE</th>
                <th>FINAL DOSSAGE FORM</th>
                <th>PACKAGING</th>
                <th>SHELF LIFE</th>
                <th>STORAGE CONDITION</th>
                <th>MANUFACTURE ADDRESS</th>
                <th>REMARK</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excel as $row) : ?>
                <tr>
                    <td align="center"><?php echo $row['no'] ?></td>
                    <td><?php echo $row['nie_no'] ?></td>
                    <td><?php echo $row['nie_status'] ?></td>
                    <td><?php echo date("d-m-Y", strtotime($row['nie_date'])) ?></td>
                    <td align="center"><?php echo date("d-m-Y", strtotime($row['nie_start_date'])) ?></td>
                    <td align="center"><?php echo date("d-m-Y", strtotime($row['nie_end_date'])) ?></td>
                    <td><?php echo $row['nie_holder'] ?></td>
                    <td><?php echo $row['product_type'] ?></td>
                    <td><?php echo $row['product_name'] ?></td>
                    <td><?php echo $row['PRODUCT_CODE'] ?></td>
                    <td><?php echo $row['fd_form'] ?></td>
                    <td><?php echo $row['packaging_presentation'] ?></td>
                    <td align="center"><?php echo $row['shelf_life'] ?></td>
                    <td><?php echo $row['storage_condition'] ?></td>
                    <td><?php echo $row['manufact_addr'] ?></td>
                    <td><?php echo $row['remark'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>