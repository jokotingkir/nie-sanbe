<?php
header("Content-type: application/vnd-ms-excel");
header('Content-Disposition: attachment; filename="' . $title . '.xls"');
header("Pragma: no-cache");
header("Expires: 0");
?>


<html>

<head>
    <style>
        th,
        td {
            padding: 10px;
            vertical-align: top;
        }

        table {
            border-collapse: separate;
            border-spacing: 5px;
        }
    </style>
</head>

<body>
    <table border="1">
        <thead>
            <tr>
                <th>NO.</th>
                <th>NOMOR REGISTRASI</th>
                <th>KODE PRODUK</th>
                <th width="425">PRODUK</th>
                <th>PIC</th>
                <th>TGL TERBIT</th>
                <th>MASA BERLAKU</th>
                <th>UNIT</th>
                <th>ALERT STATUS</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excel as $row) : ?>
                <tr>
                    <td align="center"><?php echo $row['no'] ?></td>
                    <td><?php echo $row['nie_no'] ?></td>
                    <td><?php echo $row['PRODUCT_CODE'] ?></td>
                    <td><?php echo $row['product_name'] ?></td>
                    <td><?php echo $row['pic'] ?></td>
                    <td align="center"><?php echo date('d-m-Y', strtotime($row['nie_start_date'])) ?></td>
                    <td align="center"><?php echo date('d-m-Y', strtotime($row['nie_end_date'])) ?></td>
                    <td><?php echo $row['planning_area'] ?></td>
                    <td align="center">
                        <!-- BLACK LARGE CIRCLE &#11044; &#x2b24; -->
                        <span style="font-size: 16px; color: <?php echo $row['alert_color'] ?>; vertical-align: text-bottom;">&#9899;</span>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>