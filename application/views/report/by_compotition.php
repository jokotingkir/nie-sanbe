<?php
header("Content-type: application/vnd-ms-excel");
header('Content-Disposition: attachment; filename="' . $title . '.xls"');
header("Pragma: no-cache");
header("Expires: 0");
?>

<html>

<head>
    <style>
        th,
        td {
            padding: 10px;
            vertical-align: top;
        }

        table {
            border-collapse: separate;
            border-spacing: 5px;
        }
    </style>
</head>

<body>
    <table border="1">
        <thead>
            <tr>
                <th rowspan="2">NO</th>
                <th rowspan="2">NIE NO</th>
                <th rowspan="2">PRODUCT CODE</th>
                <th rowspan="2" width="425">PRODUCT NAME</th>
                <th colspan="3">COMPOTITION</th>
            </tr>
            <tr>
                <th>ITEM NAME</th>
                <th>STRENGTH</th>
                <th>UOM</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excel as $row) : ?>
                <?php if (!empty($row['compotition'])) : ?>
                    <?php
                    $compos = $row['compotition'];
                    $temp = explode('$', $compos);
                    ?>
                    <?php if (count($temp) > 1) : ?>
                        <?php for ($i = 0; $i < count($temp); $i++) : ?>
                            <?php $item = explode('#', $temp[$i]); ?>
                            <?php if ($i == 0) : ?>
                                <tr>
                                    <td rowspan="<?php echo count($temp) ?>"><?php echo $row['no'] ?></td>
                                    <td rowspan="<?php echo count($temp) ?>"><?php echo $row['nie_no'] ?></td>
                                    <td rowspan="<?php echo count($temp) ?>"><?php echo $row['PRODUCT_CODE'] ?></td>
                                    <td rowspan="<?php echo count($temp) ?>"><?php echo $row['product_name'] ?></td>
                                    <td align="left"><?php echo $item[0] ?></td>
                                    <td align="left"><?php echo $item[1] ?></td>
                                    <td align="left"><?php echo $item[2] ?></td>
                                </tr>
                            <?php else : ?>
                                <tr>
                                    <td align="left"><?php echo $item[0] ?></td>
                                    <td align="left"><?php echo $item[1] ?></td>
                                    <td align="left"><?php echo $item[2] ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endfor; ?>
                    <?php endif; ?>

                    <?php if (count($temp) == 1) : ?>
                        <?php $item = explode('#', $temp[0]); ?>
                        <tr>
                            <td><?php echo $row['no'] ?></td>
                            <td><?php echo $row['nie_no'] ?></td>
                            <td><?php echo $row['PRODUCT_CODE'] ?></td>
                            <td><?php echo $row['product_name'] ?></td>
                            <td align="left"><?php echo $item[0] ?></td>
                            <td align="left"><?php echo $item[1] ?></td>
                            <td align="left"><?php echo $item[2] ?></td>
                        </tr>
                    <?php endif; ?>

                <?php else : ?>
                    <tr>
                        <td><?php echo $row['no'] ?></td>
                        <td><?php echo $row['nie_no'] ?></td>
                        <td><?php echo $row['PRODUCT_CODE'] ?></td>
                        <td><?php echo $row['product_name'] ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                <?php endif; ?>

            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>