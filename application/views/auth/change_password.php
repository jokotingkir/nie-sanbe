<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Ubah Password
                </div>
                <div class="card-body">
                    <?php if ($this->session->flashdata('message')) : ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo  $this->session->flashdata('message') ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo base_url() ?>Auth/update_password" method="POST">
                        <div class="form-group col-md-5">
                            <label for="main_passowrd">Password</label>
                            <input type="password" name="main_password" class="form-control" aria-describedby="passwordHelp" value="<?php echo  set_value('main_password'); ?>" />
                            <small id="passwordHelp" class="form-text text-muted">
                                Password terdiri dari 8-20 karakter, mengandung huruf dan angka, dan tidak boleh menggandung spasi atau karakter spesial (\~!@#$%^&*_/).
                            </small>
                            <?php echo form_error('main_password'); ?>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="confirm_password">Konfirmasi Password</label>
                            <input type="password" name="confirm_password" class="form-control" value="<?php echo set_value('confirm_password'); ?>" />
                            <?php echo form_error('confirm_password'); ?>
                        </div>
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-success float-right" name="submit_password" value="submit_password" style="width: 100px;">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>