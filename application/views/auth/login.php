<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap-4.1.3/dist/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/gridsm.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <title>Login NIE</title>

    <style>
        #auth-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }

        hr.style14 {
            margin-bottom: 2px;
            border: 0;
            height: 2px;
            background-image: -webkit-linear-gradient(left, #f0f0f0, #DCDCDC, #f0f0f0);
            background-image: -moz-linear-gradient(left, #f0f0f0, #DCDCDC, #f0f0f0);
            background-image: -ms-linear-gradient(left, #f0f0f0, #DCDCDC, #f0f0f0);
            background-image: -o-linear-gradient(left, #f0f0f0, #DCDCDC, #f0f0f0);
        }

        body {
            font-family: 'Open Sans', sans-serif;
        }
    </style>
</head>

<body>
    <div class="fw-background"></div>
    <div class="fw-container">
        <div class="fw-header">
            <div class="nav-master mt-3">
                <img src="<?php echo base_url() ?>assets/images/sanbe-snow.png" alt="Sanbe Farma" width="130"><br />
                <p class="mt-2" style="color: white;">Jl. Tamansari No.10, Tamansari, Bandung Wetan, Kota Bandung</p class="mt-2">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-4">&nbsp;</div>
        <form id="login_form" action="<?php echo base_url(); ?>login" method="POST">
            <div class="row mt-4 justify-content-md-center">
                <div class="col-md-5">
                    <?php if (isset($message) && !empty($message)) : ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?php echo  $message ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <input type="text" placeholder="Username" class="form-control form-control-lg" id="username" name="username" value="<?php echo set_value('username') ?>" />
                        <?php echo form_error('username'); ?>
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control form-control-lg" id="user_password" name="password" />
                        <?php echo form_error('password'); ?>
                    </div>
                    <div class="form-group float-right">
                        <div class="custom-control custom-checkbox float">
                            <input type="checkbox" class="custom-control-input" id="showPassword">
                            <label class="custom-control-label" for="showPassword">Show Password</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="submit" value="submit" type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                    </div>
                    <div class="form-group text-center">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#reset_password_form">Reset Password</a>
                    </div>
                </div>
            </div>
        </form>

        <div class="row justify-content-md-center">
            <div class="col-md-4">
                <hr class="style14" size="1" color="#CCCCCC">
            </div>
        </div>
        <div class="row mt-4 justify-content-md-center">
            <span class="text-muted">NIE-Monitoring System</span>
        </div>
        <div class="row justify-content-md-center">
            <span class="text-muted">Sistem Informasi Pengelolaan Administrasi Data NIE (Nomor Ijin Edar)</span>
        </div>
    </div>

    <!-- Completed NIE confirmation -->
    <div class="modal fade" tabindex="-1" role="dialog" id="reset_password_form">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div id="res-alert" style="display: none;"></div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Ketik email yang terdaftar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a href="javascript:void(0)" class="btn btn-primary btn-block" onclick="sendResetEmail()">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bootstrap-4.1.3/dist/js/bootstrap.js"></script>

    <script>
        $('#login_form').attr('autocomplete', 'off');

        $('#showPassword').on('click', function() {
            var x = document.getElementById("user_password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        });

        $('#reset_password_form').on('hide.bs.modal', function(e) {
            var response_alert = document.getElementById('res-alert');
            response_alert.style.display = "none";
            $('#user_email').val(null);
        })

        function sendResetEmail() {
            var user_email = document.getElementById('user_email').value;
            var response_alert = document.getElementById('res-alert');
            $.ajax({
                url: "<?php echo site_url(); ?>reset-user-password",
                type: "POST",
                dataType: "JSON",
                data: {
                    'user_email': user_email
                },
                success: function(response) {
                    // console.log(response);
                    if (response.status == "not found") {
                        var msg = '<div class="row"><div class="col"><div class="alert alert-danger" role="alert">' + response.message + '</div></div></div>';
                        response_alert.innerHTML = msg;
                        response_alert.style.display = "block";
                    } else {
                        alert(response.message);
                        response_alert.style.display = "none";
                        $('#user_email').val(null);
                        $('#reset_password_form').modal('hide');
                    }
                }
            });
        }
    </script>
</body>

</html>