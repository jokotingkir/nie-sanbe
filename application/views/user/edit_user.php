<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Edit User
                </div>
                <div class="card-body">
                    <form action="<?php echo base_url() ?>user-edit" method="POST">
                        <input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="emp_code">Employee Number</label>
                                <input type="text" class="form-control" name="emp_code" value="<?php echo set_value('emp_code', $emp_code); ?>" />
                                <?php echo form_error('emp_code'); ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nama_lengkap">Employee Name</label>
                                <input type="text" class="form-control" name="nama_lengkap" value="<?php echo set_value('nama_lengkap', $name); ?>" />
                                <?php echo form_error('nama_lengkap'); ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="tgl_lahir">Birth Date</label>
                                <input type="date" class="form-control" name="tgl_lahir" value="<?php echo set_value('tgl_lahir', $birth); ?>" />
                                <?php echo form_error('tgl_lahir'); ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" value="<?php echo set_value('email', $email); ?>" />
                                <?php echo form_error('email'); ?>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="privilege">User Level</label>
                                <select class="form-control" name="privilege" id="privilege">
                                    <option value="">-- Please Select User Level --</option>
                                    <?php foreach ($user_level as $level) : ?>
                                        <option value="<?php echo $level->id ?>" <?php echo set_select('privilege', $level->id, (!empty($privilege) && $privilege == $level->id ? TRUE : FALSE)) ?> data-deptcode="<?php echo $level->dept_code ?>" <?php if ($privilege_id == $level->id) { ?> selected <?php } ?>><?php echo $level->dept_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error('privilege'); ?>
                                <input type="hidden" name="dept_code" id="dept_code" value="<?php echo set_value('dept_code', $dept_code) ?>" />
                            </div>
                        </div>
                        <div class="form-row mt-4">&nbsp;</div>
                        <div class="form-row mt-4">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url() ?>user-list" class="btn btn-danger btn-block">Cancel</a>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('select#privilege').change(function() {
            var dept_code = $(this).find(':selected').data('deptcode');

            $('#dept_code').val(dept_code);
        });
    });
</script>