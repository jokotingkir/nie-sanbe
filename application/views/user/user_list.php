<div class="container">
    <br />
    <?php if ($this->session->flashdata('message')) : ?>
        <?php echo $this->session->flashdata('message'); ?>
    <?php endif; ?>
    <br />
    <h5>Daftar Pengguna</h5>
    <div class="row mt-3">
        <div class="col-md-7">
            <form class="form-inline" action="" method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="user_option">Filter berdasarkan</label>
                    </div>
                    <select class="custom-select" id="kategori_pencarian">
                        <!-- <option value=all">ALL</option> -->
                        <option value="emp_code">EMP CODE</option>
                        <option value="name">EMP NAME</option>
                        <option value="dept_name">PRIVILEGE</option>
                        <option value="email">EMAIL</option>
                    </select>
                </div>
                &nbsp;&nbsp;
                <div class="input-group mb-3">
                    <input type="text" class="form-control column_filter" placeholder="Ketikkan kata kunci.." name="keyword" id="kata_kunci">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="btn-filter">Cari</button>
                    </div>
                </div>
            </form>
        </div>
        
        <?php if($this->session->userdata('add_user') == '1') :?>
            <div class="col-md-5">
                <a href="<?php echo base_url(); ?>add-user" class="float-right btn btn-primary mb-3">Add User</a>
            </div>
        <?php endif;?>

    </div>
    <div class="row">
        <div class="col">
            <table class="table table-hover display" id="user_list_table">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>EMP CODE</th>
                        <th>EMP NAME</th>
                        <th>BIRTH DATE</th>
                        <th>PRIVILEGE</th>
                        <th>USERNAME</th>
                        <th>EMAIL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    <?php if ($this->session->userdata('logged_user')): ?>
        var edit_user = '<?php echo $this->session->userdata('edit_user') ?>'; 
    <?php endif; ?>

    var user_table = $('#user_list_table');
    var user_table_config;

    user_table_config = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo site_url('User/user_list'); ?>",
            "type": "POST",
            "data": function(data) {
                data.kategori_pencarian = $('#kategori_pencarian').val();
                data.kata_kunci = $('#kata_kunci').val();
            }
        },
        // select: {
        //     style: 'single'
        // },
        'stripeClasses': ['stripe1', 'stripe2'],
        "ordering": false,
        "bFilter": false,
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "dom": 'rt<"row"<"col-sm-6"l><"col-sm-6"p>>i<"clear">',
        "language": {
            "lengthMenu": "Tampilkan _MENU_ data/halaman",
            "zeroRecords": "Data tidak ditemukan",
            "infoEmpty": "Tidak ada data",
            "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
            "infoFiltered": "(Hasil pencarian dari _MAX_ data)",
            "paginate": {
                "previous": "&laquo;",
                "next": "&raquo;"
            },
            select: {
                rows: {
                    _: "%d baris dipilih",
                    0: ""
                }
            }
        },
        "columns": [{
                "data": "num",
                "width": "2%"
            },
            {
                "data": "emp_code",
                "width": "10%"
            },
            {
                "data": "name",
                "width": "15%"
            },
            {
                "data": "birth",
                "width": "20%"
            },
            {
                "data": "dept_name",
                "width": "20%"
            },
            {
                "data": "username",
                "width": "10%"
            },
            {
                "data": "email",
                "width": "20%"
            },
            {
                "data": "user_id",
                "width": "20%",
                "className": "text-center",
                "render": function(data) {
                    // var action_btn = '<a href="#" data-toggle="tooltip" data-placement="bottom" title="Reset Password"><i class="fa fa-key" aria-hidden="true" style="font-size: 11pt; color: #000000;"></i></a>&nbsp;&nbsp;';
                    var action_btn = '';
                    if(typeof edit_user != 'undefined'){
                        if(edit_user == '1'){
                            action_btn = action_btn + '<a href="<?php echo base_url() ?>user-edit/' + data + '" data-toggle="tooltip" data-placement="bottom" title="Edit User"><i class="fa fa-pencil-square-o" aria-hidden="true" style="font-size: 11pt; color: #000000;"></i></a>';
                        }
                    }

                    return action_btn;
                }
            }
        ],
        "order": [
            [1, 'asc']
        ]
    };

    user_table.DataTable(user_table_config);

    $('#btn-filter').click(function() {
        user_table.DataTable().ajax.reload();
    });

    //Reset filter ketika teks box kata kunci kosong
    $('#kata_kunci').bind('input', function() {
        if ($(this).val() == "") {
            user_table.DataTable().ajax.reload();
        }
    });
</script>