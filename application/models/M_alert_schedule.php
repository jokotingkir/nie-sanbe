<?php

class M_alert_schedule extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        // $this->uat_cbd = $this->load->database('uat_cibodas', TRUE);
    }

    // var $table = 'nie_alert_schedule'; Tidak digunakan lagi
    var $table = 'nie_email_log';

    private function deleteEmailLog($current)
    {
        $this->db->where('nie_no', $current['nie_no']);
        $this->db->where('nie_alert_date', $current['nie_end_date']);
        $this->db->delete('nie_email_log');
    }

    public function insertEmailLog($nie, $config, $current = null)
    {
        $phase_color = array('GREEN', 'YELLOW', 'RED');
        $email_log = array();

        if (isset($nie['new_prodnie']) && $nie['new_prodnie'] != "") {
            $nie_no = $nie['new_prodnie'];
        } else {
            $nie_no = $nie['prodnie'];
        }

        //Delete current email log using nie_no and current nie_end_date
        if ($current != null) {
            $this->deleteEmailLog($current);
        }

        for ($n = 0; $n < count($phase_color); $n++) {
            $row = array();
            switch ($phase_color[$n]) {
                case 'GREEN':
                    $row['phase'] = $phase_color[$n];
                    $row['nie_mail_date'] = date('Y-m-d', strtotime('-' . $config['green_notif_start'] . ' months', strtotime($nie['nieenddate'])));
                    $row['email_type'] = $phase_color[$n];
                    $row['email_subject'] = $config['email_subject_green'];
                    $row['email_body'] = $config['email_body_text_green'];
                    break;

                case 'YELLOW':
                    $row['phase'] = $phase_color[$n];
                    $row['nie_mail_date'] = date('Y-m-d', strtotime('-' . $config['yellow_notif_start'] . ' months', strtotime($nie['nieenddate'])));
                    $row['email_type'] = $phase_color[$n];
                    $row['email_subject'] = $config['email_subject_yellow'];
                    $row['email_body'] = $config['email_body_text_yellow'];
                    break;

                case 'RED':
                    $row['phase'] = $phase_color[$n];
                    $row['nie_mail_date'] = date('Y-m-d', strtotime('-' . $config['red_notif_start'] . ' months', strtotime($nie['nieenddate'])));
                    $row['email_type'] = $phase_color[$n];
                    $row['email_subject'] = $config['email_subject_red'];
                    $row['email_body'] = $config['email_body_text_red'];
                    break;
            }

            $row['nie_no'] = $nie_no;
            $row['nie_alert_date'] = $nie['nieenddate'];
            $row['deliv_flag'] = 0;
            $row['product_code'] = $nie['prodcode'];
            $row['product_name'] = $nie['prodname'];
            $row['product_type'] = $nie['product_type_desc'];

            $email_log[] = $row;
        }

        $this->db->trans_start();
        $this->db->insert_batch($this->table, $email_log);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateEmailLog($old_nie_no, $new_nie_no)
    {
        $this->db->trans_start();
        $this->db->where('nie_no', $old_nie_no);
        $this->db->update($this->table, array('nie_no' => $new_nie_no));
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    /*
    *---------------------------------------------------------------------------
    *LOGIC DIDAPAT DARI MRP NIE ALERT MONITORING SYSTEM
    *---------------------------------------------------------------------------
    */
    // public function createSchedule($alert_config, $nie)
    // {
    /*
        *---------------------------------------------------------------------------
        *variabel $alert_config berisi value start_schedule, yellow, dan red notif
        *variabel $nie berisi nilai nie_alert_id dan nie_end_date yang digunakan dalam generate schedule
        *---------------------------------------------------------------------------
        */

    /*
        $schedule = array();
        $start_schedule = $alert_config['start_schedule'];
        $yesterday = date('Y-m-d', strtotime('-1 days'));

        $schedule['nie_alert_id'] = $nie['nie_alert_id'];
        $schedule['start_schedule'] = $start_schedule;
        if (strtotime($nie['nieenddate']) <= strtotime($yesterday)) {
            $schedule['fe' . $start_schedule] = 0;
            $schedule['sq' . $start_schedule] = 0;
        } else {
            $schedule['fe' . $start_schedule] = 1;
            $schedule['sq' . $start_schedule] = 2;
        }
        $schedule['t' . $start_schedule] = $nie['nieenddate'];

        for ($n = (int) $start_schedule - 1; $n >= 1; $n--) {
            $tmp = strtotime($nie['nieenddate'] . ' ' . (($start_schedule - $n) * -1) . ' months');
            if ($n == 1) {
                if ($tmp <= strtotime($yesterday)) {
                    $schedule['fe0'] = 0;
                    $schedule['sq0'] = 0;
                } else {
                    $schedule['fe0'] = 1;
                    $schedule['sq0'] = 1;
                }
                $schedule['t0'] = date('Y-m-d', $tmp);
            }

            if ($tmp <= strtotime($yesterday)) {
                $schedule['fe' . $n] = 0;
                $schedule['sq' . $n] = 0;
            } else {
                if ($n <= (int) $alert_config['yellow_notif_start']) {
                    $schedule['fe' . $n] = 1;
                    $schedule['sq' . $n] = 1;
                } else {
                    $schedule['fe' . $n] = 1;
                    $schedule['sq' . $n] = 2;
                }
            }
            $schedule['t' . $n] = date('Y-m-d', $tmp);
        }
        
        $this->db->trans_start();
        $this->db->insert($this->table, $schedule);
        */

    /*
        *---------------------------------------------------------------------------
        *GENERATE NIE EMAIL LOG
        *---------------------------------------------------------------------------
        */

    /*
        $this->db->query("CALL generate_nie_email_log('" . $nie['nie_alert_id'] . "')");
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
        */
    // }
}
