<?php

class CI_encrypt extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //Generate random digit
    private function genRandDigit($length = 8, $specialCharacters = true)
    {
        $digits = "";
        $chars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";

        if ($specialCharacters == true)
            $chars .= "!?=/&+,.";

        for ($i = 0; $i < $length; $i++) {
            $x = mt_rand(0, strlen($chars) - 1);
            $digits .= $chars{
                $x};
        }

        return $digits;
    }

    //Generate random salt
    public function genRandSalt()
    {
        return $this->genRandDigit(8, true);
    }

    public function encryptUserPwd($password, $salt)
    {
        return sha1(md5($password) . $salt);
    }
}
