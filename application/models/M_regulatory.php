<?php 

class M_regulatory extends CI_model{

    public function getRegulatory()
    {
        $this->db->where('regulatory_status', 'Active');
        return $this->db->get('mst_regulatory')->result_array();
    }
    
}
?>