<?php 

class M_product extends CI_model{

    public function GetAllProduct()
    {
        
        $this->db->where('PRODUCT_STATUS', 'Active');
        return $this->db->get('mst_product')->result_array();
    }

    public function GetProductByType($deptcode)
    {
        $this->db->select('PRODUCT_CODE, PRODUCT_DESC');
        $this->db->where('DEPT_CODE', $deptcode);
        $this->db->where('PRODUCT_STATUS', 'Active');
        return $this->db->get('mst_product')->result_array();
    }
    
    public function getProductPopUp()
    {
        $this->db->select('mst_product.PRODUCT_CODE, mst_product.PRODUCT_DESC, mst_product.PRODUCT_OWNER, mst_product.PLANNING_AREA, mst_product.EXPIRE_DAYS, mst_prod_group4.PG4_DESC, mst_prod_group7.PG7_DESC, KodeNegaraBPOM.con_desc', false);
        $this->db->from('mst_product');
        $this->db->join('mst_prod_group4', 'mst_product.PG4_CODE = mst_prod_group4.PG4_CODE', 'left');
        $this->db->join('mst_prod_group7', 'mst_product.PG7_CODE = mst_prod_group7.PG7_CODE', 'left');
        $this->db->join('KodeNegaraBPOM', 'mst_product.exportir_code = KodeNegaraBPOM.con_code', 'left');
        $query = $this->db->get();

        return $query->result_array();
        
    }

    public function cariDataProduct($k=false)
    {
        $keyword = ($k) ? $k : $this->input->post('keyword', true);
        $this->db->select('mst_product.PRODUCT_CODE, mst_product.PRODUCT_DESC, mst_product.PRODUCT_OWNER, mst_product.PLANNING_AREA, mst_product.EXPIRE_DAYS, mst_prod_group4.PG4_DESC, mst_prod_group7.PG7_DESC, KodeNegaraBPOM.con_desc', false);
        $this->db->from('mst_product');
        $this->db->join('mst_prod_group4', 'mst_product.PG4_CODE = mst_prod_group4.PG4_CODE', 'left');
        $this->db->join('mst_prod_group7', 'mst_product.PG7_CODE = mst_prod_group7.PG7_CODE', 'left');
        $this->db->join('KodeNegaraBPOM', 'mst_product.exportir_code = KodeNegaraBPOM.con_code', 'left');
        $this->db->like('mst_product.PRODUCT_CODE', $keyword);
        $this->db->or_like('mst_product.PRODUCT_DESC', $keyword);
        $this->db->or_like('mst_product.PRODUCT_OWNER', $keyword);
        $this->db->or_like('mst_product.PLANNING_AREA', $keyword);
        $this->db->or_like('mst_product.EXPIRE_DAYS', $keyword);
        $this->db->or_like('mst_prod_group4.PG4_DESC', $keyword);
        $this->db->or_like('mst_prod_group7.PG7_DESC', $keyword);
        $this->db->or_like('KodeNegaraBPOM.con_desc', $keyword);
        $query = $this->db->get();
        $data['data'] = $query->result_array();

        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Methods: GET, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding');

        $this->output->set_content_type('application/json')->set_output(json_encode($data['data']));
    }
}




?>