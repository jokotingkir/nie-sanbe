<?php

class M_productType extends CI_model
{

    function __construct()
    {
        parent::__construct();
    }

    var $table = 'mst_product_type';

    public function GetProdType()
    {
        return $this->db->get($this->table)->result_array();
    }

    public function type_by_id($type_id)
    {
        $this->db->select('id, product_type_code, product_type_desc');
        $this->db->where('id', $type_id);
        $this->db->where('product_type_status','Active');
        
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        } else {
            $data = array();
        }
        return $data;
    }
}
