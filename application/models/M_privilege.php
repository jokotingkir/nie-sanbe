<?php

class M_privilege extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $table = "privilege";
    public $department = "mst_department";

    public function add_privilege($new_dept, $dept_code, $privilege_access)
    {
        //insert into mst_department
        $now = date("Y-m-d H:i:s");
        $dept = array(
            "dept_name" => $new_dept,
            "dept_code" => $dept_code,
            "created_at" => $now,
            "created_by" => $this->session->userdata('username'),
        );

        $this->db->insert($this->department, $dept);

        //insert into privilege table with new department id
        $privilege_access["dept_code"] = $dept_code;

        $this->db->insert($this->table, $privilege_access);
        $last_privilege_id = $this->db->insert_id();

        //Menyimpan ke table product_dossier_privilege
        if ($privilege_access['submission_page'] == 1) {
            $doc_id = $this->input->post('product_dossier_id');
            $doc_name = $this->input->post('doc_name');
            $submit = $this->input->post('psubmit_pic');
            $receive = $this->input->post('preceive_pic');
            $upload_doc = $this->input->post('pupload_pic');

            $pdp = array();
            //psubmit_pic, preceive_pic, pupload
            for ($n = 0; $n < count($doc_name); $n++) {
                $access = array($submit[$n], $receive[$n], $upload_doc[$n]);
                if (in_array(1, $access)) {
                    $row = array();
                    $row['privilege_id'] = $last_privilege_id;
                    $row['product_dossier_id'] = $doc_id[$n];
                    $row['is_submit'] = ($submit[$n] == 1) ? "Y" : "N";
                    $row['is_receive'] = ($receive[$n] == 1) ? "Y" : "N";
                    $row['is_upload'] = ($upload_doc[$n] == 1) ? "Y" : "N";

                    array_push($pdp, $row);
                }
            }

            if (count($pdp) > 0) {
                $this->db->insert_batch('product_dossier_privilege', $pdp);
            }
        }
    }

    public function update_privilege($privilege_id, $privilege)
    {
        $this->db->where('id', $privilege_id)->update($this->table, $privilege);

        // if ($privilege['submission_page'] == 1) {
        // $existing = $this->db->select('pdp_id')->get('product_dossier_privilege')->result_array();

        //     $dossier_privilege = $this->input->post('privilege_dossier_id');
        //     $doc_id = $this->input->post('product_dossier_id');
        //     $doc_name = $this->input->post('doc_name');
        //     $submit = $this->input->post('psubmit_pic');
        //     $receive = $this->input->post('preceive_pic');
        //     $upload_doc = $this->input->post('pupload_pic');

        //     $insert_submission = array();
        //     $update_submission = array();
        //     $delete_submission = array();
        //     //psubmit_pic, preceive_pic, pupload
        //     for ($n = 0; $n < count($doc_name); $n++) {
        //         $access = array($submit[$n], $receive[$n], $upload_doc[$n]);
        //         if (in_array(1, $access)) {
        //             $row = array();
        //             $row['privilege_id'] = $privilege_id;
        //             $row['product_dossier_id'] = $doc_id[$n];
        //             $row['is_submit'] = ($submit[$n] == 1) ? "Y" : "N";
        //             $row['is_receive'] = ($receive[$n] == 1) ? "Y" : "N";
        //             $row['is_upload'] = ($upload_doc[$n] == 1) ? "Y" : "N";

        //             if ($dossier_privilege[$n] != 0) {
        //                 //untuk di update jika pdp_id != 0
        //                 $row['pdp_id'] = $dossier_privilege[$n]; //product dossier privilege id
        //                 array_push($update_submission, $row); //untuk di update ke table product dossier privilege
        //             } else {
        //                 //untuk di insert jika pdp_id == 0
        //                 array_push($insert_submission, $row);
        //             }
        //         } else {
        //             array_push($delete_submission, $dossier_privilege[$n]); //untuk di hapus dari table product dossier privilege jika id tersedia
        //         }
        //     }

        //     //untuk di di insert ke table product dossier privilege
        //     if (count($insert_submission) > 0) {
        //         $this->db->insert_batch('product_dossier_privilege', $insert_submission);
        //     }

        //     //untuk di update ke table product dossier privilege
        //     if (count($update_submission) > 0) {
        //         $this->db->update_batch('product_dossier_privilege', $update_submission, 'pdp_id');
        //     }

        //     //untuk di hapus dari table product dossier privilege jika id tersedia
        //     if (count($delete_submission) > 0) {
        //         $this->db->where_in('pdp_id', array_unique($delete_submission))->delete('product_dossier_privilege');
        //     }
        // }
    }

    // without ADMIN
    public function all_department()
    {
        return $this->db->select('dept_id, dept_code, dept_name')->where('dept_code <> ', 'ADMIN')->get('mst_department')->result();
    }

    public function get_privilege_list($dept_code = "")
    {
        //masukan semua department code ke dalam array untuk kemudian dicari di table privilege
        if ($dept_code == "") {
            $department = $this->all_department();
            if (!empty($department)) {
                $array_dept = array();
                foreach ($department as $dept) {
                    array_push($array_dept, $dept->dept_code);
                }
            }

            $this->db->where_in('dept_code', $array_dept);
            $this->db->limit(1); //ambil 1 aja buat tampilan default ketika ngebuka menu nya
        } else {
            $this->db->where('dept_code', $dept_code);
        }

        return $this->db->get($this->table)->row();
    }

    public function privilege_columns()
    {
        return $this->db->query('DESCRIBE privilege')->result_array();
    }

    public function get_list_department()
    {
        return $this->db->where('dept_name <> ', 'ADMIN')->get('mst_department')->result_array();
    }

    public function available_department($group)
    {
        $this->db->select('md.dept_id, dept_name');
        $this->db->from('privilege p');
        $this->db->join('mst_department md', 'p.dept_id = md.dept_id', 'INNER');
        $this->db->where('p.class_code', $group);
        $this->db->group_by('p.dept_id');

        return $this->db->get()->result_array();
    }

    public function available_group()
    {
        $this->db->select('mcd.class_code, class_desc');
        $this->db->from('privilege p');
        $this->db->join('mst_class_dept mcd', 'p.class_code = mcd.class_code', 'INNER');
        $this->db->group_by('p.class_code');

        return $this->db->get()->result();
    }

    public function get_department($level)
    {
        if (!empty($level)) {
            $this->db->where('dept_name', strtoupper($level));
        }

        $this->db->select('dept_id, dept_name');
        $this->db->where('dept_name <> ', strtoupper("admin"));

        return $this->db->get($this->department)->row();
    }

    public function get_department_by_id($id)
    {
        return $this->db->where('dept_id', $id)->get($this->department)->row();
    }

    public function get_class_department()
    {
        return $this->db->order_by('class_code', 'DESC')->get('mst_class_dept')->result();
    }

    public function get_access_submission($privilege_id)
    {
        $this->db->select('pdp.*, prank, dossier_desc');
        $this->db->from('product_dossier_privilege pdp');
        $this->db->join('product_dossier pd', 'pdp.product_dossier_id = pd.id', 'INNER');
        $this->db->join('mst_doc_dossier md', 'pd.mst_dossier_id = md.id', 'INNER');
        $this->db->where('pdp.privilege_id', $privilege_id);

        return $this->db->get()->result();
    }

    public function class_department_by_code($class_code)
    {
        return $this->db->where('class_code', $class_code)->get('mst_class_dept')->row();
    }

    public function exist_department()
    {
        $this->db->select('p.id, p.dept_code, dept_name');
        $this->db->from($this->table . ' AS p');
        $this->db->join('mst_department md', 'p.dept_code = md.dept_code', 'INNER');

        return $this->db->get()->result();
    }

    public function deletePrivilege($dept_code)
    {
        $this->db->trans_start();
        $this->db->query('DELETE p, md FROM privilege p INNER JOIN mst_department md ON p.dept_code = md.dept_code WHERE p.dept_code = "' . $dept_code . '"');
        $this->db->trans_complete();

        if ($this->db->trans_status() == false) {
            return false;
        } else {
            return true;
        }
    }

    public function get_submission_dept()
    {
        $this->db->select('dept_id, dept_code, dept_name');
        $this->db->from('mst_department');
        $this->db->where_in('dept_code', array('QA', 'RND'));

        return $this->db->get()->result();
    }

    public function get_dept_by_deptcode($dept_code)
    {
        $this->db->select('dept_id, dept_code, dept_name');
        $this->db->where('dept_code', $dept_code);
        $this->db->from('mst_department');

        return $this->db->get()->row();
    }
}
