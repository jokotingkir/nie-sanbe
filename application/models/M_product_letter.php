<?php

class M_product_letter extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    var $table = 'product_letter';

    public function get_nie_product($product_code)
    {
        return $this->db->where('PRODUCT_CODE', $product_code)->get($this->table)->result();
    }
}
