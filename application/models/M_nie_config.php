<?php

class M_nie_config extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    var $table = 'nie_alert_config';

    public function get_config($type)
    {
        $this->db->where('product_type_code', $type);
        $this->db->where('status', 'active');
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        } else {
            $data = array();
        }
        return $data;
    }

    public function get_all_config()
    {
        $this->db->where('UPPER(status)', 'ACTIVE');
        return $this->db->get($this->table)->result_array();
    }
}
