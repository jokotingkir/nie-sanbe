<?php

class M_user extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('CI_encrypt'));
    }

    var $table = "user_credentials";
    var $column_order = array(null, 'user_id', 'name', 'birth',  'email', 'emp_code');
    var $column_search = array('name', 'dept_name',  'email', 'emp_code'); //set column field database for datatable searchable 
    var $order = array('user_id' => 'asc'); // default order

    public function add_new_user($data)
    {
        $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    private function _user_datatables_query()
    {
        if ($this->input->post('kategori_pencarian') && $this->input->post('kata_kunci')) {
            $kategori_pencarian = $this->input->post('kategori_pencarian');
            $kata_kunci = $this->input->post('kata_kunci');

            //Gunakan parameter ketiga 'before' atau 'after' jika diperlukan (sama seperti penggunaan "%" pada like)
            $this->db->like($kategori_pencarian, $kata_kunci);
        }

        $this->db->select('user_id, name, birth, username, email, EMP_CODE, md.dept_name, privilege_id');
        $this->db->from($this->table . " as uc");
        $this->db->join('mst_department as md', 'uc.dept_code = md.dept_code', 'INNER');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_user_list()
    {
        $this->_user_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_user_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->get($this->table);
        return $this->db->count_all_results();
    }

    public function is_unique_password($user_id, $password)
    {
        $query = $this->db->where('user_id', $user_id)->get($this->table)->row();

        $salt = $query->salt;
        $current_password = $query->password;

        $encrypt_password = $this->CI_encrypt->encryptUserPwd($password, $salt);

        ($current_password == $encrypt_password) ? $res = TRUE : $res = FALSE;

        return $res;
    }

    public function is_change_password($user_id)
    {
        $query = $this->db->where('user_id', $user_id)->get($this->table)->row();

        $name = $query->name;
        $birth = $query->birth;
        $salt = $query->salt;
        $current_password = $query->password;

        $temp_name = explode(" ", $name);
        $check_password = strtolower($temp_name[0]);

        $temp_birth = explode("-", $birth);
        $temp_birth[0] = substr($temp_birth[0], 2, 4);
        for ($n = 0; $n < count($temp_birth); $n++) {
            $check_password .= $temp_birth[$n]; //gabungan dari nama pertama + yymmdd (tanggal lahir)
        }

        $encrypt_password = $this->CI_encrypt->encryptUserPwd($check_password, $salt);

        ($current_password == $encrypt_password) ? $res = TRUE : $res = FALSE;

        return $res;
    }

    public function update_user_password($user_id, $new_password)
    {
        $query = $this->db->where('user_id', $user_id)->get($this->table)->row();

        $salt = $query->salt;

        $update_password = array(
            "password" => $this->CI_encrypt->encryptUserPwd($new_password, $salt),
            "password_search" => md5($new_password)
        );

        $this->db->where('user_id', $user_id)->update($this->table, $update_password);
    }

    public function get_user($user_id)
    {
        $this->db->select('user_id, EMP_CODE, name, birth, email, dept_code, privilege_id');
        $this->db->from($this->table);
        $this->db->where('user_id', $user_id);

        return $this->db->get()->result();
    }

    public function update_user($data)
    {
        $this->db->trans_start();
        $this->db->where('user_id', $data['user_id'])->update($this->table, $data);
        $this->db->trans_complete();

        if ($this->db->trans_status() == false) {
            return false;
        } else {
            return true;
        }
    }

    public function getEmailbyDept($dept_code, $planning_area = null)
    {
        $this->db->select('email');
        $this->db->from('user_credentials');
        $this->db->where('dept_code', $dept_code);

        if ($planning_area != null) {
            $this->db->where('planning_area', $planning_area);
        }

        return $this->db->get()->result_array();
    }

    public function isExistEmail($email)
    {
        $query = $this->db->where('email', $email)->get('user_credentials')->result();

        if (count($query) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function resetUserPassword($email)
    {
        $user = $this->db->where('email', $email)->get('user_credentials')->row();

        //Kembalikan password ke semula yaitu -> username + yymmdd
        $split_birth = explode("-", $user->birth);
        $split_birth[0] = substr($split_birth[0], 2, 4); //hanya ambil 2 digit terakhir dari tahun kelahiran
        $default_pass = $user->username;

        for ($n = 0; $n < count($split_birth); $n++) {
            $default_pass .= $split_birth[$n];
        }
        $encrypt_password = $this->CI_encrypt->encryptUserPwd($default_pass, $user->salt);
        $md5 = md5($default_pass); //md5 hanya digunakan untuk menemukan user dan rand salt yang cocok pada saat login

        $update_password = array(
            'password' => $encrypt_password,
            'password_search' => $md5
        );

        $this->db->where('user_id', $user->user_id);
        $this->db->update('user_credentials', $update_password);

        //Sending email to user

        $from_email = "nie_admin@sanbe-farma.com";
        $this->load->library('phpmailer_library');
        $mail = $this->phpmailer_library->load();

        $mail->isSMTP();
        $mail->Host = 'smtps.sanbe-farma.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin@smtps.sanbe-farma.com';
        $mail->Password = 'ITdeptsystem';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom($from_email, "NIE System Admin");
        $mail->addReplyTo($from_email, "NIE System Admin");
        $mail->addAddress($email, $user->name);
        $mail->Subject = 'NIE Reset Password Request';
        $mail->isHTML(true);

        $mail->Body = 'Silahkan Login ke NIE Monitoring System menggunakan password <b>' . $default_pass . '</b>';

        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
}
