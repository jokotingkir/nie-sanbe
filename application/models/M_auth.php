<?php

class M_auth extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('CI_encrypt', 'M_privilege', 'M_nieDossier'));
    }

    public $table = 'user_credentials';
    public $log = 'user_log';

    public function process_login($login_array_input = null)
    {
        if (!isset($login_array_input) || count($login_array_input) != 2) {
            return false;
        }

        $username = $login_array_input[0];
        $password = $login_array_input[1];

        //cek data di database user, data ada atau tidak
        $user = $this->get_user($username, md5($password));
        
        if (!empty($user)) {
            $user_id = $user['user_id'];
            $user_password = $user['password'];
            $user_salt = $user['salt'];

            if ($this->CI_encrypt->encryptUserPwd($password, $user_salt) === $user_password) {
                $privilege_columns = $this->M_privilege->privilege_columns();

                //set user session
                $this->session->set_userdata('logged_user', $user_id);
                $this->session->set_userdata('name', $user['name']);
                $this->session->set_userdata('username', $user['username']);
                $this->session->set_userdata('email', $user['email']);
                $this->session->set_userdata('birth', $user['birth']);
                $this->session->set_userdata('dept_code', $user['dept_code']);

                for ($n = 0; $n < count($privilege_columns); $n++) {
                    $field = $privilege_columns[$n]['Field'];
                    if (!in_array($field, array('id', 'dept_code'))) {
                        $this->session->set_userdata($field, $user[$field]);
                    }
                }

                //cek untuk hak akses submission dossier
                // if ($user['submission_page'] == 1) {
                //     $submission = $this->M_nieDossier->submission_doc_access($user['id']); //cari data akses submission berdasarkan privilege id
                //     if (count($submission) > 0) {
                //         $this->session->set_userdata('submission_privilege', $submission);
                //     }
                // }

                return true;
            }
            return false;
        }
        return false;
    }

    public function check_logged()
    {
        return ($this->session->userdata('logged_user')) ? true : false;
    }

    public function logged_id()
    {
        return ($this->check_logged()) ? $this->session->userdata('logged_user') : '';
    }

    public function false_login_check($limit = 5)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->db->where('status', 0);
        $this->db->where('ip', $ip);
        $query = $this->db->get($this->log);

        if ($query->num_rows() >= $limit) {
            return false;
        }

        return true;
    }

    private function false_login($username)
    {
        $tgl = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $insert_log = array(
            "tgl" => $tgl,
            "username" => $username,
            "ip" => $ip,
            "user_agent" => $user_agent,
            "status" => 0,
        );
        $this->db->insert($this->log, $insert_log);

        return true;
    }

    public function get_user($username, $password)
    {
        $this->db->select('user_id,`name`, username, password, salt, birth, EMP_CODE, md.dept_code, md.dept_name, email, p.*');
        $this->db->join('mst_department as md', 'uc.dept_code = md.dept_code', 'inner');
        $this->db->join('privilege as p', 'uc.dept_code = p.dept_code', 'inner');
        $this->db->from('user_credentials as uc');
        $this->db->where('username', $username);
        $this->db->where('password_search', $password);

        return $this->db->get()->row_array();
    }

    public function update_log($token, $date)
    {
        $update_log = array("expired" => $date);
        $this->db->where('token', $token);
        $this->db->update($this->log, $update_log);
    }
}
