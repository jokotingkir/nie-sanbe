<?php

class M_nieDossier extends CI_model
{
    function __construct()
    {
        parent::__construct();
    }

    var $doc_1 = "mst_nie_doc1";
    var $doc_2 = "mst_nie_doc2";
    var $doc_3 = "mst_nie_doc3";
    var $doc_4 = "mst_nie_doc4";
    var $doc_5 = "mst_nie_doc5";

    public function get_submission($nie_alert_id)
    {
        $this->db->select('id, nie_alert_id, nie_doc1_code, nie_doc2_code, nie_doc3_code, nie_doc4_code, nie_doc5_code, accepted_check, submit_flag, submit_date, pic_dept, pic_submit');
        $this->db->from('nie_doc_submission');
        $this->db->where('nie_alert_id', $nie_alert_id);

        return $this->db->get()->result();
    }

    public function get_bpom_additional_doc($nie_alert_id)
    {
        $this->db->select('*');
        $this->db->from('nie_additional_doc');
        $this->db->where('nie_alert_id', $nie_alert_id);

        return $this->db->get()->result();
    }

    public function getNieDossier($id, $product_dossier_id = null)
    {
        if ($product_dossier_id != null) {
            $this->db->where_in('product_dossier_id', $product_dossier_id);
        }

        $this->db->where('nie_alert_id', $id);
        $this->db->select('id,nie_alert_id,dossier_name,product_dossier_id,rank,submit_pic,submit_date,receive_pic,receive_date,upload_path,1 as max_upload,submit_remark, receive_remark,s.name as submit_pic_name,r.name as receive_pic_name');
        $this->db->from('nie_dossier');
        $this->db->join('user_credentials s', 'submit_pic = s.username', 'LEFT');
        $this->db->join('user_credentials r', 'receive_pic = r.username', 'LEFT');
        $this->db->order_by('rank');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getProductDossier($product_type_id, $product_dossier_id = null)
    {
        if ($product_dossier_id != null) {
            $this->db->where_in('pd.id', $product_dossier_id);
        }

        $this->db->where('mst_product_type_id', $product_type_id);
        $this->db->select('pd.*,md.dossier_code,md.dossier_desc');
        $this->db->from('product_dossier pd');
        $this->db->join('mst_doc_dossier md', 'pd.mst_dossier_id = md.id', 'INNER');
        $this->db->order_by('pd.prank');

        return $this->db->get()->result_array();
    }

    private function getTreeLvlSubmissionCode($doc_lvl_3, $unlevel_3)
    {
        $this->db->select('d1.nie_doc1_code, d2.nie_doc2_code, d3.nie_doc3_code, d4.nie_doc4_code, d5.nie_doc5_code');
        $this->db->from($this->doc_2 . ' AS d2');
        $this->db->join($this->doc_3 . ' AS d3', 'd3.nie_doc2_code = d2.nie_doc2_code', 'left');
        $this->db->join($this->doc_1 . ' AS d1', 'd2.nie_doc1_code = d1.nie_doc1_code', 'inner');
        $this->db->join($this->doc_4 . ' AS d4', 'd4.nie_doc3_code = d3.nie_doc3_code', 'left');
        $this->db->join($this->doc_5 . ' AS d5', 'd5.nie_doc4_code = d4.nie_doc4_code', 'left');

        if (!empty($unlevel_3)) {
            $temp_unlevel_3 = $unlevel_3;
            for ($n = 0; $n < count($temp_unlevel_3); $n++) {
                $temp_unlevel_3[$n] = '"' . $temp_unlevel_3[$n] . '"';
            }
            $string_unlevel_3 = implode(",", $temp_unlevel_3);

            if (!empty($doc_lvl_3)) {
                $temp_doc_3 = $doc_lvl_3;
                for ($i = 0; $i < count($temp_doc_3); $i++) {
                    $temp_doc_3[$i] = '"' . $temp_doc_3[$i] . '"';
                }
                $string_lvl_3 = implode(",", $temp_doc_3);
            }

            if (isset($string_lvl_3)) {
                $this->db->where('d2.nie_doc2_code IN (' . $string_unlevel_3 . ') OR d3.nie_doc3_code IN (' . $string_lvl_3 . ')');
            } else {
                $this->db->where('d2.nie_doc2_code IN (' . $string_unlevel_3 . ')');
            }
        } else {
            $this->db->where_in('d3.nie_doc3_code', $doc_lvl_3);
        }
        $this->db->group_by('d2.nie_doc2_code, d3.nie_doc3_code, d4.nie_doc4_code, d5.nie_doc5_code');
        $this->db->order_by('d2.nie_doc2_code');

        return $this->db->get()->result_array();
    }

    public function insertNewSubmission($data, $nie_alert_id)
    {
        $temp = $this->getTreeLvlSubmissionCode($data->lvl_3, $data->unlevel_3);

        if (count($temp) > 0) {
            $insert_new_submission = array();
            for ($n = 0; $n < count($temp); $n++) {
                $insert_temp = array(
                    'nie_alert_id' => $nie_alert_id,
                    'nie_doc1_code' => $temp[$n]['nie_doc1_code'],
                    'nie_doc2_code' => $temp[$n]['nie_doc2_code'],
                    'nie_doc3_code' => $temp[$n]['nie_doc3_code'],
                    'nie_doc4_code' => $temp[$n]['nie_doc4_code'],
                    'nie_doc5_code' => $temp[$n]['nie_doc5_code'],
                    'accepted_check' => 'not received',
                    'submit_flag' => 'not submitted',
                    'pic_dept' => $data->pic_dept_code
                );
                array_push($insert_new_submission, $insert_temp);
            }

            $this->db->trans_start();
            $this->db->insert_batch('nie_doc_submission', $insert_new_submission);
            $this->db->trans_complete();

            if ($this->db->trans_status() === false) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function saveUploadSubmission($data)
    {
        $insert_submission_remark = array(
            'nie_alert_id' => $data['nie_alert_id'],
            'submission_remark' => $data['submission_remark'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('username')
        );

        $this->db->trans_start();
        $this->db->insert('submission_remark', $insert_submission_remark);
        $last_id = $this->db->insert_id();

        if (isset($data['document_path']) && count($data['document_path']) > 0) {
            for ($n = 0; $n < count($data['document_path']); $n++) {
                $data['document_path'][$n]['submission_remark_id'] = $last_id;
            }
        }

        $this->db->insert_batch('submission_upload', $data['document_path']);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    public function insertSubmission($data)
    {
        $submission_path = './uploads/Submissions/';

        if ($_FILES["upload_dok"]["name"] != "") {
            $nie_no = str_replace(' ', '_', $data['nie_no']);
            $dossier_name = str_replace(' ', '_', $data['dossier_name']);

            $target_path = $submission_path . $nie_no . '/' . $dossier_name;

            //Create folder berdasarkan NIE Number dan Nama Dossier
            if (!file_exists($target_path)) {
                mkdir($target_path, 0777, true);
            }

            $config['upload_path'] = $target_path;
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 0;

            $this->load->library('upload', $config);

            for ($n = 0; $n < count($_FILES["upload_dok"]["name"]); $n++) {
                $_FILES["doc"]["name"] = $_FILES["upload_dok"]["name"][$n];
                $_FILES["doc"]["type"] = $_FILES["upload_dok"]["type"][$n];
                $_FILES["doc"]["tmp_name"] = $_FILES["upload_dok"]["tmp_name"][$n];
                $_FILES["doc"]["error"] = $_FILES["upload_dok"]["error"][$n];
                $_FILES["doc"]["size"] = $_FILES["upload_dok"]["size"][$n];

                $this->upload->initialize($config);
                $this->upload->do_upload('doc');
            }
        }

        $data_insert = array(
            "nie_alert_id" => $data['nie_alert_id'],
            "dossier_name" => $data['dossier_name'],
            "product_dossier_id" => $data['product_dossier_id'],
            "rank" => $data['rank'],
            "submit_date" => $data['submit_date'],
            "submit_pic" => $data['submit_pic'],
            "submit_remark" => $data['submit_remark'],
            "upload_path" => $target_path,
        );

        $this->db->insert("nie_dossier", $data_insert);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSubmission($data)
    {
        $update = array(
            "receive_date" => $data["receive_date"],
            "receive_pic" => $data["receive_pic"],
            "receive_remark" => $data["receive_remark"],
        );

        $this->db->where("id", $data["id"]);
        $this->db->update("nie_dossier", $update);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getSubmissionbyId($nie_id)
    {
        $this->db->select('id,nie_alert_id,dossier_name,rank,submit_pic,submit_date,receive_pic,receive_date, s.EMP_FIRSTNAME AS submit_name, r.EMP_FIRSTNAME AS receive_name');
        $this->db->from('nie_dossier nd');
        $this->db->join('adm_mst_emp s', 'nd.submit_pic = s.EMP_CODE', 'LEFT');
        $this->db->join('adm_mst_emp r', 'nd.receive_pic = r.EMP_CODE', 'LEFT');
        $this->db->where('nie_alert_id', $nie_id);

        return $this->db->get()->result_array();
    }

    public function get_doc_submission()
    {
        $this->db->select('pd.id, prank, mst_dossier_id, dossier_desc, mst_product_type_id, pupload, psubmit_pic, psubmit_date, preceive_pic, preceive_date');
        $this->db->from('product_dossier pd');
        $this->db->join('mst_doc_dossier md', 'pd.mst_dossier_id = md.id', 'INNER');
        $this->db->distinct();

        return $this->db->get()->result();
    }

    public function product_dossier_privilege($privilege_id)
    {
        return $this->db->where('privilege_id', $privilege_id)->get('product_dossier_privilege')->result_array();
    }

    public function submission_doc_access($privilege_id)
    {
        $this->db->select('pdp_id, product_dossier_id, prank, dossier_code, dossier_desc, mst_product_type_id, is_submit, is_receive, is_upload');
        $this->db->from('product_dossier_privilege pdp');
        $this->db->join('product_dossier pd', 'pdp.product_dossier_id = pd.id', 'INNER');
        $this->db->join('mst_doc_dossier md', 'pd.mst_dossier_id = md.id', 'INNER');
        $this->db->where('privilege_id', $privilege_id);

        return $this->db->get()->result_array();
    }

    public function get_lvl_1()
    {
        return $this->db->select('nie_doc1_code as id, nie_doc1_desc as text')->get($this->doc_1)->result();
    }

    public function get_lvl_2($doc_lvl_1)
    {
        $this->db->select('nie_doc2_code as id, nie_doc2_desc as text, requirement');
        $this->db->where_in('nie_doc1_code', $doc_lvl_1);

        return $this->db->get($this->doc_2)->result();
    }

    public function get_lvl_3($doc_lvl_2)
    {
        $this->db->select('d2.nie_doc2_code as doc2_code, nie_doc3_code as id, nie_doc3_desc as text, d3.requirement');
        $this->db->where_in('d2.nie_doc2_code', $doc_lvl_2);
        $this->db->from($this->doc_2 . ' AS d2');
        $this->db->join($this->doc_3 . ' AS d3', 'd3.nie_doc2_code = d2.nie_doc2_code', 'LEFT');
        return $this->db->get()->result();
    }

    public function get_treeLvl_2($doc_lvl_2)
    {
        $this->db->select('d2.nie_doc1_code, d1.nie_doc1_desc, d2.nie_doc2_code, d2.nie_doc2_desc, d1.requirement as doc1_requirement, d2.requirement as doc2_requirement');
        $this->db->from($this->doc_2 . ' AS d2');
        $this->db->join($this->doc_1 . ' AS d1', 'd2.nie_doc1_code = d1.nie_doc1_code', 'inner');
        $this->db->where_in('d2.nie_doc2_code', $doc_lvl_2);

        return $this->db->get()->result();
    }

    public function get_treeLvl_45($doc_lvl_3, $unlevel_3)
    {
        $this->db->query('SET SESSION group_concat_max_len = 1000000');
        $this->db->select('d1.nie_doc1_code, d1.nie_doc1_desc, d1.requirement, d2.nie_doc2_code, d2.nie_doc2_desc, d2.requirement AS doc2_requirement, GROUP_CONCAT(DISTINCT CONCAT(d3.nie_doc3_code,"#",d3.nie_doc3_desc) SEPARATOR "$") AS child_lvl_3, GROUP_CONCAT(DISTINCT CONCAT(d4.nie_doc3_code,">",d4.nie_doc4_code,"#",d4.nie_doc4_desc) SEPARATOR "$") AS child_lvl_4, GROUP_CONCAT(DISTINCT CONCAT(d5.nie_doc4_code,">",d5.nie_doc5_code,"#",d5.nie_doc5_desc) SEPARATOR "$") AS child_lvl_5');
        $this->db->from($this->doc_2 . ' AS d2');
        $this->db->join($this->doc_3 . ' AS d3', 'd3.nie_doc2_code = d2.nie_doc2_code', 'left');
        $this->db->join($this->doc_1 . ' AS d1', 'd2.nie_doc1_code = d1.nie_doc1_code', 'inner');
        $this->db->join($this->doc_4 . ' AS d4', 'd4.nie_doc3_code = d3.nie_doc3_code', 'left');
        $this->db->join($this->doc_5 . ' AS d5', 'd5.nie_doc4_code = d4.nie_doc4_code', 'left');

        if (count($unlevel_3) > 0) {
            for ($n = 0; $n < count($unlevel_3); $n++) {
                $unlevel_3[$n] = '"' . $unlevel_3[$n] . '"';
            }
            $string_unlevel_3 = implode(",", $unlevel_3);

            if (count($doc_lvl_3) > 0) {
                for ($i = 0; $i < count($doc_lvl_3); $i++) {
                    $doc_lvl_3[$i] = '"' . $doc_lvl_3[$i] . '"';
                }
                $string_lvl_3 = implode(",", $doc_lvl_3);
            }

            if (isset($string_lvl_3)) {
                $this->db->where('d2.nie_doc2_code IN (' . $string_unlevel_3 . ') OR d3.nie_doc3_code IN (' . $string_lvl_3 . ')');
            } else {
                $this->db->where('d2.nie_doc2_code IN (' . $string_unlevel_3 . ')');
            }

            $this->db->group_by('d2.nie_doc2_code, d3.nie_doc2_code');
        } else {
            $this->db->where_in('d3.nie_doc3_code', $doc_lvl_3);
            $this->db->group_by('d3.nie_doc2_code');
        }
        $this->db->order_by('d2.nie_doc2_code');

        return $this->db->get()->result_array();
    }

    public function get_treeLvl_45_checked($nie_alert_id, $pic_dept = null)
    {
        $this->db->query('SET SESSION group_concat_max_len = 1000000');
        // $this->db->select('d1.nie_doc1_code, d1.nie_doc1_desc, d1.requirement, CONCAT( d2.nie_doc2_code, IF(nd.nie_doc3_code != "", "",CONCAT("@",nd.accepted_check)) ) AS nie_doc2_code, d2.nie_doc2_desc, d2.requirement AS doc2_requirement, GROUP_CONCAT( DISTINCT CONCAT( d3.nie_doc3_code, "#", d3.nie_doc3_desc, IF(nd.nie_doc4_code != "", "",CONCAT("@",nd.accepted_check)) ) SEPARATOR "$" ) AS child_lvl_3, GROUP_CONCAT( DISTINCT CONCAT( d4.nie_doc3_code, ">", d4.nie_doc4_code, "#", d4.nie_doc4_desc, IF(nd.nie_doc5_code != "", "",CONCAT("@",nd.accepted_check)) ) SEPARATOR "$" ) AS child_lvl_4, GROUP_CONCAT( DISTINCT CONCAT( d5.nie_doc4_code, ">", d5.nie_doc5_code, "#", d5.nie_doc5_desc, IF(nd.accepted_check != "", CONCAT("@",nd.accepted_check),"") ) SEPARATOR "$" ) AS child_lvl_5');
        $this->db->select('d1.nie_doc1_code, d1.nie_doc1_desc, d1.requirement, CONCAT( d2.nie_doc2_code, IF(nd.nie_doc3_code != "", "",CONCAT("@",nd.accepted_check,"@",nd.submit_flag,"@",nd.id)) ) AS nie_doc2_code, d2.nie_doc2_desc, d2.requirement AS doc2_requirement, GROUP_CONCAT( DISTINCT CONCAT( d3.nie_doc3_code, "#", d3.nie_doc3_desc, IF(nd.nie_doc4_code != "", "",CONCAT("@",nd.accepted_check,"@",nd.submit_flag,"@",nd.id)) ) SEPARATOR "$" ) AS child_lvl_3, GROUP_CONCAT( DISTINCT CONCAT( d4.nie_doc3_code, ">", d4.nie_doc4_code, "#", d4.nie_doc4_desc, IF(nd.nie_doc5_code != "", "",CONCAT("@",nd.accepted_check,"@",nd.submit_flag,"@",nd.id)) ) SEPARATOR "$" ) AS child_lvl_4, GROUP_CONCAT( DISTINCT CONCAT( d5.nie_doc4_code, ">", d5.nie_doc5_code, "#", d5.nie_doc5_desc, IF(nd.accepted_check != "", CONCAT("@",nd.accepted_check,"@",nd.submit_flag,"@",nd.id),"") ) SEPARATOR "$" ) AS child_lvl_5');
        $this->db->from('nie_doc_submission AS nd');
        $this->db->join($this->doc_1 . ' AS d1', 'nd.nie_doc1_code = d1.nie_doc1_code', 'INNER');
        $this->db->join($this->doc_2 . ' AS d2', 'nd.nie_doc2_code = d2.nie_doc2_code', 'INNER');
        $this->db->join($this->doc_3 . ' AS d3', 'nd.nie_doc3_code = d3.nie_doc3_code', 'LEFT');
        $this->db->join($this->doc_4 . ' AS d4', 'nd.nie_doc4_code = d4.nie_doc4_code', 'LEFT');
        $this->db->join($this->doc_5 . ' AS d5', 'nd.nie_doc5_code = d5.nie_doc5_code', 'LEFT');
        $this->db->where('nd.nie_alert_id', $nie_alert_id);

        if ($pic_dept != null) {
            $this->db->where('nd.pic_dept', $pic_dept);
        }
        $this->db->group_by('d2.nie_doc2_code, d3.nie_doc2_code');
        $this->db->order_by('d2.nie_doc2_code');

        return $this->db->get()->result_array();
    }

    public function get_submission_lvl3($nie_alert_id)
    {
        $this->db->select('nie_doc3_code');
        $this->db->from('nie_doc_submission');
        $this->db->where('nie_doc3_code IS NOT NULL AND nie_doc3_code != ""');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->group_by('nie_doc3_code');

        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            $data = array();
            for ($i = 0; $i < count($result); $i++) {
                array_push($data, $result[$i]['nie_doc3_code']);
            }
        }
        return $data;
    }

    public function get_submission_unlevel3($nie_alert_id)
    {
        $this->db->select('nie_doc2_code');
        $this->db->from('nie_doc_submission');
        $this->db->where('nie_doc3_code IS NULL OR nie_doc3_code = ""');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->group_by('nie_doc2_code');

        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            $data = array();
            for ($i = 0; $i < count($result); $i++) {
                array_push($data, $result[$i]['nie_doc2_code']);
            }
        }
        return $data;
    }

    public function doc_submission_by_alertid($nie_alert_id)
    {
        $this->db->select('id, nie_doc1_code, nie_doc2_code, nie_doc3_code, nie_doc4_code, nie_doc5_code');
        $this->db->from('nie_doc_submission');
        $this->db->where('nie_alert_id', $nie_alert_id);

        return $this->db->get()->result();
    }

    public function receive_submission($receive_data, $receive_doc)
    {
        // print_r($receive_doc);
        // die;
        for ($n = 0; $n < count($receive_doc); $n++) {
            $update_submission[] = array(
                'id' => $receive_doc[$n]['id'],
                'accepted_check' => $receive_doc[$n]['checked'],
            );
        }

        $receiver = array(
            'receive_pic' => $this->session->userdata('username'),
            'receive_date' => $receive_data['receive_date'],
            'receive_remark' => $receive_data['remark_receive']
        );

        $this->db->trans_start();
        $this->db->update_batch('nie_doc_submission', $update_submission, 'id');

        $this->db->where('nie_alert_id', $receive_data['nie_alert_id']);
        $this->db->update('nie_trans_submission', $receiver);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    public function get_doc_lvl1()
    {
        return $this->db->get($this->doc_1)->result();
    }

    public function add_new_doc1($new_doc1)
    {
        $data = $this->db->query('SELECT CAST(RIGHT(nie_doc1_code, 5) AS UNSIGNED) AS last_id FROM mst_nie_doc1 ORDER BY nie_doc1_code DESC LIMIT 1')->row();

        if (!empty($data) && property_exists($data, 'last_id') && isset($data->last_id)) {
            $insert_new = array(
                'nie_doc1_code' => 'ND' . ($data->last_id + 1),
                'nie_doc1_desc' => $new_doc1
            );

            $this->db->insert($this->doc_1, $insert_new);
            return true;
        } else {
            return false;
        }
    }

    public function get_submission_pic($keyword)
    {
        $this->db->select('user_id, EMP_CODE, name');
        $this->db->from('user_credentials');
        $this->db->like('name', $keyword, 'both');

        return $this->db->get()->result();
    }

    public function available_submission_dept($nie_alert_id)
    {
        $this->db->select('pic_dept');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->from('nie_doc_submission');
        $this->db->group_by('pic_dept');

        return $this->db->get()->result_array();
    }

    public function available_bpom_dept($nie_alert_id)
    {
        $this->db->select('pic_dept');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->from('nie_additional_doc');
        $this->db->group_by('pic_dept');

        return $this->db->get()->result_array();
    }

    public function updateReceiveDoc($data)
    {
        $this->db->where('id', $data['submission_id']);

        if ($data['is_bpom'] == "true") {
            $this->db->update('nie_additional_doc', array('accepted_check' => ($data['val'] == 'y') ? 'received' : 'not received'));
        } else {
            $this->db->update('nie_doc_submission', array('accepted_check' => ($data['val'] == 'y') ? 'received' : 'not received'));
        }

        return true;
    }

    public function submitDocSubmission($data)
    {
        $submit_data = array(
            'submit_flag' => $data['val'],
            'submit_date' => date('Y-m-d H:i:s'),
            'pic_submit' => $this->session->userdata('username')
        );

        $this->db->where('id', $data['submission_id']);

        if ($data['is_bpom'] == "true") {
            $this->db->update('nie_additional_doc', $submit_data);
        } else {
            $this->db->update('nie_doc_submission', $submit_data);
        }

        return true;
    }

    public function get_exist_submission($nie_alert_id)
    {
        $this->db->select('id');
        $this->db->from('nie_doc_submission');
        $this->db->where('nie_alert_id', $nie_alert_id);

        $res = $this->db->get();

        if ($res->num_rows() > 0) {
            return "exist";
        } else {
            return "not exist";
        }
    }

    public function get_submit_document($nie_alert_id)
    {
        $this->db->select('COUNT(id) AS jumlah');
        $this->db->where('submit_flag', 'not submitted');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->from('nie_doc_submission');

        $res = $this->db->get()->row_array();
        return (int) $res['jumlah'];
    }

    public function getUploadedDoc($nie_alert_id)
    {
        $submission_remark = $this->db->where('nie_alert_id', $nie_alert_id)->get('submission_remark')->result_array();

        if (count($submission_remark) > 0) {
            for ($v = 0; $v < count($submission_remark); $v++) {
                $submission_remark[$v]['document'] = $this->db->where('submission_remark_id', $submission_remark[$v]['remark_id'])->select('filepath, filename')->get('submission_upload')->result_array();
            }
        }

        return $submission_remark;
    }

    public function saveAdditionalBpom($data)
    {
        $dept = $data['pic_dept'];

        if (is_array($data['tambahan_bpom_' . $dept]) && count($data['tambahan_bpom_' . $dept]) > 0) {
            $temp_additional = array();
            $tambahan_data = $data['tambahan_bpom_' . $dept];
            for ($n = 0; $n < count($tambahan_data); $n++) {
                $row = array(
                    "nie_alert_id" => $data['alert_id'],
                    "add_doc_desc" => $tambahan_data[$n],
                    "accepted_check" => "not received",
                    "submit_flag" => "not submitted",
                    "pic_dept" => $data['pic_dept']
                );
                $temp_additional[] = $row;
            }

            $this->db->insert_batch("nie_additional_doc", $temp_additional);
        }

        if (isset($data['uploaded_path']) && $data['uploaded_path'] != "") {
            $this->db->where("id", $data['alert_id']);
            $this->db->update("nie_alert", array("bpom_add_data_dir" => $data['uploaded_path']));
        }

        return true;
    }

    public function additionalBpom($nie_alert_id, $pic_dept)
    {
        $this->db->select('*');
        $this->db->from('nie_additional_doc');
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->where('pic_dept', $pic_dept);

        return $this->db->get()->result();
    }

    public function getFileBpom($nie_alert_id)
    {
        $this->db->select('bpom_add_data_dir');
        $this->db->from('nie_alert');
        $this->db->where('id', $nie_alert_id);

        return $this->db->get()->row_array();
    }

    public function deleteSubmissionItem($document_id, $document_type)
    {
        if ($document_type == "submission") {
            $temp_doc = str_replace("ND", "", $document_id);
            $doc_id = $temp_doc[0];

            switch ($doc_id) {
                case "2":
                    $condition = "nie_doc2_code";
                    break;
                case "3":
                    $condition = "nie_doc3_code";
                    break;
                case "4":
                    $condition = "nie_doc4_code";
                    break;
                case "5":
                    $condition = "nie_doc5_code";
                    break;

                default:
                    $condition = "nie_doc1_code";
                    break;
            }

            $this->db->trans_start();
            $this->db->where($condition, $document_id);
            $this->db->delete("nie_doc_submission");
            $this->db->trans_complete();
        } else {
            $this->db->trans_start();
            $this->db->where("id", $document_id);
            $this->db->delete("nie_additional_doc");
            $this->db->trans_complete();
        }

        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }

    public function insertAdditionBpom($bpom_data, $nie_alert_id)
    {
        $additional = array();
        for ($n = 0; $n < count($bpom_data->data); $n++) {
            $t = array(
                "nie_alert_id" => $nie_alert_id,
                "add_doc_desc" => $bpom_data->data[$n],
                "accepted_check" => "not received",
                "submit_flag" => "not submitted",
                "pic_dept" => $bpom_data->pic_dept_name
            );
            $additional[] = $t;
        }

        $this->db->trans_start();
        $this->db->insert_batch("nie_additional_doc", $additional);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return false;
        } else {
            return true;
        }
    }
}
