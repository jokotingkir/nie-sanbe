<?php 

class M_nieStatus extends CI_model{

    public function getNieStatus()
    {
        $this->db->where('stat_status', 'Active');
        return $this->db->get('nie_status')->result_array();
    }
    
}
?>