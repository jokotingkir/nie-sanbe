<?php 

class M_storageCon extends CI_model{

    public function getStorageCon()
    {
        $this->db->where('storage_condition_status', 'Active');
        return $this->db->get('mst_storage_condition')->result_array();
    }

    public function getStorageById($storage_id)
    {
        $this->db->select('storage_condition_desc');
        $this->db->where('id', $storage_id);
        $this->db->from('mst_storage_condition');

        $data = $this->db->get()->row_array();

        return $data['storage_condition_desc'];
    }
    
}
