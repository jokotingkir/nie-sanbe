<?php

class M_nie extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_nieDossier', 'M_alert_schedule', 'M_nie_config', 'M_storageCon'));
        // $this->production = $this->load->database('production', TRUE);
    }

    //NIE Config untuk tipe Export;
    var $ex_start_schedule;
    var $ex_yellow_start;
    var $ex_red_start;

    //NIE Config untuk tipe Import;
    var $start_schedule;
    var $yellow_schedule;
    var $red_schedule;

    var $table = 'nie_alert';
    var $compotition_table = 'nie_compotition';
    // var $column_order = array(null, 'nie_no', 'PRODUCT_CODE', 'product_name', 'create_user', 'nie_start_date', 'nie_end_date', 'planning_area'); //set column field database for datatable orderable
    var $column_order = array(null, 'id', 'nie_no', 'PRODUCT_CODE', 'product_name', 'create_user', 'nie_date', 'nie_start_date', 'nie_end_date', 'planning_area', 'compotition', 'packaging_presentation', 'product_owner', 'submit_status', 'bpom_submit_status', 'expiration_flag', 'product_type_desc', 'export');
    var $column_search = array('nie_no', 'PRODUCT_CODE', 'product_name', 'brand_name', 'compotition', 'planning_area'); //set column field database for datatable searchable 

    var $column_order_sarana = array(null, 'planning_area', 'manufact_addr', 'produk_terdaftar', 'produksi', 'produk_ekspor');
    var $column_search_sarana = array('planning_area', 'manufact_addr');
    var $order = array('id' => 'asc'); // default order
    var $order_unit = array('planning_area' => 'asc');

    public function compotition_by_nie_alert($nie_alert_id)
    {
        return $this->db->where('nie_alert_id', $nie_alert_id)->get('nie_compotition')->result_array();
    }

    public function get_compotition_id($nie_alert_id)
    {
        return $this->db->where('nie_alert_id', $nie_alert_id)->select('id')->get('nie_compotition')->result_array();
    }

    public function items_nie()
    {
        $this->db->query('SET @row_number=0');
        $this->db->select('(@row_number:=@row_number+1) AS num,a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.compotition,a.packaging_presentation,a.product_owner,a.submit_status, b.product_type_desc, c.export')
            ->from('nie_alert as a')
            ->join('mst_product_type as b', 'a.product_type_id=b.id', 'INNER')
            ->join('mst_product as c', 'a.product_code=c.product_code', 'INNER')
            ->where_not_in('nie_no', array('', 'aaaaaaaaa'));

        return $this->db->get()->result_array();
    }

    public function get_nie_no($id)
    {
        return $this->db->where('id', $id)->select('nie_no')->get($this->table)->row();
    }

    public function item_nie($id)
    {
        $this->db->select('a.*, b.product_type_desc, c.export, d.storage_condition_desc')
            ->from('nie_alert as a')
            ->join('mst_product_type as b', 'a.product_type_id=b.id', 'LEFT')
            ->join('mst_product as c', 'a.product_code=c.product_code', 'LEFT')
            ->join('mst_storage_condition d', 'a.storage_condition_id=d.id', 'LEFT')
            ->where('a.id', $id);

        return $this->db->get()->result_array();
    }

    public function field_nie()
    {
        return $this->db->query('DESCRIBE nie_alert')->result_array();
    }

    public function nie_config()
    {
        $this->db->select('id, start_schedule, yellow_notif_start, red_notif_start, config_type');
        $this->db->where('status', 'Active');
        $this->db->from('nie_alert_config');

        return $this->db->get()->result_array();
    }

    public function getAgent()
    {
        $this->db->select('VENDOR_CODE,COMPANY_CODE,VENDOR_NAME,CATEGORY_CODE,CURRENCY_CODE');
        $this->db->where('CATEGORY_CODE', 8);
        $this->db->from('mst_vendor');
        return $this->db->get()->result_array();
    }

    private function manufacture_address($plann_area)
    {
        $id = "";
        switch ($plann_area) {
            case 'SANBE1':
                $id = "1";
                break;
            case 'SANBE2':
                $id = "2";
                break;
            case 'SANBE3':
                $id = "3";
                break;
            case 'CAPRI':
                $id = "4";
                break;
            default:
                $id = "5";
                break;
        }

        $query = $this->db->where('id', $id)->select('manufacture_address')->get('nie_man_add')->row();

        if ($query->manufacture_address <> "") {
            $address = $query->manufacture_address;
        } else {
            $address = "-";
        }

        return $address;
    }

    private function generate_alert_color($product_type, $end_date)
    {
        $this->db->select('green_notif_start, yellow_notif_start, red_notif_start');
        $this->db->from('nie_alert_config');
        $this->db->where('product_type_code', $product_type);
        $this->db->where('status', 'active');

        $res = $this->db->get()->row_array();

        $diff = get_month_diff(date('Y-m-d'), $end_date);
        $alert_color = "";

        switch ($diff) {
            case $diff > $res['green_notif_start']:
                $alert_color = "GRAY";
                break;

            case $diff <= $res['green_notif_start']:
            case $diff > $res['yellow_notif_start']:
                $alert_color = "GREEN";
                break;

            case $diff <= $res['yellow_notif_start']:
            case $diff > $res['red_notif_start']:
                $alert_color = "YELLOW";
                break;

            case $diff <= $res['red_notif_start']:
                $alert_color = "RED";
                break;
        }

        return $alert_color;
    }

    public function tambahDataNie($data)
    {
        $address = $this->manufacture_address($data['prodplan']);
        if (isset($data['new_prodnie']) && $data['new_prodnie'] != "") {
            $nie_no = $data['new_prodnie'];
        } else {
            $nie_no = $data['prodnie'];
        }

        $data_insert = array(
            "nie_no" => $nie_no,
            "alert_status" => 'On Going',
            "nie_status" => $data['niestat'],
            "nie_date" => $data['niestartdate'],
            "nie_start_date" => $data['niestartdate'],
            "nie_end_date" => $data['nieenddate'],
            "nie_holder" => $data['niehldr'],
            "nie_publisher" => $data['niepblsh'],
            "product_type" => $data['product_type_desc'],
            "product_type_id" => $data['prodtype'],
            "product_code" => $data['prodcode'],
            "product_name" => $data['prodname'],
            "product_owner" => $data['prodown'],
            "planning_area" => $data['prodplan'],
            "fd_form" => $data['prodform'],
            "packaging_presentation" => $data['prodpckg'],
            "shelf_life" => $data['prodlife'],
            "storage_condition_id" => $data['prodscon'],
            "storage_condition" => $this->M_storageCon->getStorageById($data['prodscon']),
            "brand_name" => $data['prodbrand'],
            "agent" => $data['prodagnt'],
            "manufact_addr" => $address,
            "country" => $data['prodctry'],
            "regulatory_code" => $data['prodrglt'],
            "dispatch_date" => $data['niedspdate'],
            "certificate" => $data['prodcrtf'],
            "receive_date" => $data['niercvdate'],
            "pic" => $data['pic'],
            "submit_status" => 'not submitted',
            "bpom_submit_status" => 'not submitted',
            "alert_color" => $this->generate_alert_color($data['product_type_desc'], $data['nieenddate']),
            // "compotition" => $data['prodcmpt'],
            "remark" => $data['remark'],
            "create_user" => $this->session->userdata('username'),
            "create_date" => date('Y-m-d H:i:s')
        );

        if (isset($data['full_path']) && $data['full_path'] <> "") {
            $data_insert['nie_certificate'] = $data['full_path'];
            $data_insert['certificate_type'] = $data['file_type'];
            $data_insert['certificate_size'] = $data['file_size'];
        }
        // print_r($data);
        // die;
        $this->db->insert('nie_alert', $data_insert);

        $last_id = $this->db->insert_id();

        $nie_compotition = array();
        if (is_array($data['compotition_name']) && count($data['compotition_name']) > 0) {
            for ($n = 0; $n < count($data['compotition_name']); $n++) {
                $compotition = array();
                $compotition['nie_alert_id'] = $last_id;
                $compotition['nie_no'] = $nie_no;
                $compotition['product_code'] = $data['prodcode'];
                $compotition['product_desc'] = $data['prodname'];
                $compotition['item_code'] = (empty($data['item_code'][$n])) ? "-" : $data['item_code'][$n];
                $compotition['item_desc'] = (empty($data['compotition_name'][$n])) ? "-" : $data['compotition_name'][$n];
                $compotition['strength'] = (empty($data['compotition_strength'][$n])) ? 0 : strToFloat($data['compotition_strength'][$n]);
                $compotition['uom'] = (empty($data['compotition_uom'][$n])) ? "-" : $data['compotition_uom'][$n];

                $nie_compotition[] = $compotition;
            }
        }

        $this->db->insert_batch($this->compotition_table, $nie_compotition);

        //Untuk mengubah nie status menjadi complete apabila yand didapat adalah submit_complete
        if (isset($data['nie_id'])) {
            $this->db->where('id', $data['nie_id'])->update($this->table, array('alert_status' => 'Completed'));
        }

        return $last_id;
    }

    private function _get_datatables_sarana()
    {
        if ($this->input->post('kategori_pencarian') && $this->input->post('kata_kunci')) {
            $kategori_pencarian = $this->input->post('kategori_pencarian');
            $kata_kunci = $this->input->post('kata_kunci');

            if ($kategori_pencarian == "nama_sarana") {
                $pencarian = "na.planning_area";
            } else {
                $pencarian = "na.manufact_addr";
            }

            $this->db->like($pencarian, $kata_kunci);
        }

        // $this->db->select("na.planning_area,manufact_addr,count( DISTINCT nie_no ) AS produk_terdaftar,count( na.PRODUCT_CODE ) AS produksi,SUM( IF ( UPPER( na.product_type ) = 'EXPORT', 1, 0 ) ) AS produk_ekspor");
        // $this->db->from("nie_alert na");
        // $this->db->join("mst_product mp", "na.PRODUCT_CODE = mp.PRODUCT_CODE", "LEFT");
        // $this->db->where("na.planning_area <>", "");
        // $this->db->group_by("na.planning_area");

        $this->db->select("manufacture_name, manufacture_address, COUNT(DISTINCT na.PRODUCT_CODE) AS produk_terdaftar, COUNT(DISTINCT na.PRODUCT_CODE) AS produksi, SUM(IF(UPPER(na.product_type) = 'EXPORT', 1, 0)) AS produk_ekspor");
        $this->db->from("nie_alert na");
        $this->db->join("nie_man_add nma", "na.planning_area = nma.manufacture_code", "INNER");
        $this->db->where("na.alert_status <> ", "discontinued");
        $this->db->group_by("na.planning_area");
        $this->db->order_by("manufacture_code = 'CAPRI', nma.id");


        $i = 0;
        foreach ($this->column_search_sarana as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sarana[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_unit)) {
            $order = $this->order_unit;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_filtered_sarana()
    {
        $this->_get_datatables_sarana();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_sarana()
    {
        $this->_get_datatables_sarana();
        $this->db->get();
        return $this->db->count_all_results();
    }

    public function itemSarana()
    {
        $this->db->select("na.planning_area,manufact_addr,count( id ) AS produk_terdaftar,count( mp.PRODUCT_CODE ) AS produksi,SUM( IF ( EXPORT IS NULL, 0, 1 ) ) AS produk_ekspor");
        $this->db->from("nie_alert na");
        $this->db->join("mst_product mp", "na.PRODUCT_CODE = mp.PRODUCT_CODE", "LEFT");
        $this->db->where("na.planning_area <>", null);
        $this->db->group_by("na.planning_area");
        return $this->db->get()->result_array();
    }

    public function getSarana()
    {
        $this->_get_datatables_sarana();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function getPercentageByAlert()
    {
        // $qry = $this->db->query("CALL new_percent_by_alert('SC')"); //Parameter SC artinya Show Chart
        // return $qry->result();

        $this->db->query('SET SESSION group_concat_max_len = 1000000');
        $this->db->select('alert_color, COUNT(id) AS jumlah, GROUP_CONCAT(id) AS nie_id');
        $this->db->from('nie_alert');
        $this->db->where("(nie_end_date IS NOT NULL OR nie_end_date <> '0000-00-00')", NULL, FALSE);
        $this->db->where('alert_status <> ', 'discontinued');
        $this->db->group_by('alert_color');

        $res = $this->db->get()->result();

        $data = array();

        foreach ($res as $row) {
            $temp = array();
            $temp['chart_id'] = '1';
            $temp['alert_color'] = $row->alert_color;
            $temp['jumlah'] = $row->jumlah;
            $temp['nie_id'] = $row->nie_id;

            switch ($row->alert_color) {
                case 'BLUE':
                    $temp['color'] = '#0e73cc';
                    break;
                case 'BROWN':
                    $temp['color'] = '#c4b487';
                    break;
                case 'GREEN':
                    $temp['color'] = '#45f442';
                    break;
                case 'PINK':
                    $temp['color'] = '#ff6e9e';
                    break;
                case 'YELLOW':
                    $temp['color'] = '#e8f441';
                    break;
                case 'RED':
                    $temp['color'] = '#ef1010';
                    break;

                default:
                    $temp['color'] = '#dbd5d7';
                    break;
            }

            $data[] = $temp;
        }

        return $data;
    }

    public function getPercentageByUnit()
    {
        $this->db->query("SET SESSION group_concat_max_len = 1000000");
        $this->db->select("'2' AS chart_id, COUNT(na.planning_area) AS cnt, na.planning_area, GROUP_CONCAT(CAST(id AS CHAR(7))) AS nie_id");
        $this->db->from($this->table . " as na");
        $this->db->join("mst_product as mp", "na.PRODUCT_CODE = mp.PRODUCT_CODE", "INNER");
        $this->db->where("na.planning_area <> ", "");
        $this->db->group_by("na.planning_area");

        return $this->db->get()->result_array();
    }

    // public function getPercentageByTrans()
    // {
    //     $this->db->query("SET SESSION group_concat_max_len = 1000000");
    //     $qry = $this->db->query("SELECT '3' AS chart_id, COUNT(nie_no) AS cnt,'EXPORT' AS `status`,GROUP_CONCAT(CAST(id AS CHAR(7))) AS nie_id FROM nie_alert na LEFT JOIN mst_product mp ON na.PRODUCT_CODE = mp.PRODUCT_CODE WHERE UPPER(na.product_type) = 'EXPORT' UNION SELECT '3' AS chart_id, COUNT(nie_no) AS cnt,'IMPORT' AS `status`,GROUP_CONCAT(CAST(id AS CHAR (7))) AS nie_id FROM nie_alert na LEFT JOIN mst_product mp ON na.PRODUCT_CODE = mp.PRODUCT_CODE WHERE UPPER(na.product_type) <> 'EXPORT'");
    //     return $qry->result();
    // }

    public function getPercentageByProdType()
    {
        $this->db->query("SET SESSION group_concat_max_len = 1000000");
        $qry = $this->db->query("SELECT '3' AS chart_id, product_type_code, product_type_desc, COUNT(na.id) AS count, GROUP_CONCAT(CAST(na.id AS CHAR(7))) AS nie_id FROM nie_alert na LEFT JOIN mst_product_type mpt ON na.product_type_id = mpt.id GROUP BY mpt.id");
        return $qry->result();
    }

    public function getNieById($id)
    {
        $this->db->select('nie.*, type.product_type_desc', false);
        $this->db->from('nie_alert nie');
        $this->db->join('mst_product_type type', 'nie.product_type_id = type.id', 'left');
        $this->db->where('nie.id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateDataNie($data)
    {
        $address = $this->manufacture_address($data['prodplan']);
        $data_update = array(
            "nie_no" => $data['prodnie'],
            "alert_status" => 'On Going',
            "nie_status" => $data['niestat'],
            "nie_date" => $data['niestartdate'],
            "nie_start_date" => $data['niestartdate'],
            "nie_end_date" => $data['nieenddate'],
            "nie_holder" => $data['niehldr'],
            "nie_publisher" => $data['niepblsh'],
            //karena ini di disabled pada saat di edit atau alias ga boleh di edit makanya ga usah di pake
            // "product_type_id" => $data['prodtype'],
            "product_code" => $data['prodcode'],
            "product_name" => $data['prodname'],
            "product_owner" => $data['prodown'],
            "planning_area" => $data['prodplan'],
            "fd_form" => $data['prodform'],
            "packaging_presentation" => $data['prodpckg'],
            "shelf_life" => $data['prodlife'],
            "storage_condition_id" => $data['prodscon'],
            "brand_name" => $data['prodbrand'],
            "agent" => $data['prodagnt'],
            "manufact_addr" => $address,
            "country" => $data['prodctry'],
            "regulatory_code" => $data['prodrglt'],
            "dispatch_date" => $data['niedspdate'],
            "certificate" => $data['prodcrtf'],
            "receive_date" => $data['niercvdate'],
            "pic" => $data['pic'],
            // "compotition" => $data['prodcmpt'],
            "remark" => $data['remark'],
            "update_user" => '8856/1181112260',
            "update_date" => date('Y-m-d H:i:s')
        );

        if (isset($data['full_path']) && $data['full_path'] <> "") {
            $data_update['nie_certificate'] = $data['full_path'];
            $data_update['certificate_type'] = $data['file_type'];
            $data_update['certificate_size'] = $data['file_size'];
        }

        $this->db->where("id", $data['nie_id']);
        $this->db->update("nie_alert", $data_update);

        $nie_compotition = $this->get_compotition_id($data['nie_id']);

        if (is_array($nie_compotition) && count($nie_compotition) > 0) {
            $compotition_id = array();
            for ($p = 0; $p < count($nie_compotition); $p++) {
                array_push($compotition_id, $nie_compotition[$p]['id']);
            }

            if (count($data['compotition_id']) > 0) {
                $for_update = array_intersect($compotition_id, $data['compotition_id']); //untuk mencari id yg sama, jika id sama maka lakukan update berdasarkan compotition id
                $for_delete = array_diff($compotition_id, $data['compotition_id']); //untuk mencari id yg berbeda dari id yang ada terhadap yang di posting, jika ada lakukan delete
                $for_insert = array_diff($data['compotition_id'], $compotition_id); //untuk mencari id yg berbeda dari id yang di posting terhadap id yang ada, jika ada lakukan insert pada flag "new"

                //update,delete,insert jika terjadi perubahan pada compotition
                if (count($for_update) > 0) {
                    $update_compotition = array();
                    for ($n = 0; $n < count($for_update); $n++) {
                        $key = array_search($for_update[$n], $data['compotition_id']);
                        $row['id'] = $data['compotition_id'][$key];
                        $row['item_code'] = $data['item_code'][$key];
                        $row['item_desc'] = $data['compotition_name'][$key];
                        $row['strength'] = $data['compotition_strength'][$key];
                        $row['uom'] = $data['compotition_uom'][$key];
                        array_push($update_compotition, $row);
                    }
                    $this->db->update_batch('nie_compotition', $update_compotition, 'id');
                }

                if (count($for_delete) > 0) {
                    $this->db->where_in('id', $for_delete);
                    $this->db->delete('nie_compotition');
                }

                if (count($for_insert) > 0) {
                    $insert_compotition = array();
                    for ($n = 0; $n < count($for_insert); $n++) {
                        $key = array_search($for_insert[$n], $data['compotition_id']);
                        $row['nie_alert_id'] = $data['nie_id'];
                        $row['product_code'] = $data['prodcode'];
                        $row['product_desc'] = $data['prodname'];
                        $row['item_code'] = $data['item_code'][$key];
                        $row['item_desc'] = $data['compotition_name'][$key];
                        $row['strength'] = $data['compotition_strength'][$key];
                        $row['uom'] = $data['compotition_uom'][$key];
                        array_push($insert_compotition, $row);
                    }
                    $this->db->insert_batch('nie_compotition', $insert_compotition);
                }
            }
        }
    }

    public function getPIC($keyword)
    {
        $this->db->select('EMP_FIRSTNAME, EMP_CODE');
        $this->db->from('adm_mst_emp');
        $this->db->where('EMP_CODE <>', NULL);
        $this->db->where('EMP_CODE <>', "");
        $this->db->like('EMP_FIRSTNAME', $keyword, 'both');

        return $this->db->get()->result();
    }

    private function _get_datatables_query()
    {
        //Set nie config untuk filter berdasarkan alert color saja
        /*
        if ($this->input->post('nie_config')) {
            $nie_config = $this->input->post('nie_config');

            if (count($nie_config) != 0) {
                for ($n = 0; $n < count($nie_config); $n++) {
                    if (strtoupper($nie_config[$n]["config_type"]) == "EXPORT") {
                        $x_start_schedule = $nie_config[$n]["start_schedule"];
                        $x_yellow_schedule = $nie_config[$n]["yellow_notif_start"];
                        $x_red_schedule = $nie_config[$n]["red_notif_start"];
                    } else {
                        $start_schedule = $nie_config[$n]["start_schedule"];
                        $yellow_schedule = $nie_config[$n]["yellow_notif_start"];
                        $red_schedule = $nie_config[$n]["red_notif_start"];
                    }
                }
            }
        }
        */

        if ($this->input->post('kategori_pencarian') && $this->input->post('kata_kunci')) {
            $filter_color = "default";
            $kategori_pencarian = $this->input->post('kategori_pencarian');
            $kata_kunci = $this->input->post('kata_kunci');

            switch ($kategori_pencarian) {
                case "PRODUCT_CODE":
                    $pencarian = "a.PRODUCT_CODE";
                    break;
                case "product_name":
                    $pencarian = "a.product_name";
                    break;
                case "brand_name":
                    $pencarian = "a.brand_name";
                    break;
                case "compotition":
                    $pencarian = "a.compotition";
                    break;
                case "planning_area":
                    $pencarian = "a.planning_area";
                    break;

                default:
                    $pencarian = "a.nie_no";
                    break;
            }

            //Gunakan parameter ketiga 'before' atau 'after' jika diperlukan (sama seperti penggunaan "%" pada like, both jika keduanya %%)
            $this->db->like($pencarian, $kata_kunci);
        } else {
            $filter_color = $this->input->post('filter_color');
        }
        // Untuk sementara ditambahkan field product type hasil upload data dari template NIE yang diterima tgl 02-07-2019 dari desy & nurul
        if ($filter_color != "default") {
            // Script di bawah di hide karena semua nie harus ditampilkan meskipun sudah melebihi bulan atau tahun saat ini
            // $sql_filter_color = " LEFT JOIN mst_product as c ON a.PRODUCT_CODE=c.PRODUCT_CODE LEFT JOIN mst_product_type as b ON a.product_type_id=b.id WHERE YEAR(a.nie_end_date) >= DATE_FORMAT(NOW(),'%Y') ";

            /*
            $column_filter_color = " a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.compotition,a.packaging_presentation,a.product_owner,a.submit_status, a.bpom_submit_status, a.expiration_flag, a.brand_name, b.product_type_desc, c.export, TIMESTAMPDIFF( MONTH, DATE_FORMAT( NOW( ), '%Y-%m-%d' ), a.nie_end_date ) AS month_diff, a.product_type, a.nie_status, a.nie_holder, a.fd_form, a.shelf_life, a.storage_condition, a.manufact_addr, a.remark, (SELECT GROUP_CONCAT(item_desc SEPARATOR '; ') FROM nie_compotition nc WHERE nc.nie_alert_id = a.id) AS compotition FROM nie_alert a ";
            $sql_filter_color = " LEFT JOIN mst_product as c ON a.PRODUCT_CODE=c.PRODUCT_CODE LEFT JOIN mst_product_type as b ON a.product_type_id=b.id ";
            switch ($filter_color) {
                case "green":
                    $export = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $x_start_schedule . " AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $start_schedule . " AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "yellow":
                    $export = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $x_start_schedule . " AND TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $x_red_schedule . ") AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $start_schedule . " AND TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $red_schedule . ") AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "red":
                    $export = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $x_red_schedule . " AND NOT TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0) AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $red_schedule . " AND NOT TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0) AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "blue":
                    $blue_condition = " WHERE UPPER(a.expiration_flag) = 'EXPIRED' AND UPPER(a.bpom_submit_status) = 'SUBMITTED' ";
                    break;
                case "brown":
                    $brown_condition = " WHERE UPPER(a.expiration_flag) = 'EXPIRED' AND UPPER(a.bpom_submit_status) = 'NOT SUBMITTED' ";
                    break;
            }

            $query_filter_color = "";

            if ($filter_color != "blue" && $filter_color != "brown") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $export . " UNION " . "SELECT " . $column_filter_color . $sql_filter_color . $import . " ORDER BY id";
            }

            if ($filter_color == "blue") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $blue_condition . " ORDER BY id";
            }

            if ($filter_color == "brown") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $brown_condition . " ORDER BY id";
            }

            return $query_filter_color;
            */
            $this->db->where('alert_color', $filter_color);
        }

        // $this->db->select('a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.packaging_presentation,a.product_owner,a.submit_status, a.bpom_submit_status, a.expiration_flag, a.brand_name, b.product_type_desc, c.export, TIMESTAMPDIFF( MONTH, DATE_FORMAT( NOW( ), "%Y-%m-%d" ), a.nie_end_date ) AS month_diff, (SELECT GROUP_CONCAT(item_desc SEPARATOR "; ") FROM nie_compotition nc WHERE nc.nie_alert_id = a.id) AS compotition, a.product_type, a.nie_status, a.nie_holder, a.fd_form, a.shelf_life, a.storage_condition, a.manufact_addr, a.remark');
        $this->db->select('a.id, a.nie_no, a.PRODUCT_CODE, a.product_name, a.pic, a.nie_date, a.nie_start_date, a.nie_end_date, a.planning_area, a.packaging_presentation, a.product_owner, a.manufact_addr, a.submit_status, a.bpom_submit_status, a.expiration_flag, a.brand_name, b.product_type_desc, c.export, a.alert_color, ( SELECT GROUP_CONCAT( item_desc SEPARATOR "; " ) FROM nie_compotition nc WHERE nc.nie_alert_id = a.id ) AS compotition, a.product_type, a.nie_status, a.nie_holder, a.fd_form, a.shelf_life, a.storage_condition, a.manufact_addr, a.remark');
        $this->db->from($this->table . ' as a');
        $this->db->where("(a.nie_end_date IS NOT NULL OR a.nie_end_date <> '0000-00-00')", NULL, FALSE);
        $this->db->where('a.alert_status <> ', 'discontinued');
        $this->db->where('a.alert_status <> ', 'completed');
        $this->db->join('mst_product as c', 'a.product_code=c.product_code', 'LEFT');
        $this->db->join('mst_product_type as b', 'a.product_type_id=b.id', 'LEFT');


        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if (isset($_POST['search']['value'])) {
                if ($_POST['search']['value']) // if datatable send POST for search
                {

                    if ($i === 0) // first loop
                    {
                        $this->db->group_start(); // open bracket.
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if (count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        if ($filter_color == "default") {
            if (isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($this->order)) {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select('a.id');
        $this->db->from($this->table . ' as a');
        $this->db->where("(a.nie_end_date IS NOT NULL OR a.nie_end_date <> '0000-00-00')", NULL, FALSE);
        $this->db->where('a.alert_status <> ', 'discontinued');
        $this->db->where('a.alert_status <> ', 'completed');
        $this->db->join('mst_product as c', 'a.product_code=c.product_code', 'LEFT');
        $this->db->join('mst_product_type as b', 'a.product_type_id=b.id', 'LEFT');
        //Agar data NIE untuk tahun yang sudah lewat dari tahun saat ini tidak ditampilkan
        // $this->db->where("YEAR(a.nie_end_date) >= ", date("Y"));
        return $this->db->count_all_results();
    }

    public function getProductHistory($product_code)
    {
        $this->db->select('PRODUCT_CODE, product_name, nie_no, nie_start_date, nie_end_date, alert_status');
        $this->db->where('PRODUCT_CODE', $product_code);
        $this->db->where('alert_status', 'Completed');
        $this->db->from('nie_alert');
        $this->db->order_by('UNIX_TIMESTAMP(DATE(nie_end_date))', 'ASC');

        return $this->db->get()->result_array();
    }

    //$type untuk export atau import
    private function nieConfigbyType()
    {
        $this->db->select('id, start_schedule, yellow_notif_start, red_notif_start, config_type');
        $this->db->where('status', 'Active');
        $this->db->from('nie_alert_config');

        return $this->db->get()->result_array();
    }

    // public function drillDownByAlert($alert_color)
    // {
    //     $query = $this->db->query("CALL expand_by_alert('" . $alert_color . "')");
    //     return $query->result();
    // }

    private function _datatables_drilldown()
    {
        $this->db->query("SET SESSION group_concat_max_len = 1000000");
        $this->db->select('a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.pic,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.compotition,a.packaging_presentation,a.product_owner,a.manufact_addr,a.submit_status, a.brand_name, b.product_type_desc, c.export, a.product_type, (SELECT GROUP_CONCAT(nc.item_desc SEPARATOR "; ") FROM nie_compotition nc WHERE nc.nie_alert_id = a.id) AS compotition');
        $this->db->from($this->table . ' as a');
        $this->db->join('mst_product_type as b', 'a.product_type_id=b.id', 'LEFT');
        $this->db->join('mst_product as c', 'a.product_code=c.product_code', 'LEFT');
        $this->db->where_in("a.id", explode(',', $this->input->post('nie_id')));

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_drilldown()
    {
        $this->_datatables_drilldown();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_drilldown()
    {
        $this->_datatables_drilldown();
        $this->db->get();
        return $this->db->count_all_results();
    }

    public function filtered_drilldown()
    {
        $this->_datatables_drilldown();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCompotitionByAlertId($id)
    {
        $this->db->where('nie_alert_id', $id);
        return $this->db->get('nie_compotition')->result_array();
    }

    //Tadinya kirain cuma buat generate semua data, buat jaga2 mending pake yg di bawah aja getExcelReport
    // public function getReport()
    // {
    //     $this->db->query("SET SESSION group_concat_max_len = 1000000");
    //     $this->db->select('na.nie_no, nie_status, PRODUCT_CODE, product_name, create_user AS pic, nie_date, nie_start_date, nie_end_date, planning_area, nie_holder, product_type, fd_form, packaging_presentation, shelf_life, storage_condition, manufact_addr, remark, TIMESTAMPDIFF( MONTH, DATE_FORMAT( NOW( ), "%Y-%m-%d" ), nie_end_date ) AS month_diff, (SELECT GROUP_CONCAT(CONCAT(item_desc,"#",strength,"#",uom) SEPARATOR "$") FROM nie_compotition nc WHERE nc.nie_no = na.nie_no) AS compotition');
    //     $this->db->from($this->table . ' AS na');

    //     return $this->db->get()->result();
    // }

    private function _get_excel_query()
    {
        //Set nie config untuk filter berdasarkan alert color saja
        /*
        if ($this->input->post('nie_config')) {
            $nie_config = $this->input->post('nie_config');
            if (!is_array($nie_config)) {
                $nie_config = json_decode($nie_config, true);
            }
            if (count($nie_config) != 0) {
                for ($n = 0; $n < count($nie_config); $n++) {
                    if (strtoupper($nie_config[$n]["config_type"]) == "EXPORT") {
                        $x_start_schedule = $nie_config[$n]["start_schedule"];
                        $x_yellow_schedule = $nie_config[$n]["yellow_notif_start"];
                        $x_red_schedule = $nie_config[$n]["red_notif_start"];
                    } else {
                        $start_schedule = $nie_config[$n]["start_schedule"];
                        $yellow_schedule = $nie_config[$n]["yellow_notif_start"];
                        $red_schedule = $nie_config[$n]["red_notif_start"];
                    }
                }
            }
        }
        */

        if ($this->input->post('kategori_pencarian') && $this->input->post('kata_kunci')) {
            $filter_color = "default";
            $kategori_pencarian = $this->input->post('kategori_pencarian');
            $kata_kunci = $this->input->post('kata_kunci');

            switch ($kategori_pencarian) {
                case "PRODUCT_CODE":
                    $pencarian = "a.PRODUCT_CODE";
                    break;
                case "product_name":
                    $pencarian = "a.product_name";
                    break;
                case "brand_name":
                    $pencarian = "a.brand_name";
                    break;
                case "compotition":
                    $pencarian = "a.compotition";
                    break;
                case "planning_area":
                    $pencarian = "a.planning_area";
                    break;

                default:
                    $pencarian = "a.nie_no";
                    break;
            }

            //Gunakan parameter ketiga 'before' atau 'after' jika diperlukan (sama seperti penggunaan "%" pada like)
            $this->db->like($pencarian, $kata_kunci);
        } else {
            $filter_color = $this->input->post('filter_color');
        }
        // Untuk sementara ditambahkan field product type hasil upload data dari template NIE yang diterima tgl 02-07-2019 dari desy & nurul
        if ($filter_color != "default") {
            // Script di bawah di hide karena semua nie harus ditampilkan meskipun sudah melebihi bulan atau tahun saat ini
            // $sql_filter_color = " LEFT JOIN mst_product as c ON a.PRODUCT_CODE=c.PRODUCT_CODE LEFT JOIN mst_product_type as b ON a.product_type_id=b.id WHERE YEAR(a.nie_end_date) >= DATE_FORMAT(NOW(),'%Y') ";
            /*
            $column_filter_color = " a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.compotition,a.packaging_presentation,a.product_owner,a.submit_status, a.brand_name, b.product_type_desc, c.export, TIMESTAMPDIFF( MONTH, DATE_FORMAT( NOW( ), '%Y-%m-%d' ), a.nie_end_date ) AS month_diff, a.product_type, a.nie_status, a.nie_holder, a.fd_form, a.shelf_life, a.storage_condition, a.manufact_addr, a.remark, (SELECT GROUP_CONCAT(CONCAT(item_desc,'#',strength,'#',uom) SEPARATOR '$') FROM nie_compotition nc WHERE nc.nie_no = a.nie_no) AS compotition FROM nie_alert a ";
            $sql_filter_color = " LEFT JOIN mst_product as c ON a.PRODUCT_CODE=c.PRODUCT_CODE LEFT JOIN mst_product_type as b ON a.product_type_id=b.id ";
            switch ($filter_color) {
                case "green":
                    $export = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $x_start_schedule . " AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $start_schedule . " AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "yellow":
                    $export = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $x_start_schedule . " AND TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $x_red_schedule . ") AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $start_schedule . " AND TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) > " . $red_schedule . ") AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "red":
                    $export = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $x_red_schedule . " AND NOT TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0) AND UPPER(a.product_type) = 'EXPORT' ";
                    $import = " WHERE (TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) <= " . $red_schedule . " AND NOT TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0) AND UPPER(a.product_type) <> 'EXPORT' ";
                    break;
                case "blue":
                    $blue_condition = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0 AND (submit_status IS NOT NULL AND submit_status = 'Y') ";
                    break;
                case "brown":
                    $brown_condition = " WHERE TIMESTAMPDIFF(MONTH, DATE_FORMAT(NOW(),'%Y-%m-%d'), nie_end_date) < 0 AND (submit_status IS NULL OR submit_status = 'N') ";
                    break;
            }

            $query_filter_color = "";

            if ($filter_color != "blue" && $filter_color != "brown") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $export . " UNION " . "SELECT " . $column_filter_color . $sql_filter_color . $import . " ORDER BY id";
            }

            if ($filter_color == "blue") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $blue_condition . " ORDER BY id";
            }

            if ($filter_color == "brown") {
                $query_filter_color = "SELECT " . $column_filter_color . $sql_filter_color . $brown_condition . " ORDER BY id";
            }


            return $query_filter_color;
            */
            $this->db->where('alert_color', $filter_color);
        }

        $this->db->select('a.id,a.nie_no,a.PRODUCT_CODE,a.product_name,a.create_user,a.nie_date,a.nie_start_date,a.nie_end_date,a.planning_area,a.packaging_presentation,a.product_owner,a.submit_status, a.brand_name, b.product_type_desc, c.export, TIMESTAMPDIFF( MONTH, DATE_FORMAT( NOW( ), "%Y-%m-%d" ), a.nie_end_date ) AS month_diff, (SELECT GROUP_CONCAT(CONCAT(item_desc,"#",strength,"#",uom) SEPARATOR "$") FROM nie_compotition nc WHERE nc.nie_no = a.nie_no) AS compotition, a.product_type, a.nie_status, a.nie_holder, a.fd_form, a.shelf_life, a.storage_condition, a.manufact_addr, a.remark, a.alert_color');
        $this->db->from($this->table . ' as a');
        $this->db->where("(a.nie_end_date IS NOT NULL OR a.nie_end_date <> '0000-00-00')", NULL, FALSE);
        $this->db->where('a.alert_status <> ', 'discontinued');
        $this->db->join('mst_product as c', 'a.product_code=c.product_code', 'LEFT');
        $this->db->join('mst_product_type as b', 'a.product_type_id=b.id', 'LEFT');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if (isset($_POST['search']['value'])) {
                if ($_POST['search']['value']) // if datatable send POST for search
                {

                    if ($i === 0) // first loop
                    {
                        $this->db->group_start(); // open bracket.
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if (count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        if ($filter_color == "default") {
            if (isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($this->order)) {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
    }

    public function getExcelReport()
    {
        if ($this->input->post('kategori_pencarian') && $this->input->post('kata_kunci')) {
            $filter_color = "default";
        } else {
            $filter_color = $this->input->post('filter_color');
        }

        $this->db->query("SET SESSION group_concat_max_len = 1000000");
        if ($filter_color != "default") {
            $qry = $this->_get_excel_query();
            return $this->db->query($qry)->result();
        } else {
            $this->_get_excel_query();
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function getLastNieNumber($product_code)
    {
        $this->db->select('id, nie_no, nie_date, nie_start_date, nie_end_date');
        $this->db->where('PRODUCT_CODE', $product_code);

        return $this->db->get($this->table)->result();
    }

    public function submit_bpom_status($nie_alert_id)
    {
        $this->db->where('id', $nie_alert_id);
        $this->db->update($this->table, array('bpom_submit_status' => 'submitted'));

        //Untuk sementara, nanti tolong diubah lagi ya
        $this->db->query('CALL new_percent_by_alert("UC")');
    }

    public function getCurrentNie($nie_no)
    {
        $this->db->select('id');
        $this->db->where('nie_no', $nie_no);
        $this->db->from($this->table);
        $this->db->order_by('nie_end_date', 'desc');
        $this->db->limit(1);

        return $this->db->get()->row();
    }

    /*
    *---------------------------------------------------------------------------
    * HANYA DIGUNAKAN UNTUK METHOD BULK SCHEDULE DI NIE CONTROLLER
    *---------------------------------------------------------------------------
    */
    // public function get_all_data()
    // {
    //     $this->production->select('id AS nie_alert_id, nie_no AS prodnie, nie_date, nie_start_date AS niestartdate, nie_end_date AS nieenddate, product_type_id, product_type');

    //     return $this->production->get($this->table)->result_array();
    // }
}
