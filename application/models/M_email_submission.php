<?php

class M_email_submission extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_user'));
    }

    public function sendSubmissionEmail($nie_alert_id, $submission, $planning_area)
    {
        $from_email = $this->session->userdata('email');
        // $recipient = $this->M_user->getEmailbyDept($submission->pic_dept_code, $planning_area);
        $recipient = $this->M_user->getEmailbyDept($submission->pic_dept_code);
        $link = base_url() . "submission-process/" . $nie_alert_id;

        $this->load->library('phpmailer_library');
        $mail = $this->phpmailer_library->load();

        $mail->isSMTP();
        $mail->Host = 'smtps.sanbe-farma.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin@smtps.sanbe-farma.com';
        $mail->Password = 'ITdeptsystem';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom($from_email, "NIE Admin Registration");
        $mail->addReplyTo($from_email, "NIE Admin Registration");

        if (is_array($recipient) && count($recipient) > 0) {
            $temp = array();
            for ($j = 0; $j < count($recipient); $j++) {
                array_push($temp, $recipient[$j]['email']);
            }
            if (isset($submission->email_recipients)) {
                $to_email = array_unique(array_merge($temp, $submission->email_recipients));
            } else {
                $to_email = array_unique($temp);
            }
        } else {
            $to_email = $submission->email_recipients;
        }
        // print_r($to_email);
        // die;
        if (count($to_email) > 1) {
            for ($c = 0; $c < count($to_email); $c++) {
                $temp = explode('@', $to_email[$c]);
                $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

                if ($c == 0) {
                    $mail->addAddress($to_email[$c], $recipient_name);
                } else {
                    $mail->AddCC($to_email[$c], $recipient_name);
                }
            }
        } else {
            $temp = explode('@', $to_email[0]);
            $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

            $mail->addAddress($to_email[0], $recipient_name);
        }

        $mail->Subject = 'NIE Submission Monitoring';
        $mail->isHTML(true);

        $mail->Body = 'Klik <a href="' . $link . '">link</a> berikut untuk melihat permintaan dokumen';

        if (!$mail->send()) {
            $res = array(
                'status' => 'failed',
                'error_log' => $mail->ErrorInfo,
                'sender_from' => $from_email,
                'recipients' => implode(';', $to_email),
                'message' => $mail->Body,
                'dept_code' => $submission->pic_dept_code,
                'nie_alert_id' => $nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        } else {
            $res = array(
                'status' => 'sent',
                'error_log' => '',
                'sender_from' => $from_email,
                'recipients' => implode(';', $to_email),
                'message' => $mail->Body,
                'dept_code' => $submission->pic_dept_code,
                'nie_alert_id' => $nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        return $res;
    }

    public function sendBpomEmail($nie_alert_id, $bpom, $planning_area)
    { 
        $from_email = $this->session->userdata('email');
        // $recipient = $this->M_user->getEmailbyDept($submission->pic_dept_code, $planning_area);
        $recipient = $this->M_user->getEmailbyDept($bpom->pic_dept_code);
        $link = base_url() . "submission-process/" . $nie_alert_id;

        $this->load->library('phpmailer_library');
        $mail = $this->phpmailer_library->load();

        $mail->isSMTP();
        $mail->Host = 'smtps.sanbe-farma.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin@smtps.sanbe-farma.com';
        $mail->Password = 'ITdeptsystem';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom($from_email, "NIE Admin Registration");
        $mail->addReplyTo($from_email, "NIE Admin Registration");

        if (is_array($recipient) && count($recipient) > 0) {
            $temp = array();
            for ($j = 0; $j < count($recipient); $j++) {
                array_push($temp, $recipient[$j]['email']);
            }
            if (isset($bpom->email_recipients)) {
                $to_email = array_unique(array_merge($temp, $bpom->email_recipients));
            } else {
                $to_email = array_unique($temp);
            }
        } else {
            $to_email = $bpom->email_recipients;
        }
        // print_r($to_email);
        // die;
        if (count($to_email) > 1) {
            for ($c = 0; $c < count($to_email); $c++) {
                $temp = explode('@', $to_email[$c]);
                $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

                if ($c == 0) {
                    $mail->addAddress($to_email[$c], $recipient_name);
                } else {
                    $mail->AddCC($to_email[$c], $recipient_name);
                }
            }
        } else {
            $temp = explode('@', $to_email[0]);
            $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

            $mail->addAddress($to_email[0], $recipient_name);
        }

        $mail->Subject = 'NIE Submission Monitoring';
        $mail->isHTML(true);

        $mail->Body = 'Klik <a href="' . $link . '">link</a> berikut untuk melihat permintaan dokumen tambahan BPOM';

        if (!$mail->send()) {
            $res = array(
                'status' => 'failed',
                'error_log' => $mail->ErrorInfo,
                'sender_from' => $from_email,
                'recipients' => implode(';', $to_email),
                'message' => $mail->Body,
                'dept_code' => $bpom->pic_dept_code,
                'nie_alert_id' => $nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        } else {
            $res = array(
                'status' => 'sent',
                'error_log' => '',
                'sender_from' => $from_email,
                'recipients' => implode(';', $to_email),
                'message' => $mail->Body,
                'dept_code' => $bpom->pic_dept_code,
                'nie_alert_id' => $nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        return $res;
    }

    public function sendTambahanBpomEmail($alert_id, $pic_dept)
    {
        $email = $this->getEmailLog($alert_id, $pic_dept); //ngedapetin penerima email

        if (!empty($email) && count($email) > 0) {
            $from_email = $this->session->userdata('email');

            $link = base_url() . "submission-process/" . $alert_id;

            $this->load->library('phpmailer_library');
            $mail = $this->phpmailer_library->load();

            $mail->isSMTP();
            $mail->Host = 'smtps.sanbe-farma.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'admin@smtps.sanbe-farma.com';
            $mail->Password = 'ITdeptsystem';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->setFrom($from_email, "NIE Admin Registration");
            $mail->addReplyTo($from_email, "NIE Admin Registration");

            $recipient_tmp = explode(';', $email['recipients']);
            for ($j = 0; $j < count($recipient_tmp); $j++) {
                $temp = explode('@', $recipient_tmp[$j]);
                $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

                if ($j == 0) {
                    $mail->addAddress($recipient_tmp[$j], $recipient_name);
                } else {
                    $mail->AddCC($recipient_tmp[$j], $recipient_name);
                }
            }

            $mail->Subject = 'NIE Submission Monitoring';
            $mail->isHTML(true);

            $mail->Body = 'Klik <a href="' . $link . '">link</a> berikut untuk melihat permintaan dokumen tambahan BPOM';

            if (!$mail->send()) {
                $res = array(
                    'status' => 'failed',
                    'error_log' => $mail->ErrorInfo,
                    'sender_from' => $from_email,
                    'recipients' => $email['recipients'],
                    'message' => $mail->Body,
                    'dept_code' => $pic_dept,
                    'nie_alert_id' => $alert_id,
                    'created_at' => date('Y-m-d H:i:s')
                );
            } else {
                $res = array(
                    'status' => 'sent',
                    'error_log' => '',
                    'sender_from' => $from_email,
                    'recipients' => $email['recipients'],
                    'message' => $mail->Body,
                    'dept_code' => $pic_dept,
                    'nie_alert_id' => $alert_id,
                    'created_at' => date('Y-m-d H:i:s')
                );
            }

            return $res;
        }
    }

    private function getEmailLog($alert_id, $pic_dept)
    {
        $this->db->select('recipients');
        $this->db->from('submission_email_log');
        $this->db->where('nie_alert_id', $alert_id);
        $this->db->where('dept_code', $pic_dept);

        return $this->db->get()->row_array();
    }

    public function getEmailLogItem($email_no)
    {
        $this->db->where('no', $email_no);
        return $this->db->get('submission_email_log')->row();
    }

    public function saveEmailSubmissionLog($data)
    {
        $this->db->insert('submission_email_log', $data);
        return true;
    }

    public function updateEmailSubmissionLog($data, $email_no)
    {
        $this->db->where('no', $email_no);
        $this->db->update('submission_email_log', $data);
    }

    public function getFailedEmail($nie_alert_id, $dept_code, $is_bpom)
    {
        $this->db->where('nie_alert_id', $nie_alert_id);
        $this->db->where('dept_code', $dept_code);
        $this->db->where('status', 'failed');
        $this->db->not_like('message', $is_bpom, 'before');

        return $this->db->get('submission_email_log')->row_array();
    }

    public function resendEmail($email_no)
    {
        $email = $this->getEmailLogItem($email_no);

        $from_email = $this->session->userdata('email');

        $this->load->library('phpmailer_library');
        $mail = $this->phpmailer_library->load();

        $mail->isSMTP();
        $mail->Host = 'smtps.sanbe-farma.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin@smtps.sanbe-farma.com';
        $mail->Password = 'ITdeptsystem';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom($from_email, "NIE Admin Registration");
        $mail->addReplyTo($from_email, "NIE Admin Registration");

        $recipient_tmp = explode(';', $email->recipients);
        for ($j = 0; $j < count($recipient_tmp); $j++) {
            $temp = explode('@', $recipient_tmp[$j]);
            $recipient_name = ucwords(str_replace('_', ' ', $temp[0]));

            if ($j == 0) {
                $mail->addAddress($recipient_tmp[$j], $recipient_name);
            } else {
                $mail->AddCC($recipient_tmp[$j], $recipient_name);
            }
        }

        $mail->Subject = 'NIE Submission Monitoring';
        $mail->isHTML(true);

        $mail->Body = $email->message;

        if (!$mail->send()) {
            $res = array(
                'status' => 'failed',
                'error_log' => $mail->ErrorInfo,
                'sender_from' => $from_email,
                'recipients' => $email->recipients,
                'message' => $mail->Body,
                'dept_code' => $email->dept_code,
                'nie_alert_id' => $email->nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        } else {
            $res = array(
                'status' => 'sent',
                'error_log' => '',
                'sender_from' => $from_email,
                'recipients' => $email->recipients,
                'message' => $mail->Body,
                'dept_code' => $email->dept_code,
                'nie_alert_id' => $email->nie_alert_id,
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        return $res;
    }
}
