<?php

class M_mst_item extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    var $table = 'mst_item';

    public function get_items($keyword)
    {
        $this->db->select('ITEM_CODE,ITEM_DESC');
        $this->db->from($this->table);
        $this->db->like('ITEM_DESC', $keyword, 'both');

        return $this->db->get()->result();
    }
}
