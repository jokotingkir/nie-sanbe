<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function capitalize($word)
{
    $temp = str_replace("_", " ", $word);
    return ucwords($temp);
}

function show_access($access, $key)
{
    $temp = explode(";", $access);
    $label = str_replace($key, "", $temp[0]);
    $label = str_replace("_", " ", $label);

    if ((int) $temp[1] == 0) {
        $sign = "<i class='fa fa-times' aria-hidden='true' style='color: red; font-size: 12pt;'></i>&nbsp;";
    } else {
        $sign = "<i class='fa fa-check' aria-hidden='true' style='color: forestgreen; font-size: 12pt;'></i>&nbsp;";
    }

    if ($key == 'page')
        $label = 'View ' . $label;

    return "<label>" . $sign . ucwords($label) . "</label>";
}

function add_access($access, $key)
{
    $temp = explode(";", $access);
    $label = str_replace($key, "", $temp[0]);
    $label = str_replace("_", " ", $label);

    if ((int) $temp[1] == 1) {
        $html = "<input type='checkbox' class='sub-menu' name='" . $temp[0] . "' id='" . $temp[0] . "' checked='checked' />&nbsp;";
    } else {
        $html = "<input type='checkbox' class='sub-menu' name='" . $temp[0] . "' id='" . $temp[0] . "' />&nbsp;";
    }

    if ($key == 'page')
        $label = 'View ' . $label;

    $html .= "<label for='" . $temp[0] . "'>" . ucwords($label) . "</label>";
    return $html;
}

function strToFloat($val)
{
    if (strpos($val, ',') != FALSE)
        $val = str_replace(',', '.', $val);

    return floatval($val);
}

//untuk menampilkan pesan jika transaksi berhasil atau gagal
function show_alert($is_error, $message)
{
    ($is_error) ? $type = "success" : $type = "danger";

    $alert = '<div class="alert alert-' . $type . '" role="alert">' . $message;
    $alert = $alert . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
    $alert = $alert . '<span aria-hidden="true">&times;</span>' . '</button></div>';

    return $alert;
}

function alertOptions($config, $month_diff)
{
    if ($month_diff > $config['start_schedule']) {
        $color = "green";
    } else if ($month_diff <= $config['start_schedule'] && $month_diff > $config['red_schedule']) {
        $color = "yellow";
    } else if ($month_diff <= $config['red_schedule']) {
        $color = "red";
    }

    return $color;
}

function get_month_diff($start, $end = FALSE)
{
    $end or $end = time();
    $start = new DateTime("$start");
    $end   = new DateTime("$end");
    $diff  = $start->diff($end);
    
    return $diff->format('%y') * 12 + $diff->format('%m');
}
