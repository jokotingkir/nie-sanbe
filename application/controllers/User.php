<?php

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_user', 'CI_encrypt', 'M_privilege'));
    }

    var $user_rules = array(
        array(
            'field' => 'emp_code',
            'label' => 'Employee Code',
            'rules' => 'trim|required',
            'errors' =>
            array(
                'required' => '{field} tidak boleh kosong'
            )
        ),
        array(
            'field' => 'nama_lengkap',
            'label' => 'Employee Name',
            'rules' => 'trim|required|callback_alpha_dash_space',
            'errors' =>
            array(
                'required' => '{field} tidak boleh kosong'
            )
        ),
        array(
            'field' => 'tgl_lahir',
            'label' => 'Birth Date',
            'rules' => 'required',
            'errors' =>
            array(
                'required' => '{field} tidak boleh kosong'
            )
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email',
            'errors' =>
            array(
                'required' => '{field} tidak boleh kosong',
                'valid_email' => '{field} tidak valid'
            )
        ),
        array(
            'field' => 'privilege',
            'label' => 'User Level',
            'rules' => 'required',
            'errors' =>
            array(
                'required' => '{field} tidak boleh kosong'
            )
        )
    );

    public function index()
    {
        if (!$this->session->userdata('user_page') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        $title['judul'] = 'User List';

        $this->load->view('templates/header', $title);
        $this->load->view('user/user_list');
        $this->load->view('templates/footer');
    }

    public function user_list()
    {
        $list = $this->M_user->get_user_list();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $user) {
            $no++;
            $row = array();
            $row['num'] = $no;
            $row['user_id'] = $user->user_id;
            $row['name'] = $user->name;
            $row['birth'] = date("d-m-Y", strtotime($user->birth));
            $row['privilege'] = $user->privilege_id;
            $row['dept_name'] = $user->dept_name;
            $row['username'] = $user->username;
            $row['email'] = $user->email;
            $row['emp_code'] = $user->EMP_CODE;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_user->count_all(),
            "recordsFiltered" => $this->M_user->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function alpha_dash_space($fullname)
    {
        if ($fullname != "") {
            if (!preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
                $this->form_validation->set_message('alpha_dash_space', '%s tidak boleh mengandung angka');
                return FALSE;
            } else {
                return TRUE;
            }
        }
    }

    public function add_user()
    {
        if (!$this->session->userdata('add_user') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules($this->user_rules);
            $this->form_validation->set_error_delimiters('<small style="color: red;"><i>', '</i></small>');

            if ($this->form_validation->run() != FALSE) {
                $message = $this->save();
                $this->session->set_flashdata("message", $message);
                redirect("User");
            }
        }

        $title['judul'] = 'Add User';

        $data['user_level'] = $this->M_privilege->exist_department();
        $this->load->view('templates/header', $title);
        $this->load->view('user/add_user', $data);
        $this->load->view('templates/footer');
    }

    public function getUserLevel()
    {
        $group = $this->input->post('user_group');
        if (!empty($group)) {
            $dept = $this->M_privilege->available_department($group);

            echo json_encode($dept);
        }
    }

    private function save()
    {
        $email = $this->input->post('email');
        $emp_code = $this->input->post('emp_code');
        $name = $this->input->post('nama_lengkap');
        $birth = $this->input->post('tgl_lahir');
        $privilege_id = $this->input->post('privilege');
        $dept_code = $this->input->post('dept_code');

        $split_name = explode(" ", trim($name));
        $split_birth = explode("-", $birth);

        $username = strtolower($split_name[0]); //nama pertama dari kiri
        $password = strtolower($split_name[0]);
        $split_birth[0] = substr($split_birth[0], 2, 4); //hanya ambil 2 digit terakhir dari tahun kelahiran
        for ($n = 0; $n < count($split_birth); $n++) {
            $password .= $split_birth[$n]; //gabungan dari nama pertama sebelah kiri + yymmdd (tanggal lahir)
        }

        $rand_salt = $this->CI_encrypt->genRandSalt();
        $encrypt_password = $this->CI_encrypt->encryptUserPwd($password, $rand_salt);
        $md5 = md5($password); //md5 hanya digunakan untuk menemukan user dan rand salt yang cocok pada saat login

        $data = array(
            "EMP_CODE" => $emp_code,
            "email" => trim($email),
            "name" => trim($name),
            "birth" => $birth,
            "username" => $username,
            "password" => $encrypt_password,
            "salt" => $rand_salt,
            "password_search" => $md5,
            "privilege_id" => $privilege_id,
            "dept_code" => $dept_code,
            "created_at" => date("Y-m-d H:i:s"),
            "created_by" => $this->session->userdata("username"),
        );

        if ($this->M_user->add_new_user($data)) {
            $message = show_alert(true, "Data pengguna berhasil ditambahkan");
        } else {
            $message = show_alert(true, "Something wrong when add new user, please try again!");
        }

        return $message;
    }

    private function get_employee_by_id($user_id)
    {
        $user = $this->M_user->get_user($user_id);
        if (!empty($user)) {
            foreach ($user as $row) {
                $data['user_id'] = $user_id;
                $data['emp_code'] = $row->EMP_CODE;
                $data['name'] = $row->name;
                $data['birth'] = $row->birth;
                $data['email'] = $row->email;
                $data['dept_code'] = $row->dept_code;
                $data['privilege_id'] = $row->privilege_id;
            }
        } else {
            $data = array();
        }

        return $data;
    }

    public function edit_user($user_id = null)
    {
        if (!$this->session->userdata('edit_user') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        if ($user_id != null) {
            $data = $this->get_employee_by_id($user_id);
        }

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules($this->user_rules);
            $this->form_validation->set_error_delimiters('<small style="color: red;"><i>', '</i></small>');

            if ($this->form_validation->run() != FALSE) {
                $update = array(
                    "user_id" => $this->input->post('user_id'),
                    "email" => $this->input->post('email'),
                    "emp_code" => $this->input->post('emp_code'),
                    "name" => $this->input->post('nama_lengkap'),
                    "birth" => $this->input->post('tgl_lahir'),
                    "privilege_id" => $this->input->post('privilege'),
                    "dept_code" => $this->input->post('dept_code')
                );

                if ($this->M_user->update_user($update)) {
                    $message = show_alert(true, "Data berhasil diperbarui");
                } else {
                    $message = show_alert(false, "Gagal memperbarui data");
                }

                $this->session->set_flashdata("message", $message);
                redirect("User");
            } else {
                $id = $this->input->post('user_id');
                $data = $this->get_employee_by_id($id);
            }
        }

        $title['judul'] = 'Edit User';
        $data['user_level'] = $this->M_privilege->exist_department();

        $this->load->view('templates/header', $title);
        $this->load->view('user/edit_user', $data);
        $this->load->view('templates/footer');
    }

    public function reset_password()
    {
        $user_email = $this->input->post('user_email');

        if ($this->M_user->isExistEmail($user_email)) {
            if ($this->M_user->resetUserPassword($user_email)) {
                $response = array(
                    'status' => 'success',
                    'message' => 'Email reset password telah dikirim'
                );
            } else {
                $response = array(
                    'status' => 'failed',
                    'message' => 'Gagal mengirim email, ulangi beberapa saat lagi'
                );
            }
        } else {
            $response = array(
                'status' => 'not found',
                'message' => 'Email tidak ditemukan'
            );
        }

        echo json_encode($response);
    }
}
