<?php

class Privilege extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_privilege', 'M_nieDossier'));
    }

    public $top_class_level = "REG_NEW";
    public $top_level = "PLANT MANAGER";

    public function index($dept_code = "")
    {
        if(!$this->session->userdata('privilege_page') == '1'){
            $this->load->view('restricted_page');
            return false;
        }

        $title['judul'] = 'Privilege';

        $columns = $this->M_privilege->privilege_columns();

        //cari data pertama untuk ditampilkan
        $result = $this->M_privilege->get_privilege_list($dept_code);

        //jika data tersedia di tabel privilege
        if (!empty($result)) {
            $row = array();
            for ($n = 0; $n < count($columns); $n++) {
                $field = $columns[$n]["Field"];
                if (!in_array($field, array('id', 'dept_code'))) {
                    if (!isset($temp)) {
                        $temp = array();
                    }

                    if (strpos($field, "page") != false) {
                        $main_key_cols = str_replace("_page", "", $field); //selalu gunakan _page untuk menambahkan main page baru di database

                        if (count($temp) != 0) {
                            array_push($row, $temp);
                            unset($temp['sub']);
                        }
                        $temp['main'] = $field;
                        $temp['key'] = $main_key_cols;
                        $temp['main_value'] = $result->$field;
                        $temp['sub'] = array();
                    } else {
                        if (strpos($field, $main_key_cols) != false) {
                            $temp['sub'][] = $field . ';' . $result->$field;
                        }
                    }
                }
            }
            //untuk looping yang terakhir supaya tetap masuk ke dalam baris array
            array_push($row, $temp);
            unset($temp['sub']);
        }

        if (isset($row) && count($row) != 0) {
            $data['data_privilege'] = $row;
            // print_r($data['data_privilege']);
            // die;
            // Kalau mau nampilin dokumen apa aja yang dibolehin, dibuka aja tag comment nya
            // $data['submission_doc'] = $this->M_privilege->get_access_submission($result->id);
        }

        $data['privilege_columns'] = $columns;
        $data['department'] = $this->M_privilege->all_department();
        if (!empty($result)) {
            $_code = $result->dept_code;
        } elseif ($dept_code != "") {
            $_code = $dept_code;
        }

        $data['selected_dept_code'] = $_code;

        $this->load->view('templates/header', $title);
        $this->load->view('privilege/privilege_list', $data);
        $this->load->view('templates/footer');
    }

    public function add_level_privilege()
    {
        $title['judul'] = 'Add Level Privilege';

        $columns = $this->M_privilege->privilege_columns();

        $row = array();
        for ($n = 0; $n < count($columns); $n++) {
            $field = $columns[$n]["Field"];
            if (!in_array($field, array('id', 'dept_code'))) {
                if (!isset($temp)) {
                    $temp = array();
                }
                if (strpos($field, "page") != false) {
                    $main_key_cols = str_replace("_page", "", $field); //selalu gunakan _page untuk menambahkan main page baru di database

                    if (count($temp) != 0) {
                        array_push($row, $temp);
                        unset($temp['sub']);
                    }
                    $temp['main'] = $field;
                    $temp['key'] = $main_key_cols;
                    $temp['main_value'] = 0; //set nilai jadi 0 untuk kasus tambah privilege level baru
                    $temp['sub'] = array();
                } else {
                    if (strpos($field, $main_key_cols) != false) {
                        $temp['sub'][] = $field . ';' . 0; //set nilai jadi 0 untuk kasus tambah privilege level baru
                    }
                }
            }
        }
        //untuk looping yang terakhir supaya tetap masuk ke dalam baris array
        array_push($row, $temp);
        unset($temp['sub']);

        $data['privilege_columns'] = $columns;
        $data['data_privilege'] = $row;
        // $data['submission_doc'] = $this->M_nieDossier->get_doc_submission();
        $data['department'] = $this->M_privilege->get_list_department();
        $data['class_dept'] = $this->M_privilege->get_class_department();

        $this->load->view('templates/header', $title);
        $this->load->view('privilege/add_privilege', $data);
        $this->load->view('templates/footer');
    }

    public function add_new_level()
    {
        if ($this->input->post('submit')) {
            $new_dept = $this->input->post('new_level');
            //Create department code
            //Jika hanya terdapat 1 kata, maka kata tersebut diguanakan sebagai department code
            //Jika lebih dari 1 kata dengan spasi, maka ambil huruf pertama dari setiap kata untuk dijadikan sebagai department code
            $dept_code = "";
            $count_word = str_word_count($new_dept);
            if ($count_word == 1) {
                $dept_code = $new_dept;
            } else {
                $words = explode(" ", $new_dept);
                foreach ($words as $w) {
                    $dept_code .= $w[0];
                }
            }

            //untuk di table privilege
            $privilege_cols = $this->M_privilege->privilege_columns();
            $privilege_access = array();
            for ($n = 0; $n < count($privilege_cols); $n++) {
                $field = $privilege_cols[$n]["Field"];
                if (!in_array($field, array('id', 'dept_code'))) {
                    if ($this->input->post($field)) {
                        $privilege_access[$field] = 1;
                    } else {
                        $privilege_access[$field] = 0;
                    }
                }
            }

            $this->M_privilege->add_privilege($new_dept, $dept_code, $privilege_access);
            redirect('privilege/search/' . $dept_code);
        }
    }

    public function edit_privilege($dept_code)
    {
        $title['judul'] = 'Edit Akses';

        $columns = $this->M_privilege->privilege_columns();
        $result = $this->M_privilege->get_privilege_list($dept_code);

        if (!empty($result)) {
            $row = array();
            for ($n = 0; $n < count($columns); $n++) {
                $field = $columns[$n]["Field"];
                if (!in_array($field, array('id', 'dept_code'))) {
                    if (!isset($temp)) {
                        $temp = array();
                    }

                    if (strpos($field, "page") != false) {
                        $main_key_cols = str_replace("_page", "", $field); //selalu gunakan _page untuk menambahkan main page baru di database

                        if (count($temp) != 0) {
                            array_push($row, $temp);
                            unset($temp['sub']);
                        }
                        $temp['main'] = $field;
                        $temp['key'] = $main_key_cols;
                        $temp['main_value'] = $result->$field;
                        $temp['sub'] = array();
                    } else {
                        if (strpos($field, $main_key_cols) != false) {
                            $temp['sub'][] = $field . ';' . $result->$field;
                        }
                    }
                }
            }
            //untuk looping yang terakhir supaya tetap masuk ke dalam baris array
            array_push($row, $temp);
            unset($temp['sub']);

            //untuk filter saat redirect ke halaman list privilege setelah edit
            $data['selected_dept_code'] = $dept_code;

            $data['privilege_id'] = $result->id;
            $data['data_privilege'] = $row;
        } else {
            redirect('privilege/search/' . $dept_code);
        }

        $data['department'] = $this->M_privilege->all_department();

        // $access_doc = $this->M_nieDossier->product_dossier_privilege($result->id);
        // $doc = $this->M_nieDossier->get_doc_submission();

        // $v = 0;
        // if (count($doc) > 0) {
        //     foreach ($doc as $row) {
        //         if (count($access_doc) > 0) {
        //             for ($n = 0; $n < count($access_doc); $n++) {
        //                 if ($access_doc[$n]['product_dossier_id'] == $row->id) {
        //                     $doc[$v]->privilege_dossier_id = (isset($access_doc[$v]['pdp_id'])) ? $access_doc[$n]['pdp_id'] : 0;
        //                     $doc[$v]->is_submit = $access_doc[$n]['is_submit'];
        //                     $doc[$v]->is_receive = $access_doc[$n]['is_receive'];
        //                     $doc[$v]->is_upload = $access_doc[$n]['is_upload'];
        //                 }
        //             }
        //             $v += 1;
        //         } else {
        //             //jika data tidak tersedia di tabel product dossier privilege
        //             $doc[$v]->privilege_dossier_id = 0;
        //             $doc[$v]->is_submit = 'N';
        //             $doc[$v]->is_receive = 'N';
        //             $doc[$v]->is_upload = 'N';

        //             $v += 1;
        //         }
        //     }
        // }

        // $data['submission_doc'] = $doc;
        // if ($result->submission_page == 1) {
        //     $data['allowed_submission_doc'] = 1;
        // }

        $this->load->view('templates/header', $title);
        $this->load->view('privilege/edit_privilege', $data);
        $this->load->view('templates/footer');
    }

    public function update_level()
    {
        if ($this->input->post('submit')) {
            $privilege_cols = $this->M_privilege->privilege_columns();
            $update_data = array();
            for ($n = 0; $n < count($privilege_cols); $n++) {
                $field = $privilege_cols[$n]["Field"];
                if (!in_array($field, array('id', 'dept_code'))) {
                    if ($this->input->post($field)) {
                        $update_data[$field] = 1;
                    } else {
                        $update_data[$field] = 0;
                    }
                }
            }
            $dept_code = $this->input->post('dept_code');
            $privilege_id = $this->input->post('privilege_id');

            $this->M_privilege->update_privilege($privilege_id, $update_data);
            redirect("privilege/search/" . $dept_code, "refresh");
        }
    }

    public function delete_level($dept_code)
    {
        if ($this->M_privilege->deletePrivilege($dept_code)) {
            redirect('privilege');
        } else {
            redirect('privilege/search/' . $dept_code);
        }
    }
}
