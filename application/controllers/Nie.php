<?php

class Nie extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_user', 'M_nie', 'M_product', 'M_productType', 'M_storageCon', 'M_nieStatus', 'M_regulatory', 'M_certificate', 'M_manAddress', 'M_nieDossier', 'M_auth', 'M_mst_item', 'M_nie_config', 'M_alert_schedule', 'M_product_letter', 'M_privilege', 'M_email_submission'));
        $this->load->library('form_validation');
    }

    var $is_error = false;

    public function index()
    {
        $judul = "Cek Produk SANBE";
        $view = "nie/home";

        //Untuk mengecek jika user pertama kali login dengan password default,
        //maka user harus mengubah password terlebih dahulu
        if ($this->session->userdata('logged_user')) {
            if ($this->M_user->is_change_password($this->session->userdata('logged_user'))) {
                $judul = "Ubah Password";
                $view = "auth/change_password";
            }
        } else {
            $this->load->view('restricted_page');
            return false;
        }

        $title['judul'] = $judul;

        $this->load->view('templates/header', $title);
        $this->load->view($view);
        $this->load->view('templates/footer');
    }

    public function bulkSchedule()
    {
        if (!$this->session->userdata('logged_user')) {
            $this->load->view('restricted_page');
            return false;
        }

        /* --------------------------------------------------------------------
        * CREATE NEW SCHEDULE
        * --------------------------------------------------------------------*/
        // $result = array();
        // $data_nie = $this->M_nie->get_all_data();
        // // print_r($data_nie);
        // // die;

        // foreach ($data_nie as $row) {
        //     $product_type = $this->M_productType->type_by_id($row['product_type_id']);
        //     if (count($product_type) <> 0) {

        //         /*-- MENDAPATKAN TIPE CONFIG DARI TABLE MST_PRODUCT_TYPE --*/
        //         if ($product_type['product_type_code'] == "EXPORT") {
        //             $type_config = "EXPORT";
        //         } else {
        //             $type_config = "NON EXPORT";
        //         }
        //         $nie_config = $this->M_nie_config->get_config($type_config);
        //         /*-- --------------------------------------------------- --*/

        //         /*-- MENDAPATKAN NIE CONFIG DARI TABLE NIE_ALERT_CONFIG --*/
        //         if (count($nie_config) <> 0) {
        //             $this->M_alert_schedule->createSchedule($nie_config, $row);
        //             $message = $row['nie_alert_id'] . " <-> " . $row['prodnie'] . " --- SUCCESS";
        //         } else {
        //             $message = $row['nie_alert_id'] . " <-> " . $row['prodnie'] . " --- FAILED";
        //         }

        //         array_push($result, $message);
        //     }
        // }
        // print_r($result);
        // die;
    }

    public function tambah()
    {
        if (!$this->session->userdata('add_nie_product') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        if ($this->input->post('submit_nie') || $this->input->post('submit_complete')) {
            $insert_nie = $this->input->post();

            if (!empty($_FILES['nie_cert']['name'])) {
                //Upload nie sertifikat
                $digits = 3;
                $rand_number = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

                if ($insert_nie['new_prodnie'] != "") {
                    $filename = $insert_nie['new_prodnie'] . '-' . $rand_number;
                } else {
                    $filename = $insert_nie['prodnie'] . '-' . $rand_number;
                }

                $filename = str_replace(' ', '', $filename);
                $config['upload_path'] = './uploads/Certificates/';
                $config['allowed_types'] = 'pdf'; //validasi ekstensi .pdf
                $config['max_size'] = 10240; //10MB //validasi ukuran file yang di upload
                $config['file_name'] = $filename;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('nie_cert')) {
                    $tmp = $this->upload->data();
                    $insert_nie['file_type'] = $tmp['file_type'];
                    $insert_nie['file_size'] = $tmp['file_size'];
                    // $insert_nie['full_path'] = file_get_contents($tmp['full_path']);
                    $insert_nie['full_path'] = $config['upload_path'] . $filename;
                } else {
                    $message = show_alert(true, $this->upload->display_errors());
                }
            }

            //simpan data nie ke table nie_alert dan dapatkan last id nya
            $product_type = $this->M_productType->type_by_id($insert_nie['prodtype']);
            $insert_nie['product_type_desc'] = $product_type['product_type_desc'];

            if ($insert_nie['product_type_desc'] != 'export') {
                $insert_nie['prodcrtf'] = "";
                $insert_nie['prodagnt'] = "";
                $insert_nie['prodrglt'] = "";
            }

            $nie_alert = $this->M_nie->tambahDataNie($insert_nie);
            if ($nie_alert) {

                /*-- MENDAPATKAN NIE CONFIG DARI PRODUCT TYPE --*/
                $nie_config = $this->M_nie_config->get_config($insert_nie['product_type_desc']);
                /*-- --------------------------------------------------- --*/

                if ($this->M_alert_schedule->insertEmailLog($insert_nie, $nie_config)) {
                    $message = show_alert(true, "Data NIE berhasil disimpan");
                } else {
                    $message = show_alert(false, "Terjadi kesalahan saat generate email log");
                }

                // $insert_nie['nie_alert_id'] = $nie_alert;
                /* --------------------------------------------------------------------
                * DIRECT INSERT TO EMAIL LOG
                * --------------------------------------------------------------------*/
                // $product_type_id = $insert_nie['prodtype'];
                // $product_type = $this->M_productType->type_by_id($product_type_id);
                // if (count($product_type) <> 0) {

                /*-- MENDAPATKAN NIE CONFIG DARI PRODUCT TYPE --*/
                // $type_config = $product_type['product_type_desc'];
                // $nie_config = $this->M_nie_config->get_config($type_config);
                /*-- --------------------------------------------------- --*/

                /*-- INSERT KE TABEL EMAIL LOG --*/
                // if (count($nie_config) <> 0) {
                //     if ($this->M_alert_schedule->createSchedule($nie_config, $insert_nie)) {
                //         $message = show_alert(true, "Data NIE berhasil disimpan");
                //     } else {
                //         $message = show_alert(false, "Kesalahan saat generate email log");
                //     }
                // } else {
                //     $message = show_alert(false, "Data NIE Config tidak ditemukan");
                // }
                /*-- --------------------------------------------------- --*/
                // } else {
                //     $message = show_alert(false, "Produk tipe yang dimaksud tidak ditemukan pada tabel mst_product_type");
                // }
            } else {
                $message = show_alert(false, $this->db->error());
                return false;
            }
            $this->session->set_flashdata('message', $message);
            redirect('nie/produk');
        }


        $data['prodlist'] = $this->M_product->getProductPopUp();
        $data['typelist'] = $this->M_productType->getProdType();
        $data['conlist'] = $this->M_storageCon->getStorageCon();
        $data['statlist'] = $this->M_nieStatus->getNieStatus();
        $data['certifilist'] = $this->M_certificate->getCertificate();
        $data['regulatolist'] = $this->M_regulatory->getRegulatory();
        $data['addrlist'] = $this->M_manAddress->getManAdd();
        $data['agent'] = $this->M_nie->getAgent();

        $head_item['judul'] = 'Add New NIE';

        if ($this->input->post('keyword')) {
            $data['prodlist'] = $this->M_product->cariDataProduct();
        }

        if (isset($message) && !empty($message)) {
            $this->session->set_flashdata('message', $message);
        }

        $this->load->view('templates/header', $head_item);
        $this->load->view('nie/tambah', $data);
        $this->load->view('templates/footer');
    }

    /*Digunakan untuk validasi ekstensi file sertifikat yang di-upload*/
    /*
    public function certificate_check()
    {
        $allowed_mime_type = array('application/pdf');
        $current_mime = get_mime_by_extension($_FILES['nie_cert']['name']);
        if (isset($_FILES['nie_cert']['name']) && $_FILES['nie_cert']['name'] <> "") {
            if (in_array($current_mime, $allowed_mime_type)) {
                return true;
            } else {
                $this->form_validation->set_message('certificate_check', 'ekstensi file yang hanya diperbolehkan yaitu .pdf');
                return false;
            }
        }
    }
    */

    public function cariDataProduct()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->select('mst_product.PRODUCT_CODE, mst_product.PRODUCT_DESC, mst_product.PRODUCT_OWNER, mst_product.PLANNING_AREA, mst_product.EXPIRE_DAYS, mst_prod_group4.PG4_DESC, mst_prod_group7.PG7_DESC, KodeNegaraBPOM.con_desc', false);
        $this->db->from('mst_product');
        $this->db->join('mst_prod_group4', 'mst_product.PG4_CODE = mst_prod_group4.PG4_CODE', 'left');
        $this->db->join('mst_prod_group7', 'mst_product.PG7_CODE = mst_prod_group7.PG7_CODE', 'left');
        $this->db->join('KodeNegaraBPOM', 'mst_product.exportir_code = KodeNegaraBPOM.con_code', 'left');
        $this->db->like('mst_product.PRODUCT_CODE', $keyword);
        $this->db->or_like('mst_product.PRODUCT_DESC', $keyword);
        $this->db->or_like('mst_product.PRODUCT_OWNER', $keyword);
        $this->db->or_like('mst_product.PLANNING_AREA', $keyword);
        $this->db->or_like('mst_product.EXPIRE_DAYS', $keyword);
        $this->db->or_like('mst_prod_group4.PG4_DESC', $keyword);
        $this->db->or_like('mst_prod_group7.PG7_DESC', $keyword);
        $this->db->or_like('KodeNegaraBPOM.con_desc', $keyword);
        $query = $this->db->get();
        $data['data'] = $query->result_array();

        header('Content-type:application/json');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Methods: GET, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding');

        $this->output->set_content_type('application/json')->set_output(json_encode($data['data']));
    }

    public function all_data()
    {
        $list = $this->M_nie->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nie) {
            $no++;
            $row = array();
            $row['id'] = $nie->id;
            $row['num'] = $no;
            $row['nie_no'] = $nie->nie_no;
            $row['PRODUCT_CODE'] = $nie->PRODUCT_CODE;
            $row['product_name'] = $nie->product_name;
            $row['brand_name'] = $nie->brand_name;
            $row['create_user'] = ($nie->pic == null) ? "-" : $nie->pic;
            $row['nie_start_date'] = $nie->nie_start_date;
            $row['nie_end_date'] = $nie->nie_end_date;
            $row['planning_area'] = ($nie->planning_area == null) ? "-" : $nie->planning_area;
            $row['nie_date'] = $nie->nie_date;
            $row['compotition'] = wordwrap($nie->compotition, 50, "<br>\n");
            $row['packaging_presentation'] = $nie->packaging_presentation;
            $row['product_owner'] = ($nie->product_owner == null) ? "-" : $nie->product_owner;
            $row['manufact_addr'] = ($nie->manufact_addr == null) ? "-" : wordwrap($nie->manufact_addr, 50, "<br>\n");
            $row['submit_status'] = $nie->submit_status;
            $row['bpom_submit_status'] = $nie->bpom_submit_status;
            $row['expiration_flag'] = $nie->expiration_flag;
            $row['product_type_desc'] = $nie->product_type_desc;
            $row['product_type'] = $nie->product_type;
            $row['alert_color'] = $nie->alert_color;
            // $row['export'] = $nie->export;
            // $row['month_diff'] = (int) $nie->month_diff;


            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_nie->count_all(),
            "recordsFiltered" => $this->M_nie->count_filtered(),
            "data" => $data,
        );

        //output to json format
        echo json_encode($output);
    }

    public function alert_config()
    {
        $conf = $this->M_nie->nie_config();
        echo json_encode($conf);
    }

    public function nie_compotition()
    {
        if ($this->input->get('nie_alert_id')) {
            $compotition = $this->M_nie->compotition_by_nie_alert($this->input->get('nie_alert_id'));
            echo json_encode($compotition);
        }
    }

    public function detail($id, $alert)
    {
        if (!$this->session->userdata('detail_nie_product') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        $title['judul'] = 'Cek Produk SANBE';
        //NIE detail, menampilkan NIE yang sedang berlaku saat ini
        $nie = $this->M_nie->get_nie_no($id);
        $current_nie = $this->M_nie->getCurrentNie($nie->nie_no);

        $edit = $this->M_nie->item_nie($current_nie->id);
        if (sizeof($edit) != 0) {
            foreach ($edit as $row) {
                $data['nie_id'] = $id;
                $data['product_name'] = $row['product_name'];
                $data['product_code'] = $row['PRODUCT_CODE'];
                $data['product_type_id'] = $row['product_type_id'];
                $data['product_owner'] = $row['product_owner'];
                $data['planning_area'] = $row['planning_area'];
                $data['fd_form'] = $row['fd_form'];
                $data['packaging_presentation'] = $row['packaging_presentation'];
                $data['shelf_life'] = $row['shelf_life'];
                $data['storage_condition_id'] = $row['storage_condition_id'];
                //ditambah storage condition ngambil dari tabel nie_alert karena data yang dikasih bu nurul langsung storage condition desc nya ga pake id
                $data['storage_condition'] = $row['storage_condition'];

                $data['nie_no'] = $row['nie_no'];
                $data['nie_status'] = $row['nie_status'];
                $data['nie_start_date'] = $row['nie_start_date'];
                $data['nie_end_date'] = $row['nie_end_date'];
                $data['nie_holder'] = $row['nie_holder'];
                $data['nie_publisher'] = $row['nie_publisher'];
                $data['manufact_addr'] = ($row['manufact_addr'] == "") ? "-" : wordwrap($row['manufact_addr'], 50, "<br>\n");

                //perubahan nie compotition mengacu pada table nie_compotition berdasarkan nie_alert_id
                $data['compotition'] = $this->M_nie->getCompotitionByAlertId($id);
                $data['remark'] = $row['remark'];
                $data['brand_name'] = $row['brand_name'];
                $data['agent'] = $row['agent'];
                $data['manufact_addr'] = $row['manufact_addr'];
                $data['country'] = $row['country'];
                $data['regulatory_code'] = $row['regulatory_code'];
                $data['dispatch_date'] = $row['dispatch_date'];
                $data['receive_date'] = $row['receive_date'];
                $data['certificate'] = $row['certificate']; //YES or NO
                $data['nie_certificate'] = $row['nie_certificate'];
                $data['alert'] = $alert;
                $data['bpom_submit_status'] = $row['bpom_submit_status'];
                $data['submit_status'] = $row['submit_status'];
            }
        }

        $data['prodlist'] = $this->M_product->getProductPopUp();
        $data['typelist'] = $this->M_productType->getProdType();
        $data['conlist'] = $this->M_storageCon->getStorageCon();
        $data['statlist'] = $this->M_nieStatus->getNieStatus();
        $data['certifilist'] = $this->M_certificate->getCertificate();
        $data['regulatolist'] = $this->M_regulatory->getRegulatory();
        $data['addrlist'] = $this->M_manAddress->getManAdd();
        $data['agent'] = $this->M_nie->getAgent();
        $data['history'] = $this->M_nie->getProductHistory($data['product_code']);
        $data['submissions'] = $this->M_nieDossier->get_submission($id);

        $this->load->view('templates/header', $title);
        $this->load->view('nie/detail', $data);
        $this->load->view('templates/footer');
    }

    public function edit_item($id, $type = null)
    {
        if (!$this->session->userdata('edit_nie_product') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        //Untuk EDIT
        if ($this->input->post('submit_edit')) {
            $update_nie = $this->input->post();

            if (!empty($_FILES['nie_cert']['name'])) {
                $digits = 3;
                $rand_digit = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

                //Pindahkan file lama ke folder Deleted Certificates dan rename dengan tambahan 3 digit angka random di belakangnya
                $current_path = './uploads/Certificates/';
                $current_nie = $this->M_nie->get_nie_no($id);
                $current_filename = $current_nie->nie_no . ".pdf";
                $current_file = $current_path . $current_filename;

                if (file_exists($current_file)) {
                    rename($current_file, './uploads/Deleted_Certificates/' . $current_nie->nie_no . "_" . $rand_digit . ".pdf");
                }
                //End of Pindahkan file lama ke folder Deleted Certificates

                //Upload File baru
                $new_filename = $update_nie['prodnie'] . '-' . $rand_digit;
                $new_filename = str_replace(' ', '', $new_filename);

                $config['upload_path'] = $current_path;
                $config['allowed_types'] = 'pdf'; //validasi ekstensi .pdf
                $config['max_size'] = 2048; //2MB //validasi ukuran file yang di upload
                $config['file_name'] = $new_filename;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('nie_cert')) {
                    $tmp = $this->upload->data();
                    $update_nie['file_type'] = $tmp['file_type'];
                    $update_nie['file_size'] = $tmp['file_size'];
                    // $update_nie['full_path'] = file_get_contents($tmp['full_path']);
                    $update_nie['full_path'] = $config['upload_path'] . $new_filename;
                } else {
                    $upload_error = $this->upload->display_errors();
                }
                //End of Upload File baru
            }

            //Update table email log jika start date / end date berbeda dengan sebelumnya
            $current = $this->M_nie->getNieById($update_nie['nie_id']);

            if (strtotime($update_nie['niestartdate']) != strtotime($current['nie_start_date']) || strtotime($update_nie['nieenddate']) != strtotime($current['nie_end_date'])) {
                $nie_config = $this->M_nie_config->get_config($current['product_type_desc']);
                $update_nie['product_type_desc'] = $current['product_type_desc'];
                if ($this->M_alert_schedule->insertEmailLog($update_nie, $nie_config, $current)) {
                    $message = show_alert(true, "Data NIE berhasil diperbarui");
                } else {
                    $message = show_alert(false, "Terjadi kesalahan saat generate email log");
                }
            }


            //Update table email log jika nie number berbeda dengan sebelumnya
            if ($update_nie['prodnie'] != $current['nie_no']) {
                if ($this->M_alert_schedule->updateEmailLog($current['nie_no'], $update_nie['prodnie'])) {
                    $message = show_alert(true, "Data NIE berhasil diperbarui");
                } else {
                    $message = show_alert(false, "Terjadi kesalahan saat generate email log");
                }
            }

            //Update perubahan pada table nie_alert
            $this->M_nie->updateDataNie($update_nie);

            $this->session->set_flashdata('message', $message);
            redirect('update/' . $update_nie['nie_id']);
        }

        $edit = $this->M_nie->item_nie($id);
        if (sizeof($edit) != 0) {
            foreach ($edit as $row) {
                $data['nie_id'] = $id;
                $data['alert_status'] = $row['alert_status'];
                $data['product_name'] = $row['product_name'];
                $data['product_code'] = $row['PRODUCT_CODE'];
                $data['product_type_id'] = $row['product_type_id'];
                $data['product_owner'] = $row['product_owner'];
                $data['planning_area'] = $row['planning_area'];
                $data['fd_form'] = $row['fd_form'];
                $data['packaging_presentation'] = $row['packaging_presentation'];
                $data['shelf_life'] = $row['shelf_life'];
                $data['storage_condition_id'] = $row['storage_condition_id'];
                $data['nie_no'] = $row['nie_no'];
                $data['nie_status'] = $row['nie_status'];
                $data['nie_start_date'] = $row['nie_start_date'];
                $data['nie_end_date'] = $row['nie_end_date'];
                $data['nie_holder'] = $row['nie_holder'];
                $data['nie_publisher'] = $row['nie_publisher'];
                // $data['compotition'] = $row['compotition'];
                $data['remark'] = $row['remark'];
                $data['brand_name'] = $row['brand_name'];
                $data['agent'] = $row['agent'];
                $data['manufact_addr'] = $row['manufact_addr'];
                $data['country'] = $row['country'];
                $data['regulatory_code'] = $row['regulatory_code'];
                $data['dispatch_date'] = $row['dispatch_date'];
                $data['receive_date'] = $row['receive_date'];
                $data['pic'] = $row['pic'];
                $data['certificate'] = $row['certificate']; //YES or NO
                $data['nie_certificate'] = $row['nie_certificate'];
                $data['internal_submit_status'] = $row['submit_status'];
                $data['bpom_submit_status'] = $row['bpom_submit_status'];
            }
        }

        $data['prodlist'] = $this->M_product->getProductPopUp();
        $data['typelist'] = $this->M_productType->getProdType();
        $data['conlist'] = $this->M_storageCon->getStorageCon();
        $data['statlist'] = $this->M_nieStatus->getNieStatus();
        $data['certifilist'] = $this->M_certificate->getCertificate();
        $data['regulatolist'] = $this->M_regulatory->getRegulatory();
        $data['addrlist'] = $this->M_manAddress->getManAdd();
        $data['agent'] = $this->M_nie->getAgent();

        if (isset($upload_error)) {
            $data['upload_error'] = $upload_error;
        }

        if ($type == null) {
            $title['judul'] = 'Edit Data';
            $this->load->view('templates/header', $title);
            $this->load->view('nie/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $title['judul'] = 'Next NIE Data';
            $data['last_nie_number'] = $this->M_nie->getLastNieNumber($data['product_code']);
            $this->load->view('templates/header', $title);
            $this->load->view('nie/completed', $data);
            $this->load->view('templates/footer');
        }
    }

    public function AutocompleteProduct()
    {
        $data = $this->M_product->getProductPopUp();
        echo json_encode($data);
    }

    public function sarana()
    {
        $title['judul'] = 'Cek Produk SANBE';
        $data['sarana'] = $this->M_nie->itemSarana();

        $this->load->view('templates/header', $title);
        $this->load->view('nie/sarana', $data);
        $this->load->view('templates/footer');
    }

    public function unit_sanbe()
    {
        $sarana = $this->M_nie->getSarana();
        $data = array();
        $no = $_POST['start'];
        foreach ($sarana as $res) {
            $no++;
            $row = array();
            $row['num'] = $no;
            $row['planning_area'] = $res->manufacture_name;
            $row['address'] = $res->manufacture_address;
            $row['produk_terdaftar'] = $res->produk_terdaftar;
            $row['produksi'] = $res->produksi;
            $row['produk_ekspor'] = $res->produk_ekspor;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_nie->count_all_sarana(),
            "recordsFiltered" => $this->M_nie->count_filtered_sarana(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function produk()
    {
        if ($this->session->userdata('nie_product_page') == '1') {
            $title['judul'] = 'Cek Produk SANBE';
            $data['nie'] = $this->M_nie->items_nie();

            $this->load->view('templates/header', $title);
            $this->load->view('nie/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->load->view('restricted_page');
        }
    }

    public function percentageByAlert()
    {
        $data = $this->M_nie->getPercentageByAlert();
        echo json_encode($data);
    }

    public function percentageByUnit()
    {
        $data = $this->M_nie->getPercentageByUnit();
        echo json_encode($data);
    }

    // public function percentageByTrans()
    // {
    //     $data = $this->M_nie->getPercentageByTrans();
    //     echo json_encode($data);
    // }

    public function percentageByProductType()
    {
        $data = $this->M_nie->getPercentageByProdType();
        echo json_encode($data);
    }

    public function submission_monitoring($id)
    {
        if (!$this->session->userdata('submission_page') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        if ($this->session->userdata('url_request')) {
            $this->session->unset_userdata('url_request');
        }

        $submission = $this->M_nieDossier->get_submission($id);
        $bpom = $this->M_nieDossier->get_bpom_additional_doc($id);
        $title['judul'] = 'NIE Submission Monitoring';
        $data['nie'] = $this->M_nie->getNieById($id);
        $data['doc_1'] = $this->M_nieDossier->get_lvl_1();
        $data['submission_dept'] = $this->M_privilege->get_submission_dept();
        $data['nie_alert_id'] = $id;

        if ((!empty($submission) && count($submission) > 0) || (!empty($bpom) && count($bpom) > 0)) {
            redirect('submission-process/' . $id);
        } else {
            $this->load->view('templates/header', $title);
            $this->load->view('nie/submission', $data);
            $this->load->view('templates/footer');
        }
    }

    public function get_doc_lvl_2()
    {
        $doc_lvl_1 = $this->input->post('lvl_1');
        $doc_lvl_2 = $this->M_nieDossier->get_lvl_2($doc_lvl_1);

        echo json_encode($doc_lvl_2);
    }

    public function get_doc_lvl_3()
    {
        $doc_lvl_2 = $this->input->post('lvl_2');
        $doc_lvl_3 = $this->M_nieDossier->get_lvl_3($doc_lvl_2);

        if (empty($doc_lvl_3)) {
            $doc_lvl_3 = $this->M_nieDossier->get_treeLvl_2($doc_lvl_2);
        }

        echo json_encode($doc_lvl_3);
    }

    private function generate_tree_doc_submission($last_lvl, $pic_dept_id = null, $pic_name = null)
    {
        if (count($last_lvl) > 0) {
            $first_parent = array();
            for ($i = 0; $i < count($last_lvl); $i++) {
                $is_available = false;
                if (count($first_parent) == 0) {
                    $parent = array(
                        'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                        'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                    );
                    array_push($first_parent, $parent);
                } else {
                    for ($j = 0; $j < count($first_parent); $j++) {
                        if ($last_lvl[$i]['nie_doc1_code'] == $first_parent[$j]['parent_code']) {
                            $is_available = true;
                        }
                    }

                    if ($is_available == false) {
                        $parent = array(
                            'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                            'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                        );
                        array_push($first_parent, $parent);
                    }
                }
            }
            $nav = "";
            $accordion = "";
            $input_recipient = "";

            // Set all parent tab
            $tab = "";
            for ($n = 0; $n < count($first_parent); $n++) {
                if ($n == 0) {
                    $tab .= "<a class='nav-item nav-link active' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='true'>" . $first_parent[$n]['parent_desc'] . "</a>";
                } else {
                    $tab .= "<a class='nav-item nav-link' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='false'>" . $first_parent[$n]['parent_desc'] . "</a>";
                }
            }

            //For content
            $section = "";
            for ($k = 0; $k < count($last_lvl); $k++) {
                if ($k == 0) {
                    $section .= "<div class='tab-pane fade show active' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered sub-document'><tbody>";
                    $content = "";
                } else {
                    if ($last_lvl[$k]['nie_doc1_code'] != $last_lvl[$k - 1]['nie_doc1_code']) {
                        $section .= "</tbody></table></div></div></div></div>";
                        $section .= "<div class='tab-pane fade' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered sub-document'><tbody>";
                        $content = "";
                    }
                }
                $content = "<tr><td>" . $last_lvl[$k]['nie_doc2_desc'] . "</td></tr>";
                if ($last_lvl[$k]['child_lvl_3'] != null) {
                    $lvl_3 = explode('$', $last_lvl[$k]['child_lvl_3']);

                    for ($z = 0; $z < count($lvl_3); $z++) {
                        $temp3 = explode('#', $lvl_3[$z]);
                        $content .= "<tr><td>" . $temp3[1] . "</td></tr>";

                        if ($last_lvl[$k]['child_lvl_4'] != null) {
                            $lvl_4 = explode('$', $last_lvl[$k]['child_lvl_4']);
                            for ($v = 0; $v < count($lvl_4); $v++) {
                                $ref_lvl4 = explode('>', $lvl_4[$v]);
                                if ($ref_lvl4[0] == $temp3[0]) {
                                    $temp4 = explode('#', $ref_lvl4[1]);

                                    if ($last_lvl[$k]['child_lvl_5'] != null) {
                                        $content .= "<tr><td><div>" . $temp4[1] . "</div><br/>";
                                        $child_temp_content = "";
                                        $lvl_5 = explode('$', $last_lvl[$k]['child_lvl_5']);
                                        for ($q = 0; $q < count($lvl_5); $q++) {
                                            $ref_lvl5 = explode('>', $lvl_5[$q]);
                                            if ($ref_lvl5[0] == $temp4[0]) {
                                                $temp5 = explode('#', $ref_lvl5[1]);
                                                $child_temp_content .= "<div style='text-indent: 2%;'>" . $temp5[1] . "</div><br/>";
                                            }
                                        }
                                        $content .= $child_temp_content . "</td></tr>";
                                    } else {
                                        $content .= "<tr><td><div>" . $temp4[1] . "</div></td></tr>";
                                    }
                                }
                            }
                        }
                    }
                }
                $section .= $content;
            }

            $section .= "</tbody></table></div></div></div></div>";

            // $data = array(
            //     "tab" => $tab,
            //     "section" => $section
            // );

            $nav .= '<div class="row mt-3"><div class="col"><nav><div class="nav nav-tabs" id="nav-tab" role="tablist">' . $tab . '</div></nav>';
            $nav .= '<div class="tab-content" id="nav-tabContent" style="background-color: white;">' . $section . '</div></div></div>';

            $input_recipient .= '<div class="row mt-2"><div class="col"><div style="background-color: white;"><input name="recipient_' . $pic_dept_id . '" id="recipient_' . $pic_dept_id . '" placeholder="ketik email yang dituju"></div></div></div>';

            $accordion .= '<div class="card" id="card_' . $pic_dept_id . '"><div class="card-header" id="heading_' . $pic_dept_id . '" style="background-color: #d7eaf7;"><h6 class="mb-0">';
            $accordion .= '<a href="#" data-toggle="collapse" data-target="#collapse_' . $pic_dept_id . '" aria-expanded="true" aria-controls="collapse_' . $pic_dept_id . '" style="color: black;">' . $pic_name . ' Department</a><span class="float-right" style="color: black;">Submission Document</span></h6></div>';
            $accordion .= '<div id="collapse_' . $pic_dept_id . '" class="collapse show" aria-labelledby="heading_' . $pic_dept_id . '" data-parent="#accordion"><div class="card-body" style="background-color: #f7f7f7;">' . $input_recipient . $nav;
            $accordion .= '<div class="row mt-3"><div class="col"><button type="button" class="btn btn-danger col-md-1 float-right" onclick="removeAccordion(' . $pic_dept_id . ')">Remove</button></div></div>';
            $accordion .= '</div></div></div>';
        }

        return $accordion;
    }

    private function generate_submission_check($last_lvl)
    {
        if (count($last_lvl) > 0) {
            $first_parent = array();
            for ($i = 0; $i < count($last_lvl); $i++) {
                $is_available = false;
                if (count($first_parent) == 0) {
                    $parent = array(
                        'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                        'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                    );
                    array_push($first_parent, $parent);
                } else {
                    for ($j = 0; $j < count($first_parent); $j++) {
                        if ($last_lvl[$i]['nie_doc1_code'] == $first_parent[$j]['parent_code']) {
                            $is_available = true;
                        }
                    }

                    if ($is_available == false) {
                        $parent = array(
                            'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                            'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                        );
                        array_push($first_parent, $parent);
                    }
                }
            }

            // Set all parent tab
            $tab = "";
            for ($n = 0; $n < count($first_parent); $n++) {
                if ($n == 0) {
                    $tab .= "<a class='nav-item nav-link active' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='true'>" . $first_parent[$n]['parent_desc'] . "</a>";
                } else {
                    $tab .= "<a class='nav-item nav-link' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='false'>" . $first_parent[$n]['parent_desc'] . "</a>";
                }
            }

            //For content
            $section = "";
            for ($k = 0; $k < count($last_lvl); $k++) {
                if ($k == 0) {
                    $section .= "<div class='tab-pane fade show active' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>R</th><th>NR</th></tr></thead><tbody>";
                    $content = "";
                } else {
                    if ($last_lvl[$k]['nie_doc1_code'] != $last_lvl[$k - 1]['nie_doc1_code']) {
                        $section .= "</tbody></table></div></div></div></div>";
                        $section .= "<div class='tab-pane fade' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>R</th><th>NR</th></tr></thead><tbody>";
                        $content = "";
                    }
                }
                if ($last_lvl[$k]['child_lvl_3'] != null) {
                    $content = "<tr><td colspan='3'>" . $last_lvl[$k]['nie_doc2_desc'] . "</td></tr>";
                    $lvl_3 = explode('$', $last_lvl[$k]['child_lvl_3']);

                    for ($z = 0; $z < count($lvl_3); $z++) {
                        $temp3 = explode('#', $lvl_3[$z]);

                        if ($last_lvl[$k]['child_lvl_4'] != null) {
                            $content .= "<tr><td colspan='3'>" . $temp3[1] . "</td></tr>";
                            $lvl_4 = explode('$', $last_lvl[$k]['child_lvl_4']);
                            for ($v = 0; $v < count($lvl_4); $v++) {
                                $ref_lvl4 = explode('>', $lvl_4[$v]);
                                if ($ref_lvl4[0] == $temp3[0]) {
                                    $temp4 = explode('#', $ref_lvl4[1]);

                                    if ($last_lvl[$k]['child_lvl_5'] != null) {
                                        $child_temp_content = "";
                                        $lvl_5 = explode('$', $last_lvl[$k]['child_lvl_5']);
                                        $content .= "<tr><td colspan='3'><div>" . $temp4[1] . "</div></td></tr>";
                                        for ($q = 0; $q < count($lvl_5); $q++) {
                                            $ref_lvl5 = explode('>', $lvl_5[$q]);
                                            if ($ref_lvl5[0] == $temp4[0]) {
                                                $temp5 = explode('#', $ref_lvl5[1]);
                                                $child_temp_content .= "<tr><td><div style='text-indent: 2%;'>" . $temp5[1] . "</div></td>";
                                                $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="Y" class="custom-control-input"><label class="custom-control-label" for="y-' . $temp5[0] . '">&nbsp;</label></div></td>';
                                                $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="N" class="custom-control-input"><label class="custom-control-label" for="n-' . $temp5[0] . '">&nbsp;</label></div></td></tr>';
                                            }
                                        }
                                        $content .= $child_temp_content;
                                    } else {
                                        $content .= "<tr><td><div>" . $temp4[1] . "</div></td>";
                                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="Y" class="custom-control-input"><label class="custom-control-label" for="y-' . $temp4[0] . '">&nbsp;</label></div></td>';
                                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="N" class="custom-control-input"><label class="custom-control-label" for="n-' . $temp4[0] . '">&nbsp;</label></div></td></tr>';
                                    }
                                }
                            }
                        } else {
                            $content .= "<tr><td>" . $temp3[1] . "</td>";
                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="Y" class="custom-control-input"><label class="custom-control-label" for="y-' . $temp3[0] . '">&nbsp;</label></div></td>';
                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="N" class="custom-control-input"><label class="custom-control-label" for="n-' . $temp3[0] . '">&nbsp;</label></div></td></tr>';
                        }
                    }
                } else {
                    $content = "<tr><td>" . $last_lvl[$k]['nie_doc2_desc'] . "</td>";
                    $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $last_lvl[$k]['nie_doc2_code'] . '" name="accepted_checked_' . $last_lvl[$k]['nie_doc2_code'] . '" value="Y" class="custom-control-input"><label class="custom-control-label" for="y-' . $last_lvl[$k]['nie_doc2_code'] . '">&nbsp;</label></div></td>';
                    $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $last_lvl[$k]['nie_doc2_code'] . '" name="accepted_checked_' . $last_lvl[$k]['nie_doc2_code'] . '" value="N" class="custom-control-input"><label class="custom-control-label" for="n-' . $last_lvl[$k]['nie_doc2_code'] . '">&nbsp;</label></div></td></tr>';
                }

                $section .= $content;
            }

            $section .= "</tbody></table></div></div></div></div>";

            $data = array(
                "tab" => $tab,
                "section" => $section
            );
        }

        return $data;
    }

    public function removeSubmissionItem()
    {
        $doc_id = $this->input->post('document_id');
        $doc_type = $this->input->post('document_type');

        if ($this->M_nieDossier->deleteSubmissionItem($doc_id, $doc_type)) {
            echo json_encode("success");
        } else {
            echo json_encode("failed");
        }
    }

    public function resendFailedEmail()
    {
        $email_no = $this->input->post('email_no');
        $send_email = $this->M_email_submission->resendEmail($email_no);
        $this->M_email_submission->updateEmailSubmissionLog($send_email, $email_no);

        if ($send_email['status'] == 'failed') {
            $message = show_alert(false, "Terjadi kegagalan saat mengirim email");
        } else {
            $message = show_alert(true, "Email pemberitahuan berhasil dikirim");
        }
        $this->session->set_flashdata('message', $message);
        echo json_encode("success");
    }

    private function generate_submission_checked($nie_alert_id, $last_lvl, $pic_dept)
    {
        if (count($last_lvl) > 0) {
            $first_parent = array();
            for ($i = 0; $i < count($last_lvl); $i++) {
                $is_available = false;
                if (count($first_parent) == 0) {
                    $parent = array(
                        'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                        'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                    );
                    array_push($first_parent, $parent);
                } else {
                    for ($j = 0; $j < count($first_parent); $j++) {
                        if ($last_lvl[$i]['nie_doc1_code'] == $first_parent[$j]['parent_code']) {
                            $is_available = true;
                        }
                    }

                    if ($is_available == false) {
                        $parent = array(
                            'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                            'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                        );
                        array_push($first_parent, $parent);
                    }
                }
            }
            $nav = "";
            $accordion = "";
            // Set all parent tab
            $tab = "";
            for ($n = 0; $n < count($first_parent); $n++) {
                if ($n == 0) {
                    $tab .= "<a class='nav-item nav-link active' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='true'>" . $first_parent[$n]['parent_desc'] . "</a>";
                } else {
                    $tab .= "<a class='nav-item nav-link' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='false'>" . $first_parent[$n]['parent_desc'] . "</a>";
                }
            }

            //Untuk tambahan data BPOM
            $tab .= "<a class='nav-item nav-link' id='nav-bpom-" . $pic_dept . "-tab' data-toggle='tab' href='#nav-bpom-" . $pic_dept . "' role='tab' aria-controls='nav-bpom-" . $pic_dept . "' aria-selected='false'>TAMBAHAN DATA BPOM</a>";

            //Cek apakah email untuk RnD atau QA sudah terkirim atau belum, jika belum terkirim tambahkan tombol resend
            //parameter ketiga digunakan supaya email yang diambil cuma email submission aja, bukan email untuk tambahan data bpom
            $email = $this->M_email_submission->getFailedEmail($nie_alert_id, $pic_dept, "tambahan bpom");
            $button_resend = "";
            if (count($email) > 0) {
                $button_resend = '<button type="button" class="btn btn-danger btn-sm float-right" onclick="resendSubmissionEmail(\'' . $email['no'] . '\')"><i class="fa fa-envelope" aria-hidden="true" style="font-size: 15px;"></i>&nbsp; Resend</button>';
            }

            //For content
            $section = "";
            for ($k = 0; $k < count($last_lvl); $k++) {
                if ($k == 0) {
                    $section .= "<div class='tab-pane fade show active' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                    $content = "";
                } else {
                    if ($last_lvl[$k]['nie_doc1_code'] != $last_lvl[$k - 1]['nie_doc1_code']) {
                        $section .= "</tbody></table></div></div></div></div>";
                        $section .= "<div class='tab-pane fade' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                        $content = "";
                    }
                }

                if (strpos($last_lvl[$k]['nie_doc2_code'], "@") !== false) {
                    $d2 = explode("@", $last_lvl[$k]['nie_doc2_code']);
                }

                if ($last_lvl[$k]['child_lvl_3'] != null) {
                    $content = "<tr><td colspan='4'>" . $last_lvl[$k]['nie_doc2_desc'] . "</td></tr>";
                    $lvl_3 = explode('$', $last_lvl[$k]['child_lvl_3']);

                    for ($z = 0; $z < count($lvl_3); $z++) {
                        $temp3 = explode('#', $lvl_3[$z]);

                        if (strpos($temp3[1], "@") !== false) {
                            $d3 = explode("@", $temp3[1]);
                        } else {
                            $d3 = array($temp3[1]);
                        }

                        if ($last_lvl[$k]['child_lvl_4'] != null) {
                            $content .= "<tr><td colspan='4'>" . $d3[0] . "</td></tr>";
                            $lvl_4 = explode('$', $last_lvl[$k]['child_lvl_4']);
                            for ($v = 0; $v < count($lvl_4); $v++) {
                                $ref_lvl4 = explode('>', $lvl_4[$v]);
                                if ($ref_lvl4[0] == $temp3[0]) {
                                    $temp4 = explode('#', $ref_lvl4[1]);
                                    if (strpos($temp4[1], "@") !== false) {
                                        $d4 = explode("@", $temp4[1]);
                                    } else {
                                        $d4 = array($temp4[1]);
                                    }

                                    if ($last_lvl[$k]['child_lvl_5'] != null) {
                                        $child_temp_content = "";
                                        $lvl_5 = explode('$', $last_lvl[$k]['child_lvl_5']);
                                        $content .= "<tr><td colspan='4'><div>" . $d4[0] . "</div></td></tr>";
                                        for ($q = 0; $q < count($lvl_5); $q++) {
                                            $ref_lvl5 = explode('>', $lvl_5[$q]);
                                            if ($ref_lvl5[0] == $temp4[0]) {
                                                $temp5 = explode('#', $ref_lvl5[1]);
                                                if (strpos($temp5[1], "@") !== false) {
                                                    $d5 = explode("@", $temp5[1]);
                                                } else {
                                                    $d5 = array($temp5[1]);
                                                }

                                                if ($d5[2] == "not submitted") {
                                                    $child_temp_content .= "<tr><td><div style='text-indent: 2%;'>" . $d5[0] . "<a href='javascript:void(0)' class='float-right' onclick='removeSelectedDoc(\"" . $temp5[0] . "\")'><i class='fa fa-trash' aria-hidden='true' style='color: red; font-size: 18px;'></i></a></div></td>";
                                                    $child_temp_content .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                                                    $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $temp5[0] . '">&nbsp;</label></div></td>';
                                                    $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $temp5[0] . '">&nbsp;</label></div></td></tr>';
                                                } else {
                                                    $yes = ($d5[1] == "received") ? "checked" : "";
                                                    $no = ($d5[1] == "not received") ? "checked" : "";
                                                    $child_temp_content .= "<tr><td><div style='text-indent: 2%;'>" . $d5[0] . "<a href='javascript:void(0)' class='float-right'><i class='fa fa-trash' aria-hidden='true' style='color: gray; font-size: 18px;'></i></a></div></td>";
                                                    $child_temp_content .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                                                    $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $d5[3] . ')"><label class="custom-control-label" for="y-' . $temp5[0] . '">&nbsp;</label></div></td>';
                                                    $child_temp_content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp5[0] . '" name="accepted_checked_' . $temp5[0] . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $d5[3] . ')"><label class="custom-control-label" for="n-' . $temp5[0] . '">&nbsp;</label></div></td></tr>';
                                                }
                                            }
                                        }
                                        $content .= $child_temp_content;
                                    } else {
                                        if ($d4[2] == "not submitted") {
                                            $content .= "<tr><td><div>" . $d4[0] . "<a href='javascript:void(0)' class='float-right' onclick='removeSelectedDoc(\"" . $temp4[0] . "\")'><i class='fa fa-trash' aria-hidden='true' style='color: red; font-size: 18px;'></i></a></div></td>";
                                            $content .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $temp4[0] . '">&nbsp;</label></div></td>';
                                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $temp4[0] . '">&nbsp;</label></div></td></tr>';
                                        } else {
                                            $yes = ($d4[1] == "received") ? "checked" : "";
                                            $no = ($d4[1] == "not received") ? "checked" : "";
                                            $content .= "<tr><td><div>" . $d4[0] . "<a href='javascript:void(0)' class='float-right'><i class='fa fa-trash' aria-hidden='true' style='color: gray; font-size: 18px;'></i></a></div></td>";
                                            $content .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $d4[3] . ')"><label class="custom-control-label" for="y-' . $temp4[0] . '">&nbsp;</label></div></td>';
                                            $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp4[0] . '" name="accepted_checked_' . $temp4[0] . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $d4[3] . ')"><label class="custom-control-label" for="n-' . $temp4[0] . '">&nbsp;</label></div></td></tr>';
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($d3[2] == "not submitted") {
                                $content .= "<tr><td>" . $d3[0] . "<a href='javascript:void(0)' class='float-right' onclick='removeSelectedDoc(\"" . $temp3[0] . "\")'><i class='fa fa-trash' aria-hidden='true' style='color: red; font-size: 18px;'></i></a></td>";
                                $content .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                                $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $temp3[0] . '">&nbsp;</label></div></td>';
                                $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $temp3[0] . '">&nbsp;</label></div></td></tr>';
                            } else {
                                $yes = ($d3[1] == "received") ? "checked" : "";
                                $no = ($d3[1] == "not received") ? "checked" : "";
                                $content .= "<tr><td>" . $d3[0] . "<a href='javascript:void(0)' class='float-right'><i class='fa fa-trash' aria-hidden='true' style='color: gray; font-size: 18px;'></i></a></td>";
                                $content .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                                $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $d3[3] . ')"><label class="custom-control-label" for="y-' . $temp3[0] . '">&nbsp;</label></div></td>';
                                $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $temp3[0] . '" name="accepted_checked_' . $temp3[0] . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $d3[3] . ')"><label class="custom-control-label" for="n-' . $temp3[0] . '">&nbsp;</label></div></td></tr>';
                            }
                        }
                    }
                } else {
                    if ($d2[2] == "not submitted") {
                        $content = "<tr><td>" . $last_lvl[$k]['nie_doc2_desc'] . "<a href='javascript:void(0)' class='float-right' onclick='removeSelectedDoc(\"" . $d2[0] . "\")'><i class='fa fa-trash' aria-hidden='true' style='color: red; font-size: 18px;'></i></a></td>";
                        $content .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $d2[0] . '" name="accepted_checked_' . $d2[0] . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $d2[0] . '">&nbsp;</label></div></td>';
                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $d2[0] . '" name="accepted_checked_' . $d2[0] . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $d2[0] . '">&nbsp;</label></div></td></tr>';
                    } else {
                        $yes = ($d2[1] == "received") ? "checked" : "";
                        $no = ($d2[1] == "not received") ? "checked" : "";
                        $content = "<tr><td>" . $last_lvl[$k]['nie_doc2_desc'] . "<a href='javascript:void(0)' class='float-right'><i class='fa fa-trash' aria-hidden='true' style='color: gray; font-size: 18px;'></i></a></td>";
                        $content .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $d2[0] . '" name="accepted_checked_' . $d2[0] . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $d2[3] . ')"><label class="custom-control-label" for="y-' . $d2[0] . '">&nbsp;</label></div></td>';
                        $content .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $d2[0] . '" name="accepted_checked_' . $d2[0] . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $d2[3] . ')"><label class="custom-control-label" for="n-' . $d2[0] . '">&nbsp;</label></div></td></tr>';
                    }
                }

                $section .= $content;
            }

            $section .= "</tbody></table></div></div></div></div>";

            //Cek apakah ada tambahan data BPOM atau tidak
            $bpom_additional_doc = $this->M_nieDossier->additionalBpom($nie_alert_id, $pic_dept);

            if (count($bpom_additional_doc) > 0) {
                //cek apakah ada file yang di upload untuk tambahan data BPOM
                $file_bpom = $this->M_nieDossier->getFileBpom($nie_alert_id);

                if (!empty($file_bpom) && count($file_bpom) > 0) {
                    if ($file_bpom['bpom_add_data_dir'] != "") {
                        if (file_exists($file_bpom['bpom_add_data_dir'])) {
                            $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br />";
                            $section .= "<div class='row'><div class='col'><a class='float-right' href='" . base_url() . $file_bpom['bpom_add_data_dir'] . "'><i class='fa fa-clipboard' aria-hidden='true'></i>&nbsp;Additional File BPOM</a></div></div>";
                            $section .= "<div class='row mt-3'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                        } else {
                            $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                        }
                    } else {
                        $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                    }
                } else {
                    $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";
                }

                foreach ($bpom_additional_doc as $row) {
                    if ($row->submit_flag == "not submitted") {
                        $section .= '<tr><td>' . $row->add_doc_desc . '<a href="javascript:void(0)" class="float-right" onclick="removeSelectedBpomDoc(\'' . $row->id . '\')"><i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 18px;"></i></a></td>';
                        $section .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $row->id . '">&nbsp;</label></div></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $row->id . '">&nbsp;</label></div></td></tr>';
                    } else {
                        $yes = ($row->accepted_check == "received") ? "checked" : "";
                        $no = ($row->accepted_check == "not received") ? "checked" : "";
                        $section .= '<tr><td>' . $row->add_doc_desc . '<a href="javascript:void(0)" class="float-right"><i class="fa fa-trash" aria-hidden="true" style="color: gray; font-size: 18px;"></i></a></td>';
                        $section .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $row->id . ',true)"><label class="custom-control-label" for="y-' . $row->id . '">&nbsp;</label></div></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $row->id . ',true)"><label class="custom-control-label" for="n-' . $row->id . '">&nbsp;</label></div></td></tr>';
                    }
                }
                $section .= '</tbody></table></div></div></div></div>';
            } else {
                //Untuk tambahan data BPOM
                $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col-md-10'>";
                $section .= '<input type="text" class="form-control" name="tambahan_bpom_' . $pic_dept . '[]" id="bpom-' . $pic_dept . '" placeholder="Ketik tambahan data" /></div><div class="col-md-2"><button type="button" class="btn btn-primary btn-block" onclick="addDataBpom(\'' . $pic_dept . '\')">Add</button></div></div>';
                $section .= '<div class="row mt-4"><div class="col"><table class="table table-bordered sub-document-bpom" id="table-bpom-' . $pic_dept . '"><tbody></tbody></table></div></div>';

                //Untuk upload tambahan data BPOM
                $section .= '<div class="row mt-3" id="show-upload-' . $pic_dept . '" style="display: none;"><div class="col"><form enctype="multipart/form-data"><div class="form-group"><div class="custom-file"><input type="file" class="custom-file-input" name="bpom_upload_' . $pic_dept . '" id="bpom-upload-' . $pic_dept . '" onclick="uploadTambahanBpom(\'' . $pic_dept . '\')" /><label class="custom-file-label" for="bpom-upload-' . $pic_dept . '">Pilih File</label></div></div></form></div></div> <div class="row"><div class="col-md-10">&nbsp;</div><div class="col-md-2"><button type="button" id="submit-bpom-' . $pic_dept . '" style="display: none;" class="btn btn-primary btn-block" onclick="submitTambahanData(' . $nie_alert_id . ', \'' . $pic_dept . '\')">Submit</button></div></div>  <br /></div></div>';
            }

            // $data = array(
            //     "tab" => $tab,
            //     "section" => $section
            // );
            $dept = $this->M_privilege->get_dept_by_deptcode($pic_dept);

            $nav .= '<div class="row"><div class="col"><nav><div class="nav nav-tabs" id="nav-tab" role="tablist">' . $tab . '</div></nav>';
            $nav .= '<div class="tab-content" id="nav-tabContent" style="background-color: white;">' . $section . '</div></div></div>';

            $accordion .= '<div class="card" id="card_' . $dept->dept_id . '"><div class="card-header" id="heading_' . $dept->dept_id . '" style="background-color: #d7eaf7;"><h6 class="mb-0">';
            $accordion .= '<a href="#" data-toggle="collapse" data-target="#collapse_' . $dept->dept_id . '" aria-expanded="true" aria-controls="collapse_' . $dept->dept_id . '" style="color: black;">' . $dept->dept_name . ' Department</a>' . $button_resend . '</h6></div>';
            $accordion .= '<div id="collapse_' . $dept->dept_id . '" class="collapse show" aria-labelledby="heading_' . $dept->dept_id . '" data-parent="#accordion"><div class="card-body" style="background-color: #f7f7f7;">' . $nav;
            $accordion .= '</div></div></div>';
        } else {
            $bpom_additional_doc = $this->M_nieDossier->additionalBpom($nie_alert_id, $pic_dept);

            if (count($bpom_additional_doc) > 0) {
                $nav = "";
                $accordion = "";
                $tab = "";
                $section = "";
                $tab .= "<a class='nav-item nav-link active' id='nav-bpom-" . $pic_dept . "-tab' data-toggle='tab' href='#nav-bpom-" . $pic_dept . "' role='tab' aria-controls='nav-bpom-" . $pic_dept . "' aria-selected='true'>TAMBAHAN DATA BPOM</a>";
                $section .= "<div class='tab-pane fade show active' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th><th>R</th><th>NR</th></tr></thead><tbody>";

                foreach ($bpom_additional_doc as $row) {
                    if ($row->submit_flag == "not submitted") {
                        $section .= '<tr><td>' . $row->add_doc_desc . '<a href="javascript:void(0)" class="float-right" onclick="removeSelectedBpomDoc(\'' . $row->id . '\')"><i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 18px;"></i></a></td>';
                        $section .= '<td width="4%"><span class="badge badge-primary">ON PROCESS</span></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="Y" class="custom-control-input" disabled><label class="custom-control-label" for="y-' . $row->id . '">&nbsp;</label></div></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="N" class="custom-control-input" disabled><label class="custom-control-label" for="n-' . $row->id . '">&nbsp;</label></div></td></tr>';
                    } else {
                        $yes = ($row->accepted_check == "received") ? "checked" : "";
                        $no = ($row->accepted_check == "not received") ? "checked" : "";
                        $section .= '<tr><td>' . $row->add_doc_desc . '<a href="javascript:void(0)" class="float-right"><i class="fa fa-trash" aria-hidden="true" style="color: gray; font-size: 18px;"></i></a></td>';
                        $section .= '<td width="4%"><span class="badge badge-success">SUBMITTED</span></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="y-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="Y" class="custom-control-input" ' . $yes . ' onchange="updateDoc(\'y\',' . $row->id . ',true)"><label class="custom-control-label" for="y-' . $row->id . '">&nbsp;</label></div></td>';
                        $section .= '<td width="2%"><div class="custom-control custom-radio"><input type="radio" id="n-' . $row->id . '" name="accepted_checked_' . $row->id . '" value="N" class="custom-control-input" ' . $no . ' onchange="updateDoc(\'n\',' . $row->id . ',true)"><label class="custom-control-label" for="n-' . $row->id . '">&nbsp;</label></div></td></tr>';
                    }
                }
                $section .= '</tbody></table></div></div></div></div>';

                $dept = $this->M_privilege->get_dept_by_deptcode($pic_dept);

                $nav .= '<div class="row"><div class="col"><nav><div class="nav nav-tabs" id="nav-tab" role="tablist">' . $tab . '</div></nav>';
                $nav .= '<div class="tab-content" id="nav-tabContent" style="background-color: white;">' . $section . '</div></div></div>';

                $accordion .= '<div class="card" id="card_' . $dept->dept_id . '"><div class="card-header" id="heading_' . $dept->dept_id . '" style="background-color: #d7eaf7;"><h6 class="mb-0">';
                $accordion .= '<a href="#" data-toggle="collapse" data-target="#collapse_' . $dept->dept_id . '" aria-expanded="true" aria-controls="collapse_' . $dept->dept_id . '" style="color: black;">' . $dept->dept_name . ' Department</a><span class="float-right" style="color: black;">Additional BPOM Data</span></h6></div>';
                $accordion .= '<div id="collapse_' . $dept->dept_id . '" class="collapse show" aria-labelledby="heading_' . $dept->dept_id . '" data-parent="#accordion"><div class="card-body" style="background-color: #f7f7f7;">' . $nav;
                $accordion .= '</div></div></div></div>';
            }
        }
        return $accordion;
    }

    public function save_tambahan_bpom()
    {
        $data = $this->input->post();
        $dept = $data['pic_dept'];
        $nie = $this->M_nie->get_nie_no($data['alert_id']);

        if (isset($_FILES["bpom_upload_" . $dept]) && !empty($_FILES["bpom_upload_" . $dept])) {
            $additional_path = "uploads/Submissions/";
            $nie_no = preg_replace('/\s+/', '', $nie->nie_no);

            $target_path = $additional_path . $nie_no . "/Additional";

            $_FILES["bpom_upload_" . $dept]['name'] = preg_replace('/\s+/', '_', $_FILES["bpom_upload_" . $dept]['name']);

            if (!file_exists($target_path)) {
                mkdir($target_path, 0777, true);
            }

            $config['upload_path'] = $target_path;
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = 10240;
            $config['file_name'] =  $_FILES["bpom_upload_" . $dept]['name'];

            $this->load->library('upload', $config);
            $upload_file = "bpom_upload_" . $dept;
            if ($this->upload->do_upload($upload_file)) {
                $data['uploaded_path'] = $target_path . "/" . $_FILES["bpom_upload_" . $dept]['name'];
            }
        }

        if ($this->M_nieDossier->saveAdditionalBpom($data)) {
            $send_email = $this->M_email_submission->sendTambahanBpomEmail($data['alert_id'], $data['pic_dept']);

            if ($send_email['status'] == 'failed') {
                $message = show_alert(false, "Terjadi kegagalan saat mengirim email");
            } else {
                $message = show_alert(true, "Tambahan Data BPOM telah disimpan, dan email pemberitahuan berhasil dikirim");
            }
            $this->M_email_submission->saveEmailSubmissionLog($send_email);

            $this->session->set_flashdata('message', $message);

            echo json_encode("success");
        } else {
            echo json_encode("failed");
        }
    }

    private function generate_submission_checked_dept($nie_alert_id, $last_lvl, $pic_dept)
    {
        $nav = "";
        // $accordion = "";
        if (count($last_lvl) > 0) {
            $first_parent = array();
            for ($i = 0; $i < count($last_lvl); $i++) {
                $is_available = false;
                if (count($first_parent) == 0) {
                    $parent = array(
                        'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                        'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                    );
                    array_push($first_parent, $parent);
                } else {
                    for ($j = 0; $j < count($first_parent); $j++) {
                        if ($last_lvl[$i]['nie_doc1_code'] == $first_parent[$j]['parent_code']) {
                            $is_available = true;
                        }
                    }

                    if ($is_available == false) {
                        $parent = array(
                            'parent_code' => $last_lvl[$i]['nie_doc1_code'],
                            'parent_desc' => $last_lvl[$i]['nie_doc1_desc']
                        );
                        array_push($first_parent, $parent);
                    }
                }
            }

            // Set all parent tab
            $tab = "";
            for ($n = 0; $n < count($first_parent); $n++) {
                if ($n == 0) {
                    $tab .= "<a class='nav-item nav-link active' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='true'>" . $first_parent[$n]['parent_desc'] . "</a>";
                } else {
                    $tab .= "<a class='nav-item nav-link' id='nav-" . $first_parent[$n]['parent_code'] . "-tab' data-toggle='tab' href='#nav-" . $first_parent[$n]['parent_code'] . "' role='tab' aria-controls='nav-" . $first_parent[$n]['parent_code'] . "' aria-selected='false'>" . $first_parent[$n]['parent_desc'] . "</a>";
                }
            }

            //Untuk tambahan data BPOM
            $tab .= "<a class='nav-item nav-link' id='nav-bpom-" . $pic_dept . "-tab' data-toggle='tab' href='#nav-bpom-" . $pic_dept . "' role='tab' aria-controls='nav-bpom-" . $pic_dept . "' aria-selected='false'>TAMBAHAN DATA BPOM</a>";

            //For content
            $section = "";
            for ($k = 0; $k < count($last_lvl); $k++) {
                if ($k == 0) {
                    $section .= "<div class='tab-pane fade show active' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                    $content = "";
                } else {
                    if ($last_lvl[$k]['nie_doc1_code'] != $last_lvl[$k - 1]['nie_doc1_code']) {
                        $section .= "</tbody></table></div></div></div></div>";
                        $section .= "<div class='tab-pane fade' id='nav-" . $last_lvl[$k]['nie_doc1_code'] . "' role='tabpanel' aria-labelledby='nav-" . $last_lvl[$k]['nie_doc1_code'] . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                        $content = "";
                    }
                }

                if (strpos($last_lvl[$k]['nie_doc2_code'], "@") !== false) {
                    $d2 = explode("@", $last_lvl[$k]['nie_doc2_code']);
                }

                if ($last_lvl[$k]['child_lvl_3'] != null) {
                    $content = "<tr><td colspan='2'>" . $last_lvl[$k]['nie_doc2_desc'] . "</td></tr>";
                    $lvl_3 = explode('$', $last_lvl[$k]['child_lvl_3']);

                    for ($z = 0; $z < count($lvl_3); $z++) {
                        $temp3 = explode('#', $lvl_3[$z]);

                        if (strpos($temp3[1], "@") !== false) {
                            $d3 = explode("@", $temp3[1]);
                        } else {
                            $d3 = array($temp3[1]);
                        }

                        if ($last_lvl[$k]['child_lvl_4'] != null) {
                            $content .= "<tr><td colspan='2'>" . $d3[0] . "</td></tr>";
                            $lvl_4 = explode('$', $last_lvl[$k]['child_lvl_4']);
                            for ($v = 0; $v < count($lvl_4); $v++) {
                                $ref_lvl4 = explode('>', $lvl_4[$v]);
                                if ($ref_lvl4[0] == $temp3[0]) {
                                    $temp4 = explode('#', $ref_lvl4[1]);
                                    if (strpos($temp4[1], "@") !== false) {
                                        $d4 = explode("@", $temp4[1]);
                                    } else {
                                        $d4 = array($temp4[1]);
                                    }

                                    if ($last_lvl[$k]['child_lvl_5'] != null) {
                                        $child_temp_content = "";
                                        $lvl_5 = explode('$', $last_lvl[$k]['child_lvl_5']);
                                        $content .= "<tr><td colspan='2'><div>" . $d4[0] . "</div></td></tr>";
                                        for ($q = 0; $q < count($lvl_5); $q++) {
                                            $ref_lvl5 = explode('>', $lvl_5[$q]);
                                            if ($ref_lvl5[0] == $temp4[0]) {
                                                $temp5 = explode('#', $ref_lvl5[1]);
                                                if (strpos($temp5[1], "@") !== false) {
                                                    $d5 = explode("@", $temp5[1]);
                                                } else {
                                                    $d5 = array($temp5[1]);
                                                }

                                                $child_temp_content .= "<tr><td><div style='text-indent: 2%;'>" . $d5[0] . "</div></td>";
                                                if ($d5[2] == "not submitted") {
                                                    $child_temp_content .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $temp5[0] . '" onclick="submitSubmissionDoc(\'' . $temp5[0] . '\',' . $d5[3] . ')">Submit</button></td>';
                                                } else {
                                                    $child_temp_content .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                                                }
                                            }
                                        }
                                        $content .= $child_temp_content;
                                    } else {
                                        $content .= "<tr><td><div>" . $d4[0] . "</div></td>";
                                        if ($d4[2] == "not submitted") {
                                            $content .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $temp4[0] . '" onclick="submitSubmissionDoc(\'' . $temp4[0] . '\',' . $d4[3] . ')">Submit</button></td>';
                                        } else {
                                            $content .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                                        }
                                    }
                                }
                            }
                        } else {
                            $content .= "<tr><td>" . $d3[0] . "</td>";
                            if ($d3[2] == "not submitted") {
                                $content .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $temp3[0] . '" onclick="submitSubmissionDoc(\'' . $temp3[0] . '\',' . $d3[3] . ')">Submit</button></td>';
                            } else {
                                $content .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                            }
                        }
                    }
                } else {
                    $content = "<tr><td>" . $last_lvl[$k]['nie_doc2_desc'] . "</td>";
                    if ($d2[2] == "not submitted") {
                        $content .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $d2[0] . '" onclick="submitSubmissionDoc(\'' . $d2[0] . '\',' . $d2[3] . ')">Submit</button></td>';
                    } else {
                        $content .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                    }
                }

                $section .= $content;
            }

            $section .= "</tbody></table></div></div></div></div>";

            //Cek apakah ada tambahan data BPOM atau tidak
            $bpom_additional_doc = $this->M_nieDossier->additionalBpom($nie_alert_id, $pic_dept);

            if (count($bpom_additional_doc) > 0) {
                //cek apakah ada file yang di upload untuk tambahan data BPOM
                $file_bpom = $this->M_nieDossier->getFileBpom($nie_alert_id);
                if (!empty($file_bpom) && count($file_bpom) > 0) {
                    if ($file_bpom['bpom_add_data_dir'] != "") {
                        if (file_exists($file_bpom['bpom_add_data_dir'])) {
                            $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br />";
                            $section .= "<div class='row'><div class='col'><a class='float-right' href='" . base_url() . $file_bpom['bpom_add_data_dir'] . "'><i class='fa fa-clipboard' aria-hidden='true'></i>&nbsp;Additional File BPOM</a></div></div>";
                            $section .= "<div class='row mt-3'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                        } else {
                            $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                        }
                    } else {
                        $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                    }
                } else {
                    $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";
                }

                foreach ($bpom_additional_doc as $row) {
                    $section .= '<tr><td>' . $row->add_doc_desc . '</td>';
                    if ($row->submit_flag == "not submitted") {
                        $section .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $row->id . '" onclick="submitTambahanBpom(' . $row->id . ')">Submit</button></td>';
                    } else {
                        $section .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                    }
                }
                $section .= '</tbody></table></div></div></div></div>';
            } else {
                //Untuk tambahan data BPOM
                $section .= "<div class='tab-pane fade' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col-md-10'>";
                $section .= '<input type="text" class="form-control" name="tambahan_bpom_' . $pic_dept . '[]" id="bpom-' . $pic_dept . '" placeholder="Ketik tambahan data" /></div><div class="col-md-2"><button type="button" class="btn btn-primary btn-block" onclick="addDataBpom(\'' . $pic_dept . '\')">Add</button></div></div>';
                $section .= '<div class="row mt-4"><div class="col"><table class="table table-bordered sub-document-bpom" id="table-bpom-' . $pic_dept . '"><tbody></tbody></table></div></div>';

                //Untuk upload tambahan data BPOM
                $section .= '<div class="row mt-3" id="show-upload-' . $pic_dept . '" style="display: none;"><div class="col"><form enctype="multipart/form-data"><div class="form-group"><div class="custom-file"><input type="file" class="custom-file-input" name="bpom_upload_' . $pic_dept . '" id="bpom-upload-' . $pic_dept . '" onclick="uploadTambahanBpom(\'' . $pic_dept . '\')" /><label class="custom-file-label" for="bpom-upload-' . $pic_dept . '">Pilih File</label></div></div></form></div></div> <div class="row"><div class="col-md-10">&nbsp;</div><div class="col-md-2"><button type="button" id="submit-bpom-' . $pic_dept . '" style="display: none;" class="btn btn-primary btn-block" onclick="submitTambahanData(' . $nie_alert_id . ', \'' . $pic_dept . '\')">Submit</button></div></div>  <br /></div></div>';
            }

            // $data = array(
            //     "tab" => $tab,
            //     "section" => $section
            // );
            // $dept = $this->M_privilege->get_dept_by_deptcode($pic_dept);

            $nav .= '<div class="row"><div class="col"><nav><div class="nav nav-tabs" id="nav-tab" role="tablist">' . $tab . '</div></nav>';
            $nav .= '<div class="tab-content" id="nav-tabContent" style="background-color: white;">' . $section . '</div></div></div>';

            // $accordion .= '<div class="card" id="card_' . $dept->dept_id . '"><div class="card-header" id="heading_' . $dept->dept_id . '" style="background-color: #d7eaf7;"><h6 class="mb-0">';
            // $accordion .= '<a href="#" data-toggle="collapse" data-target="#collapse_' . $dept->dept_id . '" aria-expanded="true" aria-controls="collapse_' . $dept->dept_id . '" style="color: black;">' . $dept->dept_name . ' Department</a></h6></div>';
            // $accordion .= '<div id="collapse_' . $dept->dept_id . '" class="collapse show" aria-labelledby="heading_' . $dept->dept_id . '" data-parent="#accordion"><div class="card-body" style="background-color: #f7f7f7;">' . $nav;
            // $accordion .= '</div></div></div>';
        } else {
            $bpom_additional_doc = $this->M_nieDossier->additionalBpom($nie_alert_id, $pic_dept);

            if (count($bpom_additional_doc) > 0) {
                $nav = "";
                $tab = "";
                $section = "";
                $tab .= "<a class='nav-item nav-link active' id='nav-bpom-" . $pic_dept . "-tab' data-toggle='tab' href='#nav-bpom-" . $pic_dept . "' role='tab' aria-controls='nav-bpom-" . $pic_dept . "' aria-selected='true'>TAMBAHAN DATA BPOM</a>";
                $section .= "<div class='tab-pane fade show active' id='nav-bpom-" . $pic_dept . "' role='tabpanel' aria-labelledby='nav-bpom-" . $pic_dept . "-tab'><div class='container'><br /><div class='row'><div class='col'><table class='table table-bordered'><thead><tr><th>Document Name</th><th>Status</th></tr></thead><tbody>";

                foreach ($bpom_additional_doc as $row) {
                    $section .= '<tr><td>' . $row->add_doc_desc . '</td>';
                    if ($row->submit_flag == "not submitted") {
                        $section .= '<td width="4%"><button type="button" class="btn btn-primary btn-sm" id="btn-' . $row->id . '" onclick="submitTambahanBpom(' . $row->id . ')">Submit</button></td>';
                    } else {
                        $section .= '<td width="4%"><button type="button" class="btn btn-success btn-sm" disabled>Submitted</button></td>';
                    }
                }
                $section .= '</tbody></table></div></div></div></div>';

                $nav .= '<div class="row"><div class="col"><nav><div class="nav nav-tabs" id="nav-tab" role="tablist">' . $tab . '</div></nav>';
                $nav .= '<div class="tab-content" id="nav-tabContent" style="background-color: white;">' . $section . '</div></div></div>';
            }
        }

        return $nav;
    }

    public function submitDoc()
    {
        $data = $this->input->post();

        if ($this->M_nieDossier->submitDocSubmission($data)) {
            echo json_encode("success");
        } else {
            echo json_encode("failed");
        }
    }

    public function receiveSubmissionDoc()
    {
        $data = $this->input->post();
        if ($this->M_nieDossier->updateReceiveDoc($data)) {
            echo json_encode("success");
        } else {
            echo json_encode("failed");
        }
    }

    public function submission_save_upload()
    {
        $data = $this->input->post();
        $save_path = array();
        if ($_FILES["doc_upload"] && !empty($_FILES["doc_upload"])) {
            $submission_path = './uploads/Submissions/';
            $nie_no = preg_replace('/\s+/', '', $data['nie_no']);
            $target_path = $submission_path . $nie_no;

            //Create folder berdasarkan NIE Number dan Nama Dossier
            if (!file_exists($target_path)) {
                mkdir($target_path, 0777, true);
            }

            $config['upload_path'] = $target_path;
            $config['allowed_types'] = 'pdf';
            $this->load->library('upload', $config);

            for ($n = 0; $n < count($_FILES["doc_upload"]["name"]); $n++) {
                $temp_filename = preg_replace('/\s+/', '_', $_FILES["doc_upload"]["name"][$n]);
                $_FILES["doc"]["name"] = $temp_filename;
                $_FILES["doc"]["type"] = $_FILES["doc_upload"]["type"][$n];
                $_FILES["doc"]["tmp_name"] = $_FILES["doc_upload"]["tmp_name"][$n];
                $_FILES["doc"]["error"] = $_FILES["doc_upload"]["error"][$n];
                $_FILES["doc"]["size"] = $_FILES["doc_upload"]["size"][$n];

                $this->upload->initialize($config);
                $this->upload->do_upload('doc');

                $temp = array();
                $temp['filename'] = $temp_filename;
                $temp['filesize'] = $_FILES["doc_upload"]["size"][$n];
                $temp['filetype'] = $_FILES["doc_upload"]["type"][$n];
                $temp['filepath'] = $target_path . '/' . $temp_filename;

                $save_path[] = $temp;
            }
        }

        if (count($save_path) > 0) {
            $data['document_path'] = $save_path;
        }

        if ($this->M_nieDossier->saveUploadSubmission($data)) {
            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function receive_submission()
    {
        $receive = $this->input->post();

        $rcv_doc = array();
        foreach ($receive as $key => $value) {
            if (strpos($key, "accepted_checked_") !== false) {
                $doc_code = str_replace("accepted_checked_", "", $key);
                $check_value = array(
                    'doc_code' => $doc_code,
                    'checked' => $value
                );
                array_push($rcv_doc, $check_value);
            }
        }

        $doc = $this->M_nieDossier->doc_submission_by_alertid($receive['nie_alert_id']);

        $doc_temp = array();
        for ($n = 0; $n < count($doc); $n++) {
            $doc_str = implode("#", array_filter((array) $doc[$n]));
            array_push($doc_temp, $doc_str);
        }

        for ($v = 0; $v < count($rcv_doc); $v++) {
            if (strpos($doc_temp[$v], $rcv_doc[$v]['doc_code']) != false) {
                $tmp = explode("#", $doc_temp[$v]);
                $rcv_doc[$v]['id'] = $tmp[0];
            }
        }

        if ($this->M_nieDossier->receive_submission($receive, $rcv_doc)) {
            $message = show_alert(true, "Receive Submission berhasil disimpan");
        } else {
            $message = show_alert(false, "Gagal saat menyimpan Receive Submission");
        }

        $this->session->set_flashdata('message', $message);
        redirect("submission-process/" . $receive['nie_alert_id']);
    }

    public function get_doc_lvl_4()
    {
        $doc_lvl_3 = $this->input->post('lvl_3');
        $unlevel_3 = $this->input->post('unlevel_3');
        $pic_dept_id = $this->input->post('pic_dept_id'); //hanya digunakan sebagai penanda di accordion
        $pic_name = $this->input->post('pic_name');

        $last_lvl = $this->M_nieDossier->get_treeLvl_45($doc_lvl_3, $unlevel_3);
        $data = $this->generate_tree_doc_submission($last_lvl, $pic_dept_id, $pic_name);
        echo json_encode($data);
    }

    public function submission_process($nie_alert_id)
    {
        if (!$this->session->userdata('submission_page') == '1') {
            $this->load->view('restricted_page');
            return false;
        }

        $title['judul'] = 'NIE Submission Process';
        $data['nie'] = $this->M_nie->getNieById($nie_alert_id);
        // $submission = $this->M_nieDossier->get_submission($nie_alert_id);
        // $doc_lvl_3 = $this->M_nieDossier->get_submission_lvl3($nie_alert_id);
        // $unlevel_3 = $this->M_nieDossier->get_submission_unlevel3($nie_alert_id);

        if ($this->session->userdata('dept_code') == 'ADMIN' && $this->session->userdata('receive_submission') == '1') {
            $submission_dept = "";
            $available_dept = $this->M_nieDossier->available_submission_dept($nie_alert_id);
            if (count($available_dept) > 0) {
                for ($n = 0; $n < count($available_dept); $n++) {
                    $last_lvl = $this->M_nieDossier->get_treeLvl_45_checked($nie_alert_id, $available_dept[$n]['pic_dept']);
                    $submission_dept .= $this->generate_submission_checked($nie_alert_id, $last_lvl, $available_dept[$n]['pic_dept']);
                }
            } else {
                $bpom = $this->M_nieDossier->available_bpom_dept($nie_alert_id);
                
                for ($n = 0; $n < count($bpom); $n++) {
                    $last_lvl = array();
                    $submission_dept .= $this->generate_submission_checked($nie_alert_id, $last_lvl, $bpom[$n]['pic_dept']);
                }
            }
            $data['tree_doc_submisssion'] = $submission_dept;
            // print_r(htmlspecialchars($submission_dept));
            // die;
        } else {
            if ($this->session->userdata('submit_submission') == '1') {
                if ($this->session->userdata('dept_code') == 'RND') {
                    $last_lvl = $this->M_nieDossier->get_treeLvl_45_checked($nie_alert_id, 'RND');
                    $data['tree_doc_submisssion'] = $this->generate_submission_checked_dept($nie_alert_id, $last_lvl, 'RND');
                }

                if ($this->session->userdata('dept_code') == 'QA') {
                    $last_lvl = $this->M_nieDossier->get_treeLvl_45_checked($nie_alert_id, 'QA');
                    $data['tree_doc_submisssion'] = $this->generate_submission_checked_dept($nie_alert_id, $last_lvl, 'QA');
                }
            }
        }

        $data['nie_alert_id'] = $nie_alert_id;
        $data['document_upload'] = $this->M_nieDossier->getUploadedDoc($nie_alert_id);
        // print_r($data['document_upload']);
        // die;
        $this->load->view('templates/header', $title);
        $this->load->view('nie/submission_process', $data);
        $this->load->view('templates/footer');
    }

    // Perubahan submission yang baru
    public function submit_submission()
    {
        $is_send = false;
        $submit_doc = $this->input->post();
        print_r($submit_doc);
        die;
        $planning_area = $submit_doc['planning_area'];

        if (!empty($submit_doc['end_of_submission'])) {
            $end_of_submission = json_decode(html_entity_decode(stripslashes($submit_doc['end_of_submission'])));
        }

        if (!empty($submit_doc['email_recipients'])) {
            $email_recipient = json_decode(html_entity_decode(stripslashes($submit_doc['email_recipients'])));
        }

        if (!empty($submit_doc['end_of_bpom'])) {
            $end_of_bpom = json_decode(html_entity_decode(stripslashes($submit_doc['end_of_bpom'])));
        }

        if (!empty($submit_doc['email_recipients_bpom'])) {
            $email_recipients_bpom = json_decode(html_entity_decode(stripslashes($submit_doc['email_recipients_bpom'])));
        }

        // if (!empty($_FILES['doc_upload']['name'])) {
        //     $submission_path = './uploads/Submissions/';

        //     if ($_FILES["doc_upload"]["name"] != "") {
        //         $nie_no = str_replace(' ', '_', $this->input->post('nie_no'));
        //         $target_path = $submission_path . $nie_no . '/send';

        //         //Create folder berdasarkan NIE Number dan Nama Dossier
        //         if (!file_exists($target_path)) {
        //             mkdir($target_path, 0777, true);
        //         }

        //         $config['upload_path'] = $target_path;
        //         $config['allowed_types'] = 'pdf|zip|rar|7zip';
        //         $config['max_size'] = 0;

        //         $this->load->library('upload', $config);
        //         if ($this->upload->do_upload('doc_upload')) {
        //             $submit_doc['upload_path'] = $target_path . '/' . $_FILES["doc_upload"]["name"];
        //         }
        //     }
        // }

        if (isset($end_of_submission) && count($end_of_submission) > 0) {
            for ($n = 0; $n < count($end_of_submission); $n++) {
                if ($this->M_nieDossier->insertNewSubmission($end_of_submission[$n], $submit_doc['nie_alert_id'])) {
                    if (isset($email_recipient) && count($email_recipient) > 0) {
                        for ($v = 0; $v < count($email_recipient); $v++) {
                            if ($email_recipient[$v]->dept_id == $end_of_submission[$n]->pic_dept_id) {
                                $end_of_submission[$n]->email_recipients = $email_recipient[$v]->recipient_value;
                            }
                        }
                    }

                    $send_email = $this->M_email_submission->sendSubmissionEmail($submit_doc['nie_alert_id'], $end_of_submission[$n], $planning_area);
                    if ($send_email['status'] != 'failed') {
                        $is_send = true;
                    }
                    $this->M_email_submission->saveEmailSubmissionLog($send_email);
                }
            }
        }

        if (isset($end_of_bpom) && count($end_of_bpom) > 0) {
            for ($x = 0; $x < count($end_of_bpom); $x++) {
                if ($this->M_nieDossier->insertAdditionBpom($end_of_bpom[$x], $submit_doc['nie_alert_id'])) {
                    if (isset($email_recipients_bpom) && count($email_recipients_bpom) > 0) {
                        for ($j = 0; $j < count($email_recipients_bpom); $j++) {
                            if ($email_recipients_bpom[$j]->bpom_dept_id == $end_of_bpom[$x]->pic_dept_id) {
                                $end_of_bpom[$x]->email_recipients = $email_recipients_bpom[$j]->recipient_bpom;
                            }
                        }
                    }

                    $send_email = $this->M_email_submission->sendBpomEmail($submit_doc['nie_alert_id'], $end_of_bpom[$x], $planning_area);
                    if ($send_email['status'] != 'failed') {
                        $is_send = true;
                    }
                    $this->M_email_submission->saveEmailSubmissionLog($send_email);
                }
            }
        }

        if ($is_send == false) {
            $message = show_alert(false, "Terjadi kegagalan saat mengirim email");
        } else {
            $message = show_alert(true, "Submission Document Request telah disimpan, dan email pemberitahuan berhasil dikirim");
        }

        $this->session->set_flashdata('message', $message);
        redirect('submission-process/' . $submit_doc['nie_alert_id']);
    }

    public function getItemsDesc()
    {
        if (isset($_GET['term'])) {
            $result = $this->M_mst_item->get_items($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $arr_result[] = array(
                        'label' => $row->ITEM_DESC,
                        'item_code' => $row->ITEM_CODE,
                    );
                }
            } else {
                $arr_result[] = array(
                    'label' => "Data tidak ditemukan",
                    'item_code' => "",
                );
            }
            echo json_encode($arr_result);
        }
    }

    public function getEmployeePIC()
    {
        if (isset($_GET['term'])) {
            $result = $this->M_nie->getPIC($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $arr_result[] = array(
                        'label' => $row->EMP_FIRSTNAME,
                        'emp_code' => $row->EMP_CODE,
                    );
                }

                echo json_encode($arr_result);
            }
        }
    }

    // submission yang lama
    public function saveSubmission()
    {
        $data = array(
            'nie_no' => $this->input->post('nie_no'),
            'dossier_name' => $this->input->post('dossier_name'),
            'nie_alert_id' => $this->input->post('nie_alert_id'),
            'product_dossier_id' => $this->input->post('product_dossier_id'),
            'rank' => $this->input->post('rank'),
            'submit_remark' => $this->input->post('submit_remark'),
            'submit_date' => $this->input->post('submit_date'),
            'submit_pic' => ($this->session->userdata('logged_user')) ? $this->session->userdata('username') : $this->input->post('pic_menyerahkan'),
        );

        if ($this->M_nieDossier->insertSubmission($data)) {
            echo json_encode(array("status" => true, "data" => $data));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    public function receiveSubmission()
    {
        $data = array(
            "id" => $this->input->post('dossier_id'),
            "nie_alert_id" => $this->input->post('nie_alert_id'),
            "product_dossier_id" => $this->input->post('product_dossier_id'),
            "rank" => $this->input->post('rank'),
            "receive_date" => $this->input->post('receive_date'),
            "receive_pic" => ($this->session->userdata('logged_user')) ? $this->session->userdata('username') : $this->input->post('pic_menerima'),
            "receive_remark" => $this->input->post('receive_remark'),
        );

        if ($this->M_nieDossier->updateSubmission($data)) {
            echo json_encode(array("status" => true, "data" => $data));
        } else {
            echo json_encode(array("status" => false));
        }
    }

    public function chartDrilldown()
    {
        $list = $this->M_nie->get_datatables_drilldown();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $nie) {
            $no++;
            $row = array();
            $row['id'] = $nie->id;
            $row['num'] = $no;
            $row['nie_no'] = $nie->nie_no;
            $row['PRODUCT_CODE'] = $nie->PRODUCT_CODE;
            $row['product_name'] = $nie->product_name;
            $row['create_user'] = ($nie->pic == "") ? "-" : $nie->pic;
            $row['nie_start_date'] = $nie->nie_start_date;
            $row['nie_end_date'] = $nie->nie_end_date;
            $row['planning_area'] = ($nie->planning_area == "") ? "-" : $nie->planning_area;
            $row['brand_name'] = $nie->brand_name;
            $row['nie_date'] = $nie->nie_date;
            $row['compotition'] = $nie->compotition;
            $row['packaging_presentation'] = $nie->packaging_presentation;
            $row['product_owner'] = ($nie->product_owner == "") ? "-" : $nie->product_owner;
            $row['submit_status'] = $nie->submit_status;
            $row['product_type_desc'] = $nie->product_type_desc;
            $row['compotition'] = wordwrap($nie->compotition, 50, "<br>\n");
            $row['manufact_addr'] = ($nie->manufact_addr == "") ? "-" : wordwrap($nie->manufact_addr, 50, "<br>\n");
            // $row['export'] = $nie->export;
            $row['product_type'] = $nie->product_type;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_nie->count_all_drilldown(),
            "recordsFiltered" => $this->M_nie->filtered_drilldown(),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function exportExcel()
    {
        $list = $this->M_nie->getExcelReport();
        // $list = $this->M_nie->getReport();
        $report = array();

        $no = 0;
        foreach ($list as $nie) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['nie_no'] = $nie->nie_no;
            $row['nie_status'] = $nie->nie_status;
            $row['PRODUCT_CODE'] = $nie->PRODUCT_CODE;
            $row['product_name'] = $nie->product_name;
            $row['pic'] = ($nie->create_user == null) ? "-" : $nie->create_user;
            $row['nie_date'] = $nie->nie_date;
            $row['nie_start_date'] = $nie->nie_start_date;
            $row['nie_end_date'] = $nie->nie_end_date;
            $row['planning_area'] = ($nie->planning_area == null) ? "-" : $nie->planning_area;
            $row['nie_holder'] = $nie->nie_holder;
            $row['product_type'] = $nie->product_type;
            $row['fd_form'] = $nie->fd_form;
            $row['packaging_presentation'] = $nie->packaging_presentation;
            $row['shelf_life'] = $nie->shelf_life;
            $row['storage_condition'] = $nie->storage_condition;
            $row['manufact_addr'] = $nie->manufact_addr;
            $row['alert_color'] = $nie->alert_color;
            $row['remark'] = $nie->remark;
            $row['compotition'] = $nie->compotition;

            $report[] = $row;
        }
        // print_r($report);
        // die;
        $data['excel'] = $report;
        $current_date = date("Y-m-d");
        if ($this->input->post('tipe_export') == '1') {
            $data['title'] = $current_date . ' nie web';
            $data['nie_config'] = $this->M_nie_config->get_all_config();
            $this->load->view('report/by_interface', $data);
        } else if ($this->input->post('tipe_export') == '2') {
            $data['title'] = $current_date . ' template';
            $this->load->view('report/by_template', $data);
        } else {
            $data['title'] = $current_date . ' nie compotition';
            $this->load->view('report/by_compotition', $data);
        }
    }

    public function completedNie($nie_id)
    {
        $title['judul'] = 'Next NIE Data';

        $nie = $this->M_nie->getNieById($nie_id);
        $data['nie'] = $nie;

        $this->load->view('templates/header', $title);
        $this->load->view('nie/completed', $data);
        $this->load->view('templates/footer');
    }

    public function submit_bpom($nie_alert_id)
    {
        $this->M_nie->submit_bpom_status($nie_alert_id);
        $message = show_alert(true, "Status Submit BPOM berhasil disimpan");
        $this->session->set_flashdata('message', $message);
        redirect('update/' . $nie_alert_id);
    }
}
