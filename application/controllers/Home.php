<?php 

class Home extends CI_controller{

    public function index()
    {
        if(!$this->session->userdata('login'))
            redirect('login');
        
        $data['judul'] = 'Cuti';
        $this->load->view('templates/header', $data);
        $this->load->view('home/index');
        $this->load->view('templates/footer');
    }
}
?>