<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_auth', 'M_user'));
        // $this->session->keep_flashdata('message');
    }

    public function index()
    {
        if ($this->input->post('submit')) {
            $login_rules = array(
                array(
                    'field' => 'username',
                    'rules' => 'trim|required',
                    'errors' =>
                    array(
                        'required' => '{field} tidak boleh kosong'
                    )
                ),
                array(
                    'field' => 'password',
                    'rules' => 'trim|required',
                    'errors' =>
                    array(
                        'required' => '{field} tidak boleh kosong'
                    )
                )
            );

            $this->form_validation->set_rules($login_rules);
            $this->form_validation->set_error_delimiters('<small style="color: red;"><i>', '</i></small>');

            if ($this->form_validation->run() != FALSE) {
                $login_array = array($this->input->post('username'), $this->input->post('password'));
                if ($this->M_auth->process_login($login_array)) {
                    if ($this->session->userdata('url_request')) {
                        redirect($this->session->userdata('url_request'));
                    } else {
                        redirect('nie');
                    }
                } else {
                    $message = 'Username or Password not match';
                }
            }
        }

        if ($this->input->post('url_request')) {
            $url = $this->input->post('url_request');
            if ($url != "") {
                $this->session->set_userdata('url_request', $url);
            }
        }

        $data['judul'] = 'Login';
        if (isset($message) && !empty($message))
            $data['message'] = $message;

        $this->load->view('auth/login', $data);
    }

    public function logout()
    {
        if ($this->session->userdata('logged_user')) {
            $this->session->sess_destroy();
            redirect('/');
        }

        return true;
    }

    public function change_password()
    {
        $title['judul'] = 'Ubah Password';

        $this->load->view('templates/header', $title);
        $this->load->view('auth/change_password');
        $this->load->view('templates/footer');
    }

    //Validasi agar user tidak menggunakan password yang sama dengan password default sebelumnya
    public function unique_password()
    {
        $user_id = $this->session->userdata('logged_user');
        $password = $this->input->post('main_password');
        if ($this->M_user->is_unique_password($user_id, $password)) {
            $this->form_validation->set_message('unique_password', 'Password tidak boleh sama dengan password sebelumnya');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function update_password()
    {
        if ($this->input->post('submit_password')) {
            //do validation
            $change_password_rules = array(
                array(
                    'field' => 'main_password',
                    'label' => 'Password',
                    'rules' => 'trim|required|min_length[8]|max_length[20]|alpha_numeric|callback_unique_password',
                    'errors' =>
                    array(
                        'required' => '{field} tidak boleh kosong',
                        'alpha_numeric' => '{field} tidak boleh mengandung karakter spesial (\~!@#$%^&*_/)',
                        'min_length' => 'Panjang password minimal 8 karakter',
                        'max_length' => 'Panjang password maksimal 20 karakter',
                    )
                ),
                array(
                    'field' => 'confirm_password',
                    'label' => 'Konfirmasi Password',
                    'rules' => 'required|min_length[8]|max_length[20]|matches[main_password]',
                    'errors' =>
                    array(
                        'required' => '{field} tidak boleh kosong',
                        'min_length' => 'Panjang password minimal 8 karakter',
                        'max_length' => 'Panjang password maksimal 20 karakter',
                        'matches' => 'Password tidak sama',
                    )
                )
            );

            $this->form_validation->set_rules($change_password_rules);
            $this->form_validation->set_error_delimiters('<small style="color: red;"><i>', '</i></small>');

            if ($this->form_validation->run() == FALSE) {
                $title['judul'] = 'Ubah Password';

                $this->load->view('templates/header', $title);
                $this->load->view('auth/change_password');
                $this->load->view('templates/footer');
            } else {
                $user_id = $this->session->userdata('logged_user');
                $new_password = $this->input->post('main_password');

                $this->M_user->update_user_password($user_id, $new_password);
                $this->session->set_flashdata('message', 'Password berhasil diperbarui');
                redirect('Auth/change_password');
            }
        }
    }

    public function prevent_access()
    {
        $this->load->view('restricted_page');
    }
}
