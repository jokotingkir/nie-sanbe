<?php

class Submission extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_nieDossier'));
    }

    public function index()
    {
        $title['judul'] = 'Manage Submission';
        $data['doc_lvl1'] = $this->M_nieDossier->get_doc_lvl1();

        $this->load->view('templates/header', $title);
        $this->load->view('submission/manage_submission', $data);
        $this->load->view('templates/footer');
    }

    public function add_doc_1()
    {
        $new_doc1 = $this->input->post('new_doc1');

        if ($this->M_nieDossier->add_new_doc1($new_doc1)) {
            $response = $this->M_nieDossier->get_doc_lvl1();
        } else {
            $response = array();
        }

        echo json_encode($response);
    }

    public function getDocPIC()
    {
        if (isset($_GET['term'])) {
            $result = $this->M_nieDossier->get_submission_pic($_GET['term']);
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $arr_result[] = array(
                        'label' => $row->name,
                        'emp_code' => $row->EMP_CODE,
                        'user_id' => $row->user_id
                    );
                }
            } else {
                $arr_result[] = array(
                    'label' => "Data tidak ditemukan",
                    'emp_code' => "",
                );
            }
            echo json_encode($arr_result);
        }
    }
}
