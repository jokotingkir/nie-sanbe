<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth';
$route['404_override'] = 'Errors';
$route['translate_uri_dashes'] = FALSE;

/*
*----------------------------------------------
* AUTHENTICATION ROUTE
*----------------------------------------------
*/
$route['login'] = 'Auth';
$route['logout'] = 'Auth/logout';

/*
*----------------------------------------------
* MANAGE NIE
*----------------------------------------------
*/
$route['add-nie'] = 'Nie/tambah';
$route['update/(:any)'] = 'Nie/edit_item/$1';
$route['nie-detail/(:num)/(:any)'] = 'Nie/detail/$1/$2';
$route['submission/(:num)'] = 'Nie/submission_monitoring/$1';
$route['completed-nie/(:any)'] = 'Nie/edit_item/$1/completed';
$route['complete'] = 'Nie/tambah';
$route['submit-submission'] = 'Nie/submit_submission';
$route['submission-process/(:num)'] = 'Nie/submission_process/$1';
$route['receive-submission'] = 'Nie/receive_submission';
$route['save-bpom-doc'] = 'Nie/save_tambahan_bpom';

/*
*----------------------------------------------
* MANAGE USER
*----------------------------------------------
*/
$route['user-list'] = 'User';
$route['save-user'] = 'User/add_user';
$route['add-user'] = 'User/add_user';
$route['user-edit'] = 'User/edit_user';
$route['user-edit/(:any)'] = 'User/edit_user/$1';
$route['reset-user-password'] = 'User/reset_password';
/* ---------------------------------------------- */


/*
*----------------------------------------------
* MANAGE PRIVILEGE
*----------------------------------------------
*/
$route['privilege'] = 'Privilege';
$route['privilege/search/(:any)'] = 'Privilege/index/$1';
$route['add-privilege'] = 'Privilege/add_level_privilege';
$route['privilege/edit/(:any)'] = 'Privilege/edit_privilege/$1';
$route['privilege/update'] = 'Privilege/update_level';
$route['privilege/add'] = 'Privilege/add_new_level';
$route['privilege/delete/(:any)'] = 'Privilege/delete_level/$1';
/* ---------------------------------------------- */

/*
*----------------------------------------------
* MANAGE SUBMISSION
*----------------------------------------------
*/
$route['add-doc-1'] = 'Submission/add_doc_1';
$route['produk'] = 'Nie/produk';
$route['nie'] = 'Nie/index';
$route['sarana'] = 'Nie/sarana';
$route['manage-submission'] = 'Submission';
$route['additional-submission-upload'] = 'Nie/submission_save_upload';

$route['bulk-schedule'] = 'Nie/bulkSchedule';
